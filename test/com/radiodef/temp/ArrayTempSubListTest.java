/* MIT License
 *
 * Copyright (c) 2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.radiodef.temp;

import static com.radiodef.temp.Tools.*;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

import java.util.*;
import java.util.stream.*;
import static java.util.Collections.*;

class ArrayTempSubListTest {
    
    private ArrayTempFactory factory;
    private ArrayTempList<Long> list;
    
    @BeforeEach
    void initNewFactoryAndList() {
        factory = new ArrayTempFactory(10);
        list = factory.newList();
    }
    
    @AfterEach
    void clearFactoryAndList() {
        factory = null;
        list = null;
    }
    
    @Test
    void testConstructorAndSize() {
        ArrayTempSubList<Long> sub;
        
        assertThrows(IndexOutOfBoundsException.class, () -> new ArrayTempSubList<>(list, -1, 0), "from < 0");
        assertThrows(IndexOutOfBoundsException.class, () -> new ArrayTempSubList<>(list, 0, -1), "to < 0");
        assertThrows(IndexOutOfBoundsException.class, () -> new ArrayTempSubList<>(list, 0, 1), "to > size");
        // moved to assertion
//        assertThrows(NullPointerException.class, () -> new ArrayTempSubList<>(null, 0, 0));
        
        sub = new ArrayTempSubList<>(list, 0, 0);
        assertTrue(sub.isEmpty());
        
        addAll(list, 1L, 2L, 3L, 4L, 5L, 6L);
        
        assertThrows(IndexOutOfBoundsException.class, () -> new ArrayTempSubList<>(list, 0, list.size() + 1), "to > size");
        assertThrows(IndexOutOfBoundsException.class, () -> new ArrayTempSubList<>(list, 2, 1), "from > to");
        
        sub = new ArrayTempSubList<>(list, 1, 5);
        assertEquals(4, sub.size());
        assertEquals(List.of(2L, 3L, 4L, 5L), sub);
        
        // subList of a subList
        
        sub = new ArrayTempSubList<>(sub, 1, 3);
        assertEquals(2, sub.size());
        assertEquals(List.of(3L, 4L), sub);
    }
    
    @Test
    void testGet() {
        addAll(list, 11L, 22L, 33L, 44L, 55L);
        
        ArrayTempSubList<Long> sub = (ArrayTempSubList<Long>) list.subList(1, 4);
    
        // noinspection ResultOfMethodCallIgnored
        assertThrows(IndexOutOfBoundsException.class, () -> sub.get(-1));
        // noinspection ResultOfMethodCallIgnored
        assertThrows(IndexOutOfBoundsException.class, () -> sub.get(sub.size()));
        
        assertEquals(22L, (long) sub.get(0));
        assertEquals(33L, (long) sub.get(1));
        assertEquals(44L, (long) sub.get(2));
        
        // modifications should show through
        list.set(1, 2222L);
        list.set(2, 3333L);
        list.set(3, 4444L);
        assertEquals(2222L, (long) sub.get(0));
        assertEquals(3333L, (long) sub.get(1));
        assertEquals(4444L, (long) sub.get(2));
        
        // subList of a subList
        
        ArrayTempSubList<Long> sub1 = sub.subList(1, 3);
        assertEquals(3333L, (long) sub1.get(0));
        
        list.set(2, 333333L);
        assertEquals(333333L, (long) sub1.get(0));
    }
    
    @Test
    void testSet() {
        addAll(list, 100L, 200L, 300L, 400L, 500L);
        
        ArrayTempSubList<Long> sub = (ArrayTempSubList<Long>) list.subList(1, 4);
        
        assertThrows(IndexOutOfBoundsException.class, () -> sub.set(-1, 601L));
        assertThrows(IndexOutOfBoundsException.class, () -> sub.set(sub.size(), 601L));
        
        // note: modifications should show through
        
        assertEquals(200L, (long) sub.set(0, -2L));
        assertEquals(-2L, (long) sub.get(0));
        assertEquals(-2L, (long) list.get(1));
        
        assertEquals(300L, (long) sub.set(1, -3L));
        assertEquals(-3L, (long) sub.get(1));
        assertEquals(-3L, (long) list.get(2));
        
        assertEquals(400L, (long) sub.set(2, -4L));
        assertEquals(-4L, (long) sub.get(2));
        assertEquals(-4L, (long) list.get(3));
        
        assertEquals(List.of(100L, -2L, -3L, -4L, 500L), list);
        
        // subList of a subList
        
        ArrayTempSubList<Long> sub1 = sub.subList(1, 2);
        
        assertEquals(-3L, (long) sub1.set(0, null));
        assertNull(sub1.get(0));
        assertNull(sub.get(1));
        assertNull(list.get(2));
    }
    
    @Test
    void testAddAtIndex() {
        addAll(list, 100L, 300L, 500L);
        
        ArrayTempSubList<Long> sub = (ArrayTempSubList<Long>) list.subList(1, 2);
        assertEquals(List.of(300L), sub);
        
        assertThrows(IndexOutOfBoundsException.class, () -> sub.add(-1, 0L));
        assertThrows(IndexOutOfBoundsException.class, () -> sub.add(sub.size() + 1, 0L));
        
        // note: modifications should show through
        
        sub.add(0, 2L);
        
        assertEquals(List.of(2L, 300L), sub);
        assertEquals(List.of(100L, 2L, 300L, 500L), list);
        
        sub.add(sub.size(), 4L);
        
        assertEquals(List.of(2L, 300L, 4L), sub);
        assertEquals(List.of(100L, 2L, 300L, 4L, 500L), list);
        
        // subList of a subList
        
        ArrayTempSubList<Long> sub1 = sub.subList(1, 3);
        assertEquals(List.of(300L, 4L), sub1);
        
        sub1.add(1, 0L);
        
        assertEquals(List.of(300L, 0L, 4L), sub1);
        assertEquals(List.of(2L, 300L, 0L, 4L), sub);
        assertEquals(List.of(100L, 2L, 300L, 0L, 4L, 500L), list);
        
        // extra...
        
        assertTrue(Stream.of(sub1, sub, list).noneMatch(l -> l.contains(null)));
        sub1.add(0, null);
        assertTrue(Stream.of(sub1, sub, list).allMatch(l -> l.contains(null)));
    }
    
    @Test
    void testRemoveAtIndex() {
        addAll(list, 10L, 20L, 30L, 40L, 50L, 60L, 70L, 80L, 90L);
        
        ArrayTempSubList<Long> sub = (ArrayTempSubList<Long>) list.subList(2, 7);
        assertEquals(List.of(30L, 40L, 50L, 60L, 70L), sub);
        
        assertThrows(IndexOutOfBoundsException.class, () -> sub.remove(-1));
        assertThrows(IndexOutOfBoundsException.class, () -> sub.remove(sub.size()));
        
        // note: modifications should show through
        
        assertEquals((Long) 40L, sub.remove(1));
        
        assertTrue(Stream.of(sub, list).noneMatch(l -> l.contains(40L)));
        assertEquals(List.of(30L, 50L, 60L, 70L), sub);
        assertEquals(List.of(10L, 20L, 30L, 50L, 60L, 70L, 80L, 90L), list);
        
        assertEquals((Long) 60L, sub.remove(2));
        
        assertTrue(Stream.of(sub, list).noneMatch(l -> l.contains(60L)));
        assertEquals(List.of(30L, 50L, 70L), sub);
        assertEquals(List.of(10L, 20L, 30L, 50L, 70L, 80L, 90L), list);
        
        // subList of a subList
        
        ArrayTempSubList<Long> sub1 = sub.subList(1, 2);
        assertEquals(List.of(50L), sub1);
        
        assertEquals((Long) 50L, sub1.remove(0));
        
        assertTrue(Stream.of(sub1, sub, list).noneMatch(l -> l.contains(50L)));
        assertEquals(List.of(), sub1);
        assertEquals(List.of(30L, 70L), sub);
        assertEquals(List.of(10L, 20L, 30L, 70L, 80L, 90L), list);
        
        // extra...
        
        assertEquals((Long) 30L, sub.remove(0));
        assertEquals((Long) 70L, sub.remove(0));
        
        assertEquals(List.of(), sub);
        assertEquals(List.of(10L, 20L, 80L, 90L), list);
    }
    
    @Test
    void testRemoveRange() {
        factory = new ArrayTempFactory(32);
        
        ArrayTempList<String> bottom = factory.newList(3);
        addAll(bottom, "A", "B", "C");
        
        ArrayTempList<Integer> list = factory.newList(10);
        addAll(list, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
        
        ArrayTempList<String> top = factory.newList(3);
        addAll(top, "X", "Y", "Z");
        
        ArrayTempSubList<Integer> sub = (ArrayTempSubList<Integer>) list.subList(1, list.size() - 1);
        assertEquals(List.of(1, 2, 3, 4, 5, 6, 7, 8), sub);
        
        assertThrows(IndexOutOfBoundsException.class, () -> sub.removeRange(-1, 3)); // from < 0
        assertThrows(IndexOutOfBoundsException.class, () -> sub.removeRange(3, -1)); // to < 0
        assertThrows(IndexOutOfBoundsException.class, () -> sub.removeRange(3, sub.size() + 1)); // to > size
        assertThrows(IndexOutOfBoundsException.class, () -> sub.removeRange(4, 3)); // from > to
        
        sub.removeRange(2, 6);
        assertEquals(List.of(0, 1, 2, 7, 8, 9), list);
        assertEquals(List.of(1, 2, 7, 8), sub);
        
        Object[] expectedAfter26Remove =
            newArray(factory.array.length,
                     Mark.INSTANCE, "A", "B", "C",
                     Mark.INSTANCE, 0, 1, 2, 7, 8, 9, null, null, null, null,
                     Mark.INSTANCE, "X", "Y", "Z");
        
        assertArrayEquals(expectedAfter26Remove, factory.array);
        
        sub.removeRange(2, 2);
        assertEquals(List.of(0, 1, 2, 7, 8, 9), list);
        assertEquals(List.of(1, 2, 7, 8), sub);
        
        assertArrayEquals(expectedAfter26Remove, factory.array);
        
        sub.removeRange(0, sub.size());
        assertTrue(sub.isEmpty());
        assertEquals(List.of(0, 9), list);
        
        assertArrayEquals(
            newArray(factory.array.length,
                     Mark.INSTANCE, "A", "B", "C",
                     Mark.INSTANCE, 0, 9, null, null, null, null, null, null, null, null,
                     Mark.INSTANCE, "X", "Y", "Z"),
            factory.array);
        
        // subList of a subList
        
        addAll(sub, 1, 2, 3, 4, 5, 6, 7, 8);
        assertEquals(List.of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9), list);
        
        ArrayTempSubList<Integer> sub1 = sub.subList(2, sub.size() - 2);
        assertEquals(List.of(3, 4, 5, 6), sub1);
        
        sub1.removeRange(1, 3);
        assertEquals(List.of(0, 1, 2, 3, 6, 7, 8, 9), list);
        assertEquals(List.of(1, 2, 3, 6, 7, 8), sub);
        assertEquals(List.of(3, 6), sub1);
        
        sub1.clear();
        assertEquals(List.of(0, 1, 2, 7, 8, 9), list);
        assertEquals(List.of(1, 2, 7, 8), sub);
        assertEquals(List.of(), sub1);
    }
}