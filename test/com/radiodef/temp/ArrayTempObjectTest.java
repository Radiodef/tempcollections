/* MIT License
 *
 * Copyright (c) 2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.radiodef.temp;

import static com.radiodef.temp.ArrayTempFactory.MAX_ARRAY_SIZE;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

import java.lang.reflect.*;

class ArrayTempObjectTest {
    @Test
    void testOverrides() throws ReflectiveOperationException {
        // make sure that by using ArrayTempList we really are testing the
        // methods declared in ArrayTempObject
        
        assertTrue(Modifier.isFinal(ArrayTempObject.class.getDeclaredMethod("size").getModifiers()));
        assertTrue(Modifier.isFinal(ArrayTempObject.class.getDeclaredMethod("capacity").getModifiers()));
        assertTrue(Modifier.isFinal(ArrayTempObject.class.getDeclaredMethod("isEmpty").getModifiers()));
        assertTrue(Modifier.isFinal(ArrayTempObject.class.getDeclaredMethod("getFactory").getModifiers()));
        assertTrue(Modifier.isFinal(ArrayTempObject.class.getDeclaredMethod("isTop").getModifiers()));
        assertTrue(Modifier.isFinal(ArrayTempObject.class.getDeclaredMethod("canAdd", int.class).getModifiers()));
        assertTrue(Modifier.isFinal(ArrayTempObject.class.getDeclaredMethod("close").getModifiers()));
        
        // noinspection JavaReflectionMemberAccess
//        assertThrows(NoSuchMethodException.class, () -> ArrayTempList.class.getDeclaredMethod("maxSize"));
    }
    
    @Test
    void testGetFactory() {
        ArrayTempFactory factory = new ArrayTempFactory(10);
        assertSame(factory, factory.newList().getFactory());
        
        TempList<?> list = factory.newList();
        assertSame(factory, list.getFactory());
        
        // Note: the following is undefined, according to the TempObject contract,
        //       but this is what we expect from the implementation.
        list.close();
        assertThrows(IllegalStateException.class, list::getFactory,
            "close() should clear the factory");
    }
    
    @Test
    void testSizeAndIsEmpty() {
        ArrayTempFactory factory = new ArrayTempFactory(10);
        ArrayTempObject obj = factory.newList();
        
        assertEquals(0, obj.size);
        assertEquals(0, obj.size());
        assertTrue(obj.isEmpty());
        
        obj.size = 601;
        assertEquals(601, obj.size());
        assertFalse(obj.isEmpty());
        
        obj.size = 0;
        assertEquals(0, obj.size());
        assertTrue(obj.isEmpty());
    }
    
    @Test
    void testCapacity() {
        ArrayTempFactory factory = new ArrayTempFactory(10);
        ArrayTempObject obj = factory.newList(601);
        
        assertEquals(601, obj.capacity);
        assertEquals(601, obj.capacity());
        
        obj.capacity = 7;
        assertEquals(7, obj.capacity());
        obj.capacity = 0;
        assertEquals(0, obj.capacity());
    }
    
    @Test
    void testIsTop() {
        ArrayTempFactory factory = new ArrayTempFactory(10);
        
        ArrayTempList<?> listA, listB, listC;
        {
            listA = factory.newList();
            
            assertTrue(listA.isTop());
            
            {
                listB = factory.newList();
                
                assertFalse(listA.isTop());
                assertTrue(listB.isTop());
                
                {
                    listC = factory.newList();
                    
                    assertFalse(listA.isTop());
                    assertFalse(listB.isTop());
                    assertTrue(listC.isTop());
                    
                    listC.close();
                    
                    assertFalse(listA.isTop());
                    assertTrue(listB.isTop());
                    assertFalse(listC.isTop());
                }
                
                listB.close();
                
                assertTrue(listA.isTop());
                assertFalse(listB.isTop());
                assertFalse(listC.isTop());
            }
            
            listA.close();
            
            assertFalse(listA.isTop());
            assertFalse(listB.isTop());
            assertFalse(listC.isTop());
        }
        
        assertTrue(factory.newList().isTop());
    }
    
    @Test
    void testMaxSize() {
        ArrayTempFactory factory = new ArrayTempFactory(10);
        
        ArrayTempList<Long> list1 = factory.newList(100);
        assertEquals(MAX_ARRAY_SIZE - 1, list1.maxSize());
        
        ArrayTempList<Long> list2 = factory.newList();
        assertEquals(MAX_ARRAY_SIZE - 1 - 100 - 1, list2.maxSize());
        
        assertEquals(100, list1.maxSize());
        list1.add(1L);
        assertEquals(100, list1.maxSize());
        list1.add(1L);
        assertEquals(100, list1.maxSize());
        
        assertEquals(MAX_ARRAY_SIZE - 1 - 100 - 1, list2.maxSize());
        list2.add(2L);
        assertEquals(MAX_ARRAY_SIZE - 1 - 100 - 1, list2.maxSize());
        list2.add(2L);
        assertEquals(MAX_ARRAY_SIZE - 1 - 100 - 1, list2.maxSize());
        
        list2.close();
        assertEquals(MAX_ARRAY_SIZE - 1, list1.maxSize());
        
        assertEquals(0, list2.maxSize(), "not required");
    }
    
    @Test
    void testCanAdd() {
        ArrayTempFactory factory = new ArrayTempFactory(10);
        
        ArrayTempList<String> list = factory.newList(4);
        
        Runnable expectedWhenTop = () -> {
            assertTrue(list.canAdd(0));
            assertTrue(list.canAdd(1));
            assertTrue(list.canAdd(100));
            
            assertFalse(list.canAdd(Integer.MAX_VALUE));
            assertFalse(list.canAdd(Integer.MIN_VALUE));
        };
        expectedWhenTop.run();
        assertFalse(list.canAdd(-1));
        
        {
            list.add("x");
            
            expectedWhenTop.run();
            assertTrue(list.canAdd(-1), "1 element to remove now");
            assertFalse(list.canAdd(-2));
        }
        {
            list.add("y");
            
            expectedWhenTop.run();
            assertTrue(list.canAdd(-1), "2 elements to remove now");
            assertTrue(list.canAdd(-2), "2 elements to remove now");
            assertFalse(list.canAdd(-3));
        }
        
        {
            ArrayTempList<String> newTop = factory.newList();
            try (newTop) {
                {
                    assertTrue(newTop.canAdd(0));
                    assertTrue(newTop.canAdd(1));
                    assertTrue(newTop.canAdd(100));
                    assertFalse(newTop.canAdd(-1));
                    newTop.add("0");
                    assertTrue(newTop.canAdd(-1));
                }
                {
                    assertTrue(list.canAdd(0));
                    assertTrue(list.canAdd(2));
                    assertFalse(list.canAdd(3), "capacity was 4");
            
                    assertTrue(list.canAdd(-2), "still 2 elements");
            
                    list.add("z");
                    list.add("Q");
            
                    assertTrue(list.canAdd(0));
                    assertFalse(list.canAdd(1));
            
                    assertTrue(list.canAdd(-4));
                    assertFalse(list.canAdd(-5));
                }
            }
            assertTrue(newTop.canAdd(0));
            assertFalse(newTop.canAdd(1));
            assertFalse(newTop.canAdd(-1));
        }
        
        expectedWhenTop.run();
        
        {
            list.remove(0);
            
            expectedWhenTop.run();
            assertTrue(list.canAdd(-3));
            assertFalse(list.canAdd(-4));
        }
        {
            list.clear();
            
            expectedWhenTop.run();
            assertFalse(list.canAdd(-1));
        }
    }
    
    @Test
    void testClose() throws ReflectiveOperationException {
        Field factoryField = ArrayTempObject.class.getDeclaredField("factory");
        factoryField.setAccessible(true);
        
        ArrayTempFactory factory = new ArrayTempFactory(10);
        {
            ArrayTempObject objA = factory.newList();
            
            assertSame(factory, objA.getFactory());
            assertNull(objA.previous);
            
            assertFalse(objA.isClosed());
            
            {
                ArrayTempObject objB = factory.newList();
                
                assertSame(factory, objB.getFactory());
                assertSame(objA, objB.previous);
                
                assertFalse(objB.isClosed());
                
                objB.close();
                
                assertTrue(objB.isClosed());
                
                // noinspection ResultOfMethodCallIgnored
                assertThrows(IllegalStateException.class, objB::getFactory);
                assertNull(factoryField.get(objB));
                assertNull(objB.previous);
                
                assertThrows(IllegalStateException.class, objB::close);
            }
            
            assertFalse(objA.isClosed());
            
            objA.close();
            
            assertTrue(objA.isClosed());
    
            // noinspection ResultOfMethodCallIgnored
            assertThrows(IllegalStateException.class, objA::getFactory);
            assertNull(factoryField.get(objA));
            assertNull(objA.previous);
            
            assertThrows(IllegalStateException.class, objA::close);
        }
    }
}