/* MIT License
 *
 * Copyright (c) 2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.radiodef.temp;

import static com.radiodef.temp.ArrayTempFactory.*;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

import java.util.*;

class ArrayTempFactoryTest {
    @Test
    void testConstructor() {
        assertThrows(IllegalArgumentException.class, () -> new ArrayTempFactory(-1));
        assertEquals(0, new ArrayTempFactory(0).array.length);
        assertEquals(1, new ArrayTempFactory(1).array.length);
        assertEquals(10, new ArrayTempFactory(10).array.length);
        assertEquals(100, new ArrayTempFactory(100).array.length);
    }
    
    @Test
    void testNewList() {
        ArrayTempFactory f = new ArrayTempFactory(10);
        
        // 1
        ArrayTempList<?> list1 = f.newList();
        assertNotNull(list1);
        assertTrue(list1.isEmpty());
        assertSame(f, list1.getFactory());
        
        assertEquals(1, list1.offset);
        assertEquals(DEFAULT_LIST_EST_SIZE, list1.capacity);
        
        // 2
        ArrayTempList<?> list2 = f.newList();
        assertNotSame(list1, list2);
        
        assertEquals(2 + DEFAULT_LIST_EST_SIZE, list2.offset);
        assertEquals(DEFAULT_LIST_EST_SIZE, list2.capacity);
        
        // 3
        ArrayTempList<?> list3 = f.newList(100);
        assertEquals(3 + (2 * DEFAULT_LIST_EST_SIZE), list3.offset);
        assertEquals(100, list3.capacity);
        
        // testing marks
        assertEquals(Mark.INSTANCE, f.array[list1.offset - 1]);
        assertEquals(Mark.INSTANCE, f.array[list2.offset - 1]);
        assertEquals(Mark.INSTANCE, f.array[list3.offset - 1]);
        
        // err
        assertThrows(IllegalArgumentException.class, () -> f.newList(-1));
    }
    
    @Test
    void testNewDeque() {
        ArrayTempFactory f = new ArrayTempFactory(10);
        
        // 1
        ArrayTempDeque<?> deque1 = f.newDeque();
        assertNotNull(deque1);
        assertTrue(deque1.isEmpty());
        assertSame(f, deque1.getFactory());
        
        assertEquals(1, deque1.offset);
        assertEquals(DEFAULT_DEQUE_EST_SIZE, deque1.capacity);
        
        // 2
        ArrayTempDeque<?> deque2 = f.newDeque();
        assertNotSame(deque1, deque2);
        
        assertEquals(2 + DEFAULT_DEQUE_EST_SIZE, deque2.offset);
        assertEquals(DEFAULT_DEQUE_EST_SIZE, deque2.capacity);
        
        // 3
        ArrayTempDeque<?> deque3 = f.newDeque(100);
        assertEquals(3 + (2 * DEFAULT_DEQUE_EST_SIZE), deque3.offset);
        assertEquals(128, deque3.capacity);
        
        // testing marks
        assertEquals(Mark.INSTANCE, f.array[deque1.offset - 1]);
        assertEquals(Mark.INSTANCE, f.array[deque2.offset - 1]);
        assertEquals(Mark.INSTANCE, f.array[deque3.offset - 1]);
        
        // err
        assertThrows(IllegalArgumentException.class, () -> f.newDeque(-1));
        
        // deque special
        assertEquals(1, f.newDeque(0).capacity);
    }
    
    @Test
    void testNewSet() {
        ArrayTempFactory f = new ArrayTempFactory(10);
        
        // 1
        ArrayTempHashSet<?> set1 = f.newSet();
        assertNotNull(set1);
        assertTrue(set1.isEmpty());
        assertSame(f, set1.getFactory());
        
        int defaultCapacity = Tools.next2((int) Math.ceil(DEFAULT_SET_EST_SIZE / ArrayTempHashSet.DEFAULT_LOAD_FACTOR));
        assertEquals(2 * DEFAULT_SET_EST_SIZE, defaultCapacity);
        
        assertEquals(1, set1.offset);
        assertEquals(defaultCapacity, set1.capacity);
        assertEquals(ArrayTempHashSet.DEFAULT_LOAD_FACTOR, set1.loadFactor);
        
        // 2
        ArrayTempHashSet<?> set2 = f.newSet();
        assertNotSame(set1, set2);
        
        assertEquals(2 + defaultCapacity, set2.offset);
        assertEquals(defaultCapacity, set2.capacity);
        assertEquals(ArrayTempHashSet.DEFAULT_LOAD_FACTOR, set2.loadFactor);
        
        // 3
        ArrayTempHashSet<?> set3 = f.newSet(100, 0.25f);
        assertEquals(3 + (2 * defaultCapacity), set3.offset);
        assertEquals(512, set3.capacity, "should be next2((int) ceil(100 / 0.25f))");
        assertEquals(0.25f, set3.loadFactor);
        
        // testing marks
        assertEquals(Mark.INSTANCE, f.array[set1.offset - 1]);
        assertEquals(Mark.INSTANCE, f.array[set2.offset - 1]);
        assertEquals(Mark.INSTANCE, f.array[set3.offset - 1]);
        
        // err
        assertThrows(IllegalArgumentException.class, () -> f.newSet(-1));
        
        // hash set special
        assertEquals(1, f.newSet(0).capacity);
        
        // no longer illegal
//        int twoTo30Plus1 = (int) Math.pow(2, 30) + 1;
//        assertEquals(twoTo30Plus1, ArrayTempHashSet.MAX_CAPACITY + 1);
//        assertThrows(IllegalArgumentException.class, () -> f.newSet(twoTo30Plus1));
        
        for (float badLoadFactor : new float[] {
            -1f,
            0f,
            Math.nextUp(1f),
            Float.POSITIVE_INFINITY,
            Float.NEGATIVE_INFINITY,
            Float.NaN
        }) {
            assertThrows(IllegalArgumentException.class,
                         () -> f.newSet(1, badLoadFactor),
                         "badLoadFactor=" + badLoadFactor);
        }
    }
    
    @Test
    void testNewSetAddAllRehashCount() {
        ArrayTempFactory f = new ArrayTempFactory(0);
        
        Set<String> toCopy = new LinkedHashSet<>();
        for (int i = 0; i < Short.MAX_VALUE; ++i)
            toCopy.add("i=" + i);
        
        ArrayTempHashSet.REHASH_COUNT = 0;
        
        Set<String> set = f.newSet(toCopy);
        assertEquals(toCopy, set);
        
        assertEquals(0, ArrayTempHashSet.REHASH_COUNT);
    }
    
    @Test
    void testNewMap() {
        ArrayTempFactory f = new ArrayTempFactory(10);
        
        // 1
        ArrayTempHashMap<?, ?> map1 = f.newMap();
        assertNotNull(map1);
        assertTrue(map1.isEmpty());
        assertSame(f, map1.getFactory());
        
        int defaultCapacity = 2 * Tools.next2((int) Math.ceil(DEFAULT_MAP_EST_SIZE / ArrayTempHashMap.DEFAULT_LOAD_FACTOR));
        assertEquals(4 * DEFAULT_MAP_EST_SIZE, defaultCapacity);
        
        assertEquals(1, map1.offset);
        assertEquals(defaultCapacity, map1.capacity);
        assertEquals(ArrayTempHashMap.DEFAULT_LOAD_FACTOR, map1.loadFactor);
        
        // 2
        ArrayTempHashMap<?, ?> map2 = f.newMap();
        assertNotSame(map1, map2);
        
        assertEquals(2 + defaultCapacity, map2.offset);
        assertEquals(defaultCapacity, map2.capacity);
        assertEquals(ArrayTempHashMap.DEFAULT_LOAD_FACTOR, map2.loadFactor);
        
        // 3
        ArrayTempHashMap<?, ?> map3 = f.newMap(100, 0.25f);
        assertEquals(3 + (2 * defaultCapacity), map3.offset);
        assertEquals(1024, map3.capacity, "should be 2 * next2((int) ceil(100 / 0.25f))");
        assertEquals(0.25f, map3.loadFactor);
        
        // testing marks
        assertEquals(Mark.INSTANCE, f.array[map1.offset - 1]);
        assertEquals(Mark.INSTANCE, f.array[map2.offset - 1]);
        assertEquals(Mark.INSTANCE, f.array[map3.offset - 1]);
        
        // err
        assertThrows(IllegalArgumentException.class, () -> f.newMap(-1));
        
        // hash map special
        assertEquals(2, f.newMap(0).capacity);
        
        // no longer illegal
//        int twoTo29Plus1 = (int) Math.pow(2, 29) + 1;
//        assertEquals(twoTo29Plus1, (ArrayTempHashMap.MAX_CAPACITY / 2) + 1);
//        assertThrows(IllegalArgumentException.class, () -> f.newMap(twoTo29Plus1));
        
        for (float badLoadFactor : new float[] {
            -1f,
            0f,
            Math.nextUp(1f),
            Float.POSITIVE_INFINITY,
            Float.NEGATIVE_INFINITY,
            Float.NaN
        }) {
            assertThrows(IllegalArgumentException.class,
                         () -> f.newMap(1, badLoadFactor),
                         "badLoadFactor=" + badLoadFactor);
        }
    }
    
    @Test
    void testNewMapPutAllRehashCount() {
        ArrayTempFactory f = new ArrayTempFactory(0);
        
        Map<String, String> toCopy = new LinkedHashMap<>();
        for (int i = 0; i < Short.MAX_VALUE; ++i)
            toCopy.put("key" + i, "value" + i);
        
        ArrayTempHashMap.REHASH_COUNT = 0;
        
        Map<String, String> map = f.newMap(toCopy);
        assertEquals(toCopy, map);
        
        assertEquals(0, ArrayTempHashMap.REHASH_COUNT);
    }
    
    @Test
    void testMapCapacityAdjustment() {
        // noinspection ConstantConditions
        if (false) for (@SuppressWarnings("UnusedAssignment")
                        int i = 1; i <= (1 << 29); ++i) {
            assertEquals(Tools.next2(i << 1),
                         Tools.next2(i) << 1);
        }
    }
    
    @Test
    void testGetTop() {
        ArrayTempFactory f = new ArrayTempFactory(10);
        
        assertNull(f.getTop());
        
        try (TempList<?> list1 = f.newList()) {
            assertSame(list1, f.getTop());
            try (TempList<?> list2 = f.newList()) {
                assertSame(list2, f.getTop());
            }
            assertSame(list1, f.getTop());
        }
        
        assertNull(f.getTop());
    }
    
    // common bug
    @Test
    void testZeroSizeArrayResize() {
        ArrayTempFactory f = new ArrayTempFactory(0);
        
        assertEquals(0, f.array.length);
        
        try (ArrayTempList<?> list = f.newList(100)) {
            assertNotEquals(0, f.array.length);
            assertTrue(f.array.length >= 101);
        }
    }
    
    // basic test of array contents
    @Test
    void testStackBehavior() {
        ArrayTempFactory f = new ArrayTempFactory(10);
        Object[] original = f.array;
        
        int count = 10;
        
        Deque<ArrayTempList<?>> stack = new ArrayDeque<>();
        
        for (int i = 0; i < count; ++i) {
            ArrayTempList<?> list = f.newList();
            stack.push(list);
        }
        
        while (!stack.isEmpty()) {
            ArrayTempList<?> list = stack.pop();
            
            for (int i = 0; i < list.capacity; ++i) {
                assertNull(f.array[list.offset + i]);
            }
            
            assertEquals(Mark.INSTANCE, f.array[list.offset - 1]);
            list.close();
        }
        
        for (Object o : f.array) {
            assertNull(o);
        }
        
        // should have been resized multiple times
        assertNotSame(original, f.array);
    }
    
    @Test
    void testJavaDocClaims() {
        // The JavaDocs make some claims about the
        // results of specific computations, so make
        // sure they are correct.
        ArrayTempFactory f = new ArrayTempFactory();
        
        ArrayTempHashSet<?> set = f.newSet(6, 0.5f);
        assertEquals(16, set.capacity());
        
        ArrayTempHashMap<?, ?> map = f.newMap(6, 0.5f);
        assertEquals(32, map.capacity());
    }
}