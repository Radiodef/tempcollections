/* MIT License
 *
 * Copyright (c) 2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.radiodef.temp;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

import java.util.*;
import java.util.Map.*;

class ArrayTempHashMapIteratorTest {
    private ArrayTempFactory factory;
    private ArrayTempHashMap<String, String> map;
    
    @BeforeEach
    void initNewFactoryAndMap() {
        factory = new ArrayTempFactory(10);
        map = factory.newMap();
    }
    
    @AfterEach
    void clearFactoryAndMap() {
        factory = null;
        map = null;
    }
    
    @Test
    void testConstructors() {
        assertSame(map, new ArrayTempHashMapIterator.OfKeys<>(map).map);
        assertSame(map, new ArrayTempHashMapIterator.OfValues<>(map).map);
        assertSame(map, new ArrayTempHashMapIterator.OfEntries<>(map).map);
    }
    
    @Test
    void testKeyIterator() {
        ArrayTempHashMapIterator.OfKeys<?, ?> empty = new ArrayTempHashMapIterator.OfKeys<>(map);
        assertFalse(empty.hasNext());
        assertThrows(NoSuchElementException.class, empty::next);
        assertThrows(IllegalStateException.class, empty::remove);
        
        Map<String, String> entries = new LinkedHashMap<>();
        entries.put("ABC", "123");
        entries.put("DEF", "456");
        entries.put("GHI", "789");
        entries.put(null, null);
        
        map.putAll(entries);
        
        ArrayTempHashMapIterator.OfKeys<String, String> it = new ArrayTempHashMapIterator.OfKeys<>(map);
        
        Set<String> keysSeen = new LinkedHashSet<>();
        
        for (int i = 0; i < 4; ++i) {
            assertTrue(it.hasNext());
            String k = it.next();
            
            assertTrue(entries.containsKey(k));
            assertEquals(entries.get(k), map.get(k));
            
            assertTrue(keysSeen.add(k), "each key should be visited once");
        }
        
        assertFalse(it.hasNext());
        assertThrows(NoSuchElementException.class, it::next);
        
        assertEquals(entries.keySet(), keysSeen, "each key should be visited once");
        
        // test removals
        it = new ArrayTempHashMapIterator.OfKeys<>(map);
        
        for (int i = 0; i < 4; ++i) {
            assertTrue(it.hasNext());
            String k = it.next();
            
            assertTrue(entries.containsKey(k));
            assertTrue(map.containsKey(k));
            it.remove();
            
            assertFalse(map.containsKey(k));
            
            entries.remove(k);
            assertEquals(entries, map);
            
            assertThrows(IllegalStateException.class, it::remove);
        }
        
        assertFalse(it.hasNext());
        assertThrows(NoSuchElementException.class, it::next);
        
        assertTrue(map.isEmpty());
    }
    
    @Test
    void testValueIterator() {
        ArrayTempHashMapIterator.OfValues<?, ?> empty = new ArrayTempHashMapIterator.OfValues<>(map);
        assertFalse(empty.hasNext());
        assertThrows(NoSuchElementException.class, empty::next);
        assertThrows(IllegalStateException.class, empty::remove);
        
        Map<String, String> entries = new LinkedHashMap<>();
        entries.put("ABC", "123");
        entries.put("DEF", "456");
        entries.put("GHI", "789");
        entries.put(null, null);
        
        map.putAll(entries);
        
        ArrayTempHashMapIterator.OfValues<String, String> it = new ArrayTempHashMapIterator.OfValues<>(map);
        
        Set<String> seenValues = new LinkedHashSet<>();
        
        for (int i = 0; i < 4; ++i) {
            assertTrue(it.hasNext());
            String v = it.next();
            
            assertTrue(entries.containsValue(v));
            assertTrue(map.containsValue(v));
            
            assertTrue(seenValues.add(v), "each value should be visited once");
        }
        
        assertFalse(it.hasNext());
        assertThrows(NoSuchElementException.class, it::next);
        
        assertEquals(new HashSet<>(entries.values()), seenValues, "each value should be visited once");
        
        // test removals
        it = new ArrayTempHashMapIterator.OfValues<>(map);
        
        Map<String, String> reverse = new LinkedHashMap<>();
        entries.forEach((k, v) ->
            assertNull(reverse.put(v, k)));
        
        for (int i = 0; i < 4; ++i) {
            assertTrue(it.hasNext());
            String v = it.next();
            
            assertTrue(entries.containsValue(v));
            assertTrue(map.containsValue(v));
            it.remove();
            
            assertFalse(map.containsValue(v));
            
            String k = reverse.get(v);
            assertFalse(map.containsKey(k));
            assertEquals(v, entries.remove(k));
            assertEquals(entries, map);
            
            assertThrows(IllegalStateException.class, it::remove);
        }
        
        assertFalse(it.hasNext());
        assertThrows(NoSuchElementException.class, it::next);
        
        assertTrue(map.isEmpty());
    }
    
    @Test
    void testValueRemoveOrder() {
        map.put("A", "value");
        map.put("B", "value");
        
        @SuppressWarnings("ConstantConditions")
        String keyToRemove =
            Arrays.stream(factory.array)
                  .filter(String.class::isInstance)
                  .map(String.class::cast)
                  .findFirst()
                  .get();
        
        ArrayTempHashMapIterator.OfValues<String, String> it = new ArrayTempHashMapIterator.OfValues<>(map);
        
        assertTrue(it.hasNext());
        assertEquals("value", it.next());
        
        it.remove();
        
        assertTrue(it.hasNext());
        assertEquals("value", it.next());
        
        assertFalse(it.hasNext());
        
        assertFalse(map.containsKey(keyToRemove));
        assertEquals(Collections.singletonMap(keyToRemove.equals("A") ? "B" : "A", "value"), map);
    }
    
    @Test
    void testEntryIterator() {
        ArrayTempHashMapIterator.OfEntries<?, ?> empty = new ArrayTempHashMapIterator.OfEntries<>(map);
        assertFalse(empty.hasNext());
        assertThrows(NoSuchElementException.class, empty::next);
        assertThrows(IllegalStateException.class, empty::remove);
        
        Map<String, String> entries = new LinkedHashMap<>();
        entries.put("ABC", "123");
        entries.put("DEF", "456");
        entries.put("GHI", "789");
        entries.put(null, null);
        
        map.putAll(entries);
        
        ArrayTempHashMapIterator.OfEntries<String, String> it = new ArrayTempHashMapIterator.OfEntries<>(map);
        
        Set<Entry<String, String>> entriesSeen = new LinkedHashSet<>();
        
        for (int i = 0; i < 4; ++i) {
            assertTrue(it.hasNext());
            Entry<String, String> e = it.next();
            
            assertEquals(entries.get(e.getKey()), e.getValue());
            assertEquals(map.get(e.getKey()), e.getValue());
            
            assertTrue(entriesSeen.add(e), "each entry should be visited once");
        }
        
        assertFalse(it.hasNext());
        assertThrows(NoSuchElementException.class, it::next);
        
        assertEquals(entries.entrySet(), entriesSeen, "each entry should be visited once");
        
        // test removals
        it = new ArrayTempHashMapIterator.OfEntries<>(map);
        
        for (int i = 0; i < 4; ++i) {
            assertTrue(it.hasNext());
            Entry<String, String> e = it.next();
            
            String k = e.getKey();
            String v = e.getValue();
            
            assertEquals(entries.get(k), v);
            assertEquals(map.get(k), v);
            
            it.remove();
            
            assertFalse(map.containsKey(k));
            assertFalse(map.containsValue(v));
            
            entries.remove(k);
            assertEquals(entries, map);
            
            assertThrows(IllegalStateException.class, it::remove);
        }
        
        assertFalse(it.hasNext());
        assertThrows(NoSuchElementException.class, it::next);
        
        assertTrue(map.isEmpty());
    }
    
    @Test
    void testEntryIteratorSetValue() {
        Map<String, String> entries = new LinkedHashMap<>();
        entries.put("ABC", "123");
        entries.put("DEF", "456");
        entries.put("GHI", "789");
        
        map.putAll(entries);
        
        ArrayTempHashMapIterator.OfEntries<String, String> it = new ArrayTempHashMapIterator.OfEntries<>(map);
        while (it.hasNext()) {
            Entry<String, String> e = it.next();
            String v = e.getValue();
            
            e.setValue(Integer.toString(Integer.parseInt(v) + 111));
        }
        
        assertEquals("234", map.get("ABC"));
        assertEquals("567", map.get("DEF"));
        assertEquals("900", map.get("GHI"));
    }
    
    @Test
    void testConcurrentModification() {
        // not strictly necessary, but we do check for this
        map.put("A", "1");
        map.put("B", "2");
        map.put("C", "3");
        
        ArrayTempHashMapIterator.OfEntries<String, String> it = new ArrayTempHashMapIterator.OfEntries<>(map);
        it.next();
        
        assertEquals("3", map.remove("C"));
        
        assertThrows(ConcurrentModificationException.class, () -> {
            while (it.hasNext())
                it.next();
        });
    }
    
    @Test
    @SuppressWarnings("SimplifiableJUnitAssertion")
    void testVirtualEntry() {
        Map<String, String> expectMap = new LinkedHashMap<>();
        expectMap.put("key", "value");
        expectMap.put(null, null);
        
        map.putAll(expectMap);
        assertEquals(expectMap, map);
        
        Entry<String, String> actualStringEntry = null;
        Entry<String, String> actualNullEntry = null;
        
        Iterator<Entry<String, String>> it = new ArrayTempHashMapIterator.OfEntries<>(map);
        while (it.hasNext()) {
            Entry<String, String> e = it.next();
            if (e.getKey() != null) {
                actualStringEntry = e;
            } else {
                actualNullEntry = e;
            }
        }
        
        assertNotNull(actualStringEntry);
        assertNotNull(actualNullEntry);
        
        it = expectMap.entrySet().iterator();
        Entry<String, String> expectStringEntry = it.next();
        Entry<String, String> expectNullEntry = it.next();
        
        // int hashCode()
        assertEquals(expectStringEntry.hashCode(), actualStringEntry.hashCode());
        assertEquals(expectNullEntry.hashCode(), actualNullEntry.hashCode());
        
        // boolean equals(Object)
        assertTrue(actualStringEntry.equals(expectStringEntry));
        assertTrue(actualNullEntry.equals(expectNullEntry));
        assertFalse(actualStringEntry.equals(expectNullEntry));
        assertFalse(actualNullEntry.equals(expectStringEntry));
    
        // noinspection ObjectEqualsNull, ConstantConditions
        assertFalse(actualStringEntry.equals(null));
        // noinspection EqualsBetweenInconvertibleTypes
        assertFalse(actualStringEntry.equals("key=value"));
        // noinspection EqualsWithItself
        assertTrue(actualStringEntry.equals(actualStringEntry));
        
        // String toString()
        assertEquals("key=value", actualStringEntry.toString());
        assertEquals("null=null", actualNullEntry.toString());
        
        // K getKey()
        assertEquals("key", actualStringEntry.getKey());
        assertNull(actualNullEntry.getKey());
        
        // V getValue()
        assertEquals("value", actualStringEntry.getValue());
        assertNull(actualNullEntry.getValue());
        
        // V setValue(V)
        assertEquals("value", actualStringEntry.setValue("foo"));
        assertEquals("foo", map.get("key"));
        assertNull(actualNullEntry.setValue("bar"));
        assertEquals("bar", map.get(null));
    }
}