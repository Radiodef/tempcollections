/* MIT License
 *
 * Copyright (c) 2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.radiodef.temp;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

import java.util.*;
import static java.util.Collections.*;

class ArrayTempHashSetIteratorTest {
    private ArrayTempFactory factory;
    private ArrayTempHashSet<String> set;
    
    @BeforeEach
    void initNewFactoryAndSet() {
        factory = new ArrayTempFactory(10);
        set = factory.newSet();
    }
    
    @AfterEach
    void clearFactoryAndSet() {
        factory = null;
        set = null;
    }
    
    @Test
    void testConstructor() {
        ArrayTempHashSetIterator<String> it = new ArrayTempHashSetIterator<>(set);
        assertSame(set, it.set);
    }
    
    @Test
    void testEmpty() {
        assertTrue(set.isEmpty());
        
        assertFalse(new ArrayTempHashSetIterator<>(set).hasNext());
        assertThrows(NoSuchElementException.class, new ArrayTempHashSetIterator<>(set)::next);
    }
    
    @Test
    void testPlainIteration() {
        addAll(set, "A", "B", "C", "D", "E", "F", "P", "Q", "R", "X", "Y", "Z");
        Set<String> seen = new LinkedHashSet<>();
        ArrayTempHashSetIterator<String> it;
        
        it = new ArrayTempHashSetIterator<>(set);
        
        while (it.hasNext()) {
            assertTrue(seen.add(it.next()));
        }
        
        assertFalse(it.hasNext());
        assertThrows(NoSuchElementException.class, it::next);
        assertEquals(seen, set);
        
        assertTrue(set.remove("R"));
        assertTrue(set.removeAll(List.of("C", "D", "E")));
        
        seen.clear();
        it = new ArrayTempHashSetIterator<>(set);
        
        while (it.hasNext()) {
            assertTrue(seen.add(it.next()));
        }
        
        assertFalse(it.hasNext());
        assertThrows(NoSuchElementException.class, it::next);
        assertEquals(seen, set);
    }
    
    @Test
    void testRemove() {
        assertThrows(IllegalStateException.class, new ArrayTempHashSetIterator<>(set)::remove);
        
        addAll(set, "X", "Y", "Z", "P", "Q", "R", "L", "M", "N");
        int sizeBefore = set.size;
        ArrayTempHashSetIterator<String> it;
        
        it = new ArrayTempHashSetIterator<>(set);
        
        assertThrows(IllegalStateException.class, it::remove);
        
        Set<String> removed = new LinkedHashSet<>();
        int i = 0;
        while (it.hasNext()) {
            String next = it.next();
            assertTrue(set.contains(next));
            
            if ((i++ % 2) == 0) {
                it.remove();
                assertThrows(IllegalStateException.class, it::remove);
                
                removed.add(next);
                assertFalse(set.contains(next));
            }
        }
        
        assertEquals(sizeBefore - removed.size(), set.size);
        assertTrue(disjoint(removed, set), set::toString);
        
//        System.out.println(set);
        removed.clear();
        sizeBefore = set.size();
        
        i = 0;
        
        it = new ArrayTempHashSetIterator<>(set);
        while (it.hasNext()) {
            String next = it.next();
            
            it.remove();
            assertFalse(set.contains(next));
            assertThrows(IllegalStateException.class, it::remove);
            
            removed.add(next);
            ++i;
        }
        
        assertEquals(sizeBefore, i, removed::toString);
        
        assertEquals(0, set.size, set::toString);
        assertEquals(emptySet(), set);
    }
    
    @Test
    void testConcurrentRemoval() {
        // not required, but the implementation of next() made it possible
        
        addAll(set, "A", "B", "C", "X", "Y", "Z");
        String last = set.stream().reduce((a, b) -> b).orElseThrow(AssertionError::new);
        
        ArrayTempHashSetIterator<String> it = (ArrayTempHashSetIterator<String>) set.iterator();
        
        for (int i = set.size() - 1; i > 0; --i) {
            it.next();
        }
        
        assertTrue(set.remove(last));
        assertThrows(ConcurrentModificationException.class, it::next);
    }
}