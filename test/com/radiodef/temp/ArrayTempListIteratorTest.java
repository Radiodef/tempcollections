/* MIT License
 *
 * Copyright (c) 2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.radiodef.temp;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

import java.util.*;
import static java.util.Collections.*;
import static java.util.Arrays.*;

class ArrayTempListIteratorTest {
    
    private ArrayTempList<String> list;
    
    @BeforeEach
    void initNewList() {
        list = new ArrayTempFactory(10).newList();
    }
    
    @AfterEach
    void clearList() {
        list = null;
    }
    
    @Test
    void testInstance() {
        ArrayTempListIterator<?> iter = (ArrayTempListIterator<?>) list.listIterator();
        
        assertNotNull(iter);
        assertSame(list, iter.list);
    }
    
    @Test
    void testForwardIteration() {
        addAll(list, "A", "B", "C");
        ListIterator<?> iter = list.listIterator();
        
        for (int i = 0; i < 3; ++i) {
            assertTrue(iter.hasNext());
            assertEquals(i, iter.nextIndex());
            assertEquals(list.get(i), iter.next());
        }
        
        assertFalse(iter.hasNext());
        assertEquals(list.size(), iter.nextIndex());
        assertThrows(NoSuchElementException.class, iter::next);
        assertEquals(list.size(), iter.nextIndex()); // i.e. no change
    }
    
    @Test
    void testForwardIterationWithStartIndex() {
        addAll(list, "A", "B", "C");
        ListIterator<?> iter = list.listIterator(2);
        
        assertTrue(iter.hasNext());
        assertEquals(2, iter.nextIndex());
        assertEquals("C", iter.next());
        
        assertFalse(iter.hasNext());
    }
    
    @Test
    void testReverseIteration() {
        addAll(list, "A", "B", "C");
        ListIterator<?> iter = list.listIterator(list.size());
        
        for (int i = 2; i >= 0; --i) {
            assertTrue(iter.hasPrevious());
            assertEquals(i, iter.previousIndex());
            assertEquals(list.get(i), iter.previous());
        }
        
        assertFalse(iter.hasPrevious());
        assertEquals(-1, iter.previousIndex());
        assertThrows(NoSuchElementException.class, iter::previous);
        assertEquals(-1, iter.previousIndex()); // i.e. no change
    }
    
    @Test
    void testReverseIterationWithStartIndex() {
        addAll(list, "A", "B", "C");
        ListIterator<?> iter = list.listIterator(1);
        
        assertTrue(iter.hasPrevious());
        assertEquals(0, iter.previousIndex());
        assertEquals("A", iter.previous());
        
        assertFalse(iter.hasPrevious());
    }
    
    @Test
    void testForwardAndReverseIteration() {
        addAll(list, "A", "B", "C");
        ListIterator<?> iter = list.listIterator();
        
        // alternating between next() and previous()
        // should return the same element
        
        for (int i = 0; i < 4; ++i) {
            assertTrue(iter.hasNext());
            assertFalse(iter.hasPrevious());
            
            assertEquals(0, iter.nextIndex());
            assertEquals("A", iter.next());
            
            assertTrue(iter.hasNext());
            assertTrue(iter.hasPrevious());
            
            assertEquals(0, iter.previousIndex());
            assertEquals("A", iter.previous());
        }
        
        // forward
        for (int i = 0; i < list.size(); ++i) {
            assertEquals(i, iter.nextIndex());
            assertEquals(i - 1, iter.previousIndex());
            
            assertEquals(list.get(i), iter.next());
            assertEquals(list.get(i), iter.previous());
            assertEquals(list.get(i), iter.next());
        }
        
        // reverse
        for (int i = list.size() - 1; i >= 0; --i) {
            assertEquals(i + 1, iter.nextIndex());
            assertEquals(i, iter.previousIndex());
            
            assertEquals(list.get(i), iter.previous());
            assertEquals(list.get(i), iter.next());
            assertEquals(list.get(i), iter.previous());
        }
        
        assertTrue(iter.hasNext());
        assertEquals(0, iter.nextIndex());
        assertFalse(iter.hasPrevious());
        assertEquals(-1, iter.previousIndex());
    }
    
    @Test
    void testSet() {
        addAll(list, "A", "B", "C");
        ListIterator<String> iter = list.listIterator();
        
        assertThrows(IllegalStateException.class, () -> iter.set(""));
        
        List<String> replacements = List.of("X", "Y", "Z");
        for (int i = 0; i < list.size(); ++i) {
            iter.next();
            iter.set(replacements.get(i));
            assertEquals(replacements.get(i), list.get(i));
        }
        
        assertEquals(replacements, list);
        
        iter.set("!"); // note: multiple invocations allowed, unlike remove()
        assertEquals("!", list.get(list.size() - 1));
        
        replacements = List.of("+", "-", "*");
        for (int i = list.size() - 1; i >= 0; --i) {
            iter.previous();
            iter.set(replacements.get(i));
            assertEquals(replacements.get(i), list.get(i));
        }
        
        assertEquals(replacements, list);
        
        // testing set(E) after add(E)
        
        while (iter.hasPrevious())
            iter.previous();
        iter.set("%");
        assertEquals(List.of("%", "-", "*"), list);
        iter.add("=");
        assertEquals(List.of("=", "%", "-", "*"), list);
        
        assertThrows(IllegalStateException.class, () -> iter.set("$"));
        assertEquals(List.of("=", "%", "-", "*"), list); // i.e. no change
        assertEquals("=", iter.previous());
        
        iter.set(null); // note: shouldn't throw because of null
        assertEquals(asList(null, "%", "-", "*"), list);
        assertNull(iter.next());
        
        // test throws after invocations of remove()
        
        iter.remove();
        assertThrows(IllegalStateException.class, () -> iter.set(""));
        
        assertEquals(List.of("%", "-", "*"), list);
        assertEquals("%", iter.next());
        iter.set("/");
        assertEquals(List.of("/", "-", "*"), list);
    }
    
    @Test
    void testAdd() {
        ListIterator<String> iter = list.listIterator();
        
        assertFalse(iter.hasNext());
        
        iter.add("X");
        
        assertFalse(iter.hasNext());
        assertEquals(1, iter.nextIndex());
        assertThrows(NoSuchElementException.class, iter::next);
        
        assertTrue(iter.hasPrevious());
        assertEquals(0, iter.previousIndex());
        assertEquals("X", iter.previous());
        assertEquals("X", iter.next()); // (advance to the end again)
        
        assertEquals(List.of("X"), list);
        
        iter.add("Y");
        iter.add("Z");
        
        assertFalse(iter.hasNext());
        assertEquals(3, iter.nextIndex());
        assertThrows(NoSuchElementException.class, iter::next);
        
        assertTrue(iter.hasPrevious());
        assertEquals(2, iter.previousIndex());
        assertEquals("Z", iter.previous());
        assertTrue(iter.hasPrevious());
        assertEquals(1, iter.previousIndex());
        assertEquals("Y", iter.previous());
        assertTrue(iter.hasPrevious());
        assertEquals(0, iter.previousIndex());
        assertEquals("X", iter.previous());
        
        assertEquals(List.of("X", "Y", "Z"), list);
        
        iter.add(null); // note: shouldn't throw because of null
        assertEquals(asList(null, "X", "Y", "Z"), list);
        assertEquals("X", iter.next());
        
        iter.set("1");
        iter.add("B"); // note: shouldn't throw because of previous set(..)
        assertEquals(asList(null, "1", "B", "Y", "Z"), list);
        assertEquals("Y", iter.next());
        
        iter.set("2");
        iter.add("C"); // note: shouldn't throw because of previous set(..)
        assertEquals(asList(null, "1", "B", "2", "C", "Z"), list);
        assertEquals("Z", iter.next());
        
        iter.set("3");
        assertEquals(asList(null, "1", "B", "2", "C", "3"), list);
        assertFalse(iter.hasNext());
        
        // test that interleave with remove() does not cause problems
        
        while (iter.hasPrevious())
            iter.previous();
        assertNull(iter.next());
        iter.remove();
        assertEquals(0, iter.nextIndex());
        
        iter.add("A");
        assertEquals(asList("A", "1", "B", "2", "C", "3"), list);
    }
    
    @Test
    void testRemove() {
        ListIterator<String> iter;
        
        iter = list.listIterator();
        assertThrows(IllegalStateException.class, iter::remove);
        
        // next removals
        addAll(list, "A", "B", "C", "D", "E", "F");
        iter = list.listIterator();
        
        for (int i = list.size(); i > 0; --i) {
            assertEquals(-1, iter.previousIndex());
            assertEquals(0, iter.nextIndex());
            String s = list.get(0);
            assertEquals(s, iter.next());
            
            iter.remove();
            assertFalse(list.contains(s));
            assertEquals(i - 1, list.size());
            assertThrows(IllegalStateException.class, iter::remove);
        }
        
        assertFalse(iter.hasNext());
        assertFalse(iter.hasPrevious());
        assertTrue(list.isEmpty());
        assertThrows(IllegalStateException.class, iter::remove);
        
        // previous removals
        addAll(list, "U", "V", "W", "X", "Y", "Z");
        iter = list.listIterator(list.size());
        
        for (int i = list.size(); i > 0; --i) {
            int size = list.size();
            int last = size - 1;
            
            assertEquals(last, iter.previousIndex());
            assertEquals(size, iter.nextIndex());
            String s = list.get(last);
            assertEquals(s, iter.previous());
            
            iter.remove();
            assertFalse(list.contains(s));
            assertEquals(i - 1, list.size());
            assertThrows(IllegalStateException.class, iter::remove);
        }
        
        assertFalse(iter.hasNext());
        assertFalse(iter.hasPrevious());
        assertTrue(list.isEmpty());
        assertThrows(IllegalStateException.class, iter::remove);
        
        // back-and-forth removals
        addAll(list, "m", "n", "o", "p", "q", "r");
        iter = list.listIterator(2);
        
        assertEquals("o", iter.next());
        iter.remove();
        
        assertEquals(list.indexOf("n"), iter.previousIndex());
        assertEquals(list.indexOf("p"), iter.nextIndex());
        
        assertEquals("n", iter.previous());
        iter.remove();
        
        assertEquals(list.indexOf("m"), iter.previousIndex());
        assertEquals(list.indexOf("p"), iter.nextIndex());
        
        // testing after add(E)
        assertEquals("p", iter.next());
        iter.add("###");
        assertEquals(List.of("m", "p", "###", "q", "r"), list);
        assertThrows(IllegalStateException.class, iter::remove);
        
        assertEquals("###", iter.previous());
        iter.remove();
        
        // testing after set(E)
        assertEquals("q", iter.next());
        iter.set("@@@");
        assertEquals(List.of("m", "p", "@@@", "r"), list);
        iter.remove();
        assertEquals(List.of("m", "p", "r"), list);
        
        // extra
        while (iter.hasNext()) {
            iter.next();
            iter.remove();
        }
        while (iter.hasPrevious()) {
            iter.previous();
            iter.remove();
        }
        
        assertFalse(iter.hasNext());
        assertFalse(iter.hasPrevious());
        assertTrue(list.isEmpty());
        assertThrows(IllegalStateException.class, iter::remove);
    }
    
    @Test
    void testForEachRemaining() {
        addAll(list, "A", "B", "C", "D", "E", "F");
        
        ListIterator<String> iter = list.listIterator(3);
        
        int[] i = new int[] {3};
        iter.forEachRemaining(elem ->
            assertEquals(list.get(i[0]++), elem));
        
        assertEquals(list.size(), i[0]);
        
        ListIterator<String> end = list.listIterator(list.size());
        
        assertEquals(end.hasNext(), iter.hasNext());
        assertEquals(end.nextIndex(), iter.nextIndex());
        assertEquals(end.hasPrevious(), iter.hasPrevious());
        assertEquals(end.previousIndex(), iter.previousIndex());
        
        assertThrows(NoSuchElementException.class, iter::next);
        assertEquals("F", iter.previous());
    }
}