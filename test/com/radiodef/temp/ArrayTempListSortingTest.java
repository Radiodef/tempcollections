/* MIT License
 *
 * Copyright (c) 2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.radiodef.temp;

import static com.radiodef.temp.Tools.*;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

import java.util.*;
import java.util.stream.*;
import java.lang.reflect.*;
import static java.util.Collections.*;
import static java.util.Comparator.*;
import static java.util.Comparator.reverseOrder;

class ArrayTempListSortingTest {
    @Test
    void testInsertionSortThreshold() throws ReflectiveOperationException {
        // Just try to copy whatever the JDK is doing.
        Field field = Arrays.class.getDeclaredField("INSERTIONSORT_THRESHOLD");
        field.setAccessible(true);
        assertEquals(field.getInt(null), ArrayTempListSorting.INSERTION_SORT_THRESHOLD);
    }
    
    private static class Item {
        final String value;
        final int ordinal;
        
        private Item(String value, int ordinal) {
            this.value = value;
            this.ordinal = ordinal;
        }
        
        String value() { return value; }
        int ordinal() { return ordinal; }
        
        @Override
        public int hashCode() {
            return Objects.hashCode(value) * ordinal;
        }
        
        @Override
        public boolean equals(Object o) {
            return (o instanceof Item)
                && Objects.equals(value, ((Item) o).value)
                && ordinal == ((Item) o).ordinal;
        }
        
        @Override
        public String toString() {
            return ordinal + ":" + value;
        }
    }
    
    @FunctionalInterface
    private interface Sorter {
        <T> void sort(ArrayTempList<T> list, Comparator<? super T> comp);
    }
    
    private void testSort(Sorter sorter) {
        testBackingArrayIntegrity(sorter);
        testStability(sorter);
    }
    
    private void testBackingArrayIntegrity(Sorter sorter) {
        ArrayTempFactory factory = new ArrayTempFactory(32);
        ArrayTempList<String> list = factory.newList(7);
        
        addAll(list, "A", "B", "C", "D", "E", "F", "G");
        
        ArrayTempList<Long> top = factory.newList();
        addAll(top, 1L, 2L, 3L);
        
        assertEquals(1, list.offset);
        assertEquals(7, list.size);
        assertEquals(7, list.capacity);
        
        List<String> reversed = List.of("G", "F", "E", "D", "C", "B", "A");
        
        sorter.sort(list, reverseOrder());
        assertEquals(reversed, list);
        assertEquals(List.of(1L, 2L, 3L), top);
        
        Object[] expectedFactoryArray =
            newArray(factory.array.length, Mark.INSTANCE, "G", "F", "E", "D", "C", "B", "A",
                                           Mark.INSTANCE, 1L, 2L, 3L);
        assertArrayEquals(expectedFactoryArray, factory.array);
        
        do {
            shuffle(list);
        } while (list.equals(reversed));
        
        sorter.sort(list, reverseOrder());
        assertEquals(reversed, list);
        assertEquals(List.of(1L, 2L, 3L), top);
        
        assertArrayEquals(expectedFactoryArray, factory.array);
    }
    
    private void testStability(Sorter sorter) {
        ArrayTempFactory factory = new ArrayTempFactory(32);
        ArrayTempList<Item> items = factory.newList();
        
        Stream.of("Z", "Y", "X", "W", "V", "U", "T", "S", "R")
              .flatMap(val -> IntStream.range(0, 10).mapToObj(i -> new Item(val, i)))
              .forEach(items::add);
        
        sorter.sort(items, comparing(Item::value));
        
        assertTrue(isSorted(items, comparing(Item::value)));
        
        Map<String, List<Item>> byString = new LinkedHashMap<>();
        for (Item item : items) {
            byString.computeIfAbsent(item.value, k -> new ArrayList<>()).add(item);
        }
        
        for (List<Item> values : byString.values()) {
            assertTrue(isSorted(values, comparing(Item::ordinal)));
        }
    }
    
    @Test
    void testInsertionSort() {
        testSort(ArrayTempListSorting::insertionSort);
    }
    
    @Test
    void testMergeSort() {
        testSort(ArrayTempListSorting::mergeSort);
    }
    
    @Test
    void testJdkSort() {
        testSort(ArrayTempListSorting::jdkSort);
    }
}