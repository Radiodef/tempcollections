/* MIT License
 *
 * Copyright (c) 2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.radiodef.temp;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

import java.util.*;
import java.util.Map.Entry;
import static java.util.Map.entry;
import java.util.function.*;
import java.util.stream.*;
import static java.util.Collections.*;

class ArrayTempHashMapTest {
    
    private ArrayTempFactory factory;
    
    @BeforeEach
    void initNewFactory() {
        factory = new ArrayTempFactory(10);
    }
    
    @AfterEach
    void clearFactory() {
        factory = null;
    }
    
    @Test
    void testConstructor() {
        ArrayTempHashMap<?, ?> map = factory.newMap(10);
        
        assertEquals(1, map.offset);
        assertEquals(32, map.capacity);
        assertTrue(map.isEmpty());
    }
    
    @Test
    void testCapacity() {
        int estimatedSize = 8;
        ArrayTempHashMap<?, ?> map = factory.newMap(estimatedSize);
        
        int expectedCapacity = 2 * Tools.next2((int) Math.ceil(estimatedSize / map.loadFactor));
        assertEquals(32, expectedCapacity);
        
        assertEquals(expectedCapacity, map.capacity);
        assertEquals(expectedCapacity, map.capacity());
    }
    
    @Test
    void testZeroCapacity() {
        factory = new ArrayTempFactory(0);
        ArrayTempHashMap<Long, Byte> map = factory.newMap(0);
        
        assertEquals(2, map.capacity);
    }
    
    @Test
    void testMaxSize() {
        int size = 16;
        
        ArrayTempHashMap<?, ?> map = factory.newMap(size);
        assertEquals((ArrayTempFactory.MAX_ARRAY_SIZE - 1) / 2, map.maxSize());
        
        try (ArrayTempHashMap<?, ?> top = factory.newMap()) {
            int max = 2 * size;
            
            assertEquals(max, map.maxSize());
            assertEquals(Tools.next2((int) Math.ceil(size / map.loadFactor)), map.maxSize());
            
            assertEquals((ArrayTempFactory.MAX_ARRAY_SIZE - 1 - (2*max) - 1) / 2, top.maxSize());
        }
        
        assertEquals((ArrayTempFactory.MAX_ARRAY_SIZE - 1) / 2, map.maxSize());
    }
    
    @Test
    @SuppressWarnings("ConstantConditions")
    void testSimplePutAndGet() {
        ArrayTempHashMap<String, Double> map = factory.newMap();
        
        assertEquals(0, map.size);
        
        String key = "hello";
        Double val = Math.PI;
        
        assertNull(map.get(key));
        assertNull(map.put(key, val));
        assertEquals(val, map.get(key));
        
        assertEquals(1, map.size);
        
        val = Math.E;
        
        assertEquals((Double) Math.PI, map.put(key, val));
        assertEquals(val, map.get(key));
        
        assertEquals(1, map.size);
        
        // we do support null, so these just shouldn't throw
        key = null;
        val = null;
        assertNull(map.put(key, val));
        assertNull(map.get(key));
        
        assertEquals(2, map.size);
    }
    
    @Test
    void testPutAndGetRequiringResizeAndRehash() {
        ArrayTempHashMap<String, String> map = factory.newMap(0, 1.0f);
        
        assertEquals(0, map.size);
        assertEquals(2, map.capacity);
        
        assertNull(map.put("1", "1"));
        
        int count = 8;
        
        int resizes = 0;
        
        // should do 4, 8, then 16, for a total of 3 resizes
        for (int i = 2; i <= count; ++i) {
            String key = Integer.toString(i);
            String val = Integer.toString(i * i);
            
            int cap = map.capacity;
            
            assertNull(map.get(key));
            assertNull(map.put(key, val));
            assertEquals(val, map.get(key));
            
            if (cap != map.capacity)
                ++resizes;
        }
        
        assertEquals(3, resizes);
        assertEquals(16, map.capacity);
        
        Map<String, String> expect =
            Map.of("1", "1",
                   "2", "4",
                   "3", "9",
                   "4", "16",
                   "5", "25",
                   "6", "36",
                   "7", "49",
                   "8", "64");
        assertEquals(expect.size(), map.size());
        
        expect.forEach((key, val) ->
            assertEquals(val, map.get(key)));
        
        assertEquals(expect, map);
    }
    
    @Test
    void testFPRound() {
        int n = ArrayTempHashMap.MAX_CAPACITY;
        
        assertEquals(n, (int) (1.0f * n));
        assertTrue((int) (Math.nextDown(1.0f) * n) <= n);
    }
    
    @Test
    void testPutToCapacityWhenNotTop() {
        int cap = 8;
        ArrayTempHashMap<String, String> map = factory.newMap(cap / 2, 0.5f);
        assertEquals(2 * cap, map.capacity);
        
        assertTrue(map.canAdd(0));
        assertTrue(map.canAdd(cap));
        assertTrue(map.canAdd(cap + 1));
        
        try (ArrayTempList<Byte> top = factory.newList(List.of((byte) 1, (byte) 2))) {
            
            for (int i = 0; i < cap; ++i) {
                assertTrue(map.canAdd(1));
                assertTrue(map.canAdd(cap - i));
                assertFalse(map.canAdd(cap - i + 1));
                
                assertNull(map.put("key" + i, "value" + i));
                assertEquals(i + 1, map.size);
                assertEquals(2 * cap, map.capacity);
            }
            
            assertTrue(map.canAdd(0));
            assertFalse(map.canAdd(1));
            
            assertThrows(IllegalStateException.class, () -> map.put("key" + cap, "value" + cap));
            assertEquals(List.of((byte) 1, (byte) 2), top);
        }
        
        assertTrue(map.canAdd(0));
        assertTrue(map.canAdd(1));
        
        assertNull(map.put("key" + cap, "value" + cap));
        assertEquals(cap + 1, map.size);
        assertNotEquals(2 * cap, map.capacity);
        
        assertTrue(map.capacity >= (2 * 2 * cap));
        
        assertTrue(map.canAdd(1));
    }
    
    @Test
    void testPutAll() {
        assertThrows(NullPointerException.class, () -> factory.newMap().putAll(null));
    }
    
    @Test
    void testPutAllRegularMap() {
        ArrayTempHashMap<String, Long> map = factory.newMap();
        
        map.putAll(emptyMap());
        assertEquals(0, map.size);
        
        Map<String, Long> other = Map.of("X", 7L, "Y", 8L, "Z", 9L);
        
        map.putAll(other);
        
        assertEquals(3, map.size);
        assertEquals(other, map);
        
        other = new LinkedHashMap<>(other);
        other.put(null, 0L);
        other.put("null", null);
        other.put("X", 666L);
        
        map.putAll(other);
        
        assertEquals(5, map.size);
        assertEquals((Long) 666L, map.get("X"));
        assertEquals((Long) 8L,   map.get("Y"));
        assertEquals((Long) 9L,   map.get("Z"));
        assertEquals((Long) 0L,   map.get(null));
        assertNull(map.get("null"));
    }
    
    @Test
    void testPutAllArrayTempHashMap() {
        ArrayTempHashMap<String, Long> map = factory.newMap();
        
        map.putAll(new ArrayTempFactory(10).newMap());
        assertEquals(0, map.size);
        
        ArrayTempHashMap<String, Long> other = new ArrayTempFactory(10).newMap();
        other.put("X", 7L);
        other.put("Y", 8L);
        other.put("Z", 9L);
        
        map.putAll(other);
        
        assertEquals(3, map.size);
        assertEquals(other, map);
        
        other.put(null, 0L);
        other.put("null", null);
        other.put("X", 666L);
        
        map.putAll(other);
        
        assertEquals(5, map.size);
        assertEquals((Long) 666L, map.get("X"));
        assertEquals((Long) 8L,   map.get("Y"));
        assertEquals((Long) 9L,   map.get("Z"));
        assertEquals((Long) 0L,   map.get(null));
        assertNull(map.get("null"));
    }
    
    @Test
    void testPutIfAbsent() {
        testPutIfAbsent(new LinkedHashMap<>());
        testPutIfAbsent(factory.newMap());
    }
    
    private void testPutIfAbsent(Map<String, String> map) {
        assertTrue(map.isEmpty());
        map.putAll(Map.of("Aa", "11",
                          "Bb", "22",
                          "Cc", "33"));
        
        assertEquals("11", map.putIfAbsent("Aa", "Xx"));
        assertEquals("11", map.get("Aa"));
        assertFalse(map.containsValue("Xx"));
        
        assertEquals(3, map.size());
        
        new LinkedHashMap<>(map).forEach((k, v) -> {
            assertEquals(v, map.putIfAbsent(k, "Xx"));
            assertEquals(v, map.get(k));
            assertFalse(map.containsValue("Xx"));
        });
        
        assertEquals(3, map.size());
        
        assertFalse(map.containsKey("Dd"));
        assertNull(map.putIfAbsent("Dd", "44"));
        assertEquals("44", map.get("Dd"));
        
        assertEquals(4, map.size());
        
        assertFalse(map.containsKey("Ee"));
        assertNull(map.putIfAbsent("Ee", null), "putIfAbsent should put a null value");
        assertTrue(map.containsKey("Ee"));
        assertNull(map.get("Ee"));
        
        assertEquals(5, map.size());
        
        // putIfAbsent treats null as "absent"
        assertNull(map.putIfAbsent("Ee", "55"), "putIfAbsent should replace a null value");
        assertTrue(map.containsKey("Ee"));
        assertEquals("55", map.get("Ee"));
        
        assertEquals(5, map.size());
        
        assertNull(map.putIfAbsent(null, "null"));
        assertEquals("null", map.get(null));
        assertEquals("null", map.putIfAbsent(null, "llun"));
        assertEquals("null", map.get(null));
        
        assertEquals(6, map.size());
    }
    
    @Test
    void testPutIfAbsentRehash() {
        int size = 8;
        ArrayTempHashMap<String, String> map = factory.newMap(8, 1.0f);
        assertEquals(2*size, map.capacity());
        
        for (int i = 0; i < size; ++i) {
            map.put("key" + i, "value" + i);
        }
        
        assertEquals(size, map.size());
        assertEquals(2*size, map.capacity());
        
        assertFalse(map.containsKey("key"));
        
        try (@SuppressWarnings("unused") TempObject top = factory.newList()) {
            assertThrows(IllegalStateException.class, () -> map.putIfAbsent("key", "value"));
        }
        
        assertFalse(map.containsKey("key"));
        assertEquals(2*size, map.capacity());
        
        assertNull(map.putIfAbsent("key", "value"));
        assertNotEquals(2*size, map.capacity());
        
        assertEquals(size + 1, map.size());
        assertEquals("value", map.get("key"));
        
        for (int i = 0; i < size; ++i) {
            assertEquals("value" + i, map.get("key" + i));
        }
    }
    
    @FunctionalInterface
    private interface MapSupplier {
        <K, V> Map<K, V> get();
    }
    
    @Test
    void testMerge() {
        testMerge(HashMap::new);
        testMerge(factory::newMap);
    }
    
    private void testMerge(MapSupplier supplier) {
        Map<Integer, Integer> ints = supplier.get();
        
        int count = 10;
        for (int i = 0; i < count; ++i)
            assertNull(ints.put(i, i));
        assertEquals(count, ints.size());
        
        // plain merge
        for (int i = 0; i < count; ++i)
            assertEquals(i + 1, (int) ints.merge(i, 1, Integer::sum));
        assertEquals(count, ints.size());
        
        ints.forEach((k, v) -> assertEquals(k + 1, (int) v));
        
        // merge returning null should remove
        for (int i = 0; i < count; ++i)
            assertEquals((i & 1) == 0 ? null : (i + 2),
                         ints.merge(i, 1, (a, b) -> ((a + b) & 1) == 0 ? null : (a + b)));
        assertEquals(count / 2, ints.size());
        
        ints.forEach((k, v) -> {
            assertFalse((v & 1) == 0);
            assertEquals(k + 2, (int) v);
        });
        
        // merge with a non-existent key should result in the value
        // and not call the remapping function
        Map<String, String> strings = supplier.get();
        assertFalse(strings.containsKey("foo"));
        assertEquals("bar", strings.merge("foo", "bar", (a, b) -> fail(a + ", " + b)));
        assertEquals("bar", strings.get("foo"));
        
        // merge should call .apply(old,new), not .apply(new,old)
        assertEquals("bar-baz", strings.merge("foo", "-baz", (a, b) -> {
            assertEquals("bar", a);
            assertEquals("-baz", b);
            return a + b;
        }));
        assertEquals("bar-baz", strings.get("foo"));
        
        // null tests
        // noinspection ConstantConditions
        assertThrows(NullPointerException.class, () -> strings.merge("k", null, String::concat),
            "value must be non-null");
        // noinspection ConstantConditions
        assertThrows(NullPointerException.class, () -> strings.merge("k", "v", null),
            "remapping function must be non-null");
        
        assertFalse(strings.containsKey(null));
        // null key should be fine
        assertEquals("null", strings.merge(null, "null", (a, b) -> null));
        assertTrue(strings.containsKey(null));
        // null return value means remove
        assertNull(strings.merge(null, "null", (a, b) -> null));
        assertFalse(strings.containsKey(null));
    }
    
    @Test
    void testMergeRehash() {
        int size = 8;
        ArrayTempHashMap<String, String> map = factory.newMap(size, 1.0f);
        assertEquals(2*size, map.capacity());
        
        for (int i = 0; i < size; ++i)
            map.put("key" + i, "value" + i);
        assertEquals(size, map.size());
        assertEquals(2*size, map.capacity());
        assertTrue(map.canAdd(1));
        
        try (@SuppressWarnings("unused") TempObject top = factory.newList()) {
            assertFalse(map.canAdd(1));
            // merge should be fine if we don't put a new entry
            assertTrue(map.containsKey("key0"));
            assertEquals("value0foo", map.merge("key0", "foo", String::concat));
            assertEquals(size, map.size());
            
            assertFalse(map.containsKey("keyX"));
            assertThrows(IllegalStateException.class, () -> map.merge("keyX", "valueX", String::concat));
            assertEquals(size, map.size());
        }
        
        assertEquals(2*size, map.capacity());
        assertTrue(map.canAdd(1));
        assertEquals("valueX", map.merge("keyX", "valueX", String::concat));
        
        assertEquals(size + 1, map.size());
        assertNotEquals(2*size, map.capacity());
    }
    
    @Test
    void testMergeConcurrentModification() {
        ArrayTempHashMap<Integer, Integer> map = factory.newMap();
        
        @SuppressWarnings("Convert2Lambda")
        BiFunction<Integer, Integer, Integer> func = new BiFunction<>() {
            @Override
            public Integer apply(Integer a, Integer b) {
                if (b > 0) {
                    map.merge(b - 1, b - 1, this);
                }
                return a + b;
            }
        };
        
        assertEquals(1, (int) map.merge(1, 1, func), "func won't be called the 1st time");
        assertThrows(ConcurrentModificationException.class, () -> map.merge(1, 1, func));
    }
    
    @Test
    void testMergeThrowingException() {
        ArrayTempHashMap<String, String> map = factory.newMap();
        map.put("Abc", "Def");
        
        @SuppressWarnings("ThrowableNotThrown")
        RuntimeException aRuntimeException = new RuntimeException("a message");
        try {
            map.merge("Abc", "Ghi", (a, b) -> { throw aRuntimeException; });
            fail("exceptions thrown from the remapping function must be rethrown");
        } catch (RuntimeException x) {
            assertSame(aRuntimeException, x);
            assertEquals("Def", map.get("Abc"), "mappings must be left unchanged");
        }
    }
    
    @Test
    void testComputeIfAbsent() {
        testComputeIfAbsent(HashMap::new);
        testComputeIfAbsent(factory::newMap);
    }
    
    private void testComputeIfAbsent(MapSupplier supplier) {
        Map<String, String> strings = supplier.get();
        assertTrue(strings.isEmpty());
    
        // noinspection ConstantConditions
        assertThrows(NullPointerException.class, () -> strings.computeIfAbsent("k", null));
        
        assertEquals("bar", strings.computeIfAbsent("foo", k -> "bar"));
        assertEquals("bar", strings.get("foo"));
        assertEquals(1, strings.size());
    
        // noinspection ConstantConditions
        assertThrows(NullPointerException.class, () -> strings.computeIfAbsent("foo", null),
            "method should throw even when the mapping function would not be used");
        
        assertEquals("bar", strings.computeIfAbsent("foo",
            k -> fail("the mapping function must not be called if a mapping is present")));
        assertEquals("bar", strings.get("foo"));
        assertEquals(1, strings.size());
        
        assertNull(strings.computeIfAbsent("ABC", k -> null),
            "the map must not be modified if the mapping function returns null");
        assertFalse(strings.containsKey("ABC"));
        assertEquals(1, strings.size());
        
        assertNull(strings.put("ABC", null));
        assertTrue(strings.containsKey("ABC"));
        assertNull(strings.get("ABC"));
        assertEquals(2, strings.size());
        
        assertEquals("XYZ", strings.computeIfAbsent("ABC", k -> "XYZ"),
            "a null value is treated the same as no mapping");
        assertEquals("XYZ", strings.get("ABC"));
        assertEquals(2, strings.size());
        
        assertFalse(strings.containsKey(null));
        assertEquals("String", strings.computeIfAbsent(null, k -> "String"),
            "no special treatment of null keys");
        assertTrue(strings.containsKey(null));
        assertEquals("String", strings.get(null));
        assertEquals("String", strings.computeIfAbsent(null, k -> null));
        
        strings.clear();
        int count = 10;
        
        Map<String, String> copy = new LinkedHashMap<>();
        
        for (int i = 0; i < count; ++i) {
            assertEquals( ("key"+i+"value") , strings.computeIfAbsent( ("key"+i) , k -> (k+"value") ));
            assertEquals(i + 1, strings.size());
            copy.put( ("key"+i) , ("key"+i+"value") );
        }
        for (int i = 0; i < count; ++i) {
            assertEquals( ("key"+i+"value") , strings.computeIfAbsent( ("key"+i) , k -> ("foobar") ));
            assertEquals(count, strings.size());
        }
        
        assertEquals(copy, strings);
    }
    
    @Test
    void testComputeIfAbsentFunctionCalledMoreThanOnce() {
        Map<String, List<?>> map = factory.newMap();
        Function<String, List<?>> func = k -> new ArrayList<>();
        
        assertEquals(emptyList(), map.computeIfAbsent("1", func));
        assertEquals(emptyList(), map.computeIfAbsent("2", func));
        
        assertNotSame(map.get("1"), map.get("2"));
    }
    
    @Test
    void testComputeIfAbsentRehash() {
        int size = 8;
        ArrayTempHashMap<String, String> map = factory.newMap(size, 1.0f);
        assertEquals(2*size, map.capacity());
        
        for (int i = 0; i < size; ++i) {
            assertTrue(map.canAdd(1));
            assertEquals("value" + i, map.computeIfAbsent(Integer.toString(i), k -> "value" + k));
            assertEquals(2*size, map.capacity());
        }
        
        try (@SuppressWarnings("unused") TempObject top = factory.newList()) {
            assertFalse(map.canAdd(1));
            
            assertEquals("value1", map.computeIfAbsent("1", k -> fail("1 is present")));
            assertEquals(size, map.size());
            assertEquals(2*size, map.capacity());
            
            assertThrows(IllegalStateException.class, () -> map.computeIfAbsent("X", k -> "valueX"));
            assertEquals(size, map.size());
            assertEquals(2*size, map.capacity());
        }
        
        assertTrue(map.canAdd(1));
        assertEquals(size, map.size());
        assertEquals(2*size, map.capacity());
        
        assertEquals("valueX", map.computeIfAbsent("X", k -> "valueX"));
        
        assertEquals(size + 1, map.size());
        assertNotEquals(2*size, map.capacity());
    }
    
    @Test
    void testComputeIfAbsentThrowingException() {
        ArrayTempHashMap<String, String> map = factory.newMap(Map.of("foo", "bar"));
        @SuppressWarnings("ThrowableNotThrown")
        RuntimeException aRuntimeException = new RuntimeException("a message");
        
        try {
            map.computeIfAbsent("key", key -> { throw aRuntimeException; });
            fail("exceptions thrown by the mapping function must be relayed to the caller");
        } catch (RuntimeException x) {
            assertSame(aRuntimeException, x);
            assertEquals(Map.of("foo", "bar"), map, "mappings must be unchanged");
        }
    }
    
    @Test
    void testComputeIfAbsentConcurrentModification() {
        ArrayTempHashMap<Long, Long> map = factory.newMap();
        
        @SuppressWarnings("Convert2Lambda")
        Function<Long, Long> factorial = new Function<>() {
            @Override
            public Long apply(Long n) {
                if (n <= 1L)
                    return 1L;
                return n * map.computeIfAbsent(n - 1L, this);
            }
        };
        
        assertThrows(ConcurrentModificationException.class, () -> map.computeIfAbsent(6L, factorial));
    }
    
    @Test
    void testComputeIfPresent() {
        testComputeIfPresent(HashMap::new);
        testComputeIfPresent(factory::newMap);
    }
    
    private void testComputeIfPresent(MapSupplier supplier) {
        Map<String, String> strings = supplier.get();
        
        // noinspection ConstantConditions
        assertThrows(NullPointerException.class, () -> strings.computeIfPresent("key", null),
            "method should throw even if the function would not be used");
        
        assertNull(strings.computeIfPresent("key", (k, v) -> "value"));
        assertFalse(strings.containsKey("key"));
        
        int count = 10;
        
        for (int i = 0; i < count; ++i) {
            String key = ("key"+i);
            
            assertNull(strings.computeIfPresent( key , (k, v) -> "value"));
            assertFalse(strings.containsKey( key ));
        }
        for (int i = 0; i < count; ++i) {
            strings.put( ("key"+i) , ("value"+i) );
        }
        for (int i = 0; i < count; ++i) {
            String key = ("key"+i);
            
            assertTrue(strings.containsKey( key ));
            assertEquals( ("value"+i+"value"+i) , strings.computeIfPresent( key , (k, v) -> (v+v) ));
            assertEquals( ("value"+i+"value"+i) , strings.get( key ));
        }
        
        assertFalse(strings.containsKey(null));
        assertNull(strings.computeIfPresent(null, (k, v) -> "String"),
            "null keys get no special treatment");
        assertFalse(strings.containsKey(null));
        strings.put(null, "String");
        assertEquals("StringString", strings.computeIfPresent(null, (k, v) -> (v+v) ));
        assertEquals("StringString", strings.get(null));
        
        strings.remove(null);
        
        for (int i = 0; i < count; ++i) {
            String key = ("key"+i);
            
            assertTrue(strings.containsKey( key ));
            assertNull(strings.computeIfPresent( key , (k, v) -> null ));
            assertFalse(strings.containsKey( key ),
                "null returned from the remapping function should remove the entry");
        }
        
        assertTrue(strings.isEmpty());
        
        String key = "key";
        String val = "val";
        strings.put( key , null);
        
        assertTrue(strings.containsKey( key ));
        assertNull(strings.get( key ));
        assertEquals(1, strings.size());
        
        assertNull(strings.computeIfPresent( key , (k, v) -> val ),
            "a null value in the map is treated as being absent");
        
        assertTrue(strings.containsKey( key ));
        assertNotEquals( val , strings.get( key ));
        assertNull(strings.get( key ));
        
        strings.put( key , val );
        assertEquals( val , strings.computeIfPresent( key , (k, v) -> {
            assertEquals( key, k , "key must be passed in" );
            assertEquals( val, v , "current value must be passed in" );
            return v;
        } ));
    }
    
    @Test
    void testComputeIfPresentWhenTableIsFull() {
        int size = 8;
        ArrayTempHashMap<String, String> map = factory.newMap(size, 1.0f);
        assertEquals(2*size, map.capacity());
        
        for (int i = 0; i < size; ++i) {
            assertNull(map.put("key" + i, "value" + i));
            assertEquals(i + 1, map.size());
            assertEquals(2*size, map.capacity());
        }
        
        try (@SuppressWarnings("unused") TempObject top = factory.newList()) {
            assertFalse(map.canAdd(1));
            assertEquals(map.capacity(), 2*map.size());
            
            // computeIfPresent never resizes the table, so we just make sure
            // that it doesn't seem broken when the table is full
            assertEquals("foobar", map.computeIfPresent("key0", (k, v) -> "foobar"));
            assertNull(map.computeIfPresent("keyX", (k, v) -> "valueX"));
            
            String key1 = "key1";
            assertTrue(map.containsKey( key1 ));
            assertNull(map.computeIfPresent( key1 , (k, v) -> null));
            assertFalse(map.containsKey( key1 ));
            
            assertEquals(map.capacity() - 2, 2*map.size());
            assertTrue(map.canAdd(1));
        }
    }
    
    @Test
    void testComputeIfPresentFunctionCalledMoreThanOnce() {
        Map<String, Object> map = factory.newMap();
        
        Object anObject = new Object();
        map.put("a", anObject);
        map.put("b", anObject);
        
        BiFunction<String, Object, Object> func = (k, v) -> new Object();
        
        assertNotSame(anObject, map.computeIfPresent("a", func));
        assertNotSame(anObject, map.computeIfPresent("b", func));
        
        assertNotSame(anObject, map.get("a"));
        assertNotSame(anObject, map.get("b"));
        
        assertNotSame(map.get("a"), map.get("b"));
    }
    
    @Test
    void testComputeIfPresentThrowingException() {
        ArrayTempHashMap<Long, Long> map = factory.newMap(Map.of(1L, 100L));
        
        @SuppressWarnings("ThrowableNotThrown")
        RuntimeException aRuntimeException = new RuntimeException("a message");
        
        try {
            map.computeIfPresent(1L, (k, v) -> { throw aRuntimeException; });
            fail("exceptions thrown by the remapping function must be relayed to the caller");
        } catch (RuntimeException x) {
            assertSame(aRuntimeException, x);
            assertEquals(Map.of(1L, 100L), map);
        }
    }
    
    @Test
    void testComputeIfPresentConcurrentModification() {
        ArrayTempHashMap<Long, Long> map = factory.newMap();
        
        @SuppressWarnings("Convert2Lambda")
        BiFunction<Long, Long, Long> function = new BiFunction<>() {
            @Override
            public Long apply(Long key, Long val) {
                if (key > 0L) {
                    map.put(key - 1, val - 1);
                }
                return val + 1L;
            }
        };
        
        map.put(1L, 1L);
        assertThrows(ConcurrentModificationException.class, () -> map.computeIfPresent(1L, function));
    }
    
    @Test
    void testCompute() {
        testCompute(HashMap::new);
        testCompute(factory::newMap);
    }
    
    private void testCompute(MapSupplier supplier) {
        Map<String, String> strings = supplier.get();
        
        // noinspection ConstantConditions
        assertThrows(NullPointerException.class, () -> strings.compute("key", null));
        
        int size = 10;
        for (int i = 0; i < size; ++i) {
            String key = Integer.toString(i);
            String val = "val" + i;
            
            assertFalse(strings.containsKey( key ));
            assertEquals( val, strings.compute( key, (k, v) -> {
                assertNull(v);
                return val;
            } ));
            
            assertEquals( val, strings.get( key ));
            assertEquals( i+1, strings.size() );
        }
        
        for (int i = 0; i < size; ++i) {
            String key = Integer.toString(i);
            String val = ("val" + i + ".chars");
            
            assertEquals( val, strings.compute( key, (k, v) -> {
                assertEquals( ("val" + k), v );
                return (v + ".chars");
            } ));
            
            assertEquals( size, strings.size() );
        }
        for (int i = 0; i < size; ++i) {
            String key = Integer.toString(i);
            String val = ("val" + i + ".chars");
            
            assertEquals( val, strings.get( key ));
        }
        
        String key0 = "0";
        assertTrue(strings.containsKey( key0 ));
        assertNull(strings.compute( key0, (k, v) -> null ));
        assertFalse(strings.containsKey( key0 ),
            "returning null from the remapping function should result in a removal");
        
        assertEquals( size-1, strings.size() );
        
        assertNull(strings.put( key0, null ));
        assertEquals( size, strings.size() );
        
        assertTrue(strings.containsKey( key0 ));
        assertNull(strings.get( key0 ));
        // note that here we're removing an entry whose value actually used to be null
        assertNull(strings.compute( key0, (k, v) -> null ));
        assertFalse(strings.containsKey( key0 ),
            "returning null from the remapping function should result in a removal");
        
        --size;
        assertEquals( size, strings.size() );
        
        for (String key : new ArrayList<>(strings.keySet())) {
            assertTrue(strings.containsKey( key ));
            
            assertNull(strings.compute( key, (k, v) -> null ));
            assertFalse(strings.containsKey( key ));
            
            --size;
            assertEquals( size, strings.size() );
        }
        
        assertTrue(strings.isEmpty());
    }
    
    @Test
    void testComputeRehash() {
        int size = 8;
        ArrayTempHashMap<Integer, String> map = factory.newMap(size, 1.0f);
        assertEquals(2*size, map.capacity());
        
        for (int i = 0; i < size; ++i) {
            assertTrue(map.canAdd( 1 ));
            assertFalse(map.containsKey( i ));
            
            assertEquals( ("value" + i), map.compute( i, (k, v) -> ("value" + k) ));
            
            assertTrue(map.containsKey( i ));
            assertEquals( ("value" + i), map.get( i ));
            
            assertEquals(2*size, map.capacity());
        }
        
        assertEquals(size, map.size());
        assertEquals(2*size, map.capacity());
        
        try (@SuppressWarnings("unused") TempObject top = factory.newList()) {
            assertFalse(map.canAdd( 1 ));
            assertEquals(size, map.size());
            assertEquals(2*size, map.capacity());
            
            assertFalse(map.containsKey( 601 ));
            assertThrows(IllegalStateException.class, () -> map.compute( 601, (k, v) -> "value601" ));
            assertFalse(map.containsKey( 601 ));
            
            assertEquals(size, map.size());
            assertEquals(2*size, map.capacity());
            
            assertEquals("value1.modified", map.compute( 1, (k, v) -> (v + ".modified") ));
            
            assertEquals(size, map.size());
            assertEquals(2*size, map.capacity());
        }
        
        assertTrue(map.canAdd( 1 ));
        assertEquals(size, map.size());
        assertEquals(2*size, map.capacity());
        
        assertEquals("value601", map.compute( 601, (k, v) -> "value601" ));
        
        assertEquals(size + 1, map.size());
        assertEquals(2*2*size, map.capacity());
    }
    
    @Test
    void testComputeCallsFunctionMoreThanOnce() {
        Map<String, List<?>> map = factory.newMap();
        BiFunction<String, List<?>, List<?>> func = (k, v) -> new ArrayList<>();
        
        assertEquals(emptyList(), map.compute("1", func));
        assertEquals(emptyList(), map.compute("2", func));
        
        assertNotSame(map.get("1"), map.get("2"));
    }
    
    @Test
    void testComputeThrowingException() {
        ArrayTempHashMap<Long, Long> map = factory.newMap(Map.of(0L, 0L));
        @SuppressWarnings("ThrowableNotThrown")
        RuntimeException aRuntimeException = new RuntimeException("a message");
        try {
            map.compute(0L, (k, v) -> { throw aRuntimeException; });
            fail("exceptions must be propagated to the caller");
        } catch (RuntimeException x) {
            assertSame(aRuntimeException, x);
            assertEquals(Map.of(0L, 0L), map, "map must not have been modified");
        }
    }
    
    @Test
    void testComputeConcurrentModification() {
        ArrayTempHashMap<Long, Long> map = factory.newMap();
        
        @SuppressWarnings("Convert2Lambda")
        BiFunction<Long, Long, Long> factorial = new BiFunction<>() {
            @Override
            public Long apply(Long k, Long n) {
                if (n == null)
                    n = (k <= 1L) ? 1L : (k * map.compute(k - 1L, this));
                return n;
            }
        };
        
        assertThrows(ConcurrentModificationException.class, () -> map.compute(6L, factorial));
    }
    
    @Test
    void testGetOrDefault() {
        ArrayTempHashMap<String, Long> map = factory.newMap();
        
        assertEquals((Long) 2L, map.getOrDefault("key", 2L));
        assertEquals((Long) 2L, map.getOrDefault(null, 2L));
        
        assertNull(map.getOrDefault("key", null));
        
        map.putAll(Map.of("Abc", 123L,
                          "Def", 456L,
                          "Ghi", 789L));
        map.put(null, null);
        
        assertEquals((Long) 123L, map.getOrDefault("Abc", 0xD00DL));
        assertEquals((Long) 0xD00DL, map.getOrDefault("key", 0xD00DL));
        
        for (var e : map.entrySet()) {
            assertTrue(map.containsKey(e.getKey()));
            assertEquals(e.getValue(), map.getOrDefault(e.getKey(), 0xBAD_DEAL));
        }
        
        assertEquals((Long) 123L, map.remove("Abc"));
        assertFalse(map.containsKey("Abc"));
        assertEquals((Long) 601L, map.getOrDefault("Abc", 601L));
        
        for (var e : new LinkedHashMap<>(map).entrySet()) {
            assertTrue(map.containsKey(e.getKey()));
            assertEquals(e.getValue(), map.remove(e.getKey()));
            assertEquals((Long) 0xFAB_1D0L, map.getOrDefault(e.getKey(), 0xFAB_1D0L));
        }
        
        assertTrue(map.isEmpty());
        assertEquals((Long) 0x2_C00L, map.getOrDefault("Def", 0x2_C00L));
    }
    
    @Test
    void testClear() {
        TempList<String> bot = factory.newList(List.of("A", "B", "C"));
        
        ArrayTempHashMap<String, Integer> map =
            factory.newMap(Map.of("1", 1, "2", 2, "3", 3));
        
        TempList<String> top = factory.newList(List.of("X", "Y", "Z"));
        
        assertEquals(Map.of("1", 1, "2", 2, "3", 3), map);
        
        map.clear();
        assertEquals(emptyMap(), map);
        map.clear();
        assertEquals(emptyMap(), map);
        
        assertEquals(List.of("A", "B", "C"), bot);
        assertEquals(List.of("X", "Y", "Z"), top);
    }
    
    @Test
    void testPublicRehash() {
        Map<String, Integer> entries = Map.of("A", 1, "B", 2, "C", 3, "P", 4, "Q", 5, "R", 6, "X", 7, "Y", 8, "Z", 9);
        List<String> toRemove = List.of("B", "P", "X", "Y");
        
        ArrayTempHashMap<String, Integer> map = factory.newMap(entries);
        
        Iterator<String> it = map.keySet().iterator();
        // noinspection Java8CollectionRemoveIf
        while (it.hasNext()) {
            if (toRemove.contains( it.next() ))
                it.remove();
        }
        
        assertEquals( toRemove.size(), Arrays.stream(factory.array, map.offset, map.offset + map.capacity)
                                             .filter(Del.INSTANCE::equals)
                                             .count() );
        
        map.rehash();
        
        assertTrue( Arrays.stream(factory.array, map.offset, map.offset + map.capacity)
                          .noneMatch(Del.INSTANCE::equals) );
        
        Map<String, Integer> expect = new HashMap<>(entries);
        toRemove.forEach(expect::remove);
        assertEquals(expect, map);
    }
    
    @Test
    void testResizeComputation() {
        int n = 1 << 25;
        assertEquals((float) (n + 1), (float) n);
        
        int half = n / 2;
        int size = half / 2;
        ArrayTempHashMap<Integer, Boolean> set = factory.newMap(size, 1.0f);
        
        for (int i = 0; i < size; ++i)
            set.put(i, Boolean.TRUE);
        
        assertEquals(size, set.size());
        assertEquals(half, set.capacity());
        
        Map<Integer, Boolean> toPut = new LinkedHashMap<>(size + 1);
        for (int i = 0; i <= size; ++i)
            toPut.put(size + i, Boolean.TRUE);
        
        assertEquals(size + 1, toPut.size());
        
        ArrayTempHashMap.REHASH_COUNT = 0;
        
        set.putAll(toPut);
        
        assertEquals((2 * size) + 1, set.size());
        assertEquals(2 * n, set.capacity());
        
        if (ArrayTempHashMap.TESTING) {
            assertEquals(1, ArrayTempHashMap.REHASH_COUNT);
        }
    }
    
    @Test
    void testRemove() {
        ArrayTempHashMap<Integer, String> map = factory.newMap();
        
        assertNull(map.put(1, "A"));
        assertEquals(1, map.size);
        
        assertEquals("A", map.remove(1));
        assertEquals(0, map.size);
        
        assertTrue(Arrays.stream(factory.array, map.offset, map.offset + map.capacity)
                         .allMatch(Objects::isNull));
        
        map.putAll(Map.of(2, "B", 3, "C", 4, "D"));
        // note: null and 0 have the same hash
        map.put(null, "null");
        map.put(0, null);
        
        Object[] expect = new Object[factory.array.length];
        int i = 0;
        expect[i++] = Mark.INSTANCE;
        expect[i++] = Nil.INSTANCE;
        expect[i++] = "null";
        expect[i++] = 0;
        expect[i++] = null;
        expect[i++] = 2;
        expect[i++] = "B";
        expect[i++] = 3;
        expect[i++] = "C";
        expect[i++] = 4;
        expect[i]   = "D";
        assertArrayEquals(expect, factory.array);
        
        assertEquals("null", map.remove(null));
        
        // removing null entry should cause 0 entry to shift
        expect[1] = expect[3];
        expect[2] = expect[4];
        expect[3] = null;
        expect[4] = null;
        assertArrayEquals(expect, factory.array);
        
        assertEquals(4, map.size);
        
        for (Entry<Integer, String> e : new ArrayList<>(map.entrySet())) {
            Integer key = e.getKey();
            
            int size = map.size;
            assertEquals(map.get(key), map.remove(key));
            assertEquals(size - 1, map.size);
            
            i = map.offset + (2 * key);
            
            expect[i] = expect[i + 1] = null;
            assertArrayEquals(expect, factory.array);
        }
        
        assertEquals(emptyMap(), map);
        assertEquals(0, map.size);
        
        assertTrue(Arrays.stream(factory.array, map.offset, map.offset + map.capacity)
                         .allMatch(Objects::isNull));
    }
    
    @Test
    void testRemoveReturningBoolean() {
        ArrayTempHashMap<String, String> map = factory.newMap();
        
        assertFalse(map.removeReturningBoolean("Aa"));
        assertFalse(map.removeReturningBoolean(null));
        
        map.putAll(Map.of("Aa", "01",
                          "Bb", "02",
                          "Cc", "03",
                          "Dd", "04",
                          "Ee", "05"));
        map.put(null, null);
        assertEquals(6, map.size);
        
        assertTrue(map.containsKey("Aa"));
        assertTrue(map.containsValue("01"));
        assertTrue(map.removeReturningBoolean("Aa"));
        assertFalse(map.containsKey("Aa"));
        assertFalse(map.containsValue("01"));
        assertFalse(map.removeReturningBoolean("Aa"));
        assertEquals(5, map.size);
        
        assertTrue(map.containsKey(null));
        assertTrue(map.containsValue(null));
        assertTrue(map.removeReturningBoolean(null));
        assertFalse(map.containsKey(null));
        assertFalse(map.containsValue(null));
        assertFalse(map.removeReturningBoolean(null));
        assertEquals(4, map.size);
        
        for (var e : new LinkedHashMap<>(map).entrySet()) {
            assertTrue(map.removeReturningBoolean(e.getKey()));
            assertFalse(map.containsKey(e.getKey()));
        }
        
        assertTrue(map.isEmpty());
        
        assertTrue(Arrays.stream(factory.array, map.offset, map.offset + map.capacity)
                         .noneMatch(Objects::nonNull));
    }
    
    @Test
    void testRemoveAtIndex() {
        List<String> bot = factory.newList(List.of("A", "B", "C"));
        
        int size = 10;
        ArrayTempHashMap<Integer, String> map = factory.newMap(10 * size, 1.0f);
        
        List<String> top = factory.newList(List.of("X", "Y", "Z"));
        
        Map<Integer, String> mirror = new LinkedHashMap<>();
        IntUnaryOperator keyMaker = (int i) -> (i << 16) ^ i;
        
        for (int i = (size - 1); i >= 0; --i) {
            Integer k = keyMaker.applyAsInt(i);
            String v = "value";
            
            assertEquals(map.offset, ArrayTempHashMap.hash(k, map.offset, map.capacity),
                "these should all hash to index 0, but we put them in descending order");
            
            assertNull(map.put(k, v));
            assertNull(mirror.put(k, v));
            assertEquals(mirror, map);
            
            assertEquals(k, factory.getDirect(map, 2 * (size - 1 - i)));
        }
        
        for (int i = (size - 1); i >= 0; --i) {
            Integer k = keyMaker.applyAsInt(i);
            
            assertEquals(k, factory.getDirect(map, 0),
                "removing the entry at index 0 should cause all the others to shift");
            
            assertTrue(map.containsKey(k));
            assertTrue(map.removeAtIndex(map.offset));
            
            assertFalse(map.containsKey(k), () -> (k + " in " + map));
        }
        
        assertThrows(IllegalArgumentException.class, () -> map.removeAtIndex(map.offset - 1), "OOB");
        assertThrows(IllegalArgumentException.class, () -> map.removeAtIndex(map.offset + 1), "value");
        assertThrows(IllegalArgumentException.class, () -> map.removeAtIndex(map.offset + map.capacity), "OOB");
        
        assertTrue(map.isEmpty());
        assertFalse(map.removeAtIndex(map.offset));
        
        // all the other removals were at index 0,
        // so just test something else
        assertNull(map.put(3, "value"));
        assertEquals(3, factory.getDirect(map, 2*3));
        assertTrue(map.containsKey(3));
        assertTrue(map.removeAtIndex(map.offset + 2*3));
        assertFalse(map.containsKey(3));
        
        assertTrue(map.isEmpty());
        
        assertEquals(List.of("A", "B", "C"), bot);
        assertEquals(List.of("X", "Y", "Z"), top);
    }
    
    @Test
    void testRemoveIfExpectedValue() {
        testRemoveIfExpectedValue(new LinkedHashMap<>());
        testRemoveIfExpectedValue(factory.newMap());
    }
    
    private void testRemoveIfExpectedValue(Map<String, String> map) {
        assertTrue(map.isEmpty());
        
        assertFalse(map.remove("Aa", "01"));
        assertFalse(map.remove(null, null));
        
        map.putAll(Map.of("Aa", "01",
                          "Bb", "02",
                          "Cc", "03",
                          "Dd", "04",
                          "Ee", "05"));
        map.put(null, null);
        assertEquals(6, map.size());
        
        assertFalse(map.remove("Aa", "xx"));
        assertEquals("01", map.get("Aa"));
        
        assertTrue(map.remove("Aa", "01"));
        assertFalse(map.containsKey("Aa"));
        assertFalse(map.containsValue("01"));
        
        assertFalse(map.remove(null, "null"));
        assertNull(map.get(null));
        
        assertTrue(map.remove(null, null));
        assertFalse(map.containsKey(null));
        assertFalse(map.containsValue(null));
        
        new LinkedHashMap<>(map).forEach((k, v) -> {
            assertFalse(map.remove(v, k));
            assertTrue(map.remove(k, v));
        });
        
        assertTrue(map.isEmpty());
    }
    
    @Test
    void testReplace() {
        testReplace(new LinkedHashMap<>());
        testReplace(factory.newMap());
    }
    
    private void testReplace(Map<String, String> map) {
        assertTrue(map.isEmpty());
        
        assertNull(map.replace("Aa", "01"));
        assertNull(map.replace(null, null));
        
        map.putAll(Map.of("Aa", "01",
                          "Bb", "02",
                          "Cc", "03",
                          "Dd", "04",
                          "Ee", "05"));
        map.put(null, null);
        assertEquals(6, map.size());
        
        assertEquals("01", map.replace("Aa", "aaaaaaa"));
        assertEquals("aaaaaaa", map.get("Aa"));
        
        assertEquals("aaaaaaa", map.replace("Aa", null));
        assertTrue(map.containsKey("Aa"), "replace with null shouldn't remove");
        assertNull(map.get("Aa"));
        assertNull(map.replace("Aa", "01"));
        assertEquals("01", map.get("Aa"));
        
        assertNull(map.replace(null, "null"));
        assertEquals("null", map.get(null));
        assertEquals("null", map.replace(null, null));
        assertNull(map.get(null));
        
        assertFalse(map.containsKey("Xx"));
        assertNull(map.replace("Xx", "99"));
        assertFalse(map.containsKey("Xx"));
        assertNull(map.get("Xx"));
        
        map.remove(null);
        
        new LinkedHashMap<>(map).forEach((k, v) -> {
            assertFalse(map.containsKey(v));
            assertNull(map.replace(v, k));
            assertFalse(map.containsKey(v));
            
            assertEquals(v, map.replace(k, "0" + (Integer.parseInt(v) + 1)));
        });
        
        assertEquals("02", map.get("Aa"));
        assertEquals("03", map.get("Bb"));
        assertEquals("04", map.get("Cc"));
        assertEquals("05", map.get("Dd"));
        assertEquals("06", map.get("Ee"));
    }
    
    @Test
    void testReplaceIfExpectedValue() {
        testReplaceIfExpectedValue(new LinkedHashMap<>());
        testReplaceIfExpectedValue(factory.newMap());
    }
    
    private void testReplaceIfExpectedValue(Map<String, String> map) {
        assertTrue(map.isEmpty());
        
        assertFalse(map.replace("Aa", "01", "Xx"));
        assertFalse(map.replace(null, null, "Xx"));
        
        map.putAll(Map.of("Aa", "01",
                          "Bb", "02",
                          "Cc", "03",
                          "Dd", "04",
                          "Ee", "05"));
        map.put(null, null);
        assertEquals(6, map.size());
        
        assertTrue(map.replace("Aa", "01", "Xx"));
        assertEquals("Xx", map.get("Aa"));
        
        assertFalse(map.replace("Aa", "01", "01"));
        assertEquals("Xx", map.get("Aa"));
        
        assertTrue(map.replace("Aa", "Xx", null));
        assertTrue(map.containsKey("Aa"), "replace with null shouldn't remove");
        assertNull(map.get("Aa"));
        
        assertFalse(map.replace(null, "foo", "bar"));
        assertNull(map.get(null));
        assertTrue(map.replace(null, null, "foo"));
        assertEquals("foo", map.get(null));
        
        map.remove(null);
        
        new LinkedHashMap<>(map).forEach((k, v) -> {
            assertFalse(map.replace(k, k, "value"));
            assertEquals(v, map.get(k));
            
            assertTrue(map.replace(k, v, k + k));
        });
        
        map.forEach((k, v) -> assertEquals(k + k, v));
    }
    
    @Test
    void testContainsKey() {
        ArrayTempHashMap<String, Object> map = factory.newMap();
        
        assertFalse(map.containsKey(null));
        assertFalse(map.containsKey("foo"));
        // noinspection SuspiciousMethodCalls
        assertFalse(map.containsKey(new Object()));
        
        Object v = new Object();
        
        for (String k : List.of("X", "Y", "Z")) {
            assertFalse(map.containsKey(k));
            assertNull(map.put(k, v));
            
            assertTrue(map.containsKey(k));
        }
        
        assertFalse(map.containsKey(null));
        assertNull(map.put(null, null));
        assertTrue(map.containsKey(null));
        
        assertNull(map.remove(null));
        assertFalse(map.containsKey(null));
        
        for (String k : List.of("X", "Y", "Z")) {
            assertTrue(map.containsKey(k));
            assertEquals(v, map.remove(k));
            
            assertFalse(map.containsKey(k));
        }
        
        assertTrue(map.isEmpty());
    }
    
    @Test
    void testContainsValue() {
        ArrayTempHashMap<Integer, String> map = factory.newMap();
        
        assertFalse(map.containsValue(null));
        assertFalse(map.containsValue("foo"));
        // noinspection SuspiciousMethodCalls
        assertFalse(map.containsValue(new Object()));
        
        
        for (int i = 0; i < 4; ++i) {
            String v = Character.toString((char) ('A' + i));
            
            assertFalse(map.containsValue(v));
            assertNull(map.put(i, v));
            assertTrue(map.containsValue(v));
        }
        
        assertFalse(map.containsValue(null));
        assertNull(map.put(null, null));
        assertTrue(map.containsValue(null));
        
        for (Entry<Integer, String> e : new ArrayList<>(map.entrySet())) {
            String v = e.getValue();
            
            assertTrue(map.containsValue(v));
            assertEquals(v, map.remove(e.getKey()));
            assertFalse(map.containsValue(v));
        }
        
        assertTrue(map.isEmpty());
    }
    
    @Test
    void testContainsEntry() {
        ArrayTempHashMap<String, Integer> map = factory.newMap();
        
        assertFalse(map.containsEntry(null, null));
        assertFalse(map.containsEntry("foo", "bar"));
        assertFalse(map.containsEntry(new Object(), new Object()));
        
        for (var e : List.of(entry("X", 1),
                             entry("Y", 2),
                             entry("Z", 3))) {
            assertFalse(map.containsEntry(e.getKey(), e.getValue()));
            assertNull(map.put(e.getKey(), e.getValue()));
            assertTrue(map.containsEntry(e.getKey(), e.getValue()));
        }
        
        {
            assertTrue(map.containsEntry("X", 1));
            assertFalse(map.containsEntry("X", 0));
            assertFalse(map.containsEntry("A", 1));
        }
        
        {
            assertFalse(map.containsEntry(null, null));
            assertNull(map.put(null, null));
            assertTrue(map.containsEntry(null, null));
            
            assertFalse(map.containsEntry("null", null));
            assertFalse(map.containsEntry(null, 0));
            
            assertNull(map.remove(null));
            assertFalse(map.containsEntry(null, null));
        }
        
        for (var e : List.of(entry("X", 1),
                             entry("Y", 2),
                             entry("Z", 3))) {
            assertTrue(map.containsEntry(e.getKey(), e.getValue()));
            assertEquals(e.getValue(), map.remove(e.getKey()));
            assertFalse(map.containsEntry(e.getKey(), e.getValue()));
        }
        
        assertTrue(map.isEmpty());
    }
    
    @Test
    void testRehash() {
        testRehash(false);
        testRehash(true);
    }
    
    private void testRehash(boolean arrayResize) {
        int entryCount = 32;
        int initialCapacity = arrayResize ? entryCount : 2048;
        
        factory = new ArrayTempFactory(1 + initialCapacity);
        
        ArrayTempHashMap<Integer, String> map = factory.newMap(entryCount, 1.0f);
        
        // computes an entry at a specific index in to the backing array
        final class EntryFunction {
            private Integer key(int index) {
                return (index << 16) ^ index;
            }
            private String value(int index) {
                return "value" + index;
            }
        }
        EntryFunction f = new EntryFunction();
        
        for (int i = 0; i < entryCount; ++i) {
            Integer key = f.key(i);
            String val = f.value(i);
            
            assertFalse(map.containsKey(key));
            assertNull(map.put(key, val));
            
            assertTrue(map.containsKey(key));
            assertEquals(val, map.get(key));
            
            assertEquals(key, factory.getDirect(map, 2*i));
            assertEquals(val, factory.getDirect(map, 2*i + 1));
            
            assertEquals(2*entryCount, map.capacity());
        }
        
        Runnable assertBackingArrayIsAsExpected = () -> {
            int tableSize = map.capacity() / 2;
            
            for (int i = 0; i < tableSize; ++i) {
                Integer key = (i < map.size()) ? f.key(i) : null;
                String val = (i < map.size()) ? f.value(i) : null;
                
                assertEquals(key, factory.getDirect(map, 2*i));
                assertEquals(val, factory.getDirect(map, 2*i + 1));
            }
        };
        
        assertEquals(entryCount, map.size());
        assertBackingArrayIsAsExpected.run();
        
        Map<Integer, String> before = new LinkedHashMap<>(map);
        {
            Integer key = f.key(entryCount);
            String val = f.value(entryCount);
            
            Object[] array = factory.array;
            
            assertNull(map.put(key, val));
            
            assertNotEquals(2*entryCount, map.capacity());
            assertEquals(arrayResize, array != factory.array);
            
            assertEquals(entryCount + 1, map.size());
            assertBackingArrayIsAsExpected.run();
            
            before.put(key, val);
            assertEquals(before, map);
        }
    }
    
    @Test
    void testRehashWithRemovals() {
        testRehashWithRemovals(false, 0xFAB_CAFE_DEAL);
        testRehashWithRemovals(true,  0xFAB_CAFE_DEAL);
        long now = System.currentTimeMillis();
        try {
            testRehashWithRemovals(false, now);
            testRehashWithRemovals(true,  now);
        } catch (Throwable x) {
            System.out.println("testRehashWithRemovals() seed was " + now);
            throw x;
        }
    }
    
    private void testRehashWithRemovals(boolean arrayResize, long seed) {
        int entryCount = 256;
        int mapCapacity = 4 * entryCount;
        int tableSize = mapCapacity / 2;
        int initialCapacity = arrayResize ? 2*entryCount : 8192;
        
        factory = new ArrayTempFactory(initialCapacity);
        
        ArrayTempHashMap<String, String> map = factory.newMap(entryCount, 0.5f);
        assertEquals(mapCapacity, map.capacity());
        
        Random rand = new Random(seed);
        List<Entry<String, String>> entries = new ArrayList<>();
        
        // fill with random entries
        
        for (int i = 0; i < entryCount; ++i) {
            String key = rand.nextInt() + "." + i;
            String val = "value" + i;
            assertFalse(map.containsKey(key));
            
            assertNull(map.put(key, val));
            assertEquals(mapCapacity, map.capacity());
            
            entries.add(entry(key, val));
        }
        
        assertEquals(entryCount, map.size());
        assertEquals(map.capacity(), 4*map.size());
        
        // find all the indexes where there is no entry in the factory array
        
        List<Integer> vacancies = new ArrayList<>();
        
        for (int i = 0; i < tableSize; ++i) {
            if (factory.getDirect(map, 2*i) == null)
                vacancies.add(i);
        }
        
        assertEquals(entryCount, vacancies.size());
        map.clear();
        
        // insert entries at all of the vacant indexes
        
        for (int vacancy : vacancies) {
            String key = Character.toString((char) vacancy);
            String val = "value" + vacancy;
            
            assertEquals(key.hashCode(), vacancy);
            
            assertFalse(map.containsKey(key));
            assertNull(map.put(key, val));
            
            assertEquals(key, factory.getDirect(map, 2*vacancy));
            assertEquals(val, factory.getDirect(map, 2*vacancy + 1));
        }
        
        assertEquals(entryCount, map.size());
        assertEquals(mapCapacity, map.capacity());
        
        // insert a deletion marker at all of the vacant indexes
        
        Iterator<String> keyIterator = map.keySet().iterator();
        while (keyIterator.hasNext()) {
            String k = keyIterator.next();
            
            keyIterator.remove();
            assertEquals(Del.INSTANCE, factory.getDirect(map, 2*k.hashCode()));
        }
        
        assertTrue(map.isEmpty());
        assertEquals(mapCapacity, map.capacity());
        
        // re-fill with the random entries
        
        for (var e : entries) {
            assertFalse(map.containsKey(e.getKey()));
            assertNull(map.put(e.getKey(), e.getValue()));
            assertEquals(e.getValue(), map.get(e.getKey()));
        }
        
        assertEquals(entryCount, map.size());
        assertEquals(mapCapacity, map.capacity());
        
        // make sure we didn't lose any deletion marks
        
        assertEquals( entryCount, IntStream.range(0, tableSize)
                                           .mapToObj(i -> factory.getDirect(map, 2*i))
                                           .filter(Del.INSTANCE::equals)
                                           .count() );
        
        // finally, rehash the map
        
        Object[] array = factory.array;
        Map<String, String> before = new LinkedHashMap<>(map);
        
        assertNull(map.put("key", "value"));
        assertEquals("value", map.get("key"));
        
        assertEquals(arrayResize, array != factory.array);
        assertNotEquals(mapCapacity, map.capacity());
        
        // make sure we skipped all of the deletion marks
        
        assertTrue(Arrays.stream(factory.array, map.offset, map.offset + map.capacity)
                         .noneMatch(Del.INSTANCE::equals));
        
        // and make sure all of the entries are still accessible
        
        before.put("key", "value");
        assertEquals(before, map);
    }
    
    @Test
    void testReplaceAll() {
        ArrayTempHashMap<String, Integer> map = factory.newMap();
        
        assertThrows(NullPointerException.class, () -> map.replaceAll(null));
        
        int count = 10;
        
        for (int i = 0; i < count; ++i) {
            map.put("key" + i, i);
        }
        
        map.put(null, -1);
        
        int size = count + 1;
        assertEquals(size, map.size);
        
        int[] r = new int[] {0};
        
        map.replaceAll((k, v) -> {
            assertEquals((v == -1) ? null : ("key" + v), k);
            ++r[0];
            return (k == null) ? null : (v + 1);
        });
        
        assertEquals(size, r[0]);
        assertEquals(size, map.size);
        
        for (int i = 0; i < count; ++i) {
            assertEquals(i + 1, (int) map.get("key" + i));
        }
        
        assertTrue(map.containsKey(null));
        assertNull(map.get(null));
    
        // noinspection ThrowableNotThrown
        RuntimeException aRuntimeException = new RuntimeException("foo");
        try {
            map.replaceAll((k, v) -> { throw aRuntimeException; });
            fail("replaceAll must relay exceptions to the caller");
        } catch (RuntimeException x) {
            assertSame(aRuntimeException, x);
        }
    }
    
    @Test
    void testForEach() {
        ArrayTempHashMap<Integer, String> map = factory.newMap();
        
        assertThrows(NullPointerException.class, () -> map.forEach(null));
        
        String[] values = {"A", "B", "C", "D", "E", "F"};
        for (int i = 0; i < values.length; ++i)
            map.put((i == 0) ? null : i, values[i]);
        
        int[] i = new int[] {0};
        
        map.forEach((Integer k, String v) -> {
            assertEquals((i[0] == 0) ? null : i[0], k);
            assertEquals(values[i[0]++], v);
        });
        
        assertEquals(i[0], map.size);
    }
    
    @Test
    void testKeySet() {
        ArrayTempHashMap<Integer, String> map = factory.newMap();
        ArrayTempHashMapKeySet<Integer> keySet = (ArrayTempHashMapKeySet<Integer>) map.keySet();
        
        assertNotNull(keySet);
        assertSame(map, keySet.map);
        
        assertSame(map.keySet(), map.keySet());
    }
    
    @Test
    void testValues() {
        ArrayTempHashMap<Integer, String> map = factory.newMap();
        ArrayTempHashMapValues<String> values = (ArrayTempHashMapValues<String>) map.values();
        
        assertNotNull(values);
        assertSame(map, values.map);
        
        assertSame(map.values(), map.values());
    }
    
    @Test
    void testEntrySet() {
        ArrayTempHashMap<Integer, String> map = factory.newMap();
        ArrayTempHashMapEntrySet<Integer, String> entrySet = (ArrayTempHashMapEntrySet<Integer, String>) map.entrySet();
        
        assertNotNull(entrySet);
        assertSame(map, entrySet.map);
        
        assertSame(map.entrySet(), map.entrySet());
    }
    
    @Test
    void testHashCode() {
        ArrayTempHashMap<String, String> actual = factory.newMap();
        
        assertEquals(emptyMap().hashCode(), actual.hashCode());
        
        Map<String, String> expect =
            Map.of("ABC", "123",
                   "DEF", "456",
                   "GHI", "789",
                   "JKL", "101",
                   "MNO", "112",
                   "PQR", "131",
                   "STU", "415",
                   "VWX", "161",
                   "Z",   "718");
        actual.putAll(expect);
        
        assertEquals(expect.hashCode(), actual.hashCode());
        
        expect = new LinkedHashMap<>(expect);
        expect.put(null, null);
        actual.put(null, null);
        
        assertEquals(expect.hashCode(), actual.hashCode());
    }
    
    @Test
    @SuppressWarnings("SimplifiableJUnitAssertion")
    void testEquals() {
        ArrayTempHashMap<String, String> actual = factory.newMap();
    
        assertTrue(actual.equals(emptyMap()));
        // noinspection EqualsWithItself
        assertTrue(actual.equals(actual));
    
        // noinspection ConstantConditions, ObjectEqualsNull
        assertFalse(actual.equals(null));
        // noinspection EqualsBetweenInconvertibleTypes
        assertFalse(actual.equals(new ArrayList<>()));
        
        Map<String, String> expect =
            Map.of("ABC", "123",
                   "DEF", "456",
                   "GHI", "789",
                   "JKL", "101",
                   "MNO", "112",
                   "PQR", "131",
                   "STU", "415",
                   "VWX", "161",
                   "Z",   "718");
        actual.putAll(expect);
        assertTrue(actual.equals(expect));
        
        expect = new LinkedHashMap<>(expect);
        expect.put(null, null);
        assertFalse(actual.equals(expect));
        
        actual.put(null, null);
        assertTrue(actual.equals(expect));
        
        assertEquals("131", expect.remove("PQR"));
        assertFalse(actual.equals(expect));
        
        // testing caught exceptions
        
        Map<Object, Object> any = factory.newMap();
        any.put("ABC", "123");
        any.put(0xABC, 0x123);
        
        Map<String, String> checked = new HashMap<>(Map.of("ABC", "123", "0xABC", "0x123")) {
            @Override
            public boolean containsKey(Object o) {
                // noinspection RedundantCast
                return super.containsKey((String) o);
            }
            @Override
            public String get(Object o) {
                // noinspection RedundantCast
                return super.get((String) o);
            }
        };
        
        ArrayTempHashMap.CAUGHT = null;
        
        assertFalse(any.equals(checked));
        
        if (ArrayTempHashMap.TESTING) {
            // noinspection ConstantConditions
            assertNotNull(ArrayTempHashMap.CAUGHT);
            assertEquals(ClassCastException.class, ArrayTempHashMap.CAUGHT.getClass());
        }
    }
    
    @Test
    void testToString() {
        ArrayTempHashMap<Integer, String> actual = factory.newMap(64);
        assertEquals("{}", actual.toString());
        
        actual.put(1, "X");
        assertEquals("{1=X}", actual.toString());
        
        actual.put(2, "Y");
        assertEquals("{1=X, 2=Y}", actual.toString());
        
        actual.put(3, "Z");
        assertEquals("{1=X, 2=Y, 3=Z}", actual.toString());
        
        actual.put(null, null);
        assertEquals("{null=null, 1=X, 2=Y, 3=Z}", actual.toString());
    }
}