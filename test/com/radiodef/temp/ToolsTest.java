/* MIT License
 *
 * Copyright (c) 2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.radiodef.temp;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

import java.util.*;
import java.math.*;

class ToolsTest {
    @Test
    void testRequireNonNegative() {
        assertThrows(IllegalArgumentException.class, () -> Tools.requireNonNegative(-1, "test"));
        assertEquals(0, Tools.requireNonNegative(0, "test"));
        assertEquals(1, Tools.requireNonNegative(1, "test"));
    }
    
    @Test
    void testRequireRange() {
        int min = 1;
        int max = 10; // note: exclusive
        assertThrows(IllegalArgumentException.class, () -> Tools.requireRange(min-1, min, max, "test"));
        assertThrows(IllegalArgumentException.class, () -> Tools.requireRange(max,   min, max, "test"));
        assertThrows(IllegalArgumentException.class, () -> Tools.requireRange(max+1, min, max, "test"));
        assertEquals(min,   Tools.requireRange(min,   min, max, "test"));
        assertEquals(max-1, Tools.requireRange(max-1, min, max, "test"));
    }
    
    @Test
    void testRequireRangeClosed() {
        int min = 1;
        int max = 10; // note: inclusive
        assertThrows(IllegalArgumentException.class, () -> Tools.requireRangeClosed(min-1, min, max, "test"));
        assertThrows(IllegalArgumentException.class, () -> Tools.requireRangeClosed(max+1, min, max, "test"));
        assertEquals(min,   Tools.requireRangeClosed(min,   min, max, "test"));
        assertEquals(max-1, Tools.requireRangeClosed(max-1, min, max, "test"));
        assertEquals(max,   Tools.requireRangeClosed(max,   min, max, "test"));
    }
    
    @Test
    void testNewArray() {
        Object[] expect = new Object[6];
        expect[0] = "A";
        expect[1] = "B";
        expect[2] = "C";
        
        assertArrayEquals(expect, Tools.newArray(6, "A", "B", "C"));
        
        assertEquals(2, Tools.newArray(2).length);
        
        // noinspection ResultOfMethodCallIgnored
        assertThrows(NullPointerException.class, () -> Tools.newArray(0, (Object[]) null));
        // noinspection ResultOfMethodCallIgnored
        assertThrows(NegativeArraySizeException.class, () -> Tools.newArray(-1, "x"));
    }
    
    @Test
    void testIsSorted() {
        // noinspection ConstantConditions
        assertThrows(NullPointerException.class, () -> Tools.isSorted(null, Comparator.naturalOrder()));
        assertThrows(NullPointerException.class, () -> Tools.isSorted(Collections.emptyList(), null));
        
        assertTrue(Tools.isSorted(new ArrayList<String>(), Comparator.naturalOrder()));
        assertTrue(Tools.isSorted(List.of(1), Comparator.naturalOrder()));
        
        assertTrue(Tools.isSorted(List.of(1, 2), Comparator.naturalOrder()));
        assertFalse(Tools.isSorted(List.of(2, 1), Comparator.naturalOrder()));
        
        assertTrue(Tools.isSorted(List.of("a", "A", "B", "g", "X", "y", "Y"), String.CASE_INSENSITIVE_ORDER));
        
        assertFalse(Tools.isSorted(List.of("a", "A", "B", "g", "Y", "y", "x"), String.CASE_INSENSITIVE_ORDER));
        
        assertFalse(Tools.isSorted(List.of("X", "A", "p", "-", "$", "a"), String.CASE_INSENSITIVE_ORDER));
    }
    
    @Test
    @SuppressWarnings({"UnusedAssignment", "ConstantConditions"})
    void testCeilingLog2() {
        if (false) {
            double ln2 = Math.log(2);
            // Note: this block is disabled because it takes a long time, but the tests pass.
            for (int i = 1; i > 0; ++i) {
                int expect;
                // 536870912 is 2^29, at which point the assertion
                // fails because the double arithmetic actually gives
                // the wrong result.
                if (i == 536870912) {
                    assertEquals(536870912, (int) Math.pow(2, 29));
                    expect = 29;
                } else {
                    expect = (int) Math.ceil(Math.log(i) / ln2);
                }
                
                int actual = Tools.ceilLog2(i);
                
                assertEquals(expect, actual, "i=" + i);
            }
        }
        
        assertThrows(ArithmeticException.class, () -> Tools.ceilLog2(-1));
        assertThrows(ArithmeticException.class, () -> Tools.ceilLog2(0));
    }
    
    @Test
    void testNext2() {
        assertEquals(1, Tools.next2(Integer.MIN_VALUE));
        assertEquals(1, Tools.next2(-1));
        
        assertEquals(1, Tools.next2(0));
        
        assertEquals(1, Tools.next2(1));
        assertEquals(2, Tools.next2(2));
        assertEquals(4, Tools.next2(3));
        
        int two30 = (int) Math.pow(2, 30);
    
        // noinspection ConstantConditionalExpression
        long max = false ? Integer.MAX_VALUE : Short.MAX_VALUE; // Note: Integer.MAX_VALUE tested and passes
        
        for (long i = 1; i <= max; ++i) {
            int n = (int) i;
            
            int expect = n;
            
            while (Integer.bitCount(expect) != 1) {
                expect += Integer.lowestOneBit(expect);
            }
            
            int actual = Tools.next2(n);
            
            assertTrue((n <= two30) ? (actual >= n) : (actual < 0));
            assertEquals(expect, actual);
        }
        
        assertEquals(two30, Tools.next2(two30));
        
        assertEquals(Integer.MIN_VALUE, Tools.next2(two30 + 1));
        assertEquals(Integer.MIN_VALUE, Tools.next2(Integer.MAX_VALUE));
    }
    
    @Test
    void testIsPowerOf2() {
        assertFalse(Tools.isPowerOf2(Integer.MIN_VALUE));
        assertFalse(Tools.isPowerOf2(-1));
        assertFalse(Tools.isPowerOf2(0));
        
        assertFalse(Tools.isPowerOf2(3));
        assertFalse(Tools.isPowerOf2(6));
        assertFalse(Tools.isPowerOf2(2000));
        
        for (int pow = 1; pow > 0; pow <<= 1) {
            assertTrue(Tools.isPowerOf2(pow));
        }
        
        for (int x = 0; x < 31; ++x) {
            assertTrue(Tools.isPowerOf2((int) Math.pow(2, x)));
        }
    }
    
    @Test
    strictfp void testToCeilFloat() {
        int start = 1 << 24;
        
        assertEquals(start, (int) (float) start);
        assertEquals(start - 1, (int) Math.nextDown((float) start));
        
        int lo = start;
        int mid = start + 1;
        int hi = start + 2;
        
        assertEquals(lo, (int) (float) mid);
        assertEquals(hi, (int) Tools.toCeilFloat(mid));
        
        assertEquals(lo, (int) Tools.toCeilFloat(lo), "lo is even");
        assertEquals(hi, (int) Tools.toCeilFloat(hi), "hi is even");
    }
    
    @Test
    strictfp void testToCeilFloatLong() {
        boolean all = false; // true has been tested and all pass
        // noinspection ConstantConditions
        long min = all ? Integer.MIN_VALUE : (1 << 24);
        // noinspection ConstantConditions
        long max = all ? Integer.MAX_VALUE : (1 << 30);
        
        for (long i = min; i <= max; ++i) {
            int num = (int) i;
            
            float implicit = num;
            
            float expect;
            
            if ((int) implicit < num) {
                // Just try to do this in some way
                // that doesn't use nextUp.
                int bits = 1;
                do {
                    expect = num + bits;
                    bits = (bits << 1) | 1;
                } while ((int) expect < num);
//                expect = Math.nextUp(implicit);
//                assertTrue((int) expect >= num);
            } else {
                expect = implicit;
            }
            
            assertEquals(naiveToCeilFloat(num), expect);
            
            float actual = Tools.toCeilFloat(num);
            
            if (actual != expect) {
                System.out.println("testToCeilFloat()");
                System.out.println("    num      = " + num);
                System.out.println("    binary   = " + toBinary(num));
                System.out.println("    implicit = " + (long) implicit);
                System.out.println("    expect   = " + (long) expect);
                System.out.println("    actual   = " + (long) actual);
                fail("expected " + (long) expect + "; found " + (long) actual);
            }
        }
    }
    
    // Note: The bitwise implementation failed to perform
    //       better than the naive implementation with an
    //       if statement and Math.nextUp.
//    @Test
    strictfp void testToCeilFloatPerformance() {
        int start = 1 << 24;
        int end = 1 << 30;
        long result = 0;
        // warm-up
        for (int i = start; i <= end; ++i) {
            result ^= (long) Tools.toCeilFloat(i);
            result ^= (long) naiveToCeilFloat(i);
            result ^= System.nanoTime();
        }
        // test
        long actualStart = System.nanoTime();
        for (int i = start; i <= end; ++i) {
            result ^= (long) Tools.toCeilFloat(i);
        }
        long actualEnd = System.nanoTime();
        long naiveStart = System.nanoTime();
        for (int i = start; i <= end; ++i) {
            result ^= (long) naiveToCeilFloat(i);
        }
        long naiveEnd = System.nanoTime();
        // results
        System.out.println("testToCeilFloatPerformance()");
        System.out.println("    result = " + Long.toHexString(result));
        int count = (end - start) + 1;
        double actualAverage = (actualEnd - actualStart) / (double) count;
        double naiveAverage = (naiveEnd - naiveStart) / (double) count;
        System.out.println("    actual = " + BigDecimal.valueOf(actualAverage).toPlainString());
        System.out.println("    naive  = " + BigDecimal.valueOf(naiveAverage).toPlainString());
        assertTrue(actualAverage < naiveAverage);
    }
    
    private static strictfp float naiveToCeilFloat(int n) {
        float f = n;
        
        if ((int) f < n) {
            f = Math.nextUp(f);
        }
        
        return f;
    }
    
    @SuppressWarnings("StringConcatenationInLoop")
    private static String toBinary(int n) {
        String str = Integer.toBinaryString(n);
        while (str.length() < Integer.SIZE)
            str = "0" + str;
        String out = str.substring(0, 4);
        for (int i = 4; i < Integer.SIZE; i += 4)
            out += "_" + str.substring(i, i + 4);
        return out;
    }
}