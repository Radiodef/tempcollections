/* MIT License
 *
 * Copyright (c) 2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.radiodef.temp;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

import java.util.*;
import java.util.Map.*;
import static java.util.Map.entry;
import java.util.AbstractMap.*;

class ArrayTempHashMapEntrySetTest {
    private ArrayTempFactory factory;
    private ArrayTempHashMap<String, String> map;
    
    @BeforeEach
    void initNewFactoryAndMap() {
        factory = new ArrayTempFactory(10);
        map = factory.newMap();
    }
    
    @AfterEach
    void clearFactoryAndMap() {
        factory = null;
        map = null;
    }
    
    @Test
    void testConstructor() {
        assertSame(map, new ArrayTempHashMapEntrySet<>(map).map);
    }
    
    @Test
    void testIterator() {
        ArrayTempHashMapEntrySet<String, String> entrySet = new ArrayTempHashMapEntrySet<>(map);
        
        assertNotNull(entrySet.iterator());
        assertSame(map, entrySet.iterator().map);
        assertNotSame(entrySet.iterator(), entrySet.iterator());
    }
    
    @Test
    void testSize() {
        ArrayTempHashMapEntrySet<String, String> entrySet = new ArrayTempHashMapEntrySet<>(map);
        
        for (int i = 0; i < 10; ++i) {
            map.put("key" + i, "value" + i);
            assertEquals(i + 1, map.size);
            assertEquals(map.size, entrySet.size());
        }
    }
    
    @Test
    void testClear() {
        map.putAll(Map.of("A", "1", "B", "2", "C", "3"));
        assertEquals(3, map.size);
        
        ArrayTempHashMapEntrySet<String, String> entrySet = new ArrayTempHashMapEntrySet<>(map);
        assertEquals(3, entrySet.size());
        
        entrySet.clear();
        assertTrue(map.isEmpty());
        assertTrue(entrySet.isEmpty());
    }
    
    @Test
    void testAddAndAddAll() {
        ArrayTempHashMapEntrySet<String, String> entrySet = new ArrayTempHashMapEntrySet<>(map);
        
        Entry<String, String> e = new SimpleImmutableEntry<>("A", "1");
        
        assertThrows(UnsupportedOperationException.class, () -> entrySet.add(e));
        assertThrows(UnsupportedOperationException.class, () -> entrySet.addAll(List.of(e)));
    }
    
    @Test
    void testRemove() {
        map.putAll(Map.of("A", "1", "B", "2", "C", "3"));
        
        ArrayTempHashMapEntrySet<String, String> entrySet = new ArrayTempHashMapEntrySet<>(map);
        
        assertFalse(entrySet.remove(null));
        // noinspection SuspiciousMethodCalls
        assertFalse(entrySet.remove(new Object()));
        assertEquals(3, map.size);
        
        Entry<String, String> e;
        
        e = new SimpleImmutableEntry<>("X", "1");
        assertFalse(entrySet.remove(e));
        e = new SimpleImmutableEntry<>("A", "2");
        assertFalse(entrySet.remove(e));
        
        assertEquals(3, map.size);
        
        e = new SimpleImmutableEntry<>("A", "1");
        assertTrue(entrySet.remove(e));
        assertEquals(2, map.size);
        assertFalse(map.containsKey("A"));
        
        e = new SimpleImmutableEntry<>("B", "2");
        assertTrue(entrySet.remove(e));
        assertEquals(1, map.size);
        assertFalse(map.containsKey("B"));
        
        e = new SimpleImmutableEntry<>("C", "3");
        assertTrue(entrySet.remove(e));
        assertEquals(0, map.size);
        assertFalse(map.containsKey("C"));
        
        assertFalse(entrySet.remove(e));
    }
    
    // Note: these are implemented by AbstractSet
    @Test
    void testRemoveAndRetainAll() {
        map.putAll(Map.of("A", "1",
                          "B", "2",
                          "C", "3",
                          "D", "4",
                          "E", "5",
                          "F", "6"));
        ArrayTempHashMapEntrySet<String, String> entrySet = new ArrayTempHashMapEntrySet<>(map);
        
        List<Entry<String, String>> toRemove =
            List.of(entry("B", "2"),
                    entry("C", "3"),
                    entry("D", "0"));
        
        assertTrue(entrySet.removeAll(toRemove));
        
        assertEquals(4, map.size);
        
        assertFalse(map.containsKey("B"));
        assertFalse(map.containsKey("C"));
        assertEquals("4", map.get("D"));
        
        List<Entry<String, String>> toRetain =
            List.of(entry("A", "1"),
                    entry("F", "6"),
                    entry("E", "0"));
        
        assertTrue(entrySet.retainAll(toRetain));
        
        assertEquals(2, map.size);
        
        assertFalse(map.containsKey("D"));
        assertFalse(map.containsKey("E"));
        assertEquals("1", map.get("A"));
        assertEquals("6", map.get("F"));
    
        // noinspection SuspiciousMethodCalls
        assertFalse(entrySet.removeAll(Collections.emptySet()));
        // noinspection CollectionAddedToSelf
        assertFalse(entrySet.retainAll(entrySet));
        
        assertTrue(entrySet.retainAll(Collections.emptySet()));
        assertTrue(map.isEmpty());
    }
    
    @Test
    void testContains() {
        map.putAll(Map.of("A", "1",
                          "B", "2",
                          "C", "3",
                          "D", "4",
                          "E", "5",
                          "F", "6"));
        ArrayTempHashMapEntrySet<String, String> entrySet = new ArrayTempHashMapEntrySet<>(map);
        
        assertFalse(entrySet.contains(null));
        // noinspection SuspiciousMethodCalls
        assertFalse(entrySet.contains(new Object()));
        
        assertFalse(entrySet.contains(entry("A", "XXX")));
        assertFalse(entrySet.contains(entry("XXX", "1")));
        assertFalse(entrySet.contains(entry(0, "")));
        assertFalse(entrySet.contains(entry("", 0)));
        
        map.forEach((k, v) -> assertTrue(entrySet.contains(entry(k, v))));
        
        map.put(null, null);
        // Map.entry(..) requires non null
        // noinspection SuspiciousMethodCalls
        assertFalse(entrySet.contains(new SimpleImmutableEntry<>(null, "null")));
        // noinspection SuspiciousMethodCalls
        assertFalse(entrySet.contains(new SimpleImmutableEntry<>("null", null)));
        // noinspection SuspiciousMethodCalls
        assertTrue(entrySet.contains(new SimpleImmutableEntry<>(null, null)));
    }
    
    @Test
    void testHashCode() {
        Map<String, String> expect =
            Map.of("A", "1",
                   "B", "2",
                   "C", "3");
        map.putAll(expect);
        
        assertEquals(expect.entrySet().hashCode(), new ArrayTempHashMapEntrySet<>(map).hashCode());
    }
    
    @Test
    @SuppressWarnings("SimplifiableJUnitAssertion")
    void testEquals() {
        Map<String, String> expect =
            Map.of("A", "1",
                   "B", "2",
                   "C", "3");
        map.putAll(expect);
        ArrayTempHashMapEntrySet<String, String> entrySet = new ArrayTempHashMapEntrySet<>(map);
        
        assertTrue(entrySet.equals(expect.entrySet()));
        assertTrue(entrySet.equals(new ArrayTempHashMapEntrySet<>(factory.newMap(expect))));
        
        assertFalse(entrySet.equals(Collections.emptySet()));
        
        expect = new HashMap<>(expect);
        expect.put("B", "601");
        assertFalse(entrySet.equals(expect.entrySet()));
    
        // noinspection ObjectEqualsNull, ConstantConditions
        assertFalse(entrySet.equals(null));
        assertFalse(entrySet.equals(new Object()));
        // noinspection EqualsWithItself
        assertTrue(entrySet.equals(entrySet));
    }
}