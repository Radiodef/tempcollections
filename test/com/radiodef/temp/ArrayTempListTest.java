/* MIT License
 *
 * Copyright (c) 2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.radiodef.temp;

import static com.radiodef.temp.Tools.*;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

import java.util.*;
import static java.util.Collections.*;
import static java.util.Arrays.*;
import static java.util.Comparator.*;
import static java.util.Comparator.reverseOrder;

class ArrayTempListTest {
    
    private ArrayTempFactory factory;
    
    @BeforeEach
    void initNewFactory() {
        factory = new ArrayTempFactory(10);
    }
    
    @AfterEach
    void clearFactory() {
        factory = null;
    }
    
    @Test
    void testConstructor() {
        ArrayTempList<?> list = factory.newList(10);
        
        assertEquals(1, list.offset);
        assertEquals(10, list.capacity);
        assertTrue(list.isEmpty());
    }
    
    @Test
    void testZeroCapacity() {
        factory = new ArrayTempFactory(0);
        ArrayTempList<Long> list = factory.newList(0);
        
        list.add(1L);
        assertArrayEquals(newArray(factory.array.length, Mark.INSTANCE, 1L), factory.array);
    }
    
    @Test
    void testCapacityIncrease() {
        ArrayTempList<Integer> list = factory.newList(0);
        assertEquals(0, list.capacity);
        
        list.add(6);
        assertEquals(List.of(6), list);
        
        int cap = list.capacity;
        assertNotEquals(0, cap);
        
        for (int i = 0; i < cap; ++i) {
            list.add(6);
        }
        
        assertNotEquals(cap, list.capacity);
        
        ArrayTempList<String> top = factory.newList();
        top.add("s");
        
        assertThrows(IllegalStateException.class, () -> {
            // noinspection InfiniteLoopStatement
            for (;;) {
                list.add(601);
            }
        });
        
        assertEquals("s", factory.array[top.offset]);
        assertEquals(Mark.INSTANCE, factory.array[top.offset - 1]);
        assertEquals(601, factory.array[top.offset - 2]);
    }
    
    @Test
    void testLargeCapacityIncrease() {
        ArrayTempList<Short> list = factory.newList();
        
        int capacity = list.capacity;
        int resizeCount = 0;
        
        for (int i = 0; i < Short.MAX_VALUE; ++i) {
            list.add((short) i);
            
            if (list.capacity != capacity) {
                capacity = list.capacity;
                ++resizeCount;
            }
        }
        
        assertNotEquals(0, resizeCount);
        
        System.out.println("resizeCount=" + resizeCount);
        System.out.println("list.capacity=" + list.capacity);
        
        for (int i = 0; i < Short.MAX_VALUE; ++i) {
            assertEquals(i, (short) list.get(i));
        }
    }
    
    @Test
    void testTrimCapacity() {
        ArrayTempList<String> list = factory.newList(10);
        assertEquals(10, list.capacity);
        
        list.trimCapacity();
        assertEquals(0, list.capacity);
        
        list.add("A");
        list.add("B");
        list.add("C");
        list.add("D");
        assertNotEquals(list.size, list.capacity);
        
        list.trimCapacity();
        assertEquals(list.size, list.capacity);
        
        try (@SuppressWarnings("unused") TempObject top = factory.newList()) {
            assertThrows(IllegalStateException.class, list::trimCapacity);
        }
        
        assertEquals(List.of("A", "B", "C", "D"), list);
    }
    
    @Test
    void testSizeAndIsEmpty() {
        ArrayTempList<String> list = factory.newList();
        
        assertEquals(0, list.size());
        assertTrue(list.isEmpty());
        
        List<String> source = List.of("ABC", "DEF", "GHI");
        
        for (int i = 0; i < source.size(); ++i) {
            list.add(source.get(i));
            assertEquals(i + 1, list.size());
            assertFalse(list.isEmpty());
        }
    }
    
    @Test
    void testClear() {
        ArrayTempList<String> list = factory.newList();
        addAll(list, "ABC", "DEF", "GHI");
        
        list.clear();
        assertTrue(list.isEmpty());
        
        assertNull(factory.getDirect(list, 0));
        assertNull(factory.getDirect(list, 1));
        assertNull(factory.getDirect(list, 2));
    }
    
    @Test
    void testPlainAdd() {
        ArrayTempList<String> list = factory.newList(6);
        
        List<String> toAdd = asList("ABC", "DEF", "GHI", "JKL", null);
        
        for (int i = 0; i < toAdd.size(); ++i) {
            assertTrue(list.canAdd(1));
            assertTrue(list.add(toAdd.get(i)));
            
            assertEquals(toAdd.get(i), factory.getDirect(list, i));
            assertEquals(toAdd.subList(0, i + 1), list, list::toString);
        }
        
        // should throw if we aren't the top
        
        try (ArrayTempList<Double> top = factory.newList()) {
            assertEquals(list.size() + 1, list.capacity());
            
            assertTrue(list.canAdd(1));
            assertTrue(list.add("X")); // capacity not yet full
            
            assertFalse(list.canAdd(1));
            assertThrows(IllegalStateException.class, () -> list.add("Y")); // capacity full
        }
        
        assertTrue(list.canAdd(1));
        
        assertTrue(list.add("Y"));
        assertEquals(asList("ABC", "DEF", "GHI", "JKL", null, "X", "Y"), list);
    }
    
    @Test
    void testAddAtIndex() {
        ArrayTempList<String> list = factory.newList(10);
        addAll(list, "X", "Y", "Z");
        
        List<String> expect = new ArrayList<>(list);
        List<String> toAdd = asList("A", "B", "C", null);
        
        for (String e : toAdd) {
            list.add(1, e);
            expect.add(1, e);
            
            assertEquals(e, list.get(1));
            assertEquals(expect.size(), list.size());
            assertEquals(expect, list);
        }
        
        list.add(list.size(), "PQR");
        assertEquals("PQR", list.get(list.size() - 1));
        
        list.add(list.size() - 1, "STU");
        assertEquals("STU", list.get(list.size() - 2));
        
        assertEquals(asList("X", null, "C", "B", "A", "Y", "Z", "STU", "PQR"), list);
        
        // should throw if we aren't the top
        
        try (ArrayTempList<Double> top = factory.newList()) {
            assertEquals(list.size() + 1, list.capacity());
            
            list.add(2, "3"); // capacity not yet full
            assertThrows(IllegalStateException.class, () -> list.add(2, "2")); // capacity full
        }
        
        list.add(2, "2");
        assertEquals(asList("X", null, "2", "3", "C", "B", "A", "Y", "Z", "STU", "PQR"), list);
    }
    
    @Test
    void testPlainAddAll() {
        ArrayTempList<String> list = factory.newList(5);
        
        list.add("ABC");
        
        assertFalse(list.addAll(Collections.emptyList()));
        // noinspection ConstantConditions
        assertThrows(NullPointerException.class, () -> list.addAll(null));
        
        Set<String> toAdd = new LinkedHashSet<>(List.of("1", "2", "3"));
        
        assertTrue(list.addAll(toAdd));
        assertEquals(List.of("ABC", "1", "2", "3"), list);
        
        // should throw if we aren't the top
        
        try (ArrayTempList<Double> top = factory.newList()) {
            assertEquals(list.size() + 1, list.capacity());
            
            assertTrue(list.addAll(List.of("X"))); // capacity not yet full
            assertThrows(IllegalStateException.class, () -> list.addAll(List.of("Y"))); // capacity full
        }
        
        assertTrue(list.addAll(List.of("Y")));
        assertEquals(List.of("ABC", "1", "2", "3", "X", "Y"), list);
        
        // bad Iterator test
        
        list.clear();
        addAll(list, "A", "B", "C");
        List<String> badList = new ListWithBadIterator<>(2);
        addAll(badList, "X", "Y", "Z");
        
        assertThrows(ListWithBadIterator.BadIteratorException.class, () -> list.addAll(badList));
        assertEquals(List.of("A", "B", "C", "X", "Y"), list);
        assertEquals(5, list.size());
        
        for (int i = list.offset + list.size(); i < factory.array.length; ++i) {
            assertNull(factory.array[i]);
        }
    }
    
    @Test
    void testAddAllAtIndex() {
        ArrayTempList<String> list = factory.newList(8);
        
        assertFalse(list.addAll(0, Collections.emptyList()));
        assertThrows(IndexOutOfBoundsException.class, () -> list.addAll(1, Collections.emptyList()));
        
        assertTrue(list.addAll(0, List.of("A", "B", "C")));
        assertEquals(List.of("A", "B", "C"), list);
        
        assertFalse(list.addAll(0, Collections.emptyList()));
        assertThrows(IndexOutOfBoundsException.class, () -> list.addAll(-1, Collections.emptyList()));
        assertThrows(IndexOutOfBoundsException.class, () -> list.addAll(list.size() + 1, Collections.emptyList()));
        // noinspection ConstantConditions
        assertThrows(NullPointerException.class, () -> list.addAll(0, null));
        
        Set<String> toAdd = new LinkedHashSet<>(List.of("X", "Y", "Z"));
        
        assertTrue(list.addAll(1, toAdd));
        assertEquals(List.of("A", "X", "Y", "Z", "B", "C"), list);
        assertTrue(list.addAll(list.size(), List.of("")));
        assertEquals(List.of("A", "X", "Y", "Z", "B", "C", ""), list);
        
        // should throw if we aren't the top
        
        try (ArrayTempList<Double> top = factory.newList()) {
            assertEquals(list.size() + 1, list.capacity());
            
            assertTrue(list.addAll(0, List.of("3"))); // capacity not yet full
            assertThrows(IllegalStateException.class, () -> list.addAll(0, List.of("2"))); // capacity full
        }
        
        assertTrue(list.addAll(0, List.of("2")));
        assertEquals(List.of("2", "3", "A", "X", "Y", "Z", "B", "C", ""), list);
        
        // bad Iterator test
        
        list.clear();
        addAll(list, "A", "B", "C");
        List<String> badList = new ListWithBadIterator<>(2);
        addAll(badList, "X", "Y", "Z");
        
        assertThrows(ListWithBadIterator.BadIteratorException.class, () -> list.addAll(1, badList));
        assertEquals(List.of("A", "X", "Y", "B", "C"), list);
        assertEquals(5, list.size());
        
        for (int i = list.offset + list.size(); i < factory.array.length; ++i) {
            assertNull(factory.array[i]);
        }
    }
    
    @Test
    void testRemoveAtIndex() {
        ArrayTempList<String> list = factory.newList(10);
        
        assertThrows(IndexOutOfBoundsException.class, () -> list.remove(0));
        
        addAll(list, "A", "B", "C");
        
        assertThrows(IndexOutOfBoundsException.class, () -> list.remove(-1));
        assertThrows(IndexOutOfBoundsException.class, () -> list.remove(list.size()));
        
        assertEquals("B", list.remove(1));
        assertEquals(2, list.size());
        assertEquals(List.of("A", "C"), list);
        assertArrayEquals(newArray(factory.array.length, Mark.INSTANCE, "A", "C"), factory.array);
        
        assertEquals("A", list.remove(0));
        assertEquals(1, list.size());
        assertEquals(List.of("C"), list);
        assertArrayEquals(newArray(factory.array.length, Mark.INSTANCE, "C"), factory.array);
        
        assertEquals("C", list.remove(0));
        assertEquals(0, list.size());
        assertEquals(List.of(), list);
        assertArrayEquals(newArray(factory.array.length, Mark.INSTANCE), factory.array);
    }
    
    @Test
    void testRemoveObject() {
        ArrayTempList<String> list = factory.newList();
        addAll(list, "A", "B", "C", null);
        
        assertFalse(list.remove("Z"));
        
        assertTrue(list.remove("B"));
        assertEquals(asList("A", "C", null), list);
        
        assertTrue(list.remove(null));
        assertEquals(List.of("A", "C"), list);
        
        assertTrue(list.remove("A"));
        assertEquals(List.of("C"), list);
        
        assertTrue(list.remove("C"));
        assertEquals(List.of(), list);
        
        // note: this is tested more rigorously in testRemoveAtIndex
        assertArrayEquals(newArray(factory.array.length, Mark.INSTANCE), factory.array);
    }
    
    @Test
    void testRemoveAll() {
        ArrayTempList<String> list = factory.newList();
        addAll(list, "A", "B", "C", null, "X", "Y", "Z");
    
        // noinspection SuspiciousMethodCalls
        assertFalse(list.removeAll(Collections.emptySet()));
        assertFalse(list.removeAll(asList("1", "2", "3")));
        // noinspection ConstantConditions
        assertThrows(NullPointerException.class, () -> list.removeAll(null));
        
        Set<String> toRemove = new HashSet<>(asList("B", null, "Y"));
        
        assertTrue(list.removeAll(toRemove));
        assertEquals(List.of("A", "C", "X", "Z"), list);
        
        assertArrayEquals(newArray(factory.array.length, Mark.INSTANCE, "A", "C", "X", "Z"), factory.array,
            "removeAll should null out the removed indexes at the end of the array");
        
        assertTrue(list.removeAll(List.of("1", "C", "2")),
            "removeAll should work when the Collection contains other stuff");
        assertEquals(List.of("A", "X", "Z"), list);
        
        // double so we have 2 copies of every element
        list.addAll(new ArrayList<>(list));
        
        for (String s : new LinkedHashSet<>(list)) {
            assertNotEquals(list.indexOf(s), list.lastIndexOf(s));
            
            assertTrue(list.removeAll(singleton(s)));
            assertFalse(list.contains(s), "removeAll should remove both copies");
        }
        
        assertTrue(list.isEmpty());
        assertArrayEquals(newArray(factory.array.length, Mark.INSTANCE), factory.array,
            "the array should be empty at this point");
        
        // test when Collection.contains(Object) throws
        
        factory = new ArrayTempFactory(100);
        ArrayTempList<Integer> ints = factory.newList(10);
        
        addAll(ints, 1, 2, 3, 4, 5, 6, null, 7, 8, 9);
        assertEquals(ints.capacity(), ints.size());
        
        ArrayTempList<String> top = factory.newList(3);
        addAll(top, "A", "B", "C");
        
        List<Integer> nonNullsToRemove = List.of(4, 5, 6, 7, 8);
        // removes 4, 5, 6, then throws, skipping removal of 7 and 8
        assertThrows(NullPointerException.class, () -> ints.removeAll(nonNullsToRemove));
        
        assertEquals(asList(1, 2, 3, null, 7, 8, 9), ints);
        assertEquals(asList("A", "B", "C"), top);
        
        Object[] factoryArrayExpect =
            newArray(factory.array.length,
                     Mark.INSTANCE, 1, 2, 3, null, 7, 8, 9, null, null, null,
                     Mark.INSTANCE, "A", "B", "C");
        
        assertArrayEquals(factoryArrayExpect, factory.array);
    }
    
    @Test
    void testRetainAll() {
        ArrayTempList<String> list = factory.newList();
        addAll(list, "A", "B", "C", null, "X", "Y", "Z");
        
        assertFalse(list.retainAll(new ArrayList<>(list)));
        // noinspection ConstantConditions
        assertThrows(NullPointerException.class, () -> list.removeAll(null));
        
        Set<String> toRetain = new HashSet<>(asList("A", "C", "X", "Z"));
        
        assertTrue(list.retainAll(toRetain));
        assertEquals(List.of("A", "C", "X", "Z"), list);
        
        assertArrayEquals(newArray(factory.array.length, Mark.INSTANCE, "A", "C", "X", "Z"), factory.array,
            "retainAll should null out the removed indexes at the end of the array");
        
        assertTrue(list.retainAll(List.of("1", "X", "2", "Z")),
            "retainAll should work when the Collection contains other stuff");
        assertEquals(List.of("X", "Z"), list);
        
        assertTrue(list.retainAll(Collections.emptySet()));
        assertTrue(list.isEmpty());
        
        // double so we have 2 copies of every element
        addAll(list, "X", "Y", "Z", "X", "Y", "Z");
        
        for (String s : new LinkedHashSet<>(list)) {
            assertNotEquals(list.indexOf(s), list.lastIndexOf(s));
            
            Set<String> notS = new LinkedHashSet<>(list);
            notS.remove(s);
            
            assertTrue(list.retainAll(notS));
            assertFalse(list.contains(s), "retainAll should remove both copies");
        }
        
        assertTrue(list.isEmpty());
        assertArrayEquals(newArray(factory.array.length, Mark.INSTANCE), factory.array,
            "the array should be empty at this point");
        
        // test when Collection.contains(Object) throws
        
        factory = new ArrayTempFactory(100);
        ArrayTempList<Integer> ints = factory.newList(10);
        
        addAll(ints, 1, 2, 3, 4, 5, 6, null, 7, 8, 9);
        assertEquals(ints.capacity(), ints.size());
        
        ArrayTempList<String> top = factory.newList(3);
        addAll(top, "A", "B", "C");
        
        List<Integer> nonNullsToRetain = List.of(1, 2, 3, 9);
        // removes 4, 5, 6, then throws, skipping removal of 7 and 8
        assertThrows(NullPointerException.class, () -> ints.retainAll(nonNullsToRetain));
        
        assertEquals(asList(1, 2, 3, null, 7, 8, 9), ints);
        assertEquals(asList("A", "B", "C"), top);
        
        Object[] factoryArrayExpect =
            newArray(factory.array.length,
                     Mark.INSTANCE, 1, 2, 3, null, 7, 8, 9, null, null, null,
                     Mark.INSTANCE, "A", "B", "C");
        
        assertArrayEquals(factoryArrayExpect, factory.array);
    }
    
    @Test
    void testRemoveIf() {
        ArrayTempList<String> list = factory.newList();
        addAll(list, "p", "q", "r", "s", "t", "u", null, "x", "y", "z");
        
        assertFalse(list.removeIf(s -> false));
        assertThrows(NullPointerException.class, () -> list.removeIf(null));
        
        java.util.function.Predicate<String> cond =
            (String s) -> (s == null) || (s.charAt(0) > 'r' && s.charAt(0) != 'y');
        
        assertTrue(list.removeIf(cond));
        assertEquals(List.of("p", "q", "r", "y"), list);
        
        assertArrayEquals(newArray(factory.array.length, Mark.INSTANCE, "p", "q", "r", "y"), factory.array,
            "removeIf should null out the removed indexes at the end of the array");
        
        assertTrue(list.removeIf(s -> true));
        assertTrue(list.isEmpty());
        
        // test when Predicate.test throws
        
        factory = new ArrayTempFactory(100);
        ArrayTempList<Integer> ints = factory.newList(10);
        
        addAll(ints, 1, 2, 3, 4, 5, 6, null, 7, 8, 9);
        assertEquals(ints.capacity(), ints.size());
        
        ArrayTempList<String> top = factory.newList(3);
        addAll(top, "A", "B", "C");
        
        java.util.function.Predicate<Integer> nonNullCond =
            (Integer n) -> ( n <= 3 ) || ( n == 9 );
        // removes 1, 2, 3, then throws, skipping removal of 9
        assertThrows(NullPointerException.class, () -> ints.removeIf(nonNullCond));
        
        assertEquals(asList(4, 5, 6, null, 7, 8, 9), ints);
        assertEquals(asList("A", "B", "C"), top);
        
        Object[] factoryArrayExpect =
            newArray(factory.array.length,
                     Mark.INSTANCE, 4, 5, 6, null, 7, 8, 9, null, null, null,
                     Mark.INSTANCE, "A", "B", "C");
        
        assertArrayEquals(factoryArrayExpect, factory.array);
    }
    
    @Test
    void testRemoveRange() {
        ArrayTempList<String> bottom = factory.newList(3);
        addAll(bottom, "A", "B", "C");
        
        ArrayTempList<Integer> list = factory.newList(10);
        addAll(list, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
        
        ArrayTempList<String> top = factory.newList(3);
        addAll(top, "X", "Y", "Z");
        
        assertThrows(IndexOutOfBoundsException.class, () -> list.removeRange(-1, 3)); // from < 0
        assertThrows(IndexOutOfBoundsException.class, () -> list.removeRange(3, -1)); // to < 0
        assertThrows(IndexOutOfBoundsException.class, () -> list.removeRange(3, list.size() + 1)); // to > size
        assertThrows(IndexOutOfBoundsException.class, () -> list.removeRange(4, 3)); // from > to
        
        list.removeRange(3, 7);
        assertEquals(List.of(0, 1, 2, 7, 8, 9), list);
        
        Object[] expectedAfter37Remove =
            newArray(factory.array.length,
                     Mark.INSTANCE, "A", "B", "C",
                     Mark.INSTANCE, 0, 1, 2, 7, 8, 9, null, null, null, null,
                     Mark.INSTANCE, "X", "Y", "Z");
        
        assertArrayEquals(expectedAfter37Remove, factory.array);
        
        list.removeRange(2, 2);
        assertEquals(List.of(0, 1, 2, 7, 8, 9), list);
        
        assertArrayEquals(expectedAfter37Remove, factory.array);
        
        list.removeRange(0, list.size());
        assertTrue(list.isEmpty());
        
        assertArrayEquals(
            newArray(factory.array.length,
                     Mark.INSTANCE, "A", "B", "C",
                     Mark.INSTANCE, null, null, null, null, null, null, null, null, null, null,
                     Mark.INSTANCE, "X", "Y", "Z"),
            factory.array);
    }
    
    @Test
    void testReplaceAll() {
        ArrayTempList<Integer> list = factory.newList();
        
        assertThrows(NullPointerException.class, () -> list.replaceAll(null));
        
        addAll(list, 1, 2, 3, 4, 5, 6, 7, 8, 9);
        
        list.replaceAll(n -> 2 * n);
        
        for (int i = 0; i < list.size(); ++i) {
            assertEquals(2 * (i + 1), (int) list.get(i));
        }
    }
    
    @Test
    @SuppressWarnings("ResultOfMethodCallIgnored")
    void testGet() {
        ArrayTempList<String> list = factory.newList();
        
        assertThrows(IndexOutOfBoundsException.class, () -> list.get(-1));
        assertThrows(IndexOutOfBoundsException.class, () -> list.get(0));
        
        list.add("ABC");
        list.add("DEF");
        list.add("GHI");
        
        assertEquals("ABC", list.get(0));
        assertEquals("DEF", list.get(1));
        assertEquals("GHI", list.get(2));
        
        assertThrows(IndexOutOfBoundsException.class, () -> list.get(3));
    }
    
    @Test
    void testSet() {
        ArrayTempList<String> list = factory.newList();
        
        assertThrows(IndexOutOfBoundsException.class, () -> list.set(-1, ""));
        assertThrows(IndexOutOfBoundsException.class, () -> list.set(0, ""));
        
        list.add("ABC");
        list.add("DEF");
        list.add("GHI");
        
        assertEquals("ABC", list.set(0, "123"));
        assertEquals("123", list.get(0));
        
        assertEquals("DEF", list.set(1, "456"));
        assertEquals("456", list.get(1));
        
        assertEquals("GHI", list.set(2, null));
        assertNull(list.get(2));
        
        assertThrows(IndexOutOfBoundsException.class, () -> list.set(3, ""));
    }
    
    @Test
    void testToObjectArray() {
        ArrayTempList<Long> list = factory.newList();
        
        assertArrayEquals(new Object[0], list.toArray());
        
        addAll(list, 6L, 0L, 1L);
        assertArrayEquals(new Object[] {6L, 0L, 1L}, list.toArray());
        
        list.toArray()[0] = 7L;
        assertNotEquals(7L, list.get(0));
        
        assertNotSame(list.toArray(), list.toArray());
    }
    
    @Test
    void testToTArray() {
        ArrayTempList<Long> list = factory.newList(3);
        
        Long[] longArr; // assign to this so if the method returns a different type
                        // we would throw a ClassCastException instead of just passing
                        // straight to method which accepts Object[]
        
        for (int i = 0; i < 3; ++i) {
            longArr = list.toArray(new Long[i]);
            assertArrayEquals(new Long[i], longArr,
                "toArray on empty List should be a no-op");
        }
    
        // noinspection ConstantConditions
        assertThrows(NullPointerException.class, () -> list.toArray(null));
        
        addAll(list, 6L, 0L, 1L);
    
        // noinspection SuspiciousToArrayCall
        assertThrows(ArrayStoreException.class, () -> list.toArray(new String[0]));
        // noinspection SuspiciousToArrayCall, ToArrayCallWithZeroLengthArrayArgument
        assertThrows(ArrayStoreException.class, () -> list.toArray(new String[list.size()]));
        
        longArr = new Long[3];
        assertSame(longArr, list.toArray(longArr),
            "toArray should not resize the array we pass if it's the right size");
        assertArrayEquals(new Long[] {6L, 0L, 1L}, longArr);
        
        longArr = list.toArray(new Long[0]);
        assertArrayEquals(new Long[] {6L, 0L, 1L}, longArr,
            "toArray should resize the array we pass it if it's too small");
        
        Number[] numberArr;
        
        numberArr = new Number[3];
        assertSame(numberArr, list.toArray(numberArr),
            "toArray should not resize the array we pass if it's the right size");
        assertEquals(Number[].class, numberArr.getClass(),
            "toArray should return an array of the same type as we pass to it");
        assertArrayEquals(new Number[] {6L, 0L, 1L}, numberArr);
        
        numberArr = list.toArray(new Number[1]);
        assertEquals(Number[].class, numberArr.getClass(),
            "toArray should return an array of the same type as we pass to it");
        assertArrayEquals(new Number[] {6L, 0L, 1L}, numberArr,
            "toArray should resize the array we pass it if it's too small");
        
        // test to make sure we don't copy anything past
        // the bounds of the original List<Long>
        ArrayTempList<Double> top = factory.newList(3);
        addAll(top, 3.0, 1.0, 4.0);
        
        Object[] backingExpect =
            newArray(factory.array.length, Mark.INSTANCE, 6L, 0L, 1L, Mark.INSTANCE, 3.0, 1.0, 4.0);
        assertArrayEquals(backingExpect, factory.array);
        
        numberArr = new Number[10];
        Arrays.fill(numberArr, 0f);
        assertSame(numberArr, list.toArray(numberArr));
        
        Number[] expect = new Number[10];
        Arrays.fill(expect, 0f);
        expect[0] = 6L;
        expect[1] = 0L;
        expect[2] = 1L;
        expect[3] = null;
        assertArrayEquals(expect, numberArr);
    }
    
    @Test
    void testIndexOf() {
        ArrayTempList<String> list = factory.newList();
        addAll(list, "A", null, "C", "D");
        
        assertEquals(-1, list.indexOf("X"));
        
        for (int i = 0; i < list.size(); ++i) {
            assertEquals(i, list.indexOf(list.get(i)));
        }
        
        list.add("C");
        assertEquals("C", list.get(2));
        assertEquals("C", list.get(4));
        assertEquals(2, list.indexOf("C"), "must be first index of");
        
        assertTrue(list.remove("D"));
        assertEquals(-1, list.indexOf("D"));
    }
    
    @Test
    void testLastIndexOf() {
        ArrayTempList<String> list = factory.newList();
        addAll(list, "A", null, "C", "D");
        
        assertEquals(-1, list.lastIndexOf("X"));
        
        for (int i = 0; i < list.size(); ++i) {
            assertEquals(i, list.lastIndexOf(list.get(i)));
        }
        
        list.add("C");
        assertEquals("C", list.get(2));
        assertEquals("C", list.get(4));
        assertEquals(4, list.lastIndexOf("C"), "must be last index of");
        
        assertTrue(list.remove("D"));
        assertEquals(-1, list.lastIndexOf("D"));
    }
    
    @Test
    void testContainsObject() {
        ArrayTempList<String> list = factory.newList();
        addAll(list, "A", null, "C", "D");
        
        assertFalse(list.contains("X"));
        
        for (String s : list) {
            assertTrue(list.contains(s));
        }
        
        assertTrue(list.remove("C"));
        assertFalse(list.contains("C"));
    }
    
    @Test
    void testContainsAll() {
        ArrayTempList<String> list = factory.newList();
        addAll(list, "A", null, "C", "D");
    
        // noinspection ResultOfMethodCallIgnored, ConstantConditions
        assertThrows(NullPointerException.class, () -> list.containsAll(null));
        assertTrue(list.containsAll(Collections.emptyList()));
        // noinspection CollectionAddedToSelf
        assertTrue(list.containsAll(list));
        
        for (String s : list) {
            // noinspection RedundantCollectionOperation
            assertTrue(list.containsAll(singleton(s)));
        }
        
        assertTrue(list.containsAll(List.of("A", "C")));
        assertFalse(list.containsAll(List.of("A", "X")));
        assertFalse(list.containsAll(List.of("X", "Y")));
    }
    
    // also see ArrayTempListIteratorTest
    @Test
    void testIterators() {
        ArrayTempList<String> list = factory.newList();
        
        assertNotNull(list.iterator());
        assertNotNull(list.listIterator());
        assertNotNull(list.listIterator(0));
        
        assertThrows(IndexOutOfBoundsException.class, () -> list.listIterator(-1));
        assertThrows(IndexOutOfBoundsException.class, () -> list.listIterator(1));
        
        list.add("");
        assertEquals("", list.listIterator(0).next());
        assertFalse(list.listIterator(1).hasNext());
        
        assertThrows(IndexOutOfBoundsException.class, () -> list.listIterator(2));
        
        assertNotSame(list.iterator(), list.iterator());
        assertNotSame(list.listIterator(), list.listIterator());
        assertNotSame(list.listIterator(1), list.listIterator(1));
    }
    
    @Test
    void testSpliterator() {
        ArrayTempList<Integer> list = factory.newList();
        addAll(list, 0, 1, 2, 3, 4);
        
        Spliterator<Integer> split = list.spliterator();
        assertNotNull(split);
        
        assertEquals(Spliterator.SIZED | Spliterator.SUBSIZED | Spliterator.ORDERED, split.characteristics());
        assertEquals(list.size(), split.getExactSizeIfKnown());
        
        int[] i = new int[1];
        // noinspection StatementWithEmptyBody
        while (split.tryAdvance(n ->
            assertEquals(i[0]++, (int) n)));
        
        assertEquals(list.size(), i[0]);
    }
    
    // also see ArrayTempSubListTest
    @Test
    void testSubList() {
        ArrayTempList<Integer> list = factory.newList();
        ArrayTempSubList<Integer> sub;
        
        sub = (ArrayTempSubList<Integer>) list.subList(0, 0);
        assertTrue(sub.isEmpty());
        
        assertThrows(IndexOutOfBoundsException.class, () -> list.subList(-1, 0), "from < 0");
        assertThrows(IndexOutOfBoundsException.class, () -> list.subList(0, -1), "to < 0");
        assertThrows(IndexOutOfBoundsException.class, () -> list.subList(0, 1), "to > size");
        
        addAll(list, 1, 2, 3, 4, 5, 6, 7, 8, 9);
        
        sub = (ArrayTempSubList<Integer>) list.subList(0, list.size());
        assertEquals(list.size(), sub.size());
        assertThrows(IndexOutOfBoundsException.class, () -> list.subList(0, list.size() + 1), "to > size");
        assertThrows(IndexOutOfBoundsException.class, () -> list.subList(2, 1), "from > to");
        
        sub = (ArrayTempSubList<Integer>) list.subList(3, 6);
        assertEquals(List.of(4, 5, 6), sub);
    }
    
    @Test
    void testForEach() {
        ArrayTempList<Integer> list = factory.newList();
        
        assertThrows(NullPointerException.class, () -> list.forEach(null));
        
        addAll(list, 0, 1, 2, 3, 4, 5);
        
        int[] i = new int[1];
        list.forEach(elem ->
            assertEquals(i[0]++, (int) elem));
        
        assertEquals(list.size(), i[0]);
    }
    
    @Test
    void testSort() {
        ArrayTempList<String> list = factory.newList();
        addAll(list, "a", "b", "c", "p", "q", "r", "x", "y", "z");
        
        list.sort(reverseOrder());
        assertEquals(List.of("z", "y", "x", "r", "q", "p", "c", "b", "a"), list);
        
        do {
            shuffle(list);
        } while (isSorted(list, reverseOrder()));
        
        list.sort(reverseOrder());
        assertEquals(List.of("z", "y", "x", "r", "q", "p", "c", "b", "a"), list);
        
        list.sort(null); // note: uses naturalOrder()
        assertEquals(List.of("a", "b", "c", "p", "q", "r", "x", "y", "z"), list);
        
        do {
            shuffle(list);
        } while (isSorted(list, naturalOrder()));
        
        list.sort(naturalOrder());
        assertEquals(List.of("a", "b", "c", "p", "q", "r", "x", "y", "z"), list);
        
        ArrayTempList<Object> list2 = factory.newList();
        addAll(list2, new Object(), new Object());
        
        assertThrows(ClassCastException.class, () -> list2.sort(null));
    }
    
    @Test
    void testHashCode() {
        List<String> list = factory.newList();
        
        assertEquals(1, list.hashCode());
        
        addAll(list, "A", "B", "C");
        assertEquals(asList("A", "B", "C").hashCode(), list.hashCode());
        
        list.add(null);
        assertEquals(asList("A", "B", "C", null).hashCode(), list.hashCode());
    }
    
    @Test
    @SuppressWarnings("SimplifiableJUnitAssertion")
    void testEquals() {
        List<String> list = factory.newList();
    
        // noinspection ConstantConditions, ObjectEqualsNull
        assertFalse(list.equals(null));
        assertFalse(list.equals(new Object()));
        assertTrue(list.equals(Collections.emptyList()));
        // noinspection EqualsWithItself
        assertTrue(list.equals(list));
        
        addAll(list, "A", "B", "C");
        assertTrue(list.equals(asList("A", "B", "C")));
        
        list.add(null);
        assertTrue(list.equals(asList("A", "B", "C", null)));
        
        assertTrue(list.equals(new LinkedList<>(asList("A", "B", "C", null))));
        // noinspection EqualsBetweenInconvertibleTypes
        assertFalse(list.equals(new LinkedHashSet<>(asList("A", "B", "C", null))));
    }
    
    @Test
    void testToString() {
        List<String> list = factory.newList();
        assertEquals("[]", list.toString());
        
        list.add("A");
        assertEquals("[A]", list.toString());
        
        list.add("B");
        assertEquals("[A, B]", list.toString());
        
        list.add("C");
        assertEquals("[A, B, C]", list.toString());
    }
}