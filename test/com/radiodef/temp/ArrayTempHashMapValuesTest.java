/* MIT License
 *
 * Copyright (c) 2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.radiodef.temp;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

import java.util.*;
import java.util.Map.*;

class ArrayTempHashMapValuesTest {
    private ArrayTempFactory factory;
    private ArrayTempHashMap<String, String> map;
    
    @BeforeEach
    void initNewFactoryAndMap() {
        factory = new ArrayTempFactory(10);
        map = factory.newMap();
    }
    
    @AfterEach
    void clearFactoryAndMap() {
        factory = null;
        map = null;
    }
    
    @Test
    void testConstructor() {
        assertSame(map, new ArrayTempHashMapValues<>(map).map);
    }
    
    @Test
    void testIterator() {
        ArrayTempHashMapValues<String> values = new ArrayTempHashMapValues<>(map);
        
        assertNotNull(values.iterator());
        assertSame(map, values.iterator().map);
        assertNotSame(values.iterator(), values.iterator());
    }
    
    @Test
    void testSize() {
        ArrayTempHashMapValues<String> values = new ArrayTempHashMapValues<>(map);
        
        for (int i = 0; i < 10; ++i) {
            map.put("key" + i, "value" + i);
            assertEquals(i + 1, map.size);
            assertEquals(map.size, values.size());
        }
        
        map.put("key0", map.get("key1"));
        assertEquals(map.size, values.size());
        
        assertEquals(new HashSet<>(values).size() + 1, values.size());
    }
    
    @Test
    void testClear() {
        map.putAll(Map.of("A", "1", "B", "2", "C", "3"));
        assertEquals(3, map.size);
        
        ArrayTempHashMapValues<String> values = new ArrayTempHashMapValues<>(map);
        assertEquals(3, values.size());
        
        values.clear();
        assertTrue(map.isEmpty());
        assertTrue(values.isEmpty());
    }
    
    @Test
    void testAddAndAddAll() {
        ArrayTempHashMapValues<String> values = new ArrayTempHashMapValues<>(map);
        
        assertThrows(UnsupportedOperationException.class, () -> values.add("X"));
        assertThrows(UnsupportedOperationException.class, () -> values.addAll(List.of("X", "Y")));
    }
    
    @Test
    void testContains() {
        ArrayTempHashMapValues<String> values = new ArrayTempHashMapValues<>(map);
        
        assertFalse(values.contains("0"));
        assertNull(map.put("X", "0"));
        assertTrue(values.contains("0"));
        
        assertEquals("0", map.remove("X"));
        assertFalse(values.contains("0"));
        
        assertFalse(values.contains(null));
        map.put(null, null);
        assertTrue(values.contains(null));
        
        assertNull(map.remove(null));
        assertFalse(values.contains(null));
    }
    
    @Test
    void testRemove() {
        Map<String, String> entries = new LinkedHashMap<>();
        // noinspection CollectionAddAllCanBeReplacedWithConstructor
        entries.putAll(Map.of("A", "11",
                              "B", "22",
                              "C", "33",
                              "D", "44",
                              "E", "55",
                              "F", "66"));
        entries.put("G", null);
        entries.put(null, "00");
        
        ArrayTempHashMapValues<String> values = new ArrayTempHashMapValues<>(map);
        
        assertFalse(values.remove(null));
        assertFalse(values.remove("11"));
        
        map.putAll(entries);
        
        // plain test
        
        assertFalse(values.contains("xx"));
        assertFalse(values.remove("xx"));
        
        assertTrue(values.contains("33"));
        assertTrue(values.remove("33"));
        
        assertFalse(map.containsKey("C"));
        assertFalse(map.containsValue("33"));
        assertFalse(values.contains("33"));
        
        assertTrue(values.contains(null));
        assertTrue(values.remove(null));
        
        assertFalse(map.containsKey("G"));
        assertFalse(map.containsValue(null));
        assertFalse(values.contains(null));
        
        for (Entry<String, String> e : new LinkedHashMap<>(map).entrySet()) {
            String k = e.getKey();
            String v = e.getValue();
            
            assertTrue(values.contains(v));
            assertTrue(values.remove(v));
            
            assertFalse(values.contains(v));
            assertFalse(map.containsKey(k));
            assertFalse(map.containsValue(v));
        }
        
        // order test
        
        ArrayTempHashMap<Integer, String> intMap = factory.newMap();
        intMap.put(1, "a");
        intMap.put(2, "a");
        
        assertEquals(1,   factory.array[intMap.offset + 2]);
        assertEquals("a", factory.array[intMap.offset + 3]);
        assertEquals(2,   factory.array[intMap.offset + 4]);
        assertEquals("a", factory.array[intMap.offset + 5]);
        
        ArrayTempHashMapValues<String> strValues = new ArrayTempHashMapValues<>(intMap);
        
        assertTrue(strValues.remove("a"));
        assertTrue(strValues.contains("a"));
        assertEquals(1, strValues.size());
        assertEquals(Map.of(2, "a"), intMap);
    }
    
    // Note: implemented by AbstractSet
    @Test
    void testRemoveAll() {
        ArrayTempHashMapValues<String> values = new ArrayTempHashMapValues<>(map);
        map.putAll(Map.of("X", "0", "Y", "1", "Z", "2"));
        
        assertTrue(values.removeAll(Set.of("0", "1")));
        
        assertFalse(map.containsKey("X"));
        assertFalse(map.containsKey("Y"));
        assertFalse(map.containsValue("0"));
        assertFalse(map.containsValue("1"));
        
        assertEquals(List.of("2"), new ArrayList<>(values));
        assertEquals(Map.of("Z", "2"), map);
    
        // noinspection SuspiciousMethodCalls
        assertFalse(values.removeAll(Collections.emptySet()));
        
        assertEquals(Map.of("Z", "2"), map);
    }
    
    // Note: implemented by AbstractSet
    @Test
    void testRetainAll() {
        ArrayTempHashMapValues<String> values = new ArrayTempHashMapValues<>(map);
        map.putAll(Map.of("X", "0", "Y", "1", "Z", "2"));
        
        assertTrue(values.retainAll(Set.of("2")));
        
        assertFalse(map.containsKey("X"));
        assertFalse(map.containsKey("Y"));
        assertFalse(map.containsValue("0"));
        assertFalse(map.containsValue("1"));
        
        assertEquals(List.of("2"), new ArrayList<>(values));
        assertEquals(Map.of("Z", "2"), map);
    
        // noinspection SuspiciousMethodCalls
        assertTrue(values.retainAll(Collections.emptySet()));
        
        assertEquals(Collections.emptyMap(), map);
    }
}