/* MIT License
 *
 * Copyright (c) 2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.radiodef.temp;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

import java.util.*;

class ArrayTempHashMapKeySetTest {
    private ArrayTempFactory factory;
    private ArrayTempHashMap<String, String> map;
    
    @BeforeEach
    void initNewFactoryAndMap() {
        factory = new ArrayTempFactory(10);
        map = factory.newMap();
    }
    
    @AfterEach
    void clearFactoryAndMap() {
        factory = null;
        map = null;
    }
    
    @Test
    void testConstructor() {
        assertSame(map, new ArrayTempHashMapKeySet<>(map).map);
    }
    
    @Test
    void testIterator() {
        ArrayTempHashMapKeySet<String> keySet = new ArrayTempHashMapKeySet<>(map);
        
        assertNotNull(keySet.iterator());
        assertSame(map, keySet.iterator().map);
        assertNotSame(keySet.iterator(), keySet.iterator());
    }
    
    @Test
    void testSize() {
        ArrayTempHashMapKeySet<String> keySet = new ArrayTempHashMapKeySet<>(map);
        
        for (int i = 0; i < 10; ++i) {
            map.put("key" + i, "value" + i);
            assertEquals(i + 1, map.size);
            assertEquals(map.size, keySet.size());
        }
    }
    
    @Test
    void testClear() {
        map.putAll(Map.of("A", "1", "B", "2", "C", "3"));
        assertEquals(3, map.size);
        
        ArrayTempHashMapKeySet<String> keySet = new ArrayTempHashMapKeySet<>(map);
        assertEquals(3, keySet.size());
        
        keySet.clear();
        assertTrue(map.isEmpty());
        assertTrue(keySet.isEmpty());
    }
    
    @Test
    void testAddAndAddAll() {
        ArrayTempHashMapKeySet<String> keySet = new ArrayTempHashMapKeySet<>(map);
        
        assertThrows(UnsupportedOperationException.class, () -> keySet.add("X"));
        assertThrows(UnsupportedOperationException.class, () -> keySet.addAll(List.of("X", "Y")));
    }
    
    @Test
    void testContains() {
        ArrayTempHashMapKeySet<String> keySet = new ArrayTempHashMapKeySet<>(map);
        
        assertFalse(keySet.contains("X"));
        assertNull(map.put("X", "0"));
        assertTrue(keySet.contains("X"));
        
        assertEquals("0", map.remove("X"));
        assertFalse(keySet.contains("X"));
        
        assertFalse(keySet.contains(null));
        map.put(null, null);
        assertTrue(keySet.contains(null));
        
        assertNull(map.remove(null));
        assertFalse(keySet.contains(null));
    }
    
    @Test
    void testRemove() {
        ArrayTempHashMapKeySet<String> keySet = new ArrayTempHashMapKeySet<>(map);
        
        assertFalse(keySet.remove("X"));
        assertNull(map.put("X", "0"));
        assertTrue(keySet.remove("X"));
        assertFalse(map.containsKey("X"));
        
        assertFalse(keySet.remove(null));
        assertNull(map.put(null, null));
        assertTrue(keySet.remove(null));
        assertFalse(map.containsKey(null));
    }
    
    // Note: implemented by AbstractSet
    @Test
    void testRemoveAll() {
        ArrayTempHashMapKeySet<String> keySet = new ArrayTempHashMapKeySet<>(map);
        map.putAll(Map.of("X", "0", "Y", "1", "Z", "2"));
        
        assertTrue(keySet.removeAll(Set.of("X", "Y")));
        
        assertFalse(map.containsKey("X"));
        assertFalse(map.containsKey("Y"));
        
        assertEquals(Set.of("Z"), keySet);
        assertEquals(Map.of("Z", "2"), map);
    
        // noinspection SuspiciousMethodCalls
        assertFalse(keySet.removeAll(Collections.emptySet()));
        
        assertEquals(Map.of("Z", "2"), map);
    }
    
    // Note: implemented by AbstractSet
    @Test
    void testRetainAll() {
        ArrayTempHashMapKeySet<String> keySet = new ArrayTempHashMapKeySet<>(map);
        map.putAll(Map.of("X", "0", "Y", "1", "Z", "2"));
        
        assertTrue(keySet.retainAll(Set.of("Z")));
        
        assertFalse(map.containsKey("X"));
        assertFalse(map.containsKey("Y"));
        
        assertEquals(Set.of("Z"), keySet);
        assertEquals(Map.of("Z", "2"), map);
    
        // noinspection SuspiciousMethodCalls
        assertTrue(keySet.retainAll(Collections.emptySet()));
        
        assertEquals(Collections.emptyMap(), map);
    }
}