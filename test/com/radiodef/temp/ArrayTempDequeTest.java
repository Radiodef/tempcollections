/* MIT License
 *
 * Copyright (c) 2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.radiodef.temp;

import static com.radiodef.temp.Tools.*;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

import java.util.*;
import java.util.function.*;
import java.util.stream.*;
import static java.util.Arrays.asList;
import static java.util.Collections.*;
import static java.util.stream.Collectors.*;

class ArrayTempDequeTest {
    
    private ArrayTempFactory factory;
    
    @BeforeEach
    void initNewFactory() {
        factory = new ArrayTempFactory(10);
    }
    
    @AfterEach
    void clearFactory() {
        factory = null;
    }
    
    @Test
    void testConstructor() {
        ArrayTempDeque<?> deque = factory.newDeque(10);
        
        assertEquals(1, deque.offset);
        assertEquals(16, deque.capacity);
        assertTrue(deque.isEmpty());
    }
    
    @Test
    void testResizeArrayIntegrityWhenHeadLessThanTail() {
        ArrayTempList<String> bot = factory.newList(List.of("A", "B", "C"));
        
        ArrayTempDeque<Integer> q = factory.newDeque(4);
        q.addLast(0);
        q.addLast(1);
        q.addLast(2);
        q.addLast(3);
        assertEquals(4, q.size);
        assertEquals(4, q.capacity);
        
        Object[] expect =
            newArray(factory.array.length,
                     Mark.INSTANCE, "A", "B", "C",
                     Mark.INSTANCE, 0, 1, 2, 3);
        assertArrayEquals(expect, factory.array);
        
        q.addLast(4);
        assertEquals(5, q.size);
        assertEquals(8, q.capacity);
        
        expect = Arrays.copyOf(expect, factory.array.length);
        expect[9] = 4;
        assertArrayEquals(expect, factory.array);
        
        assertEquals(List.of("A", "B", "C"), bot);
    }
    
    @Test
    void testResizeArrayIntegrityWhenTailLessThanHead() {
        ArrayTempList<String> bot = factory.newList(List.of("A", "B", "C"));
        
        ArrayTempDeque<Integer> q = factory.newDeque(8);
        
        q.addLast(4);
        q.addLast(3);
        q.addLast(2);
        q.addLast(1);
        q.addLast(0);
        q.addFirst(5);
        q.addFirst(6);
        
        assertEquals(7, q.size);
        assertEquals(8, q.capacity);
        
        Object[] expect =
            newArray(factory.array.length,
                     Mark.INSTANCE, "A", "B", "C",
                     Mark.INSTANCE, 4, 3, 2, 1, 0, null, 6, 5);
        assertArrayEquals(expect, factory.array, () -> Arrays.toString(factory.array));
        
        q.addFirst(7);
        assertEquals(8, q.size);
        assertEquals(8, q.capacity);
        
        expect[q.offset + 5] = 7;
        assertArrayEquals(expect, factory.array, () -> Arrays.toString(factory.array));
        
        q.addFirst(8);
        assertEquals(9, q.size);
        assertEquals(16, q.capacity);
        
        expect =
            newArray(factory.array.length,
                     Mark.INSTANCE, "A", "B", "C",
                     Mark.INSTANCE, 4, 3, 2, 1, 0, null, null, null, null, null, null, null, 8, 7, 6, 5);
        assertArrayEquals(expect, factory.array, () -> Arrays.toString(factory.array));
        
        assertEquals(List.of("A", "B", "C"), bot);
    }
    
    @Test
    void testOffer() {
        testOfferLast(Deque::offer);
    }
    
    @Test
    void testOfferLast() {
        testOfferLast(Deque::offerLast);
    }
    
    private void testOfferLast(BiPredicate<Deque<String>, String> offer) {
        ArrayTempDeque<String> q = factory.newDeque(4);
        
        String x = "x";
        
        assertTrue(offer.test( q, x ));
        assertEquals( x, q.peekFirst() );
        assertEquals( x, q.peekLast() );
        
        String y = "y";
        
        assertTrue(offer.test( q, y ));
        assertEquals( x, q.peekFirst() );
        assertEquals( y, q.peekLast() );
        
        String z = "z";
        
        assertTrue(offer.test( q, z ));
        assertEquals( x, q.peekFirst() );
        assertEquals( z, q.peekLast() );
        
        assertEquals(List.of("x", "y", "z"), q.toArrayList());
        
        try (TempList<?> top = factory.newList(List.of(601))) {
            
            String s = "s";
            
            assertTrue(offer.test( q, s ));
            assertEquals(4, q.size);
            assertEquals(4, q.capacity);
            
            String t = "t";
            
            assertFalse(offer.test( q, t ));
            assertEquals(4, q.size);
            assertEquals(4, q.capacity);
            
            assertEquals(List.of(601), top);
        }
        
        String t = "t";
        
        assertTrue(offer.test( q, t ));
        assertEquals(5, q.size);
        assertEquals(8, q.capacity);
        
        assertTrue(offer.test( q, null ));
        assertEquals(6, q.size);
        assertEquals(8, q.capacity);
        
        assertEquals(asList("x", "y", "z", "s", "t", null), q.toArrayList());
    }
    
    @Test
    void testOfferFirst() {
        testOfferFirst(Deque::offerFirst);
    }
    
    private void testOfferFirst(BiPredicate<Deque<String>, String> offer) {
        ArrayTempDeque<String> q = factory.newDeque(4);
        
        String x = "x";
        
        assertTrue(offer.test( q, x ));
        assertEquals( x, q.peekFirst() );
        assertEquals( x, q.peekLast() );
        
        String y = "y";
        
        assertTrue(offer.test( q, y ));
        assertEquals( y, q.peekFirst() );
        assertEquals( x, q.peekLast() );
        
        String z = "z";
        
        assertTrue(offer.test( q, z ));
        assertEquals( z, q.peekFirst() );
        assertEquals( x, q.peekLast() );
        
        assertEquals(List.of("z", "y", "x"), q.toArrayList());
        
        try (TempList<?> top = factory.newList(List.of(601))) {
            
            String s = "s";
            
            assertTrue(offer.test( q, s ));
            assertEquals(4, q.size);
            assertEquals(4, q.capacity);
            
            String t = "t";
            
            assertFalse(offer.test( q, t ));
            assertEquals(4, q.size);
            assertEquals(4, q.capacity);
            
            assertEquals(List.of(601), top);
        }
        
        String t = "t";
        
        assertTrue(offer.test( q, t ));
        assertEquals(5, q.size);
        assertEquals(8, q.capacity);
        
        assertTrue(offer.test( q, null ));
        assertEquals(6, q.size);
        assertEquals(8, q.capacity);
        
        assertEquals(asList(null, "t", "s", "z", "y", "x"), q.toArrayList());
    }
    
    @Test
    void testOfferFirstGenerally() {
        testOfferGenerally( Deque::offerFirst, Deque::descendingIterator );
    }
    
    @Test
    void testOfferLastGenerally() {
        testOfferGenerally( Deque::offerLast, Deque::iterator );
    }
    
    @Test
    void testOfferGenerally() {
        testOfferGenerally( Deque::offer, Deque::iterator );
    }
    
    private void testOfferGenerally(BiPredicate<Deque<String>, String> offer,
                                    Function<Deque<String>, Iterator<String>> iterator) {
        Supplier<String> elements = Arrays.asList("ABCDEFGHIJKLMNOPQRSTUV".split("")).iterator()::next;
        
        int size = 16;
        
        ArrayTempDeque<String> q = factory.newDeque(size);
        assertEquals(0, q.size);
        assertEquals(size, q.capacity);
        
        List<String> list = new ArrayList<>();
        
        for (int i = 0; i < size; ++i) {
            String e = elements.get();
            
            assertTrue(q.canAdd(1));
            
            assertFalse(q.contains(e));
            assertTrue(offer.test(q, e));
            assertTrue(q.contains(e));
            assertEquals(i + 1, q.size);
            assertEquals(size, q.capacity);
            
            list.add(e);
        }
        
        Iterator<String> it = iterator.apply(q);
        
        for (int i = 0; i < size; ++i) {
            assertTrue(it.hasNext());
            assertEquals(list.get(i), it.next());
        }
        
        assertFalse(it.hasNext());
        
        {
            String e = elements.get();
            
            try (@SuppressWarnings("unused")
                 TempObject obj = factory.newList(List.of(601))) {
                assertFalse(q.canAdd(1), "must not overflow when not the top collection");
                
                assertFalse(q.contains(e));
                assertFalse(offer.test(q, e));
                assertFalse(q.contains(e));
                
                assertEquals(List.of(601), obj, "must not overflow");
            }
            
            assertTrue(q.canAdd(1));
            
            assertEquals(size, q.capacity());
            
            assertFalse(q.contains(e));
            assertTrue(offer.test(q, e)); // note: resizes the array
            assertTrue(q.contains(e));
            
            list.add(e);
        }
        
        {
            assertEquals(2 * size, q.capacity(), "the last add must have resized the array");
            // make sure that nothing was e.g. left unreachable after a resize
            
            it = iterator.apply(q);
            
            for (String e : list) {
                assertTrue(it.hasNext());
                assertEquals(e, it.next());
            }
            
            assertFalse(it.hasNext());
        }
        
        // should accept null, of course
        assertFalse(q.contains(null));
        assertTrue(offer.test(q, null));
        assertTrue(q.contains(null));
    }
    
    @Test
    void testAdd() {
        testAddLast((q, e) -> assertTrue(q.add(e)));
    }
    
    @Test
    void testAddLast() {
        testAddLast(Deque::addLast);
    }
    
    private void testAddLast(BiConsumer<Deque<String>, String> add) {
        ArrayTempDeque<String> q = factory.newDeque(4);
        assertEquals(4, q.capacity);
        
        int size = 0;
        assertEquals(size++, q.size);
        
        add.accept(q, "X");
        assertEquals("X", q.peekFirst());
        assertEquals("X", q.peekLast());
        assertEquals(size++, q.size);
        add.accept(q, "Y");
        assertEquals("X", q.peekFirst());
        assertEquals("Y", q.peekLast());
        assertEquals(size++, q.size);
        add.accept(q, "Z");
        assertEquals(size++, q.size);
        assertEquals("X", q.peekFirst());
        assertEquals("Z", q.peekLast());
        
        try (TempList<?> list = factory.newList(List.of(601))) {
            add.accept(q, "1");
            assertEquals(size, q.size);
            assertEquals(4, q.capacity);
            
            assertFalse(q.canAdd(1));
            try {
                add.accept(q, "2");
                fail("q must be full");
            } catch (IllegalStateException ignored) {
            }
            assertEquals(size++, q.size);
            
            assertEquals(List.of(601), list);
        }
        
        assertEquals(4, q.capacity);
        add.accept(q, "2");
        assertEquals(8, q.capacity);
        assertEquals(size++, q.size);
        
        add.accept(q, null);
        assertEquals(size, q.size);
        assertEquals(8, q.capacity);
        
        assertEquals(asList("X", "Y", "Z", "1", "2", null), q.toArrayList());
    }
    
    @Test
    void testPush() {
        testAddFirst(Deque::push);
    }
    
    @Test
    void testAddFirst() {
        testAddFirst(Deque::addFirst);
    }
    
    private void testAddFirst(BiConsumer<Deque<String>, String> add) {
        ArrayTempDeque<String> q = factory.newDeque(4);
        assertEquals(4, q.capacity);
        
        int size = 0;
        assertEquals(size++, q.size);
        
        add.accept(q, "X");
        assertEquals("X", q.peekFirst());
        assertEquals("X", q.peekLast());
        assertEquals(size++, q.size);
        add.accept(q, "Y");
        assertEquals("Y", q.peekFirst());
        assertEquals("X", q.peekLast());
        assertEquals(size++, q.size);
        add.accept(q, "Z");
        assertEquals("Z", q.peekFirst());
        assertEquals("X", q.peekLast());
        assertEquals(size++, q.size);
        
        try (TempList<?> list = factory.newList(List.of(601))) {
            add.accept(q, "1");
            assertEquals(size, q.size);
            assertEquals(4, q.capacity);
            
            assertFalse(q.canAdd(1));
            try {
                add.accept(q, "2");
                fail("q must be full");
            } catch (IllegalStateException ignored) {
            }
            assertEquals(size++, q.size);
            
            assertEquals(List.of(601), list);
        }
        
        assertEquals(4, q.capacity);
        add.accept(q, "2");
        assertEquals(8, q.capacity);
        assertEquals(size++, q.size);
        
        add.accept(q, null);
        assertEquals(size, q.size);
        assertEquals(8, q.capacity);
        
        assertEquals(asList(null, "2", "1", "Z", "Y", "X"), q.toArrayList());
    }
    
    @Test
    void testAddAll() {
        ArrayTempDeque<String> q = factory.newDeque(8);
        assertEquals(0, q.size());
        
        // noinspection ConstantConditions
        assertThrows(NullPointerException.class, () -> q.addAll(null));
        assertFalse(q.addAll(emptyList()));
        assertEquals(0, q.size());
        
        List<String> xyz = List.of("X", "Y", "Z");
        
        assertTrue(q.addAll(xyz));
        assertEquals(3, q.size());
        assertEquals(xyz, q.toArrayList());
        
        assertTrue(q.addAll(xyz));
        assertEquals(6, q.size());
        assertEquals(List.of("X", "Y", "Z", "X", "Y", "Z"), q.toArrayList());
        
        assertEquals(8, q.capacity());
        
        List<String> abc = List.of("A", "B", "C");
        
        try (TempList<Integer> top = factory.newList(List.of(601))) {
            assertTrue(q.canAdd(2));
            assertFalse(q.canAdd(3));
            
            assertThrows(IllegalStateException.class, () -> q.addAll(abc));
            
            assertFalse(q.canAdd(1));
            assertEquals(8, q.size());
            assertEquals(8, q.capacity());
            assertEquals(List.of("X", "Y", "Z", "X", "Y", "Z", "A", "B"), q.toArrayList());
            
            assertEquals(List.of(601), top);
        }
        
        assertTrue(q.addAll(abc));
        assertEquals(11, q.size());
        assertEquals(16, q.capacity());
        assertEquals(List.of("X", "Y", "Z", "X", "Y", "Z", "A", "B", "A", "B", "C"), q.toArrayList());
        
        List<String> bigList =
            IntStream.rangeClosed(Character.MIN_VALUE, Character.MAX_VALUE)
                     .mapToObj(c -> Character.toString((char) c))
                     .collect(toList());
        assertEquals(65_536, bigList.size());
        
        q.clear();
        ArrayTempDeque.RESIZES = 0;
        
        assertTrue(q.addAll(bigList));
        
        assertEquals(1, ArrayTempDeque.RESIZES);
        assertEquals(bigList.size(), q.capacity());
        assertEquals(bigList, q.toArrayList());
    }
    
    @Test
    void testClear() {
        ArrayTempList<String> bot = factory.newList(List.of("A", "B", "C"));
        
        ArrayTempDeque<Integer> q = factory.newDeque();
        assertTrue(q.isEmpty());
        
        q.clear();
        
        int count = 1001;
        for (int i = 0; i < count; ++i) {
            if ((i & 1) == 0)
                q.addLast(i);
            else
                q.addFirst(i);
        }
        
        assertEquals(count, q.size);
        
        ArrayTempList<String> top = factory.newList(List.of("X", "Y", "Z"));
        
        q.clear();
        assertEquals(0, q.size);
        assertTrue(q.isEmpty());
        
        Object[] expect = new Object[factory.array.length];
        int i = 0;
        expect[i++] = Mark.INSTANCE;
        expect[i++] = "A";
        expect[i++] = "B";
        expect[i++] = "C";
        expect[i++] = Mark.INSTANCE;
        assertEquals(q.offset, i);
        i = top.offset - 1;
        expect[i++] = Mark.INSTANCE;
        expect[i++] = "X";
        expect[i++] = "Y";
        expect[i++] = "Z";
        assertEquals(top.offset + top.size, i);
        
        assertArrayEquals(expect, factory.array);
        
        for (i = 0; i < q.capacity; ++i)
            q.addLast(i);
        assertEquals(q.capacity, q.size);
        
        q.clear();
        
        assertEquals(0, q.size);
        assertArrayEquals(expect, factory.array);
        
        assertEquals(List.of("A", "B", "C"), bot);
        assertEquals(List.of("X", "Y", "Z"), top);
    }
    
    @Test
    void testPeek() {
        testPeekFirst(Deque::peek);
    }
    
    @Test
    void testPeekFirst() {
        testPeekFirst(Deque::peekFirst);
    }
    
    @Test
    void testElement() {
        testGetFirst(Deque::element);
    }
    
    @Test
    void testGetFirst() {
        testGetFirst(Deque::getFirst);
    }
    
    private void testPeekFirst(Function<Deque<String>, String> peek) {
        testPeekOrGetFirst(peek, q -> assertNull(peek.apply(q)));
    }
    
    private void testGetFirst(Function<Deque<String>, String> get) {
        testPeekOrGetFirst(get, q -> {
            try {
                get.apply( q );
                fail("expected exception");
            } catch (NoSuchElementException ignored) {
            }
        });
    }
    
    private void testPeekOrGetFirst(Function<Deque<String>, String> peek,
                                    Consumer<Deque<String>> emptyTest) {
        ArrayTempDeque<String> q = factory.newDeque();
        
        assertTrue(q.isEmpty());
        emptyTest.accept( q );
        
        q.addFirst("X"); // [X]
        assertEquals("X", peek.apply( q ));
        
        q.addLast("Y"); // [X,Y]
        assertEquals("X", peek.apply( q ));
        
        q.addFirst("Z"); // [Z,X,Y]
        assertEquals("Z", peek.apply( q ));
        
        q.removeFirst(); // [X,Y]
        assertEquals("X", peek.apply( q ));
        
        q.removeFirst(); // [Y]
        assertEquals("Y", peek.apply( q ));
        
        q.addFirst("G"); // [G,Y]
        assertEquals("G", peek.apply( q ));
        
        q.removeLast(); // [G]
        assertEquals("G", peek.apply( q ));
        
        q.removeFirst(); // []
        assertTrue(q.isEmpty());
        emptyTest.accept( q );
        
        q.addLast(null); // [null]
        assertNull(peek.apply( q ));
        
        q.removeFirst(); // []
        assertTrue(q.isEmpty());
        emptyTest.accept( q );
    }
    
    @Test
    void testPeekLast() {
        testPeekLast(Deque::peekLast);
    }
    
    @Test
    void testGetLast() {
        testGetLast(Deque::getLast);
    }
    
    private void testPeekLast(Function<Deque<String>, String> peek) {
        testPeekOrGetLast(peek, q -> assertNull(peek.apply(q)));
    }
    
    private void testGetLast(Function<Deque<String>, String> get) {
        testPeekOrGetLast(get, q -> {
            try {
                get.apply( q );
                fail("expected exception");
            } catch (NoSuchElementException ignored) {
            }
        });
    }
    
    private void testPeekOrGetLast(Function<Deque<String>, String> peek,
                                   Consumer<Deque<String>> emptyTest) {
        ArrayTempDeque<String> q = factory.newDeque();
        
        assertTrue(q.isEmpty());
        emptyTest.accept( q );
        
        q.addLast("X"); // [X]
        assertEquals("X", peek.apply( q ));
        
        q.addFirst("Y"); // [Y,X]
        assertEquals("X", peek.apply( q ));
        
        q.addLast("Z"); // [Y,X,Z]
        assertEquals("Z", peek.apply( q ));
        
        q.removeLast(); // [Y,X]
        assertEquals("X", peek.apply( q ));
        
        q.removeLast(); // [Y]
        assertEquals("Y", peek.apply( q ));
        
        q.addLast("G"); // [Y,G]
        assertEquals("G", peek.apply( q ));
        
        q.removeFirst(); // [G]
        assertEquals("G", peek.apply( q ));
        
        q.removeLast(); // []
        assertTrue(q.isEmpty());
        emptyTest.accept( q );
        
        q.addFirst(null); // [null]
        assertNull(peek.apply( q ));
        
        q.removeLast(); // []
        assertTrue(q.isEmpty());
        emptyTest.accept( q );
    }
    
    @Test
    void testPop() {
        testRemoveFirst(Deque::pop);
    }
    
    @Test
    void testRemove() {
        testRemoveFirst(Deque::remove);
    }
    
    @Test
    void testRemoveFirst() {
        testRemoveFirst(Deque::removeFirst);
    }
    
    private void testRemoveFirst(Function<Deque<String>, String> remove) {
        testRemoveFirst(remove, q -> {
            try {
                String e = remove.apply( q );
                fail("found " + e);
            } catch (NoSuchElementException ignored) {
            }
        });
    }
    
    @Test
    void testPoll() {
        testPollFirst(Deque::poll);
    }
    
    @Test
    void testPollFirst() {
        testPollFirst(Deque::pollFirst);
    }
    
    private void testPollFirst(Function<Deque<String>, String> poll) {
        testRemoveFirst(poll, q -> assertNull( poll.apply( q ) ));
    }
    
    private void testRemoveFirst(Function<Deque<String>, String> remove,
                                 Consumer<Deque<String>> emptyTest) {
        ArrayTempDeque<String> q = factory.newDeque();
        
        assertTrue( q.isEmpty() );
        assertEquals( 0, q.size() );
        emptyTest.accept( q );
        
        q.addFirst( "Abc" ); // [Abc]
        assertFalse( q.isEmpty() );
        assertEquals( 1, q.size() );
        assertEquals( List.of( "Abc" ), q.toArrayList() );
        
        assertEquals( "Abc", remove.apply( q ) ); // []
        assertEquals( emptyList(), q.toArrayList() );
        
        assertTrue( q.isEmpty() );
        assertEquals( 0, q.size() );
        emptyTest.accept( q );
        
        q.addLast( "Def" ); // [Def]
        assertFalse( q.isEmpty() );
        assertEquals( 1, q.size() );
        assertEquals( List.of( "Def" ), q.toArrayList() );
        
        assertEquals( "Def", remove.apply( q ) ); // []
        assertEquals( emptyList(), q.toArrayList() );
        
        assertTrue( q.isEmpty() );
        assertEquals( 0, q.size() );
        emptyTest.accept( q );
        
        q.addFirst( "Abc" ); // [Abc]
        q.addLast( "Def" ); // [Abc,Def]
        assertFalse( q.isEmpty() );
        assertEquals( 2, q.size() );
        assertEquals( List.of( "Abc", "Def" ), q.toArrayList() );
        
        assertEquals( "Abc", remove.apply( q ) ); // [Def]
        assertEquals( 1, q.size() );
        assertEquals( List.of( "Def" ), q.toArrayList() );
        
        q.addLast( null ); // [Def,null]
        assertEquals( 2, q.size() );
        assertEquals( asList( "Def", null ), q.toArrayList() );
        
        assertEquals( "Def", remove.apply( q ) ); // [null]
        assertEquals( 1, q.size() );
        assertEquals( singletonList(null), q.toArrayList() );
        
        assertNull( remove.apply( q ) ); // []
        assertEquals( emptyList(), q.toArrayList() );
        
        assertTrue( q.isEmpty() );
        assertEquals( 0, q.size() );
        emptyTest.accept( q );
        
        // make deque like [..., 8,6,4,2,0,1,3,5,7,9 ...]
        int count = 8_192;
        for (int i = 0; i < count; ++i) {
            if ((i & 1) == 0)
                q.addFirst( i+" is even" );
            else
                q.addLast( i+" is odd" );
        }
        assertEquals( count, q.size() );
        
        for (int i = (count - 2); i >= 0; i -= 2)
            assertEquals( i+" is even", remove.apply( q ) );
        
        assertEquals( count/2, q.size() );
        
        for (int i = 1; i < count; i += 2)
            assertEquals( i+" is odd", remove.apply( q ) );
        
        assertTrue( q.isEmpty() );
        assertEquals( 0, q.size() );
        emptyTest.accept( q );
    }
    
    @Test
    void testRemoveLast() {
        testRemoveLast(Deque::removeLast, q -> {
            try {
                String e = q.removeLast();
                fail("found " + e);
            } catch (NoSuchElementException ignored) {
            }
        });
    }
    
    @Test
    void testPollLast() {
        testRemoveLast(Deque::pollLast, q -> assertNull( q.pollLast() ));
    }
    
    private void testRemoveLast(Function<Deque<String>, String> remove,
                                Consumer<Deque<String>> emptyTest) {
        ArrayTempDeque<String> q = factory.newDeque();
        
        assertTrue( q.isEmpty() );
        assertEquals( 0, q.size() );
        emptyTest.accept( q );
        
        q.addLast( "Abc" ); // [Abc]
        assertFalse( q.isEmpty() );
        assertEquals( 1, q.size() );
        assertEquals( List.of( "Abc" ), q.toArrayList() );
        
        assertEquals( "Abc", remove.apply( q ) ); // []
        assertEquals( emptyList(), q.toArrayList() );
        
        assertTrue( q.isEmpty() );
        assertEquals( 0, q.size() );
        emptyTest.accept( q );
        
        q.addFirst( "Def" ); // [Def]
        assertFalse( q.isEmpty() );
        assertEquals( 1, q.size() );
        assertEquals( List.of( "Def" ), q.toArrayList() );
        
        assertEquals( "Def", remove.apply( q ) ); // []
        assertEquals( emptyList(), q.toArrayList() );
        
        assertTrue( q.isEmpty() );
        assertEquals( 0, q.size() );
        emptyTest.accept( q );
        
        q.addLast( "Def" ); // [Def]
        q.addFirst( "Abc" ); // [Abc,Def]
        assertFalse( q.isEmpty() );
        assertEquals( 2, q.size() );
        assertEquals( List.of( "Abc", "Def" ), q.toArrayList() );
        
        assertEquals( "Def", remove.apply( q ) ); // [Abc]
        assertEquals( 1, q.size() );
        assertEquals( List.of( "Abc" ), q.toArrayList() );
        
        q.addFirst( null ); // [null,Abc]
        assertEquals( 2, q.size() );
        assertEquals( asList( null, "Abc" ), q.toArrayList() );
        
        assertEquals( "Abc", remove.apply( q ) ); // [null]
        assertEquals( 1, q.size() );
        assertEquals( singletonList(null), q.toArrayList() );
        
        assertNull( remove.apply( q ) ); // []
        assertEquals( emptyList(), q.toArrayList() );
        
        assertTrue( q.isEmpty() );
        assertEquals( 0, q.size() );
        emptyTest.accept( q );
        
        // make deque like [..., 8,6,4,2,0,1,3,5,7,9 ...]
        int count = 8_192;
        for (int i = 0; i < count; ++i) {
            if ((i & 1) == 0)
                q.addFirst( i+" is even" );
            else
                q.addLast( i+" is odd" );
        }
        assertEquals( count, q.size() );
        
        for (int i = (count - 1); i > 0; i -= 2)
            assertEquals( i+" is odd", remove.apply( q ) );
        
        assertEquals( count/2, q.size() );
        
        for (int i = 0; i < count; i += 2)
            assertEquals( i+" is even", remove.apply( q ) );
        
        assertTrue( q.isEmpty() );
        assertEquals( 0, q.size() );
        emptyTest.accept( q );
    }
    
    @Test
    void testRemoveObject() {
        testRemoveFirstOccurrence(Deque::remove);
        testRandomRemovals(Deque::remove);
        testRemoveShifting(Deque::remove);
    }
    
    @Test
    void testRemoveFirstOccurrence() {
        testRemoveFirstOccurrence(Deque::removeFirstOccurrence);
        testRandomRemovals(Deque::removeFirstOccurrence);
        testRemoveShifting(Deque::removeFirstOccurrence);
    }
    
    private void testRemoveFirstOccurrence(BiPredicate<Deque<String>, String> remove) {
        ArrayTempDeque<String> q = factory.newDeque();
        
        assertFalse( remove.test( q, null ) );
        assertFalse( remove.test( q, "Aa" ) );
        
        q.add( null );
        assertTrue( q.contains( null ) );
        assertTrue( remove.test( q, null ) );
        assertFalse( remove.test( q, null ) );
        
        assertEquals( 0, q.size() );
        
        q.add( "Aa" );
        assertTrue( q.contains( "Aa" ) );
        assertTrue( remove.test( q, "Aa" ) );
        assertFalse( remove.test( q, "Aa" ) );
        
        assertEquals( 0, q.size() );
        
        addAll( q, "Aa", "Xx", "Bb", "Xx", "Cc" );
        
        assertTrue( remove.test( q, "Xx" ) );
        assertEquals( List.of( "Aa", "Bb", "Xx", "Cc" ), q.toArrayList(),
            "should have removed the first occurrence of Xx" );
        
        assertFalse( remove.test( q, "Yy" ) );
        
        assertEquals( "Aa", q.peekFirst() );
        assertEquals( "Cc", q.peekLast() );
        
        assertTrue( remove.test( q, "Bb" ) );
        assertEquals( List.of( "Aa", "Xx", "Cc" ), q.toArrayList() );
        assertTrue( remove.test( q, "Aa" ) );
        assertEquals( List.of( "Xx", "Cc" ), q.toArrayList() );
        assertTrue( remove.test( q, "Cc" ) );
        assertEquals( singletonList( "Xx" ), q.toArrayList() );
        assertTrue( remove.test( q, "Xx" ) );
        assertEquals( emptyList(), q.toArrayList() );
    }
    
    @Test
    void testRemoveLastOccurrence() {
        testRemoveLastOccurrence(Deque::removeLastOccurrence);
        testRandomRemovals(Deque::removeLastOccurrence);
        testRemoveShifting(Deque::removeLastOccurrence);
    }
    
    private void testRemoveLastOccurrence(BiPredicate<Deque<String>, String> remove) {
        ArrayTempDeque<String> q = factory.newDeque();
        
        assertFalse( remove.test( q, null ) );
        assertFalse( remove.test( q, "Aa" ) );
        
        q.add( null );
        assertTrue( q.contains( null ) );
        assertTrue( remove.test( q, null ) );
        assertFalse( remove.test( q, null ) );
        
        assertEquals( 0, q.size() );
        
        q.add( "Aa" );
        assertTrue( q.contains( "Aa" ) );
        assertTrue( remove.test( q, "Aa" ) );
        assertFalse( remove.test( q, "Aa" ) );
        
        assertEquals( 0, q.size() );
        
        addAll( q, "Aa", "Xx", "Bb", "Xx", "Cc" );
        
        assertTrue( remove.test( q, "Xx" ) );
        assertEquals( List.of( "Aa", "Xx", "Bb", "Cc" ), q.toArrayList(),
            "should have removed the last occurrence of Xx" );
        
        assertFalse( remove.test( q, "Yy" ) );
        
        assertEquals( "Aa", q.peekFirst() );
        assertEquals( "Cc", q.peekLast() );
        
        assertTrue( remove.test( q, "Bb" ) );
        assertEquals( List.of( "Aa", "Xx", "Cc" ), q.toArrayList() );
        assertTrue( remove.test( q, "Aa" ) );
        assertEquals( List.of( "Xx", "Cc" ), q.toArrayList() );
        assertTrue( remove.test( q, "Cc" ) );
        assertEquals( singletonList( "Xx" ), q.toArrayList() );
        assertTrue( remove.test( q, "Xx" ) );
        assertEquals( emptyList(), q.toArrayList() );
    }
    
    private void testRandomRemovals(BiPredicate<Deque<String>, String> remove) {
        testRandomRemovals(remove, false);
        testRandomRemovals(remove, true);
    }
    
    private void testRandomRemovals(BiPredicate<Deque<String>, String> remove,
                                    boolean splitElementsInArray) {
        ArrayTempDeque<String> q = factory.newDeque();
        
        int count = 703;
        Random rand = new Random(0xBEA7D_D00DL);
        
        int middleRemovals = 0;
        
        for (int i = 0; i < count; ++i) {
            String e = i + "." + rand.nextInt();
            if (splitElementsInArray) {
                if (rand.nextBoolean())
                    q.addLast(e);
                else
                    q.addFirst(e);
            } else {
                q.addLast(e);
            }
        }
        
        assertEquals(q.tail < q.head, splitElementsInArray);
        
        while (!q.isEmpty()) {
            // picks a random element
            int i = q.offset + ((q.head - q.offset + rand.nextInt( q.size )) & (q.capacity - 1));
            
            if (i != q.head && i != q.tail)
                ++middleRemovals;
            
            String e = (String) factory.array[ i ];
            
            assertTrue( q.contains( e ) );
            assertTrue( remove.test( q, e ) );
            assertFalse( q.contains( e ) );
            assertFalse( remove.test( q, e ) );
        }
        
        assertTrue( middleRemovals > (count/2) && middleRemovals != count,
            "just make sure we are testing some decent variety with this seed" );
    }
    
    private void testRemoveShifting(BiPredicate<Deque<String>, String> remove) {
        factory = new ArrayTempFactory();
        
        ArrayTempList<Integer> bot = factory.newList(singleton(123));
        
        ArrayTempDeque<String> q = factory.newDeque(8);
        
        // testing removal in contiguous range
        
        q.addLast("A");
        q.addLast("B");
        q.addLast("C");
        q.addLast("D");
        q.addLast("E");
        q.addLast("F");
        assertEquals(List.of("A", "B", "C", "D", "E", "F"), q.toArrayList());
        
        ArrayTempList<Integer> top = factory.newList(singleton(456));
        
        Object[] arr =
            newArray(factory.array.length,
                     Mark.INSTANCE, 123,
                     Mark.INSTANCE, "A", "B", "C", "D", "E", "F", null, null,
                     Mark.INSTANCE, 456);
        assertArrayEquals(arr, factory.array);
        
        assertTrue(remove.test( q, "C" ));
        assertEquals(List.of("A", "B", "D", "E", "F"), q.toArrayList());
        
        arr =
            newArray(factory.array.length,
                     Mark.INSTANCE, 123,
                     Mark.INSTANCE, "A", "B", "D", "E", "F", null, null, null,
                     Mark.INSTANCE, 456);
        assertArrayEquals(arr, factory.array);
        
        // testing removal in 0..tail section of non-contiguous range
        
        q.clear();
        q.addLast("D");
        q.addLast("E");
        q.addLast("F");
        q.addLast("G");
        q.addFirst("C");
        q.addFirst("B");
        q.addFirst("A");
        assertEquals(List.of("A", "B", "C", "D", "E", "F", "G"), q.toArrayList());
        
        arr =
            newArray(factory.array.length,
                     Mark.INSTANCE, 123,
                     Mark.INSTANCE, "D", "E", "F", "G", null, "A", "B", "C",
                     Mark.INSTANCE, 456);
        assertArrayEquals(arr, factory.array);
        
        assertTrue(remove.test( q, "E" ));
        assertEquals(List.of("A", "B", "C", "D", "F", "G"), q.toArrayList());
        
        arr =
            newArray(factory.array.length,
                     Mark.INSTANCE, 123,
                     Mark.INSTANCE, "D", "F", "G", null, null, "A", "B", "C",
                     Mark.INSTANCE, 456);
        assertArrayEquals(arr, factory.array);
        
        // testing removal in head..capacity section of non-contiguous range
        
        q.clear();
        q.addLast("E");
        q.addLast("F");
        q.addLast("G");
        q.addFirst("D");
        q.addFirst("C");
        q.addFirst("B");
        q.addFirst("A");
        assertEquals(List.of("A", "B", "C", "D", "E", "F", "G"), q.toArrayList());
        
        arr =
            newArray(factory.array.length,
                     Mark.INSTANCE, 123,
                     Mark.INSTANCE, "E", "F", "G", null, "A", "B", "C", "D",
                     Mark.INSTANCE, 456);
        assertArrayEquals(arr, factory.array);
        
        assertTrue(remove.test( q, "C" ));
        assertEquals(List.of("A", "B", "D", "E", "F", "G"), q.toArrayList());
        
        // noinspection ConstantConditions
        if (false) {
            // assuming we always remove the tail element
            // noinspection UnusedAssignment
            arr =
                newArray(factory.array.length,
                         Mark.INSTANCE, 123,
                         Mark.INSTANCE, "F", "G", null, null, "A", "B", "D", "E",
                         Mark.INSTANCE, 456);
            assertArrayEquals(arr, factory.array);
        } else {
            // assuming we always do the simplest arraycopy
            arr =
                newArray(factory.array.length,
                         Mark.INSTANCE, 123,
                         Mark.INSTANCE, "E", "F", "G", null, null, "A", "B", "D",
                         Mark.INSTANCE, 456);
            assertArrayEquals(arr, factory.array);
        }
        
        // testing removal when head==tail
        q.clear();
        q.addLast("X");
        q.addLast("Y");
        q.addLast("Z");
        q.removeFirst();
        q.removeFirst();
        assertEquals(singletonList("Z"), q.toArrayList());
        
        arr =
            newArray(factory.array.length,
                     Mark.INSTANCE, 123,
                     Mark.INSTANCE, null, null, "Z", null, null, null, null, null,
                     Mark.INSTANCE, 456);
        assertArrayEquals(arr, factory.array);
        
        assertTrue(remove.test( q, "Z" ));
        assertEquals(emptyList(), q.toArrayList());
        assertEquals(ArrayTempDeque.NOT_INDEX, q.head);
        assertEquals(ArrayTempDeque.NOT_INDEX, q.tail);
        
        arr =
            newArray(factory.array.length,
                     Mark.INSTANCE, 123,
                     Mark.INSTANCE, null, null, null, null, null, null, null, null,
                     Mark.INSTANCE, 456);
        assertArrayEquals(arr, factory.array);
        
        //
        
        assertEquals(singletonList(123), bot);
        assertEquals(singletonList(456), top);
    }
    
    @Test
    void testContains() {
        ArrayTempDeque<String> q = factory.newDeque();
        
        assertFalse(q.contains(null));
        assertFalse(q.contains("Abc"));
        
        q.addLast("Abc");
        assertTrue(q.contains("Abc"));
        // noinspection RedundantStringConstructorCall
        assertTrue(q.contains(new String("Abc")));
        q.addFirst(null);
        assertTrue(q.contains(null));
        
        q.removeLast();
        assertFalse(q.contains("Abc"));
        q.removeFirst();
        assertFalse(q.contains(null));
        
        int count = 37;
        
        for (int i = 0; i < count; ++i) {
            String e = "i=" + i;
            assertFalse(q.contains(e));
            q.addLast(e);
            assertTrue(q.contains(e));
        }
        assertEquals(count, q.size());
        
        // double up
        for (int i = 0; i < count; ++i) {
            String e = "i=" + i;
            assertTrue(q.contains(e));
            q.addFirst(e);
            assertTrue(q.contains(e));
        }
        assertEquals(2*count, q.size());
        
        for (int i = count-1; i >= 0; --i) {
            String e = "i=" + i;
            assertTrue(q.contains(e));
            assertEquals(e, q.removeLast());
            assertTrue(q.contains(e));
        }
        assertEquals(count, q.size());
        
        for (int i = count-1; i >= 0; --i) {
            String e = "i=" + i;
            assertTrue(q.contains(e));
            assertEquals(e, q.removeFirst());
            assertFalse(q.contains(e));
        }
        assertEquals(0, q.size());
        assertTrue(q.isEmpty());
        
        for (int i = 0; i < count; ++i) {
            assertFalse(q.contains("i=" + i));
        }
    }
    
    @Test
    void testContainsAll() {
        ArrayTempDeque<String> q = factory.newDeque();
    
        // noinspection RedundantCollectionOperation
        assertFalse(q.containsAll(singleton("Abc")));
        assertTrue(q.containsAll(emptyList()));
        
        // noinspection ConstantConditions, ResultOfMethodCallIgnored
        assertThrows(NullPointerException.class, () -> q.containsAll(null));
        
        q.add("Abc");
        // noinspection RedundantCollectionOperation
        assertTrue(q.containsAll(singleton("Abc")));
        assertFalse(q.containsAll(List.of("Abc", "Xyz")));
        
        q.add("Xyz");
        assertTrue(q.containsAll(List.of("Abc", "Xyz")));
        assertFalse(q.containsAll(List.of("Abc", "Xyz", "Qrs")));
        
        q.add(null);
        // noinspection RedundantCollectionOperation
        assertTrue(q.containsAll(singleton(null)));
        assertTrue(q.containsAll(asList("Xyz", null, "Xyz", "Abc")));
        assertFalse(q.containsAll(asList(null, "Qrs")));
        
        q.remove();
        assertFalse(q.contains("Abc"));
        // noinspection RedundantCollectionOperation
        assertFalse(q.containsAll(singleton("Abc")));
        assertFalse(q.containsAll(List.of("Abc", "Xyz")));
    }
    
    @Test
    void testRemoveAll() {
        ArrayTempDeque<String> q = factory.newDeque();
    
        // noinspection ConstantConditions
        assertThrows(NullPointerException.class, () -> q.removeAll(null));
        
        addAll( q, "A", "B", "C", "D", "E", "F", "G", "H", "I" );
    
        // noinspection SuspiciousMethodCalls
        assertFalse( q.removeAll( emptySet() ) );
        assertEquals( 9, q.size() );
        
        assertTrue( q.removeAll( List.of("B", "G", "H") ) );
        assertEquals( List.of("A", "C", "D", "E", "F", "I"), q.toArrayList() );
        
        // separate in to 2 chunks
        List.of( "P", "Q", "R", "S", "T", "U", "V" ).forEach(q::addFirst);
        assertEquals( List.of("V", "U", "T", "S", "R", "Q", "P", "A", "C", "D", "E", "F", "I"), q.toArrayList() );
        assertTrue( q.tail < q.head );
        
        assertTrue( q.removeAll( List.of("F", "I", "P", "U") ) );
        assertEquals( List.of("V", "T", "S", "R", "Q", "A", "C", "D", "E" ), q.toArrayList() );
        
        assertTrue( q.removeAll( q.toArrayList() ) );
        assertTrue( q.isEmpty() );
    }
    
    @Test
    void testRemoveAllExceptions() {
        TempList<Integer> bot = factory.newList(List.of(6, 0, 1));
        
        int cap = 16;
        ArrayTempDeque<String> q = factory.newDeque(cap);
        
        // setup for test #1
        q.addLast("U");
        q.addLast("V");
        q.addLast("W");
        q.addLast("X");
        q.addLast("Y");
        q.addLast("Z");
        q.addFirst("F");
        q.addFirst("E");
        q.addFirst("D");
        q.addFirst("C");
        q.addFirst("B");
        q.addFirst("A");
        assertEquals(List.of("A", "B", "C", "D", "E", "F", "U", "V", "W", "X", "Y", "Z"), q.toArrayList());
        
        TempList<Integer> top = factory.newList(List.of(7, 3, 2));
        
        Object[] expect =
            newArray(factory.array.length,
                     Mark.INSTANCE, 6, 0, 1,
                     Mark.INSTANCE, "U", "V", "W", "X", "Y", "Z",
                                    null, null, null, null,
                                    "A", "B", "C", "D", "E", "F",
                     Mark.INSTANCE, 7, 3, 2);
        assertArrayEquals(expect, factory.array);
        
        List<?> toRemove1 = new ArrayList<>(List.of("B", "C", "F", "U", "V")) {
            @Override
            public boolean contains(Object o) {
                if ("F".equals(o)) {
                    throw new IllegalArgumentException("F");
                }
                return super.contains(o);
            }
        };
        
        // noinspection SuspiciousMethodCalls
        assertThrows(IllegalArgumentException.class, () -> q.removeAll(toRemove1));
        
        assertEquals(10, q.size());
        // should remove B and C, then throw
        assertEquals(List.of("A", "D", "E", "F", "U", "V", "W", "X", "Y", "Z"), q.toArrayList());
        
        expect =
            newArray(factory.array.length,
                     Mark.INSTANCE, 6, 0, 1,
                     Mark.INSTANCE, "W", "X", "Y", "Z",
                                    null, null, null, null, null, null,
                                    "A", "D", "E", "F", "U", "V",
                     Mark.INSTANCE, 7, 3, 2);
        assertArrayEquals(expect, factory.array);
        
        // setup for test #2
        q.addLast("#");
        q.addLast("@");
        q.addLast("%");
        assertEquals(List.of("A", "D", "E", "F", "U", "V", "W", "X", "Y", "Z", "#", "@", "%"), q.toArrayList());
        expect =
            newArray(factory.array.length,
                     Mark.INSTANCE, 6, 0, 1,
                     Mark.INSTANCE, "W", "X", "Y", "Z", "#", "@", "%",
                                    null, null, null,
                                    "A", "D", "E", "F", "U", "V",
                     Mark.INSTANCE, 7, 3, 2);
        assertArrayEquals(expect, factory.array);
        
        List<?> toRemove2 = new ArrayList<>(List.of("D", "E", "F", "Y", "@")) {
            @Override
            public boolean contains(Object o) {
                if ("Y".equals(o)) {
                    throw new IllegalArgumentException("Y");
                }
                return super.contains(o);
            }
        };
        
        // noinspection SuspiciousMethodCalls
        assertThrows(IllegalArgumentException.class, () -> q.removeAll(toRemove2));
        
        // should remove D, E and F, then throw
        assertEquals(List.of("A", "U", "V", "W", "X", "Y", "Z", "#", "@", "%"), q.toArrayList());
        
        expect =
            newArray(factory.array.length,
                     Mark.INSTANCE, 6, 0, 1,
                     Mark.INSTANCE, "Z", "#", "@", "%",
                                    null, null, null, null, null, null,
                                    "A", "U", "V", "W", "X", "Y",
                     Mark.INSTANCE, 7, 3, 2);
        assertArrayEquals(expect, factory.array);
        
        // setup for test #3
        q.clear();
        addAll(q, "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L");
        assertEquals(12, q.size());
        expect =
            newArray(factory.array.length,
                     Mark.INSTANCE, 6, 0, 1,
                     Mark.INSTANCE, "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L",
                                    null, null, null, null,
                     Mark.INSTANCE, 7, 3, 2);
        assertArrayEquals(expect, factory.array);
        
        List<?> toRemove3 = new ArrayList<>(List.of("B", "D", "E", "F", "G", "J")) {
            @Override
            public boolean contains(Object o) {
                if ("G".equals(o)) {
                    throw new IllegalArgumentException("G");
                }
                return super.contains(o);
            }
        };
        
        // noinspection SuspiciousMethodCalls
        assertThrows(IllegalArgumentException.class, () -> q.removeAll(toRemove3));
        
        // should remove B, D, E and F, then throw
        assertEquals(List.of("A", "C", "G", "H", "I", "J", "K", "L"), q.toArrayList());
        
        expect =
            newArray(factory.array.length,
                     Mark.INSTANCE, 6, 0, 1,
                     Mark.INSTANCE, "A", "C", "G", "H", "I", "J", "K", "L",
                                    null, null, null, null, null, null, null, null,
                     Mark.INSTANCE, 7, 3, 2);
        assertArrayEquals(expect, factory.array);
        
        // end
        assertEquals(List.of(6, 0, 1), bot);
        assertEquals(List.of(7, 3, 2), top);
    }
    
    @Test
    void testRetainAll() {
        ArrayTempDeque<String> q = factory.newDeque();
    
        // noinspection ConstantConditions
        assertThrows(NullPointerException.class, () -> q.retainAll(null));
        
        addAll( q, "A", "B", "C", "D", "E", "F", "G", "H", "I" );
        
        assertFalse( q.retainAll( q.toArrayList() ) );
        assertEquals( 9, q.size() );
        
        assertTrue( q.retainAll( List.of("A", "C", "E") ) );
        assertEquals( List.of("A", "C", "E"), q.toArrayList() );
        
        // separate in to 2 chunks
        List.of( "P", "Q", "R", "S", "T", "U", "V" ).forEach(q::addFirst);
        assertEquals( List.of("V", "U", "T", "S", "R", "Q", "P", "A", "C", "E"), q.toArrayList() );
        assertTrue( q.tail < q.head );
        
        assertTrue( q.retainAll( List.of("C", "V", "S") ) );
        assertEquals( List.of("V", "S", "C"), q.toArrayList() );
        
        assertTrue( q.retainAll( emptySet() ) );
        assertTrue( q.isEmpty() );
    }
    
    @Test
    void testRemoveIf() {
        ArrayTempDeque<String> q = factory.newDeque();
        
        assertThrows(NullPointerException.class, () -> q.removeIf(null));
        
        addAll( q, "A", "Q", "X", "B", "R", "Y", "C", "S", "Z" );
        
        assertFalse( q.removeIf( s -> false ) );
        assertEquals( 9, q.size() );
        
        assertTrue( q.removeIf( s -> s.charAt(0) < 'R' ) );
        assertEquals( List.of("X", "R", "Y", "S", "Z"), q.toArrayList() );
        
        // separate in to 2 chunks
        List.of( "@", "_", "$", "~", "^", "&" ).forEach(q::addFirst);
        assertEquals( List.of("&", "^", "~", "$", "_", "@", "X", "R", "Y", "S", "Z"), q.toArrayList() );
        assertTrue( q.tail < q.head );
        
        assertTrue( q.removeIf( s -> s.charAt(0) < 'Y' ) );
        assertEquals( List.of("^", "~", "_", "Y", "Z"), q.toArrayList() );
        
        assertTrue( q.removeIf( s -> true ) );
        assertTrue( q.isEmpty() );
    }
    
    @Test
    void testToObjectArray() {
        @SuppressWarnings("unused")
        TempObject bot = factory.newList(singleton("Abc"));
        
        ArrayTempDeque<Integer> q = factory.newDeque(4);
        
        assertEquals(0, q.toArray().length);
        assertNotSame(q.toArray(), q.toArray());
        
        q.addLast(1);
        q.addLast(2);
        q.addLast(3);
        
        try (@SuppressWarnings("unused")
             TempObject top = factory.newList(singleton("Xyz"))) {
            assertArrayEquals(new Object[] {1, 2, 3}, q.toArray());
        }
        
        q.addFirst(7);
        q.addFirst(8);
        q.addFirst(9);
        
        try (@SuppressWarnings("unused")
             TempObject top = factory.newList(singleton("Xyz"))) {
            assertArrayEquals(new Object[] {9, 8, 7, 1, 2, 3}, q.toArray());
        }
    }
    
    @Test
    void testToTArray() {
        @SuppressWarnings("unused")
        TempObject bot = factory.newList(singleton("Abc"));
        
        ArrayTempDeque<Integer> q = factory.newDeque();
    
        // noinspection ConstantConditions
        assertThrows(NullPointerException.class, () -> q.toArray(null));
        
        Integer[] intArr;
        
        // empty
        intArr = new Integer[0];
        assertSame(intArr, q.toArray(intArr));
        
        addAll( q, 0, 1, 2, 3, 4 );
        @SuppressWarnings("unused")
        TempObject top = factory.newList(singleton("Xyz"));
        
        // must resize
        assertNotSame(intArr, q.toArray(intArr));
        assertArrayEquals(new Integer[] {0, 1, 2, 3, 4}, q.toArray(intArr));
        
        // must not resize, must set intArr[5] to null
        intArr = new Integer[6];
        Arrays.fill(intArr, 9);
        assertSame(intArr, q.toArray(intArr));
        assertArrayEquals(new Integer[] {0, 1, 2, 3, 4, null}, intArr);
        
        // must not resize, must set intArr[5] to null
        intArr = new Integer[200];
        Arrays.fill(intArr, 9);
        Integer[] expectedInts = new Integer[intArr.length];
        // noinspection RedundantCast
        Arrays.setAll(expectedInts, i -> (i < 5) ? (Integer) i : (i == 5) ? null : (Integer) 9);
        assertSame(intArr, q.toArray(intArr));
        assertArrayEquals(expectedInts, intArr);
        
        // must create a new array of the exact same type
        Comparable<?>[] compArr = new Comparable<?>[3];
        assertNotSame(compArr, q.toArray(compArr));
        compArr = q.toArray(compArr);
        assertEquals(Comparable[].class, compArr.getClass());
        assertArrayEquals(new Comparable<?>[] {0, 1, 2, 3, 4}, compArr);
        
        // noinspection SuspiciousToArrayCall
        assertThrows(ArrayStoreException.class, () -> q.toArray(new String[0]));
        q.clear();
        String[] strArr = new String[1];
        // noinspection SuspiciousToArrayCall
        assertSame(strArr, q.toArray(strArr)); // can't reasonably throw ASE
    }
    
    @Test
    void testToTArrayAllPaths() {
        @SuppressWarnings("unused")
        TempObject bot = factory.newList(singleton("Abc"));
        
        ArrayTempDeque<Integer> q = factory.newDeque(4);
        q.addLast(1);
        q.addLast(2);
        q.addLast(3);
        
        try (@SuppressWarnings("unused")
             TempObject top = factory.newList(singleton("Xyz"))) {
            
            // inLength < size, head <= tail
            Number[] numArr = new Number[] {9, 9};
            
            assertTrue(numArr.length < q.size());
            assertTrue(q.head <= q.tail);
            
            assertNotSame(numArr, q.toArray(numArr));
            assertEquals(Number[].class, q.toArray(numArr).getClass());
            assertArrayEquals(new Number[] {1, 2, 3}, q.toArray(numArr));
            assertArrayEquals(new Number[] {9, 9}, numArr);
            
            // inLength >= size, head <= tail
            Integer[] intArr = new Integer[3];
            
            assertTrue(intArr.length >= q.size());
            assertTrue(q.head <= q.tail);
            
            assertSame(intArr, q.toArray(intArr));
            assertArrayEquals(new Integer[] {1, 2, 3}, intArr);
        }
        
        q.addFirst(-1);
        q.addFirst(-2);
        q.addFirst(-3);
        
        try (@SuppressWarnings("unused")
             TempObject top = factory.newList(singleton("Xyz"))) {
            
            // inLength >= size, head > tail
            Integer[] intArr = new Integer[8];
            Arrays.fill(intArr, 1000);
            
            assertTrue(intArr.length >= q.size());
            assertFalse(q.head <= q.tail);
            
            assertSame(intArr, q.toArray(intArr));
            assertArrayEquals(new Integer[] {-3, -2, -1, 1, 2, 3, null, 1000}, intArr);
            
            // inLength < size, head > tail
            Comparable<?>[] compArr = new Comparable<?>[] {8};
            
            assertTrue(compArr.length < q.size());
            assertFalse(q.head <= q.tail);
            
            assertNotSame(compArr, q.toArray(compArr));
            assertEquals(Comparable[].class, q.toArray(compArr).getClass());
            assertArrayEquals(new Comparable<?>[] {-3, -2, -1, 1, 2, 3}, q.toArray(compArr));
            assertArrayEquals(new Comparable<?>[] {8}, compArr);
        }
    }
    
    @Test
    void testToArraySetElementToNullWhenEmpty() {
        ArrayTempDeque<String> q = factory.newDeque();
        
        String[] arr = {"test"};
        q.toArray(arr);
        
        assertNull(arr[0]);
    }
    
    @Test
    void testToArrayList() {
        ArrayTempDeque<String> q = factory.newDeque();
        assertEquals(new ArrayList<>(), q.toArrayList());
        
        q.addLast("X");
        q.addLast("Y");
        q.addLast("Z");
        assertEquals(List.of("X", "Y", "Z"), q.toArrayList());
        
        q.addFirst("C");
        q.addFirst("B");
        q.addFirst("A");
        assertEquals(List.of("A", "B", "C", "X", "Y", "Z"), q.toArrayList());
    }
    
    @Test
    void testIterator() {
        ArrayTempDeque<?> q = factory.newDeque(List.of(1, 2, 3));
        
        ArrayTempDequeIterator.Ascending<?> it = (ArrayTempDequeIterator.Ascending<?>) q.iterator();
        assertNotNull(it);
        assertSame(q, it.q);
        
        for (int i = 0; i < q.size(); ++i) {
            assertTrue(it.hasNext());
            it.next();
        }
        
        assertNotSame(q.iterator(), q.iterator());
    }
    
    @Test
    void testDescendingIterator() {
        ArrayTempDeque<?> q = factory.newDeque(List.of(1, 2, 3));
        
        ArrayTempDequeIterator.Descending<?> it = (ArrayTempDequeIterator.Descending<?>) q.descendingIterator();
        assertNotNull(it);
        assertSame(q, it.q);
        
        for (int i = 0; i < q.size(); ++i) {
            assertTrue(it.hasNext());
            it.next();
        }
        
        assertNotSame(q.iterator(), q.iterator());
    }
    
    @Test
    void testForEach() {
        int count = 10;
        ArrayTempDeque<Integer> q = factory.newDeque(2*count);
        
        assertThrows(NullPointerException.class, () -> q.forEach(null));
        
        for (int i = 0; i < count; ++i)
            q.addLast(i);
        
        int[] idx = new int[] {0};
        q.forEach(e -> assertEquals(idx[0]++, (int) e, q::toString));
        assertEquals(count, idx[0]);
        
        for (int i = 1; i <= count; ++i)
            q.addFirst(-i);
        
        idx[0] = -count;
        q.forEach(e -> assertEquals(idx[0]++, (int) e, q::toString));
        assertEquals(count, idx[0]);
    }
    
    @Test
    void testToString() {
        ArrayTempDeque<String> q = factory.newDeque();
        assertEquals("[]", q.toString());
        
        q.addFirst("Y");
        assertEquals("[Y]", q.toString());
        
        q.addFirst("X");
        assertEquals("[X, Y]", q.toString());
        
        q.addLast("Z");
        assertEquals("[X, Y, Z]", q.toString());
    }
}