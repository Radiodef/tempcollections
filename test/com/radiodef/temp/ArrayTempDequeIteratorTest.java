/* MIT License
 *
 * Copyright (c) 2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.radiodef.temp;

import com.radiodef.temp.ArrayTempDequeIterator.*;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

import java.util.*;
import static java.util.Collections.*;

class ArrayTempDequeIteratorTest {
    
    private ArrayTempFactory factory;
    private ArrayTempDeque<String> q;
    
    @BeforeEach
    void initNewFactoryAndQ() {
        factory = new ArrayTempFactory(10);
        q = factory.newDeque();
    }
    
    @AfterEach
    void clearFactoryAndQ() {
        factory = null;
        q = null;
    }
    @Test
    void testAscendingConstructor() {
        Ascending<String> it;
        
        it = new Ascending<>(q);
        assertSame(q, it.q);
        assertFalse(it.hasNext());
        
        addAll(q, "A", "B", "C");
        
        it = new Ascending<>(q);
        assertSame(q, it.q);
        assertTrue(it.hasNext());
    }
    
    @Test
    void testDescendingConstructor() {
        Descending<String> it;
        
        it = new Descending<>(q);
        assertSame(q, it.q);
        assertFalse(it.hasNext());
        
        addAll(q, "A", "B", "C");
        
        it = new Descending<>(q);
        assertSame(q, it.q);
        assertTrue(it.hasNext());
    }
    
    @Test
    void testAscendingSimpleIteration() {
        Ascending<String> it;
        
        it = (Ascending<String>) q.iterator();
        
        assertFalse(it.hasNext());
        assertThrows(NoSuchElementException.class, it::next);
        
        String[] elements = {"A", "B", "C", "D", "E", "F"};
        q.addFirst("C");
        q.addFirst("B");
        q.addFirst("A");
        q.addLast("D");
        q.addLast("E");
        q.addLast("F");
        
        it = (Ascending<String>) q.iterator();
        
        for (String e : elements) {
            assertTrue(it.hasNext());
            assertEquals(e, it.next());
        }
        
        assertFalse(it.hasNext());
        assertThrows(NoSuchElementException.class, it::next);
    }
    
    @Test
    void testDescendingSimpleIteration() {
        Descending<String> it;
        
        it = (Descending<String>) q.descendingIterator();
        
        assertFalse(it.hasNext());
        assertThrows(NoSuchElementException.class, it::next);
        
        String[] elements = {"F", "E", "D", "C", "B", "A"};
        q.addFirst("C");
        q.addFirst("B");
        q.addFirst("A");
        q.addLast("D");
        q.addLast("E");
        q.addLast("F");
        
        it = (Descending<String>) q.descendingIterator();
        
        for (String e : elements) {
            assertTrue(it.hasNext());
            assertEquals(e, it.next());
        }
        
        assertFalse(it.hasNext());
        assertThrows(NoSuchElementException.class, it::next);
    }
    
    @Test
    void testAscendingIteratorWhenFull() {
        q = factory.newDeque(4);
        addAll(q, "A", "B", "C", "D");
        assertEquals(4, q.size());
        assertEquals(4, q.capacity());
        
        Ascending<String> it = (Ascending<String>) q.iterator();
        for (int i = 0; i < 4; ++i)
            it.next();
        
        assertFalse(it.hasNext());
    }
    
    @Test
    void testDescendingIteratorWhenFull() {
        q = factory.newDeque(4);
        addAll(q, "A", "B", "C", "D");
        assertEquals(4, q.size());
        assertEquals(4, q.capacity());
        
        Descending<String> it = (Descending<String>) q.descendingIterator();
        for (int i = 0; i < 4; ++i)
            it.next();
        
        assertFalse(it.hasNext());
    }
    
    @Test
    void testAscendingRemove() {
        clearFactoryAndQ();
        factory = new ArrayTempFactory();
        
        ArrayTempList<Integer> bot = factory.newList(singleton(333));
        
        List<String> elements = List.of("A", "B", "C", "D", "E", "F");
        q = factory.newDeque();
        q.addAll(elements);
        
        ArrayTempList<Integer> top = factory.newList(singleton(666));
        
        Ascending<String> it = (Ascending<String>) q.iterator();
        
        assertThrows(IllegalStateException.class, it::remove);
        
        for (String e : elements) {
            assertTrue(it.hasNext());
            assertEquals(e, it.next());
            
            it.remove();
            assertFalse(q.contains(e));
            
            assertThrows(IllegalStateException.class, it::remove);
        }
        
        assertFalse(it.hasNext());
        assertTrue(q.isEmpty());
        
        {
            Object[] a = new Object[factory.array.length];
            int i = 0;
            a[i++] = Mark.INSTANCE;
            a[i++] = 333;
            a[i++] = Mark.INSTANCE;
            assertEquals(q.offset, i);
            i = top.offset-1;
            a[i++] = Mark.INSTANCE;
            a[i++] = 666;
            assertEquals(top.offset + top.size, i);
            assertArrayEquals(a, factory.array);
        }
        
        q.addLast("X");
        q.addLast("Y");
        q.addLast("Z");
        q.addFirst("C");
        q.addFirst("B");
        q.addFirst("A");
        assertTrue(q.tail < q.head);
        assertEquals(List.of("A", "B", "C", "X", "Y", "Z"), q.toArrayList());
        
        it = (Ascending<String>) q.iterator();
        assertEquals("A", it.next());
        assertEquals("B", it.next());
        
        it.remove();
        assertEquals(List.of("A", "C", "X", "Y", "Z"), q.toArrayList());
        
        assertThrows(IllegalStateException.class, it::remove);
        
        assertEquals("C", it.next());
        assertEquals("X", it.next());
        assertEquals("Y", it.next());
        
        it.remove();
        assertEquals(List.of("A", "C", "X", "Z"), q.toArrayList());
        
        assertEquals("Z", it.next());
        
        assertFalse(it.hasNext());
        
        assertEquals(singletonList(333), bot);
        assertEquals(singletonList(666), top);
    }
    
    @Test
    void testDescendingRemove() {
        clearFactoryAndQ();
        factory = new ArrayTempFactory();
        
        ArrayTempList<Integer> bot = factory.newList(singleton(333));
        
        List<String> elements = new ArrayList<>(List.of("A", "B", "C", "D", "E", "F"));
        q = factory.newDeque();
        q.addAll(elements);
        
        ArrayTempList<Integer> top = factory.newList(singleton(666));
        
        reverse(elements);
        Descending<String> it = (Descending<String>) q.descendingIterator();
        
        assertThrows(IllegalStateException.class, it::remove);
        
        for (String e : elements) {
            assertTrue(it.hasNext());
            assertEquals(e, it.next());
            
            it.remove();
            assertFalse(q.contains(e));
            
            assertThrows(IllegalStateException.class, it::remove);
        }
        
        assertFalse(it.hasNext());
        assertTrue(q.isEmpty());
        
        {
            Object[] a = new Object[factory.array.length];
            int i = 0;
            a[i++] = Mark.INSTANCE;
            a[i++] = 333;
            a[i++] = Mark.INSTANCE;
            assertEquals(q.offset, i);
            i = top.offset-1;
            a[i++] = Mark.INSTANCE;
            a[i++] = 666;
            assertEquals(top.offset + top.size, i);
            assertArrayEquals(a, factory.array);
        }
        
        q.addLast("X");
        q.addLast("Y");
        q.addLast("Z");
        q.addFirst("C");
        q.addFirst("B");
        q.addFirst("A");
        assertTrue(q.tail < q.head);
        assertEquals(List.of("A", "B", "C", "X", "Y", "Z"), q.toArrayList());
        
        it = (Descending<String>) q.descendingIterator();
        assertEquals("Z", it.next());
        assertEquals("Y", it.next());
        
        it.remove();
        assertEquals(List.of("A", "B", "C", "X", "Z"), q.toArrayList());
        
        assertThrows(IllegalStateException.class, it::remove);
        
        assertEquals("X", it.next());
        assertEquals("C", it.next());
        assertEquals("B", it.next());
        
        it.remove();
        assertEquals(List.of("A", "C", "X", "Z"), q.toArrayList());
        
        assertEquals("A", it.next());
        
        assertFalse(it.hasNext());
        
        assertEquals(singletonList(333), bot);
        assertEquals(singletonList(666), top);
    }
}