/* MIT License
 *
 * Copyright (c) 2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.radiodef.temp;

import static com.radiodef.temp.Tools.*;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

import java.util.*;
import java.util.stream.*;
import java.util.function.*;
import static java.util.Collections.*;
import static java.util.function.Predicate.*;
import static java.util.stream.Collectors.*;

class ArrayTempHashSetTest {
    
    private ArrayTempFactory factory;
    
    @BeforeEach
    void initNewFactory() {
        factory = new ArrayTempFactory(10);
    }
    
    @AfterEach
    void clearFactory() {
        factory = null;
    }
    
    @Test
    void testConstructor() {
        ArrayTempHashSet<?> set = factory.newSet(10);
        
        assertEquals(1, set.offset);
        assertEquals(16, set.capacity);
        assertTrue(set.isEmpty());
    }
    
    @Test
    void testZeroCapacity() {
        factory = new ArrayTempFactory(0);
        ArrayTempHashSet<Long> set = factory.newSet(0);
        
        assertEquals(1, set.capacity);
    }
    
    @Test
    void testFPRound() {
        int n = ArrayTempHashSet.MAX_CAPACITY;
        
        assertEquals(n, (int) (1.0f * n));
        assertTrue((int) (Math.nextDown(1.0f) * n) <= n);
    }
    
    @Test
    void testAdd() {
        ArrayTempHashSet<String> set = factory.newSet();
        
        assertTrue(set.add("A"));
        assertFalse(set.add("A"));
        
        assertEquals(Set.of("A"), set);
        
        assertTrue(set.add("B"));
        assertFalse(set.add("B"));
        
        assertEquals(Set.of("A", "B"), set);
        
        assertTrue(set.add(null));
        assertFalse(set.add(null));
        
        Set<String> expect = new HashSet<>(Arrays.asList("A", "B", null));
        assertEquals(expect, set);
        
        for (int i = 0; i < set.capacity; ++i) {
            Object elem = factory.getDirect(set, i);
            if (elem != null) {
                // noinspection SuspiciousMethodCalls
                assertTrue(expect.remove(Nil.unbox(elem)));
            }
        }
        
        assertTrue(expect.isEmpty());
        
        // add(..) after remove(..)
        assertTrue(set.remove("A"));
        assertFalse(set.contains("A"));
        assertTrue(set.add("A"));
    }
    
    @Test
    void testAddWhenNotTop() {
        ArrayTempList<String> bot = factory.newList(List.of("A", "B", "C"));
        
        int setSize = 16;
        ArrayTempHashSet<String> set = factory.newSet(setSize, 1.0f);
        assertEquals(setSize, set.capacity);
        
        assertTrue(set.canAdd(0));
        assertTrue(set.canAdd(setSize));
        assertTrue(set.canAdd(setSize + 1));
        
        try (ArrayTempList<String> top = factory.newList(List.of("X", "Y", "Z"))) {
            assertTrue(set.canAdd(setSize));
            assertFalse(set.canAdd(setSize + 1));
            
            for (int i = 0; i < setSize; ++i) {
                assertTrue(set.canAdd(1));
                assertTrue(set.canAdd(setSize - i));
                assertFalse(set.canAdd(setSize - i + 1));
                
                set.add(Integer.toString(i));
            }
            
            assertEquals(List.of("A", "B", "C"), bot);
            assertEquals(List.of("X", "Y", "Z"), top);
            
            assertFalse(set.canAdd(1));
            assertTrue(set.canAdd(0));
            
            // throws test
            Set<String> copy = new LinkedHashSet<>(set);
            assertThrows(IllegalStateException.class, () -> set.add("999"));
            assertFalse(set.add("0"));
            
            assertEquals(copy, set);
            assertEquals(List.of("A", "B", "C"), bot);
            assertEquals(List.of("X", "Y", "Z"), top);
            
            // array test
            Object[] expect = new Object[factory.array.length];
            int i = 0;
            expect[i++] = Mark.INSTANCE;
            expect[i++] = "A";
            expect[i++] = "B";
            expect[i++] = "C";
            expect[i++] = Mark.INSTANCE;
            for (String e : set)
                expect[i++] = e;
            expect[i++] = Mark.INSTANCE;
            expect[i++] = "X";
            expect[i++] = "Y";
            expect[i++] = "Z";
            
            assertEquals(top.offset + top.size, i);
            assertArrayEquals(expect, factory.array);
        }
        
        assertTrue(set.canAdd(1));
        assertEquals(setSize, set.capacity);
        assertTrue(set.add("999"));
        assertNotEquals(setSize, set.capacity);
        assertTrue(set.canAdd(1));
    }
    
    @Test
    void testResizeFail() {
        ArrayTempHashSet<String> set = factory.newSet(0, 1.0f);
        
        try (ArrayTempList<Long> top = factory.newList()) {
            top.add(0L);
            
            assertTrue(set.add("X"));
            assertFalse(set.add("X"));
            
            assertThrows(IllegalStateException.class, () -> set.add("Y"));
            
            assertArrayEquals(newArray(factory.array.length, Mark.INSTANCE, "X", Mark.INSTANCE, 0L),
                factory.array);
        }
        
        assertTrue(set.add("Y"));
    }
    
    @Test
    void testResizeWithBackingArray() {
        testResizeWithBackingArray(false);
        testResizeWithBackingArray(true);
    }
    
    private void testResizeWithBackingArray(boolean arrayResize) {
        factory = new ArrayTempFactory(arrayResize ? 42 : 1024);
        
        int cap = 16;
        ArrayTempHashSet<Integer> set = factory.newSet(cap / 2, 0.5f);
        assertEquals(cap, set.capacity);
        
        int half = cap / 2;
        
        // this should fill the first half of the array exactly
        for (int i = 0; i < half; ++i)
            assertTrue(set.add(i));
        assertEquals(cap, set.capacity);
        assertEquals(IntStream.range(0, half).boxed().collect(toSet()), set);
        
        // and this should fill the first half of the array with deletions
        Iterator<Integer> it = set.iterator();
        for (int i = 0; i < half; ++i, it.remove())
            it.next();
        assertEquals(0, set.size);
        assertEquals(emptySet(), set);

        Object[] array = factory.array;
        int offset = set.offset;
        
        assertTrue( Arrays.stream(array, offset, offset + half)
                          .allMatch(isEqual(Del.INSTANCE)) );
        assertTrue( Arrays.stream(array, offset + half, offset + cap)
                          .allMatch(Objects::isNull) );
        
        // this should fill the second half of the array exactly
        for (int i = half; i < cap; ++i)
            assertTrue(set.add(i));
        assertEquals(cap, set.capacity);
        assertEquals(IntStream.range(half, cap).boxed().collect(toSet()), set);
        
        assertTrue( Arrays.stream(array, offset, offset + half)
                          .allMatch(isEqual(Del.INSTANCE)) );
        assertTrue( Arrays.stream(array, offset + half, offset + cap)
                          .allMatch(Integer.class::isInstance) );
        
        // cause a rehash
        assertTrue(set.add(cap));
        assertEquals(IntStream.range(half, cap + 1).boxed().collect(toSet()), set);
        
        assertEquals(arrayResize, array != factory.array);
        array = factory.array;
        
        assertNotEquals(cap, set.capacity);
        cap = set.capacity;
        
        // rehash should remove deletion marks
        assertTrue( Arrays.stream(array, offset, offset + cap)
                          .noneMatch(isEqual(Del.INSTANCE)) );
        
        int size = half + 1;
        assertEquals(size, set.size);
        
        assertEquals( (cap - size), Arrays.stream(array, offset, offset + cap)
                                          .filter(Objects::isNull)
                                          .count() );
        assertEquals( size, Arrays.stream(array, offset, offset + cap)
                                  .filter(Integer.class::isInstance)
                                  .count() );
    }
    
    @Test
    void testAddsAboveLoadFactorWhenCapacityLimited() {
        int cap = 8;
        ArrayTempHashSet<String> set = factory.newSet(cap / 2, 0.5f);
        assertEquals(cap, set.capacity);
        
        assertTrue(set.canAdd(0));
        assertTrue(set.canAdd(cap));
        assertTrue(set.canAdd(cap + 1));
        
        try (ArrayTempList<Integer> top = factory.newList(List.of(6, 0, 1))) {
            for (int i = 0; i < cap; ++i) {
                assertTrue(set.canAdd(1));
                assertTrue(set.canAdd(cap - i));
                assertFalse(set.canAdd(cap - i + 1));
                
                assertTrue(set.add("i=" + i));
                
                assertEquals(cap, set.capacity);
            }
            
            assertTrue(set.canAdd(0));
            assertFalse(set.canAdd(1));
            
            assertThrows(IllegalStateException.class, () -> set.add("i=" + cap));
            assertEquals(List.of(6, 0, 1), top);
        }
        
        assertTrue(set.canAdd(1));
        
        assertTrue(set.add("i=" + cap));
        assertEquals(IntStream.rangeClosed(0, cap).mapToObj(i -> "i=" + i).collect(toSet()), set);
        
        assertNotEquals(cap, set.capacity);
        
        assertTrue(set.canAdd(1));
    }
    
    @Test
    void testAddWithDeletions() {
        ArrayTempHashSet<String> set = factory.newSet();
        
        int count = 256;
        
        for (int i = 0; i < count; ++i) {
            assertTrue(set.add("i=" + i));
        }
        
        int cap = set.capacity;
        
        Set<String> copy = new LinkedHashSet<>(set);
        List<String> elements = new ArrayList<>(set);
        shuffle(elements);
        
        int subCount = 16;
        assertEquals(0, count % subCount);
        
        for (int i = 0; i < (count / subCount); ++i) {
            List<String> toRemove = new ArrayList<>();
            for (int j = 0; j < subCount; ++j) {
                toRemove.add(elements.remove(elements.size() - 1));
            }
            
            assertTrue(set.removeAll(toRemove));
            assertEquals(count - subCount, set.size());
            
            shuffle(toRemove);
            
            for (String e : toRemove) {
                assertFalse(set.contains(e));
                assertTrue(set.add(e));
                assertTrue(set.contains(e));
            }
            
            assertEquals(count, set.size());
        }
        
        for (String s : copy) {
            assertTrue(set.contains(s), "all elements should still be accessible");
        }
        
        assertEquals(cap, set.capacity, "should not have resized");
        assertEquals(copy, set);
    }
    
    @Test
    void testAddAndResize() {
        ArrayTempList<String> bottom = factory.newList(List.of("A", "B", "C"));
        
        ArrayTempHashSet<String> set = factory.newSet(0);
        
        int max = Short.MAX_VALUE;
        
        int capacity = set.capacity;
        int resizeCount = 0;
        
        Set<String> expect = new HashSet<>();
        Random rand = new Random();
        
        for (int i = 0; i < max; ++i) {
            String e = i + "#" + rand.nextInt();
            
            assertTrue(set.add(e));
            assertFalse(set.add(e));
            
            expect.add(e);
            
            if (capacity != set.capacity) {
                ++resizeCount;
                capacity = set.capacity;
                
                assertEquals(expect, set);
            }
        }
        
        assertEquals(max, set.size());
        assertNotEquals(0, resizeCount);
        
        assertEquals(expect, set);
        
        assertTrue(factory.array.length >= 1 + max);
        
        for (int i = 0; i < set.capacity; ++i) {
            Object elem = factory.getDirect(set, i);
            
            if (elem != null) {
                // noinspection RedundantCast
                assertTrue(expect.remove((String) elem));
            }
        }
        
        assertTrue(expect.isEmpty(), expect::toString);
        
        assertArrayEquals(new Object[] { Mark.INSTANCE, "A", "B", "C", Mark.INSTANCE },
                          Arrays.copyOf(factory.array, set.offset));
        assertEquals(List.of("A", "B", "C"), bottom);
    }
    
    @Test
    void testAddAll() {
        ArrayTempHashSet<String> set = factory.newSet();
        
        assertFalse(set.addAll(emptySet()));
        // noinspection ConstantConditions
        assertThrows(NullPointerException.class, () -> set.addAll(null));
        
        assertTrue(set.addAll(List.of("X", "Y", "Z")));
        assertEquals(Set.of("X", "Y", "Z"), set);
        
        assertFalse(set.addAll(List.of("X", "Y", "Z")));
        
        assertTrue(set.addAll(List.of("P", "Q", "X")));
        assertEquals(Set.of("P", "Q", "X", "Y", "Z"), set);
        
        assertFalse(set.addAll(emptySet()));
        
        // test resize
        set.clear();
        
        List<String> toAdd = IntStream.range(0, Short.MAX_VALUE).mapToObj(String::valueOf).collect(toList());
        
        ArrayTempHashSet.REHASH_COUNT = 0;
        
        assertTrue(set.addAll(toAdd));
        
        if (ArrayTempHashSet.TESTING) {
            assertEquals(1, ArrayTempHashSet.REHASH_COUNT);
        }
        
        assertTrue(set.containsAll(toAdd));
        // noinspection AssertEqualsBetweenInconvertibleTypes
        assertEquals(new HashSet<>(toAdd), set);
    }
    
    @Test
    void testAddAllWhenCapacityFull() {
        int size = 8;
        ArrayTempHashSet<String> set = factory.newSet(size, 1.0f);
        assertEquals(size, set.capacity);
        
        int dif = 2;
        for (int i = dif; i < size; ++i)
            set.add("i=" + i);
        Set<String> expect = new LinkedHashSet<>(set);
        
        try (TempList<?> top = factory.newList(singletonList(601))) {
            assertTrue(set.canAdd(2));
            assertFalse(set.canAdd(3));
            
            // throws at Z
            assertThrows(IllegalStateException.class, () -> set.addAll(List.of("X", "Y", "Z")));
            assertNotEquals(expect, set);
            assertEquals(size, set.size());
            expect.add("X");
            expect.add("Y");
            assertEquals(expect, set);
            
            set.remove("X");
            set.remove("Y");
            assertEquals(size - dif, set.size());
            
            // this should be fine
            assertTrue(set.addAll(List.of(set.iterator().next(), "X", "X", "Y")));
            assertEquals(size, set.size());
            assertEquals(expect, set);
            
            assertEquals(singletonList(601), top);
        }
    }
    
    @Test
    void testRemoveObject() {
        ArrayTempList<Long> bot = factory.newList(List.of(6L, 0L, 1L));
        
        ArrayTempHashSet<String> set = factory.newSet();
        
        assertFalse(set.remove(null));
        assertFalse(set.remove("D"));
        addAll(set, "A", "B", "C", "D", "E", "F", "P", "Q", "R", "X", "Y", "Z");
        
        ArrayTempList<Long> top = factory.newList(List.of(7L, 1L, 2L));
        
        assertTrue(set.remove("D"));
        assertFalse(set.contains("D"));
        
        assertEquals(Set.of("A", "B", "C", "E", "F", "P", "Q", "R", "X", "Y", "Z"), set);
        
        assertTrue(set.add(null));
        assertTrue(set.contains(null));
        assertTrue(set.remove(null));
        
        assertEquals(Set.of("A", "B", "C", "E", "F", "P", "Q", "R", "X", "Y", "Z"), set);
        
        for (String s : new ArrayList<>(set)) {
            assertTrue(set.contains(s));
            assertTrue(set.remove(s));
            assertFalse(set.contains(s));
            
            Object[] expect = new Object[factory.array.length];
            int i = 0;
            expect[i++] = Mark.INSTANCE;
            expect[i++] = 6L;
            expect[i++] = 0L;
            expect[i++] = 1L;
            expect[i++] = Mark.INSTANCE;
            for (int j = 0; j < set.capacity; ++j)
                expect[i++] = factory.getDirect(set, j);
            expect[i++] = Mark.INSTANCE;
            expect[i++] = 7L;
            expect[i++] = 1L;
            expect[i++] = 2L;
            
            assertEquals(top.offset + top.capacity, i);
            assertArrayEquals(expect, factory.array);
        }
        
        assertTrue(set.isEmpty());
        
        assertEquals(List.of(6L, 0L, 1L), bot);
        assertEquals(List.of(7L, 1L, 2L), top);
    }
    
    @Test
    void testCircularSearch() {
        // This is testing an issue where the table is full
        // and the search algorithm is checking something like
        // while (array[i] != null) ...; and never checks if
        // the search has made a full circle.
        ArrayTempHashSet<String> set = factory.newSet(16, 1.0f);
        
        for (int i = 0; set.size < set.capacity; ++i) {
            assertTrue(set.add("i=" + i));
        }
        
        // Both of these calls should return.
        assertFalse(set.contains("hi"));
        assertFalse(set.remove("hi"));
    }
    
    @Test
    void testRemoveLarge() {
        ArrayTempHashSet<String> set = factory.newSet();
        
        int count = 3001;
        
        List<String> elements = new ArrayList<>(count);
        Random rand = new Random();
        
        for (int i = 0; i < count; ++i) {
            String s = i + "#" + Integer.toHexString(rand.nextInt());
            assertTrue(set.add(s));
            elements.add(s);
        }
        
        shuffle(elements, rand);
        
        for (int i = 0; i < count; ++i) {
            String s = elements.get(i);
            
            assertTrue(set.contains(s));
            assertTrue(set.remove(s));
            assertFalse(set.contains(s));
            
            // every other element should still be findable
            for (int other = i + 1; other < count; ++other) {
                assertTrue(set.contains( elements.get(other) ));
            }
        }
        
        assertTrue(set.isEmpty());
        
        int i = 0;
        assertEquals(Mark.INSTANCE, factory.array[i++]);
        
        for (; i < factory.array.length; ++i) {
            assertNull(factory.array[i]);
        }
    }
    
    private static class BadList extends ArrayList<String> {
        private final List<String> removed = new ArrayList<>();
        private final int throwAfter;
        private final Class<? extends RuntimeException> type;
        private final boolean containsResult;
        
        private BadList(List<String> elements,
                        int throwAfter,
                        Class<? extends RuntimeException> type,
                        boolean containsResult) {
            super(elements);
            this.throwAfter = throwAfter;
            this.type = type;
            this.containsResult = containsResult;
        }
        
        @Override
        public boolean contains(Object obj) {
            if (removed.size() < throwAfter) {
                if (super.contains(obj) == containsResult) {
                    removed.add((String) obj);
                    return containsResult;
                }
                return !containsResult;
            }
            
            try {
                throw type.getConstructor().newInstance();
            } catch (ReflectiveOperationException x) {
                throw new RuntimeException(x);
            }
        }
    }
    
    @Test
    void testRemoveAll() {
        ArrayTempHashSet<String> set = factory.newSet();
    
        // noinspection SuspiciousMethodCalls
        assertFalse(set.removeAll(emptySet()));
        assertFalse(set.removeAll(Set.of("601")));
        // noinspection ConstantConditions
        assertThrows(NullPointerException.class, () -> set.removeAll(null));
        
        addAll(set, "A", "B", "C", "D", "E", "F", "P", "Q", "R", "X", "Y", "Z");
        
        // 1
        assertTrue(set.removeAll(List.of("C", "D", "E")));
        
        List.of("C", "D", "E")
            .forEach(s -> assertFalse(set.contains(s), s));
        assertEquals(Set.of("A", "B", "F", "P", "Q", "R", "X", "Y", "Z"), set);
        
        assertFalse(set.removeAll(List.of("C", "D", "E")));
        
        // 2
        assertTrue(set.removeAll(List.of("B", "F", "P", "601")));
        
        List.of("B", "F", "P", "601")
            .forEach(s -> assertFalse(set.contains(s), s));
        assertEquals(Set.of("A", "Q", "R", "X", "Y", "Z"), set);
        
        // 3
        assertTrue(set.removeAll(new ArrayList<>(set)));
        assertTrue(set.isEmpty());
        
        // when Collection.contains(..) throws
        
        addAll(set, "11", "22", "33", "44", "55", "66", "77", "88", "99", "00");
        Set<String> before = new LinkedHashSet<>(set);
        
        int r = 3;
        
        BadList badList = new BadList(List.of("33", "44", "55", "66", "77", "88"),
                                      r,
                                      ClassCastException.class,
                                      true);
        
        assertThrows(ClassCastException.class, () -> set.removeAll(badList));
        
        assertNotEquals(new HashSet<>(badList), new HashSet<>(badList.removed),
            "should not have removed all the elements");
        assertEquals(r, badList.removed.size());
        
        assertEquals(before.size() - r, set.size,
            "size should be different and reflect removals exactly");
        
        for (String removed : badList.removed) {
            assertFalse(set.contains(removed), "must be removed");
        }
        
        before.removeAll(badList.removed);
        assertEquals(before, set);
    }
    
    @Test
    void testRetainAll() {
        ArrayTempHashSet<String> set = factory.newSet();
    
        // noinspection SuspiciousMethodCalls
        assertFalse(set.retainAll(new ArrayList<>(set)));
        // noinspection ConstantConditions
        assertThrows(NullPointerException.class, () -> set.retainAll(null));
        
        addAll(set, "A", "B", "C", "D", "E", "F", "P", "Q", "R", "X", "Y", "Z");
        
        // 1
        assertTrue(set.retainAll(List.of("A", "B", "F", "P", "Q", "R", "X", "Y", "Z"))); // not C,D,E
        
        List.of("C", "D", "E")
            .forEach(s -> assertFalse(set.contains(s), s));
        assertEquals(Set.of("A", "B", "F", "P", "Q", "R", "X", "Y", "Z"), set);
        
        assertFalse(set.retainAll(List.of("A", "B", "F", "P", "Q", "R", "X", "Y", "Z"))); // not C,D,E
        
        // 2
        assertTrue(set.retainAll(List.of("A", "Q", "R", "X", "Y", "Z"))); // not B,F,P
        
        List.of("B", "F", "P")
            .forEach(s -> assertFalse(set.contains(s), s));
        assertEquals(Set.of("A", "Q", "R", "X", "Y", "Z"), set);
        
        // 3
        assertTrue(set.retainAll(emptyList()));
        assertTrue(set.isEmpty());
        
        // when Collection.contains(..) throws
        
        addAll(set, "11", "22", "33", "44", "55", "66", "77", "88", "99", "00");
        Set<String> before = new LinkedHashSet<>(set);
        
        int r = 3;
        
        BadList badList = new BadList(List.of("11", "22", "99", "00"), // not 33,44,55,66,77,88
                                      r,
                                      ClassCastException.class,
                                      false);
        
        assertThrows(ClassCastException.class, () -> set.retainAll(badList));
        
        assertNotEquals(Set.of("33", "44", "55", "66", "77", "88"), new HashSet<>(badList.removed),
            "should not have removed all the elements");
        assertEquals(r, badList.removed.size());
        
        assertEquals(before.size() - r, set.size,
            "size should be different and reflect removals exactly");
        
        for (String removed : badList.removed) {
            assertFalse(set.contains(removed), "must be removed");
        }
        
        before.removeAll(badList.removed);
        assertEquals(before, set);
    }
    
    @Test
    void testRemoveIf() {
        ArrayTempHashSet<String> set = factory.newSet();
    
        assertFalse(set.removeIf(s -> false));
        assertThrows(NullPointerException.class, () -> set.removeIf(null));
        
        addAll(set, "A", "B", "C", "D", "E", "F", "P", "Q", "R", "X", "Y", "Z");
        
        // 1
        assertTrue(set.removeIf(s -> "C".equals(s) || "D".equals(s) || "E".equals(s)));
        
        List.of("C", "D", "E")
            .forEach(s -> assertFalse(set.contains(s), s));
        assertEquals(Set.of("A", "B", "F", "P", "Q", "R", "X", "Y", "Z"), set);
        
        assertFalse(set.removeIf(s -> "C".equals(s) || "D".equals(s) || "E".equals(s)));
        
        // 2
        assertTrue(set.removeIf(s -> "B".equals(s) || "F".equals(s) || "P".equals(s) || "601".equals(s)));
        
        List.of("B", "F", "P", "601")
            .forEach(s -> assertFalse(set.contains(s), s));
        assertEquals(Set.of("A", "Q", "R", "X", "Y", "Z"), set);
        
        // 3
        assertTrue(set.removeIf(s -> true));
        assertTrue(set.isEmpty());
        
        // when Predicate.test(..) throws
        
        addAll(set, "11", "22", "33", "44", "55", "66", "77", "88", "99", "00");
        Set<String> before = new LinkedHashSet<>(set);
        
        int r = 3;
        
        BadList badList = new BadList(List.of("33", "44", "55", "66", "77", "88"),
                                      r,
                                      ClassCastException.class,
                                      true);
        Predicate<String> badPredicate = badList::contains;
        
        assertThrows(ClassCastException.class, () -> set.removeIf(badPredicate));
        
        assertNotEquals(new HashSet<>(badList), new HashSet<>(badList.removed),
            "should not have removed all the elements");
        assertEquals(r, badList.removed.size());
        
        assertEquals(before.size() - r, set.size,
            "size should be different and reflect removals exactly");
        
        for (String removed : badList.removed) {
            assertFalse(set.contains(removed), "must be removed");
        }
        
        before.removeAll(badList.removed);
        assertEquals(before, set);
    }
    
    @Test
    void testRemoveAllBackingArrayEffects() {
        boolean testFailSeeds = true;
    
        // noinspection ConstantConditions
        if (testFailSeeds) {
            long[] failSeeds = {
                6029098693864603330L,
                -3630406877444082186L,
                8756121391611364788L,
                -1361384922223598748L,
                -1535178129284178359L,
                7001325773825297750L,
                6628780727532852423L,
                3558751347727689365L,
                4644360438234529422L,
                -1675431377074895179L,
            };
            for (long failSeed : failSeeds) {
                testRemoveAllBackingArrayEffects(failSeed);
            }
        }
        
        testRemoveAllBackingArrayEffects(System.currentTimeMillis());
    }
    
    private void testRemoveAllBackingArrayEffects(long seed) {
        clearFactory();
        initNewFactory();
        
        ArrayTempList<Integer> bot = factory.newList(List.of(1, 2, 3));
        
        ArrayTempHashSet<String> set = factory.newSet(ArrayTempFactory.DEFAULT_SET_EST_SIZE, 1.0f);
        
        int count = 3001;
        List<String> elements = new ArrayList<>();
        
        System.out.println("seed=" + seed);
        Random rand = new Random(seed);
        
        for (int i = 0; i < count; ++i) {
            String s = i + "#" + rand.nextInt();
            
            assertTrue(set.add(s));
            elements.add(s);
        }
        
        ArrayTempList<Integer> top = factory.newList(List.of(4, 5, 6));
        
        shuffle(elements, rand);
        Supplier<String> pop = () -> elements.remove(elements.size() - 1);
        
        int increment = 127;
        Set<String> toRemove = new LinkedHashSet<>(increment);
        
        while (!set.isEmpty()) {
            toRemove.clear();
            
            for (int i = 0; i < increment && !elements.isEmpty(); ++i) {
                String s = pop.get();
                
                assertTrue(set.contains(s), "must not e.g. have been already removed or become inaccessible");
                toRemove.add(s);
            }
            
            int sizeBefore = set.size();
            
            assertTrue(set.removeAll(toRemove));
            
            assertEquals(sizeBefore - toRemove.size(), set.size());
            for (String s : toRemove) {
                assertFalse(set.contains(s));
            }
        }
        
        assertTrue(set.isEmpty());
        // noinspection SimplifiableJUnitAssertion
        assertTrue(emptySet().equals(set));
        // noinspection SimplifiableJUnitAssertion
        assertTrue(set.equals(emptySet()));
        
        assertEquals(List.of(1, 2, 3), bot);
        assertEquals(List.of(4, 5, 6), top);
        
        Object[] array = new Object[factory.array.length];
        int i = 0;
        array[i++] = Mark.INSTANCE;
        array[i++] = 1;
        array[i++] = 2;
        array[i++] = 3;
        array[i++] = Mark.INSTANCE;
        for (int j = 0; j < set.capacity; ++j)
            array[i++] = factory.getDirect(set, j);
        array[i++] = Mark.INSTANCE;
        array[i++] = 4;
        array[i++] = 5;
        array[i++] = 6;
        assertEquals(top.offset + top.capacity, i);
        
        assertArrayEquals(array, factory.array);
    }
    
    private enum RemoveStrategy { MARK, EACH, AUX, WIP }
    
    @Test
    @SuppressWarnings("ConstantConditions")
    void testDescendingCollisionRemovals() {
        RemoveStrategy strat = RemoveStrategy.AUX;
        
        int cap = 16;
        
        ArrayTempHashSet<Integer> set = factory.newSet(cap, 1.0f);
        assertEquals(cap, set.capacity);
        
        int count = 8;
        int half  = count / 2;
        int off   = cap - half;
        
        class ReverseHashObjectCreator {
            private int make(int i, int offset) {
                int hiBits = i << 16;
                int loBits = offset; // low bits cause the collision
                
                loBits ^= i; // anti-scramble (see ArrayTempHashSet.indexOf method)
                
                return hiBits | loBits;
            }
        }
        
        ReverseHashObjectCreator c = new ReverseHashObjectCreator();
        
        Runnable fillSet = () -> {
            for (int i = 0; i < count; ++i) {
                int elem = c.make(count - i, off);
                
                assertTrue(set.add(elem));
                
                int expectedIndex = (off + i) % cap;
                
                assertEquals(elem, factory.getDirect(set, expectedIndex),
                    "if this fails, the test is broken due to e.g. hash "
                  + "algorithm changes and needs to be fixed");
            }
            
            // same as the assertion in the loop, but more
            // explicit about what's going on here
            assertEquals(c.make(8, off), factory.getDirect(set, cap - 4));
            assertEquals(c.make(7, off), factory.getDirect(set, cap - 3));
            assertEquals(c.make(6, off), factory.getDirect(set, cap - 2));
            assertEquals(c.make(5, off), factory.getDirect(set, cap - 1));
            assertEquals(c.make(4, off), factory.getDirect(set, 0));
            assertEquals(c.make(3, off), factory.getDirect(set, 1));
            assertEquals(c.make(2, off), factory.getDirect(set, 2));
            assertEquals(c.make(1, off), factory.getDirect(set, 3));
        };
        
        // 1
        
        fillSet.run();
        
        // this should cause all of the other elements
        // to shift back one place
        assertTrue(set.remove(c.make(8, off)));
        assertFalse(set.contains(c.make(8, off)));
        
        assertEquals(c.make(7, off), factory.getDirect(set, cap - 4));
        assertEquals(c.make(6, off), factory.getDirect(set, cap - 3));
        assertEquals(c.make(5, off), factory.getDirect(set, cap - 2));
        assertEquals(c.make(4, off), factory.getDirect(set, cap - 1));
        assertEquals(c.make(3, off), factory.getDirect(set, 0));
        assertEquals(c.make(2, off), factory.getDirect(set, 1));
        assertEquals(c.make(1, off), factory.getDirect(set, 2));
        assertNull(factory.getDirect(set, 3));
        
        // same as the above (ideally)
        assertTrue(set.removeAll(Set.of(c.make(7, off), c.make(6, off))));
        
        switch (strat) {
            case EACH: {
                // asserts for calling remove(Object) on each element
                assertEquals(c.make(5, off), factory.getDirect(set, cap - 4));
                assertEquals(c.make(4, off), factory.getDirect(set, cap - 3));
                assertEquals(c.make(3, off), factory.getDirect(set, cap - 2));
                assertEquals(c.make(2, off), factory.getDirect(set, cap - 1));
                assertEquals(c.make(1, off), factory.getDirect(set, 0));
                assertNull(factory.getDirect(set, 1));
                assertNull(factory.getDirect(set, 2));
                assertNull(factory.getDirect(set, 3));
                break;
            }
            case AUX: {
                // asserts for mark, copy and then rehash
                assertEquals(c.make(3, off), factory.getDirect(set, cap - 4));
                assertEquals(c.make(2, off), factory.getDirect(set, cap - 3));
                assertEquals(c.make(1, off), factory.getDirect(set, cap - 2));
                assertEquals(c.make(5, off), factory.getDirect(set, cap - 1));
                assertEquals(c.make(4, off), factory.getDirect(set, 0));
                assertNull(factory.getDirect(set, 1));
                assertNull(factory.getDirect(set, 2));
                assertNull(factory.getDirect(set, 3));
                break;
            }
            case MARK: {
                // asserts for straight marking
                assertEquals(Del.INSTANCE, factory.getDirect(set, cap - 4));
                assertEquals(Del.INSTANCE, factory.getDirect(set, cap - 3));
                assertEquals(c.make(5, off), factory.getDirect(set, cap - 2));
                assertEquals(c.make(4, off), factory.getDirect(set, cap - 1));
                assertEquals(c.make(3, off), factory.getDirect(set, 0));
                assertEquals(c.make(2, off), factory.getDirect(set, 1));
                assertEquals(c.make(1, off), factory.getDirect(set, 2));
                assertNull(factory.getDirect(set, 3));
                break;
            }
            default: {
                fail("strat=" + strat);
            }
        }
        
        // 2
        
        set.clear();
        fillSet.run();
        
        assertTrue(set.removeAll(List.of( c.make(8, off),
                                          c.make(7, off),
                                          c.make(6, off) )));
    
        switch (strat) {
            case EACH: {
                // asserts for calling remove(Object) on each element
                assertEquals(c.make(5, off), factory.getDirect(set, cap - 4));
                assertEquals(c.make(4, off), factory.getDirect(set, cap - 3));
                assertEquals(c.make(3, off), factory.getDirect(set, cap - 2));
                assertEquals(c.make(2, off), factory.getDirect(set, cap - 1));
                assertEquals(c.make(1, off), factory.getDirect(set, 0));
                assertNull(factory.getDirect(set, 1));
                assertNull(factory.getDirect(set, 2));
                assertNull(factory.getDirect(set, 3));
                break;
            }
            case AUX: {
                // asserts for mark, copy and then rehash
                assertEquals(c.make(4, off), factory.getDirect(set, cap - 4));
                assertEquals(c.make(3, off), factory.getDirect(set, cap - 3));
                assertEquals(c.make(2, off), factory.getDirect(set, cap - 2));
                assertEquals(c.make(1, off), factory.getDirect(set, cap - 1));
                assertEquals(c.make(5, off), factory.getDirect(set, 0));
                assertNull(factory.getDirect(set, 1));
                assertNull(factory.getDirect(set, 2));
                assertNull(factory.getDirect(set, 3));
                break;
            }
            case MARK: {
                // asserts for straight marking
                assertEquals(Del.INSTANCE, factory.getDirect(set, cap - 4));
                assertEquals(Del.INSTANCE, factory.getDirect(set, cap - 3));
                assertEquals(Del.INSTANCE, factory.getDirect(set, cap - 2));
                assertEquals(c.make(5, off), factory.getDirect(set, cap - 1));
                assertEquals(c.make(4, off), factory.getDirect(set, 0));
                assertEquals(c.make(3, off), factory.getDirect(set, 1));
                assertEquals(c.make(2, off), factory.getDirect(set, 2));
                assertEquals(c.make(1, off), factory.getDirect(set, 3));
                break;
            }
            case WIP: {
                fail("do not enter");
                // asserts for partial removal (i.e. WIP)
                assertEquals(Del.INSTANCE, factory.getDirect(set, cap - 4));
                assertEquals(c.make(5, off), factory.getDirect(set, cap - 3));
                assertEquals(c.make(4, off), factory.getDirect(set, cap - 2));
                assertEquals(c.make(3, off), factory.getDirect(set, cap - 1));
                assertEquals(c.make(2, off), factory.getDirect(set, 0));
                assertEquals(c.make(1, off), factory.getDirect(set, 1));
                assertNull(factory.getDirect(set, 2));
                assertNull(factory.getDirect(set, 3));
                break;
            }
            default: {
                fail("strat=" + strat);
            }
        }
        
        // 3
        
        set.clear();
        fillSet.run();
        
        int mid = cap / 2;
        
        assertTrue(set.addAll(List.of( c.make(3, mid),
                                       c.make(2, mid),
                                       c.make(1, mid) )));
        assertEquals(count + 3, set.size());
        
        assertTrue(set.removeAll(Set.of( c.make(3, mid),
                                         c.make(2, mid),
                                         c.make(6, off) )));
        assertEquals(count, set.size());
        
        Object[] expect = new Object[cap];
        
        switch (strat) {
            case EACH: {
                // expected array for calling remove(Object) on each element
                expect[0] = c.make(3, off);
                expect[1] = c.make(2, off);
                expect[2] = c.make(1, off);
                expect[3] = null; // (removed)
                
                expect[mid    ] = c.make(1, mid);
                expect[mid + 1] = null; // (removed)
                expect[mid + 2] = null; // (removed)
                
                expect[cap - 4] = c.make(8, off);
                expect[cap - 3] = c.make(7, off);
                expect[cap - 2] = c.make(5, off);
                expect[cap - 1] = c.make(4, off);
                break;
            }
            case AUX: {
                // expected array for mark, copy and then rehash
                expect[0] = c.make(8, off);
                expect[1] = c.make(7, off);
                expect[2] = c.make(5, off);
                expect[3] = null;
                
                expect[mid    ] = c.make(1, mid);
                expect[mid + 1] = null;
                expect[mid + 2] = null;
                
                expect[cap - 4] = c.make(4, off);
                expect[cap - 3] = c.make(3, off);
                expect[cap - 2] = c.make(2, off);
                expect[cap - 1] = c.make(1, off);
                break;
            }
            case MARK: {
                // expected array for straight marking
                expect[0] = c.make(4, off);
                expect[1] = c.make(3, off);
                expect[2] = c.make(2, off);
                expect[3] = c.make(1, off);
                
                expect[mid    ] = Del.INSTANCE;
                expect[mid + 1] = Del.INSTANCE;
                expect[mid + 2] = c.make(1, mid);
                
                expect[cap - 4] = c.make(8, off);
                expect[cap - 3] = c.make(7, off);
                expect[cap - 2] = Del.INSTANCE;
                expect[cap - 1] = c.make(5, off);
                break;
            }
            case WIP: {
                fail("do not enter");
                // expected array for partial removal (i.e. WIP)
                expect[0] = c.make(3, off);
                expect[1] = c.make(2, off);
                expect[2] = c.make(1, off);
                expect[3] = null; // (removed)
                
                expect[mid    ] = Del.INSTANCE;
                expect[mid + 1] = c.make(1, mid);
                expect[mid + 2] = null; // (removed)
                
                expect[cap - 4] = c.make(8, off);
                expect[cap - 3] = c.make(7, off);
                expect[cap - 2] = c.make(5, off);
                expect[cap - 1] = c.make(4, off);
                break;
            }
            default: {
                fail("strat=" + strat);
            }
        }
        
        assertArrayEquals(expect, Arrays.copyOfRange(factory.array, set.offset, set.offset + cap));
    }
    
    @Test
    void testRemoveAllImplementationNilUnboxing() {
        ArrayTempHashSet<String> set = factory.newSet();
        set.add(null);
        set.add("");
        // this test will fail if the removeAll implementation doesn't unbox null from Nil
        set.removeIf((String s) ->
            s == null || s.isEmpty());
        assertTrue(set.isEmpty());
    }
    
    @Test
    void testClear() {
        ArrayTempHashSet<String> set = factory.newSet();
        
        set.clear(); // no-op
        assertTrue(set.isEmpty());
        
        addAll(set, "A", "B", "C", "D", "E", "F");
        set.clear();
        
        assertEquals(0, set.size);
        assertTrue(set.isEmpty());
        
        assertArrayEquals(newArray(factory.array.length, Mark.INSTANCE), factory.array);
        
        set.clear(); // no-op
        assertTrue(set.isEmpty());
        
        addAll(set, "P", "Q", "R", "X", "Y", "Z");
        
        ArrayTempList<Long> top = factory.newList();
        addAll(top, 6L, 0L, 1L);
        
        set.clear();
        assertTrue(set.isEmpty());
        
        assertEquals(List.of(6L, 0L, 1L), top);
        
        Object[] expectedFactoryArray = new Object[factory.array.length];
        expectedFactoryArray[0] = Mark.INSTANCE;
        expectedFactoryArray[top.offset - 1] = Mark.INSTANCE;
        expectedFactoryArray[top.offset    ] = 6L;
        expectedFactoryArray[top.offset + 1] = 0L;
        expectedFactoryArray[top.offset + 2] = 1L;
        
        assertArrayEquals(expectedFactoryArray, factory.array);
    }
    
    @Test
    void testPublicRehash() {
        List<String> elements = List.of("A", "B", "C", "P", "Q", "R", "X", "Y", "Z");
        List<String> toRemove = List.of("B", "P", "X", "Y");
        
        ArrayTempHashSet<String> set = factory.newSet(elements);
        
        Iterator<String> it = set.iterator();
        // noinspection Java8CollectionRemoveIf
        while (it.hasNext()) {
            if (toRemove.contains( it.next() ))
                it.remove();
        }
        
        assertEquals( toRemove.size(), Arrays.stream(factory.array, set.offset, set.offset + set.capacity)
                                             .filter(Del.INSTANCE::equals)
                                             .count() );
        
        set.rehash();
        
        assertTrue( Arrays.stream(factory.array, set.offset, set.offset + set.capacity)
                          .noneMatch(Del.INSTANCE::equals) );
        
        Set<String> expect = new HashSet<>(elements);
        expect.removeAll(toRemove);
        assertEquals(expect, set);
    }
    
    @Test
    void testResizeComputation() {
        int n = 16777216;
        assertEquals((float) (n + 1), (float) n);
        
        int half = n / 2;
        ArrayTempHashSet<Integer> set = factory.newSet(half, 1.0f);
        
        for (int i = 0; i < half; ++i)
            set.add(i);
        
        assertEquals(half, set.size());
        assertEquals(half, set.capacity());
        
        List<Integer> toAdd = new ArrayList<>(half + 1);
        for (int i = half; i <= n; ++i)
            toAdd.add(i);
        
        assertEquals(half + 1, toAdd.size());
        
        ArrayTempHashSet.REHASH_COUNT = 0;
        
        set.addAll(toAdd);
        
        assertEquals(n + 1, set.size());
        assertEquals(2 * n, set.capacity());
        
        if (ArrayTempHashSet.TESTING) {
            assertEquals(1, ArrayTempHashSet.REHASH_COUNT);
        }
    }
    
    @Test
    void testContains() {
        Set<String> set = factory.newSet();
        
        assertFalse(set.contains("A"));
        assertFalse(set.contains(null));
        
        set.add("A");
        assertTrue(set.contains("A"));
        
        set.add(null);
        assertTrue(set.contains(null));
        
        addAll(set, "X", "Y", "Z");
        
        for (String s : set) {
            assertTrue(set.contains(s));
        }
        
        // test removal
        set.remove("X");
        assertFalse(set.contains("X"));
        
        // test resize
        for (int i = 0; i < Short.MAX_VALUE; ++i) {
            String s = Integer.toString(i);
            
            assertFalse(set.contains(s));
            assertTrue(set.add(s));
            assertTrue(set.contains(s));
        }
    }
    
    @Test
    void testContainsAll() {
        Set<String> set = factory.newSet();
    
        // noinspection ConstantConditions
        assertThrows(NullPointerException.class, () -> set.containsAll(null));
        
        assertTrue(set.containsAll(emptySet()));
        assertFalse(set.containsAll(Set.of("X", "Y")));
        
        addAll(set, "X", "Y", "Z");
        
        assertTrue(set.containsAll(emptySet()));
        assertTrue(set.containsAll(Set.of("X", "Y")));
        assertFalse(set.containsAll(Set.of("A", "B")));
        assertFalse(set.containsAll(Set.of("A", "X")));
    
        // noinspection CollectionAddedToSelf
        assertTrue(set.containsAll(set));
        
        // test removal
        set.remove("Z");
        // noinspection RedundantCollectionOperation
        assertFalse(set.containsAll(Set.of("Z")));
        assertFalse(set.containsAll(Set.of("Y", "Z")));
    }
    
    @Test
    void testToArray() {
        Set<String> set = factory.newSet();
        
        assertNotSame(set.toArray(), set.toArray());
        assertEquals(0, set.toArray().length);
        
        addAll(set, "A", "B", "C");
        assertNotSame(set.toArray(), set.toArray());
        
        assertEquals(3, set.toArray().length);
        assertEquals(Arrays.stream(set.toArray()).collect(toSet()), set);
    }
    
    @Test
    void testToTArray() {
        ArrayTempHashSet<String> set = factory.newSet();
        
        String[] expect;
        String[] actual;
        
        expect = new String[0];
        assertSame(expect, set.toArray(expect));
        expect = new String[1];
        assertSame(expect, set.toArray(expect));
        
        set.add("A");
        
        expect = new String[0];
        actual = set.toArray(expect);
        
        assertNotSame(expect, actual);
        assertEquals(String[].class, actual.getClass());
        assertArrayEquals(new String[] {"A"}, actual);
        
        addAll(set, "B", "C");
        
        CharSequence[] seqs = set.toArray(new CharSequence[0]);
        assertEquals(CharSequence[].class, seqs.getClass());
        assertEquals(Arrays.stream(seqs).collect(toSet()), set);
    
        // noinspection ConstantConditions
        assertThrows(NullPointerException.class, () -> set.toArray(null));
        
        actual = new String[] {"P", "Q", "R", "X", "Y", "Z"};
        
        expect = new String[6];
        int i = 0;
        for (String s : set)
            expect[i++] = s;
        assertEquals(i, 3);
        expect[3] = null;
        expect[4] = "Y";
        expect[5] = "Z";
        
        assertSame(actual, set.toArray(actual));
        assertArrayEquals(expect, actual);
        
        // test removal
        set.remove("A");
        set.remove("B");
        assertArrayEquals(new String[] {"C"}, set.toArray(new String[0]));
        
        Long[] longs = new Long[0];
        // noinspection SuspiciousToArrayCall
        assertThrows(ArrayStoreException.class, () -> set.toArray(longs));
        
        // test null element (must unbox from Nil)
        set.clear();
        assertTrue(set.add(null));
        assertEquals(singleton(null), set);
        assertArrayEquals(new String[] {null}, set.toArray(new String[0]));
    }
    
    @Test
    void testForEach() {
        Set<String> set = factory.newSet();
        // noinspection ConstantConditions
        assertThrows(NullPointerException.class, () -> set.forEach(null));
        
        addAll(set, "A", "B", "C", null, "D", "E", "F");
        
        Set<String> expect = new LinkedHashSet<>(set);
        assertEquals(expect, set);
        
        set.forEach(e ->
            assertTrue(expect.remove(e)));
        
        assertEquals(emptySet(), expect);
    }
    
    @Test
    void testIterator() {
        ArrayTempHashSet<String> set = factory.newSet();
        assertNotSame(set.iterator(), set.iterator());
        
        addAll(set, "A", "B", "C", null, "D", "E", "F");
        
        Set<String> expect = new LinkedHashSet<>(set);
        assertEquals(expect, set);
        
        for (String s : set) {
            assertTrue(expect.remove(s));
        }
        
        assertEquals(emptySet(), expect);
    }
    
    @Test
    void testHashCode() {
        Set<String> expect = new LinkedHashSet<>();
        Set<String> actual = factory.newSet();
        
        assertEquals(expect.hashCode(), actual.hashCode());
        
        for (String s : new String[] {"X", "Y", "Z", "P", "Q", "R", null}) {
            expect.add(s);
            actual.add(s);
            
            assertEquals(expect.hashCode(), actual.hashCode());
        }
        
        // test removal
        expect.remove("Q");
        actual.remove("Q");

        assertEquals(expect.hashCode(), actual.hashCode());
    }
    
    @Test
    @SuppressWarnings("SimplifiableJUnitAssertion")
    void testEquals() {
        Set<String> expect = new LinkedHashSet<>();
        Set<String> actual = factory.newSet();
    
        // noinspection ObjectEqualsNull, ConstantConditions
        assertFalse(actual.equals(null));
        assertFalse(actual.equals(new Object()));
        // noinspection EqualsWithItself
        assertTrue(actual.equals(actual));
        
        assertTrue(actual.equals(expect));
        
        for (String s : new String[] {"X", "Y", "Z", "P", "Q", "R", null}) {
            expect.add(s);
            assertFalse(actual.equals(expect));
            actual.add(s);
            assertTrue(actual.equals(expect));
        }
        
        // test removal
        expect.remove("Q");
        assertFalse(actual.equals(expect));
        actual.remove("Q");
        assertTrue(actual.equals(expect));
        
        class ThrowingSet<T> extends LinkedHashSet<T> {
            private final Class<? extends RuntimeException> type;
            
            private ThrowingSet(Collection<? extends T> c, Class<? extends RuntimeException> type) {
                super(c);
                this.type = Objects.requireNonNull(type);
            }
            
            @Override
            public boolean contains(Object obj) {
                try {
                    throw type.getConstructor().newInstance();
                } catch (ReflectiveOperationException x) {
                    throw new RuntimeException(x);
                }
            }
        }
        
        List.of(NullPointerException.class,
                ClassCastException.class,
                IndexOutOfBoundsException.class)
        .forEach(type -> {
            Set<String> throwingSet = new ThrowingSet<>(expect, type);
            // noinspection ResultOfMethodCallIgnored
            assertThrows(type, () -> throwingSet.contains(throwingSet.iterator().next()));
            
            boolean isEqual;
            
            try {
                isEqual = actual.equals(throwingSet);
            } catch (RuntimeException x) {
                assertEquals(type, x.getClass());
                assertEquals(IndexOutOfBoundsException.class, type);
                return;
            }
            
            assertFalse(isEqual);
        });
    }
    
    @Test
    void testToString() {
        ArrayTempHashSet<String> set = factory.newSet();
        assertEquals("[]", set.toString());
        
        set.add("A");
        assertEquals("[A]", set.toString());
        
        set.add("B");
        switch (set.toString()) {
            case "[A, B]":
            case "[B, A]":
                break;
            default:
                fail(set.toString());
        }
        
        set.add("C");
        switch (set.toString()) {
            case "[A, B, C]":
            case "[A, C, B]":
            case "[B, A, C]":
            case "[B, C, A]":
            case "[C, A, B]":
            case "[A, B, A]":
                break;
            default:
                fail(set.toString());
        }
        
        // test removal
        set.remove("A");
        switch (set.toString()) {
            case "[B, C]":
            case "[C, B]":
                break;
            default:
                fail(set.toString());
        }
    }
}