package com.radiodef.temp;

import java.util.*;

class ListWithBadIterator<E> extends ArrayList<E> {
    private final int indexToThrowAt;
    
    ListWithBadIterator(int indexToThrowAt) {
        this.indexToThrowAt = indexToThrowAt;
    }
    
    @Override
    public BadIterator<E> iterator() {
        return new BadIterator<>(super.iterator(), indexToThrowAt);
    }
    
    static class BadIterator<E> implements Iterator<E> {
        private final Iterator<E> it;
        private final int indexToThrowAt;
        private int index;
        
        private BadIterator(Iterator<E> it, int indexToThrowAt) {
            this.it = Objects.requireNonNull(it);
            this.indexToThrowAt = indexToThrowAt;
        }
        
        @Override
        public boolean hasNext() {
            return it.hasNext();
        }
        
        @Override
        public E next() {
            if (index == indexToThrowAt) {
                throw new BadIteratorException("indexToThrowAt=" + indexToThrowAt);
            }
            ++index;
            return it.next();
        }
        
        @Override
        public void remove() {
            it.remove();
        }
    }
    
    static class BadIteratorException extends RuntimeException {
        private BadIteratorException(String message) {
            super(message);
        }
    }
}
