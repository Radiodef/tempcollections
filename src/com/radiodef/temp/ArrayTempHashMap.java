/* MIT License
 *
 * Copyright (c) 2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.radiodef.temp;

import java.util.*;
import java.util.function.*;

/**
 * <p>{@code ArrayTempHashMap} is the {@link TempMap} implementation for
 * {@link ArrayTempFactory}. {@code ArrayTempHashMap} is an open addressed
 * hash table with linear probing. Keys and values are stored directly in
 * the factory array, interleaved.</p>
 *
 * <p>This map implementation supports both {@code null} keys and {@code null}
 * values.</p>
 *
 * <p>Each {@code ArrayTempHashMap} is created with a {@code float} load
 * factor, which is the maximum ratio of entries to table slots which the
 * map will normally allow. If the map's {@link #size() size()} grows to a
 * point where this ratio would exceed the load factor, the map will attempt
 * to grow and rehash its table to a larger capacity. For example, if the
 * load factor is {@code 0.5f}, then the map will attempt to maintain a
 * table with twice as many slots as its size.</p>
 *
 * <p>The ratio of size to table slots for a given {@code ArrayTempHashMap}
 * can be i.e. understood as the following:</p>
 *
 *<pre><code>(float) map.size() / (float) (map.capacity() / 2)</code></pre>
 *
 * <p>If that ratio exceeds the map's load factor, the map will attempt to
 * increase its table capacity by requesting more space from the factory.
 * ({@link #capacity() capacity()} is the length of the array space reserved
 * by the map. Note that the map's capacity is divided by 2 because
 * {@code ArrayTempHashMap} uses 2 array slots for each entry.)</p>
 *
 * <p>Using a lower load factor will reduce the number of collisions at the
 * cost of extra storage space. The default load factor is <code>{@value #DEFAULT_LOAD_FACTOR}</code>.
 * Since {@code ArrayTempHashMap} uses open addressing, the load factor
 * may not be greater than {@code 1.0f}, because the map's size is strictly
 * limited by the length of the table.</p>
 *
 * <p>(A map which is not at the top of the factory stack will allow its size
 * to grow past what its load factor would normally allow. Such a map can be
 * considered to have an effective load factor of {@code 1.0f}, meaning that
 * it will allow its size to increase until its table is actually full.)</p>
 *
 * <h3>Notes about iteration</h3>
 *
 * <p>Because keys and values are stored directly in the table, no instances
 * of {@link Map.Entry Map.Entry} are actually used internally by the data
 * structure. {@code Map.Entry} instances returned by the map's
 * {@link #entrySet() entrySet()} are created temporarily. (See the documentation
 * for {@code Map.Entry} for a description of the general contract around these
 * temporary objects.)</p>
 *
 * <p>Because iterating the entry set creates many temporary {@code Map.Entry}
 * objects, the most efficient way to iterate over an {@code ArrayTempHashMap} is
 * via the {@link #forEach(BiConsumer) forEach(BiConsumer&lt;K,V&gt;)} method
 * instead.</p>
 *
 * <h3>Notes about aggregate removals</h3>
 *
 * <p>The {@link Iterator#remove() Iterator.remove()} implementation of the
 * iterators created by this map's {@link #entrySet() entrySet()}, {@link #keySet() keySet()}
 * and {@link #values() values()} remove an entry by simply marking its place
 * in the table as deleted. This is efficient for a small number of removals,
 * but may degrade lookup performance if the table has too many of these deletion
 * marks. Code which relies on {@code Iterator.remove()} may wish to invoke the
 * {@link #rehash() rehash()} method after iteration is completed, which will
 * remove any deletion marks:</p>
 *
 *<pre><code>{@code ArrayTempHashMap<K, V>} map = ...;
 *for ({@code Iterator<K>} it = map.keySet().iterator(); it.hasNext();) {
 *    ...
 *    if (...(it.next()))
 *        it.remove(); // marks the entry as deleted
 *}
 *map.rehash(); // rehash the table when iteration is complete</code></pre>
 *
 * <p>(However, note that {@code rehash()} should never be invoked <em>during</em>
 * iteration.)</p>
 *
 * @param <K> the key type of this map
 * @param <V> the value type of this map
 * @see   ArrayTempFactory#newMap(int, float)
 */
public final class ArrayTempHashMap<K, V> extends ArrayTempOpenAddressedHashTable
        implements TempMap<K, V> {
    
//    static final int MAX_ENTRY_COUNT = MAX_CAPACITY >>> 1;
    
    static final class Store<K, V> {
        transient ArrayTempHashMapKeySet<K> keySet;
        transient ArrayTempHashMapValues<V> values;
        transient ArrayTempHashMapEntrySet<K, V> entrySet;
        transient BiConsumer<K, V> putConsumer;
    }
    
    private transient Store<K, V> store;
    
    private Store<K, V> getStore() {
        Store<K, V> store = this.store;
        return (store != null) ? store : (this.store = new Store<>());
    }
    
    ArrayTempHashMap(ArrayTempFactory factory,
                     ArrayTempObject previous,
                     int offset,
                     int capacity,
                     float loadFactor) {
        super(factory, previous, offset, capacity, loadFactor);
    }
    
    static final boolean TESTING = true;
    
    @Override
    public final int maxSize() {
        return super.maxSize() >>> 1;
    }
    
    static int hash(Object key, int offset, int capacity) {
        int h = key.hashCode();
        return offset + (((h ^ (h >>> 16)) & ((capacity >>> 1) - 1)) << 1);
    }
    
    private static int next(int i, int offset, int capacity) {
        return offset + ((i - offset + 2) & (capacity - 1));
    }
    
    static boolean isKey(Object o) {
        return o != null && o != Del.INSTANCE;
    }
    
    private boolean resizeIfNecessary(int additionalSize) {
        assert additionalSize >= 0 : additionalSize;
        
        if (additionalSize == 0)
            return false;
        
        int size = this.size;
        int capacity = this.capacity;
        float loadFactor = this.loadFactor;
        
        // Note: if we aren't the top, then we allow ourselves to be
        //       filled all the way up, as if by loadFactor=1.0
        int effectiveCapacity = isTop() ? (int) (capacity * loadFactor) : capacity;
        
        // Note: (int) (capacity * loadFactor) could have yielded an odd number,
        //       so we are rounding the lowest bit off here. I don't think it
        //       will ever matter, but it's happening.
        int freeEntries = (effectiveCapacity >>> 1) - size;
        
        if (additionalSize <= freeEntries)
            return false;
        
        // Note: Sizes are passed as 2x because we count 2 slots for each entry.
        int newCapacity = nextCapacity(capacity, size << 1, additionalSize << 1, loadFactor);
        if (newCapacity != NO_CAPACITY) {
            resizeAndRehash(capacity, newCapacity);
            return true;
        }
        
        throw new OutOfMemoryError("capacity=" + capacity
                                 + ", size=" + size
                                 + ", additionalSize=" + additionalSize);
    }
    
    private void resizeAndRehash(int oldCapacity, int newCapacity) {
        assert newCapacity > oldCapacity : "old capacity=" + oldCapacity + ", new capacity=" + newCapacity;
        int capToRequest = newCapacity - oldCapacity;
        
        ArrayTempFactory factory = getFactory();
        Object[] oldArray = factory.array;
        
        int availableCap = factory.ensureAvailableCapacity(this, capToRequest);
        
        assert availableCap >= capToRequest : "requested=" + capToRequest + ", available=" + availableCap;
        
        capacity = newCapacity;
        Object[] newArray = factory.array;
        
        if (oldArray != newArray) {
            rehashWithOldArray(oldArray, oldCapacity, newArray, newCapacity, offset);
        } else {
            rehashWithFactoryArray(factory, oldArray, offset, oldCapacity, newCapacity);
        }
    }
    
    private static void rehashWithOldArray(Object[] oldArr, int oldCap,
                                           Object[] newArr, int newCap,
                                           int offset) {
        assert oldArr != newArr;
        rehash(oldArr, offset, oldCap,
               newArr, offset, newCap);
    }
    
    /**
     * <p>Rehashes the table. This causes each entry's location in the table
     * to be recomputed, and removes any deletion marks which were placed by the
     * {@link Iterator#remove() remove()} method of any of this map's iterators.</p>
     *
     * <p>This method has a similar effect to the following:</p>
     *
     *<pre><code>{@code Map<K, V> entries = new LinkedHashMap<>(this);}
     *this.clear();
     *this.putAll(entries);</code></pre>
     */
    public final void rehash() {
        ArrayTempFactory factory = getFactory();
        rehashWithFactoryArray(factory, factory.array, offset, capacity, capacity);
    }
    
    private static void rehashWithFactoryArray(ArrayTempFactory factory,
                                               Object[] array,
                                               int offset,
                                               int oldCap,
                                               int newCap) {
        try (ArrayTempList<Object> auxList = factory.newList(oldCap)) {
            Object[] aux = factory.array;
            
            if (aux != array) {
                // If the factory array was resized as a result of newList,
                // then we can just go back to rehashing with the old array.
                rehashWithOldArray(array, oldCap, aux, newCap, offset);
                return;
            }
            
            int auxOff = auxList.offset;
            System.arraycopy(array, offset, aux, auxOff, oldCap);
            
            rehash(aux, auxOff, oldCap, array, offset, newCap);
        }
    }
    
    @SuppressWarnings("unused")
    private static void rehashWithCopy(Object[] array, int offset, int oldCap, int newCap) {
        Object[] copy = Arrays.copyOfRange(array, offset, offset + oldCap);
        
        rehash(copy, 0, oldCap,
               array, offset, newCap);
    }
    
    static int REHASH_COUNT = 0;
    
    private static void rehash(Object[] srcArr, int srcOff, int srcCap,
                               Object[] dstArr, int dstOff, int dstCap) {
        if (TESTING) {
            ++REHASH_COUNT;
        }
        
        Arrays.fill(dstArr, dstOff, dstOff + dstCap, null);
        srcCap += srcOff;
        
        for (int s = srcOff; s < srcCap; s += 2) {
            Object key = srcArr[s];
            
            if (isKey(key)) {
                int d = hash(key, dstOff, dstCap);
                
                while (dstArr[d] != null)
                    d = next(d, dstOff, dstCap);
                
                dstArr[d] = key;
                dstArr[d + 1] = srcArr[s + 1];
            }
        }
    }
    
    @Override
    public final void clear() {
        int size = this.size;
        if (size != 0) {
            int off = offset;
            Arrays.fill(getFactory().array, off, off + capacity, null);
            this.size = 0;
        }
    }
    
    @Override
    public final boolean containsKey(Object key) {
        assert isExternal(key) : key;
        if (isEmpty())
            return false;
        
        key = Nil.box(key);
        
        Object[] arr = getFactory().array;
        int off = offset;
        int cap = capacity;
        
        int start = hash(key, off, cap);
        int i = start;
        
        do {
            Object e = arr[i];
            if (e == null)
                return false;
            if (e != Del.INSTANCE && key.equals(e))
                return true;
        } while ((i = next(i, off, cap)) != start);
        
        return false;
    }
    
    @Override
    public final boolean containsValue(Object val) {
        if (isEmpty())
            return false;
        
        Object[] arr = getFactory().array;
        
        if (val == null) {
            for (int i = offset, end = (i + capacity); i < end; i += 2)
                if (isKey(arr[i]) && (arr[i + 1] == null))
                    return true;
        } else {
            for (int i = offset, end = (i + capacity); i < end; i += 2)
                if (isKey(arr[i]) && val.equals(arr[i + 1]))
                    return true;
        }
        
        return false;
    }
    
    final boolean containsEntry(Object key, Object val) {
        assert isExternal(key) : key;
        if (isEmpty())
            return false;
        
        key = Nil.box(key);
        
        Object[] arr = getFactory().array;
        int off = offset;
        int cap = capacity;
        int start = hash(key, off, cap);
        int i = start;
        
        do {
            Object e = arr[i];
            if (e == null)
                return false;
            if (e != Del.INSTANCE && key.equals(e))
                return Objects.equals(val, arr[i + 1]);
        } while ((i = next(i, off, cap)) != start);
        
        return false;
    }
    
    @Override
    public final V get(Object key) {
        return getOrDefault(key, null);
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public final V getOrDefault(Object key, V def) {
        assert isExternal(key) : key;
        if (isEmpty())
            return def;
        
        key = Nil.box(key);
        
        Object[] arr = getFactory().array;
        int off = offset;
        int cap = capacity;
        
        int start = hash(key, off, cap);
        int i = start;
        
        do {
            Object e = arr[i];
            
            if (isKey(e) && e.equals(key))
                return (V) arr[i + 1];
            
        } while ((i = next(i, off, cap)) != start);
        
        return def;
    }
    
    /**
     * <p>Associates the specified value with the specified key in this map.
     * If the map previously contained a mapping for the key, the old value
     * is replaced by the specified value.</p>
     *
     * @param  key the key to associate the specified value with
     * @param  val the value to associate with the specified key
     * @return the old value associated with the specified key, or {@code null}
     *         if there was no previous entry
     * @throws IllegalStateException
     *         if there was no previous mapping for the specified key and
     *         {@code this.canAdd(1)} would return {@code false}
     * @see    ArrayTempHashMap#canAdd(int)
     */
    @Override
    @SuppressWarnings("unchecked")
    public final V put(K key, V val) {
        assert isExternal(key) : key;
        return (V) put(key, val, 1);
    }
    
    private Object put(Object key, Object val, int requiredAdditionalSize) {
        if (requiredAdditionalSize < 0)
            // StackOverflowError trap
            throw new AssertionError(requiredAdditionalSize);
        
        assert requiredAdditionalSize <= 1 : requiredAdditionalSize;
        
        key = Nil.box(key);
        
        Object[] arr = getFactory().array;
        int off = offset;
        int cap = capacity;
        
        int start = hash(key, off, cap);
        int i = start;
        
        do {
            Object e = arr[i];
            
            if (isKey(e)) {
                if (key.equals(e)) {
                    ++i;
                    Object old = arr[i];
                    arr[i] = val;
                    return old;
                }
            } else {
                if (resizeIfNecessary(requiredAdditionalSize))
                    // we were resized, so the computed index
                    // may be incorrect
                    return put(key, val, requiredAdditionalSize - 1);
                arr[i] = key;
                arr[i + 1] = val;
                ++size;
                return null;
            }
        } while ((i = next(i, off, cap)) != start);
        
        // this means we made a full circle and never found a vacant slot
        
        assert requiredAdditionalSize > 0 : requiredAdditionalSize;
        boolean resized = resizeIfNecessary(requiredAdditionalSize);
        assert resized;
        return put(key, val, requiredAdditionalSize - 1);
    }
    
    private BiConsumer<K, V> putConsumer() {
        Store<K, V> store = getStore();
        BiConsumer<K, V> putConsumer = store.putConsumer;
        return (putConsumer != null) ? putConsumer : (store.putConsumer = this::put);
    }
    
    /**
     * <p>Copies all of the mappings from the specified map to this map. An
     * invocation of this method is equivalent to {@code map.forEach(this::put)}.</p>
     *
     * <p>If this map is not at the top of the factory stack then given an</p>
     *
     *<pre><code>int n = this.maxSize() - this.size();</code></pre>
     *
     * <p>then up to {@code n} new entries will be added to this map. After
     * adding {@code n} new entries to this map (in the order they are encountered
     * by the specified map's {@link Map#forEach(BiConsumer) forEach(BiConsumer&lt;K,V&gt;)}
     * method), if further entries are encountered with keys which are not
     * already present in this map, then this map is modified no further and
     * the method throws an {@link IllegalStateException}.</p>
     *
     * @param  map the entries to copy in to this map
     * @throws NullPointerException if the specified map is {@code null}
     * @throws IllegalStateException
     *         if <code>this.canAdd(&hellip;)</code> would return {@code false}
     *         with its argument as the number of keys in the specified map
     *         which are not already in this map
     * @see    ArrayTempHashMap#canAdd(int)
     */
    @Override
    public final void putAll(Map<? extends K, ? extends V> map) {
        if (map.isEmpty()) {
            return;
        }
        if (isTop()) {
            resizeIfNecessary(map.size());
        }
        
        if (map instanceof ArrayTempHashMap<?, ?>) {
            ArrayTempHashMap<?, ?> that = (ArrayTempHashMap<?, ?>) map;
            
            Object[] arr = that.getFactory().array;
            for (int i = that.offset, end = (i + that.capacity); i < end; i += 2) {
                Object k = arr[i];
                if (isKey(k)) {
                    put(k, arr[i + 1], 1);
                }
            }
        } else {
            map.forEach(putConsumer());
        }
    }
    
    /**
     * <p>If the specified key is not already associated with a value (or
     * is mapped to {@code null}) associates it with the specified value
     * and returns {@code null}, otherwise returns the current value.</p>
     *
     * @param  key the key to associate the specified value with
     * @param  val the value to associate with the specified key
     * @return the previous value associated with the specified key, or
     *         {@code null} if there was no previous mapping
     * @throws IllegalStateException
     *         if there was no previous mapping for the specified key and
     *         {@code this.canAdd(1)} would return {@code false}
     * @see    ArrayTempHashMap#canAdd(int)
     *
     */
    @Override
    @SuppressWarnings("unchecked")
    public final V putIfAbsent(K key, V val) {
        assert isExternal(key) : key;
        Object keyAsObj = Nil.box(key);
        
        Object[] arr = getFactory().array;
        int off = offset;
        int cap = capacity;
        
        int start = hash(keyAsObj, off, cap);
        int i = start;
        do {
            Object e = arr[i];
            if (isKey(e)) {
                if (keyAsObj.equals(e)) {
                    ++i;
                    V old = (V) arr[i];
                    if (old == null)
                        arr[i] = val;
                    return old;
                }
            } else {
                arr[i] = keyAsObj;
                arr[i + 1] = val;
                ++size;
                return null;
            }
        } while ((i = next(i, off, cap)) != start);
        
        // table was full
        boolean resized = resizeIfNecessary(1);
        assert resized;
        return (V) put(keyAsObj, val, 0);
    }
    
    /**
     * <p>Computes a mapping for the specified key and its current value
     * (or {@code null} if there is no current mapping).</p>
     *
     * <p>Retrieves the current value associated with the specified key
     * and invokes the remapping function as if by {@code func.apply(key,currentValue)}.
     * If there was no value associated with the specified key, then the
     * function is invoked as if by {@code func.apply(key,null)}.</p>
     *
     * <p>If the new value returned by the remapping function is not
     * {@code null}, then the new value is associated with the specified
     * key. Otherwise, if the function returns {@code null}, then the
     * entry is removed from the map (if it existed).</p>
     *
     * @param  key  the key to compute the value for
     * @param  func the remapping function for computing a value
     * @return the new value associated with the specified key, or {@code null}
     *         if the mapping was removed
     * @throws NullPointerException
     *         if the remapping function is {@code null}
     * @throws ConcurrentModificationException
     *         under some circumstances if the remapping function attempts
     *         to modify this map
     * @throws IllegalStateException
     *         if there was no previous mapping for the specified key,
     *         the remapping function returns a non-{@code null} value
     *         and {@code this.canAdd(1)} would return {@code false}
     * @see    ArrayTempHashMap#canAdd(int)
     */
    @Override
    public final V compute(K key, BiFunction<? super K, ? super V, ? extends V> func) {
        assert isExternal(key) : key;
        Objects.requireNonNull(func);
        
        int i = indexOfKeyOrEmpty(key);
        Object[] arr = getFactory().array;
        
        boolean existed = isKey(arr[i]);
        @SuppressWarnings("unchecked")
        V val = (V) arr[i + 1];
        
        int size = this.size;
        
        val = func.apply(key, val);
        
        if (size != this.size)
            throw new ConcurrentModificationException(
                "the compute remapping function must not modify the map");
        
        if (val == null) {
            if (existed) {
                removeAtIndexPrivate(i);
            }
        } else {
            if (!existed) {
                this.size = size + 1;
                arr[i] = Nil.box(key);
            }
            arr[i + 1] = val;
        }
        
        return val;
    }
    
    @Override
    public final V computeIfPresent(K key, BiFunction<? super K, ? super V, ? extends V> func) {
        assert isExternal(key) : key;
        Objects.requireNonNull(func);
        
        int i = indexOf(key);
        if (i == NO_INDEX)
            return null;
        
        Object[] arr = getFactory().array;
        
        @SuppressWarnings("unchecked")
        V val = (V) arr[i + 1];
        if (val == null)
            return null;
        
        int size = this.size;
        
        val = func.apply(key, val);
        
        if (size != this.size)
            throw new ConcurrentModificationException(
                "the computeIfPresent remapping function must not modify the map");
        
        if (val == null)
            removeAtIndexPrivate(i);
        else
            arr[i + 1] = val;
        
        return val;
    }
    
    /**
     * <p>If the specified key is not already associated with a value or
     * is associated with {@code null}, computes a value to associate it
     * with.</p>
     *
     * <p>If the key is already associated with a non-{@code null} value,
     * then the method takes no further action and that value is returned.
     * Otherwise, if the key was not associated with a value or was associated
     * with {@code null}, invokes the mapping function as if by {@code func.apply(key)}.</p>
     *
     * <p>If the function, returns {@code null}, then the method takes no
     * further action and the method returns {@code null}. Otherwise, the
     * new value returned by the function is associated with the specified
     * key and returned.</p>
     *
     * @param  key  the key to compute a mapping for
     * @param  func the mapping function for computing a value
     * @return the current value associated with the specified key, or
     *         {@code null} if no mapping existed and the mapping function
     *         returned {@code null}
     * @throws NullPointerException
     *         if the mapping function is {@code null}
     * @throws ConcurrentModificationException
     *         under some circumstances if the mapping function attempts
     *         to modify this map
     * @throws IllegalStateException
     *         if there was no previous mapping for the specified key,
     *         the mapping function returns a non-{@code null} value
     *         and {@code this.canAdd(1)} would return {@code false}
     * @see    ArrayTempHashMap#canAdd(int)
     */
    @Override
    public final V computeIfAbsent(K key, Function<? super K, ? extends V> func) {
        assert isExternal(key) : key;
        Objects.requireNonNull(func);
        
        int i = indexOfKeyOrEmpty(key);
        Object[] arr = getFactory().array;
        
        boolean existed = false;
        
        if (isKey(arr[i])) {
            @SuppressWarnings("unchecked")
            V old = (V) arr[i + 1];
            if (old != null)
                return old;
            
            existed = true;
        }
        
        int size = this.size;
        
        V val = func.apply(key);
        
        if (size != this.size)
            throw new ConcurrentModificationException(
                "the computeIfAbsent mapping function must not modify the map");
        
        if (val != null) {
            if (!existed) {
                this.size = size + 1;
                arr[i] = Nil.box(key);
            }
            arr[i + 1] = val;
        }
        
        return val;
    }
    
    private int indexOfKeyOrEmpty(Object key) {
        key = Nil.box(key);
        
        boolean rehash = true;
        
        for (;;) {
            Object[] arr = getFactory().array;
            int off = offset;
            int cap = capacity;
            
            int start = hash(key, off, cap);
            int i = start;
            
            Object e;
            do {
                if (!isKey(e = arr[i]) || key.equals(e))
                    return i;
            } while ((i = next(i, off, cap)) != start);
            
            if (!rehash) {
                throw new AssertionError("there was a problem rehashing the table");
            }
            
            rehash = false;
            boolean resized = resizeIfNecessary(1);
            assert resized;
        }
    }
    
    /**
     * <p>If the specified key is not already associated with a value,
     * associates it with the specified non-{@code null} value.</p>
     *
     * <p>Otherwise, invokes the merge function as if by
     * {@code func.apply(oldValue,newValue)} where {@code oldValue}
     * is the previous value already associated with the specified
     * key and {@code newValue} is the value provided as an argument
     * to this method.</p>
     *
     * <p>If the merge function returns {@code null}, then the entry
     * is removed from the map and the method returns {@code null}.
     * Otherwise, the computed value returned by the merge function
     * is associated with the specified key and returned.</p>
     *
     * @param  key  the key whose value ought to be merged
     * @param  val  the new value to merge
     * @param  func the merge function for computing a value
     * @return the new value associated with the specified key, or
     *         {@code null} if the entry was removed
     * @throws NullPointerException
     *         if the specified value or merge function is {@code null}
     * @throws ConcurrentModificationException
     *         under some circumstances if the merge function attempts
     *         to modify this map
     * @throws IllegalStateException
     *         if there was no previous mapping for the specified key
     *         and {@code this.canAdd(1)} would return {@code false}
     * @see    ArrayTempHashMap#canAdd(int)
     */
    @Override
    public final V merge(K key, V val, BiFunction<? super V, ? super V, ? extends V> func) {
        assert isExternal(key) : key;
        Objects.requireNonNull(val, "value");
        Objects.requireNonNull(func, "remapping function");
        
        Object keyAsObj = Nil.box(key);
        
        Object[] arr = getFactory().array;
        int off = offset;
        int cap = capacity;
        
        int start = hash(keyAsObj, off, cap);
        int i = start;
        
        do {
            Object e = arr[i];
            if (isKey(e)) {
                if (keyAsObj.equals(e)) {
                    @SuppressWarnings("unchecked")
                    V old = (V) arr[i + 1];
                    if (old != null) {
                        int size = this.size;
                        
                        val = func.apply(old, val);
                        
                        if (size != this.size)
                            throw new ConcurrentModificationException(
                                "the merge remapping function must not modify the map");
                        
                        if (val == null) {
                            removeAtIndexPrivate(i);
                            return null;
                        }
                    }
                    
                    arr[i + 1] = val;
                    return val;
                }
            } else {
                arr[i] = keyAsObj;
                arr[i + 1] = val;
                ++size;
                return val;
            }
        } while ((i = next(i, off, cap)) != start);
        
        // table was full
        boolean resized = resizeIfNecessary(1);
        assert resized;
        put(keyAsObj, val, 0);
        return val;
    }
    
    @Override
    public final V remove(Object key) {
        assert isExternal(key) : key;
        if (size != 0) {
            int i = indexOf(key);
            
            if (i != NO_INDEX) {
                @SuppressWarnings("unchecked")
                V val = (V) getFactory().array[i + 1];
                
                removeAtIndexPrivate(i);
                return val;
            }
        }
        return null;
    }
    
    /**
     * <p>Removes the entry with the specified key, returning {@code true}
     * if this map changed as a result of the call and {@code false}
     * otherwise.</p>
     *
     * <p>This method is equivalent to {@code Map.remove(Object)}, but
     * the return value is useful for determining whether an entry was
     * actually removed. {@code Map.remove(Object)} returns {@code null}
     * both in the case that no entry was removed and in the case that
     * the value associated with the removed key was {@code null}.</p>
     *
     * @param  key the key whose associated mapping should be removed
     * @return {@code true} if there was an entry in this map with
     *         the specified key and {@code false} otherwise
     */
    final boolean removeReturningBoolean(Object key) {
        assert isExternal(key) : key;
        if (size != 0) {
            int i = indexOf(key);
            
            if (i != NO_INDEX) {
                removeAtIndexPrivate(i);
                return true;
            }
        }
        return false;
    }
    
    /**
     * @param  index the absolute index in to the factory array whose entry
     *               should be removed
     * @return {@code true} if an entry was removed at the specified index
     *         and {@code false} if there was no entry
     * @throws IllegalArgumentException
     *         if the index is less than {@code this.offset}, greater than
     *         {@code this.offset+this.capacity} or points to a value
     */
    final boolean removeAtIndex(int index) {
        int off = offset;
        int cap = capacity;
        
        Tools.requireRange(index, off, off + cap, "index");
        if (((index - off) & 1) != 0) {
            throw new IllegalArgumentException(index + " is a value");
        }
        
        if (isKey(getFactory().array[index])) {
            removeAtIndexPrivate(index);
            return true;
        }
        return false;
    }
    
    private static final int NO_INDEX = -1;
    
    private int indexOf(Object key) {
        key = Nil.box(key);
        
        Object[] arr = getFactory().array;
        int off = offset;
        int cap = capacity;
        
        int start = hash(key, off, cap);
        int i = start;
        
        Object e;
        for (;;) {
            if ((e = arr[i]) == null)
                break;
            if (e != Del.INSTANCE && key.equals(e))
                return i;
            if ((i = next(i, off, cap)) == start)
                break;
        }
        return NO_INDEX;
    }
    
    private void removeAtIndexPrivate(int i) {
        Object[] arr = getFactory().array;
        int off = offset;
        int cap = capacity;
        
        assert isKey(arr[i]) : (i + "=" + arr[i]);
        assert ((i - off) & 1) == 0 : i;
        
        arr[i] = arr[i + 1] = null;
        int j = i;
        
        Object e;
        for (;;) {
            j = next(j, off, cap);
            
            if ((e = arr[j]) == null)
                break;
            
            if (e != Del.INSTANCE) {
                int k = hash(e, off, cap);
                
                if (!( (i < j) ? (k <= i || j < k) : (j < k && k <= i) ))
                    continue;
            }
            
            arr[i] = e;
            arr[i + 1] = arr[j + 1];
            arr[j] = arr[j + 1] = null;
            i = j;
        }
        
        --size;
    }
    
    @Override
    public final boolean remove(Object key, Object val) {
        assert isExternal(key) : key;
        if (size != 0) {
            int i = indexOf(key);
            
            if (i != NO_INDEX) {
                if (Objects.equals(getFactory().array[i + 1], val)) {
                    removeAtIndexPrivate(i);
                    return true;
                }
            }
        }
        return false;
    }
    
    @Override
    public final V replace(K key, V val) {
        assert isExternal(key) : key;
        if (size != 0) {
            int i = indexOf(key);
            
            if (i != NO_INDEX) {
                Object[] arr = getFactory().array;
                ++i;
                @SuppressWarnings("unchecked")
                V old = (V) arr[i];
                arr[i] = val;
                return old;
            }
        }
        return null;
    }
    
    @Override
    public final boolean replace(K key, V oldVal, V newVal) {
        assert isExternal(key) : key;
        if (size != 0) {
            int i = indexOf(key);
            
            if (i != NO_INDEX) {
                Object[] arr = getFactory().array;
                ++i;
                if (Objects.equals(arr[i], oldVal)) {
                    arr[i] = newVal;
                    return true;
                }
            }
        }
        return false;
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public final void replaceAll(BiFunction<? super K, ? super V, ? extends V> func) {
        Objects.requireNonNull(func);
        if (size == 0)
            return;
        Object[] arr = getFactory().array;
        
        for (int i = offset, end = (i + capacity); i < end; i += 2) {
            Object k = arr[i];
            if (isKey(k)) {
                arr[i + 1] = func.apply((K) Nil.unbox(k), (V) arr[i + 1]);
            }
        }
    }
    
    @Override
    public final Set<K> keySet() {
        Store<K, V> store = getStore();
        ArrayTempHashMapKeySet<K> keySet = store.keySet;
        return (keySet != null) ? keySet : (store.keySet = new ArrayTempHashMapKeySet<>(this));
    }
    
    @Override
    public final Collection<V> values() {
        Store<K, V> store = getStore();
        ArrayTempHashMapValues<V> values = store.values;
        return (values != null) ? values : (store.values = new ArrayTempHashMapValues<>(this));
    }
    
    @Override
    public final Set<Entry<K, V>> entrySet() {
        Store<K, V> store = getStore();
        ArrayTempHashMapEntrySet<K, V> entrySet = store.entrySet;
        return (entrySet != null) ? entrySet : (store.entrySet = new ArrayTempHashMapEntrySet<>(this));
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public final void forEach(BiConsumer<? super K, ? super V> action) {
        Objects.requireNonNull(action);
        
        if (size == 0)
            return;
        
        Object[] arr = getFactory().array;
        
        for (int i = offset, end = (i + capacity); i < end; i += 2) {
            Object k = arr[i];
            if (isKey(k)) {
                action.accept((K) Nil.unbox(k), (V) arr[i + 1]);
            }
        }
    }
    
    @Override
    public final int hashCode() {
        int hash = 0;
        
        int size = this.size;
        if (size != 0) {
            Object[] arr = getFactory().array;
            
            for (int i = offset, end = (i + capacity); i < end; i += 2) {
                Object key = arr[i];
                
                if (isKey(key)) {
                    Object val = arr[i + 1];
                    hash += key.hashCode() ^ ((val == null) ? 0 : val.hashCode());
                    
                    if (0 == --size)
                        break;
                }
            }
        }
        
        return hash;
    }
    
    // see equals JUnit test
    static Throwable CAUGHT = null;
    
    @Override
    public final boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj instanceof Map<?, ?>) {
            Map<?, ?> that = (Map<?, ?>) obj;
            
            int size = this.size;
            if (size != that.size())
                return false;
            if (size == 0)
                return true;
            
            Object[] arr = getFactory().array;
            for (int i = offset, end = (i + capacity); i < end; i += 2) {
                Object k = arr[i];
                
                if (isKey(k)) {
                    k = Nil.unbox(k);
                    Object v = arr[i + 1];
                    
                    try {
                        if ((v == null) ? !(that.containsKey(k) && that.get(k) == null)
                                        : !v.equals(that.get(k)))
                            return false;
                    } catch (ClassCastException | NullPointerException x) {
                        if (TESTING)
                            CAUGHT = x;
                        return false;
                    }
                    
                    if (0 == --size)
                        break;
                }
            }
            return true;
        }
        return false;
    }
    
    @Override
    public final String toString() {
        int size = this.size;
        if (size == 0)
            return "{}";
        
        StringBuilder b = new StringBuilder(32 * size).append('{');
        
        Object[] arr = getFactory().array;
        for (int i = offset, end = (i + capacity); i < end; i += 2) {
            Object k = arr[i];
            
            if (isKey(k)) {
                b.append(k).append('=').append(arr[i + 1]);
                
                if (0 == --size)
                    break;
                
                b.append(", ");
            }
        }
        
        return b.append('}').toString();
    }
}