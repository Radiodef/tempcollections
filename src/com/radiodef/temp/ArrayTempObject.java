/* MIT License
 *
 * Copyright (c) 2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.radiodef.temp;

/**
 * <p>{@code ArrayTempObject} is the abstract supertype of all collections
 * and maps created by {@link ArrayTempFactory}.</p>
 *
 * <p>Subclasses of this will make a good-faith attempt to simply behave as
 * if they are empty when interacted with after being closed by an invocation
 * of {@link #close() close()}. In cases where this is not possible (such as
 * {@link java.util.List#add(Object) add(E)} being called on a list), they will
 * instead make a good-faith attempt to throw an {@link IllegalStateException}.</p>
 *
 * @see ArrayTempList
 * @see ArrayTempDeque
 * @see ArrayTempHashSet
 * @see ArrayTempHashMap
 */
public abstract class ArrayTempObject implements TempObject {
    /**
     * This is private so that the subclasses have to go through the
     * {@link #getFactory()} method. This ensures nicer behavior of
     * wrong accesses after an object has been closed.
     */
    private ArrayTempFactory factory;
    
    ArrayTempObject previous;
    
    final int offset;
    int capacity;
    int size;
    
    ArrayTempObject(ArrayTempFactory factory,
                    ArrayTempObject previous,
                    int offset,
                    int capacity) {
        assert factory != null;
        assert offset >= 0 : "offset=" + offset;
        assert capacity >= 0 : "capacity=" + capacity;
        
        this.factory = factory;
        this.previous = previous;
        this.offset = offset;
        this.capacity = capacity;
    }
    
    /**
     * <p>Returns the length of the section of the factory's backing array
     * which is currently reserved by this collection or map.</p>
     *
     * <p>Note that while the capacity is a direct measure of the amount of
     * space available to this collection or map, it may not always be a
     * 1:1 measure of how many <em>elements</em> or <em>entries</em> it
     * can store. Some subclasses may use more than one array element for
     * each increment of size.</p>
     *
     * @return the length of the section of the factory's backing array
     *         which is currently reserved by this collection or map
     * @see    #maxSize()
     */
    public final int capacity() {
        return capacity;
    }
    
    /**
     * {@inheritDoc}
     *
     * <p>The size of an {@code ArrayTempObject} which has been closed by
     * invoking its {@link #close() close()} method is always 0.</p>
     *
     * @return {@inheritDoc}
     */
    @Override
    public final int size() {
        return size;
    }
    
    /**
     * {@inheritDoc}
     *
     * <p>An {@code ArrayTempObject} which has been closed by invoking its
     * {@link #close() close()} method is always empty.</p>
     *
     * @return {@inheritDoc}
     */
    @Override
    public final boolean isEmpty() {
        return size == 0;
    }
    
    /**
     * Returns the factory which created this object.
     *
     * @return the factory which created this object
     * @throws IllegalStateException
     *         if this {@code ArrayTempObject} has been closed by invoking
     *         its {@link #close() close()} method
     */
    @Override
    public final ArrayTempFactory getFactory() {
        ArrayTempFactory factory = this.factory;
        if (factory == null)
            throw new IllegalStateException("this was closed");
        return factory;
    }
    
    /**
     * <p>Returns {@code true} if this {@code ArrayTempObject} is at the top
     * of the factory stack, and {@code false} otherwise. The maximum
     * size of a collection or map which is not at the top of the factory
     * stack is restricted to some proportion of its current capacity.</p>
     *
     * <p>If an {@code ArrayTempObject} has been closed by invoking its
     * {@link #close() close()} method, then this method returns {@code false}.</p>
     *
     * @return {@code true} if this {@code ArrayTempObject} is at the top
     *         of the factory stack, and {@code false} otherwise.
     * @see    #maxSize()
     * @see    #canAdd(int)
     * @see    #capacity()
     */
    public final boolean isTop() {
        ArrayTempFactory factory = this.factory;
        return (factory != null) && (this == factory.top);
    }
    
    /**
     * <p>Returns the current maximum size of this collection or map. If this
     * collection or map is capacity-restricted (because it's not at the top
     * of the factory stack), then its size may be increased up to this maximum
     * (inclusively), at which point additional attempts to increase its size
     * will fail with an {@link IllegalStateException}.</p>
     *
     * <p>Specifically, given an</p>
     *
     *<pre><code>int n = this.maxSize() - this.size();</code></pre>
     *
     * <p>a capacity-restricted collection or map will support {@code n}
     * additional elements or entries.</p>
     *
     * <p>If this collection or map is <em>not</em> capacity-restricted (because
     * it's at the top of the factory stack), then the method returns a reasonable
     * ceiling estimate for a maximum size given a general estimate of a JVM's
     * maximum length for an array. This maximum size estimate may be less than
     * the maximum array length because this collection may share the factory array
     * with some number of other collections. The estimate does <em>not</em> take
     * in to account any other factors, such as the Java heap size or memory
     * restrictions of the computer system running the JVM.</p>
     *
     * <p>Additionally, this implementation will return 0 if this {@code ArrayTempObject}
     * has been closed by invoking its {@link #close() close()} method.</p>
     *
     * @return {@inheritDoc}
     */
    @Override
    public int maxSize() {
        ArrayTempFactory factory = this.factory;
        if (factory == null)
            // this was closed
            return 0;
        return (this == factory.top) ? (ArrayTempFactory.MAX_ARRAY_SIZE - offset)
                                     : capacity;
    }
    
    /**
     * {@inheritDoc}
     *
     * <p>Additionally, this implementation will instead return {@code changeInSize == 0}
     * if this {@code ArrayTempObject} has been closed by invoking its {@link #close() close()}
     * method.</p>
     *
     * @param  changeInSize {@inheritDoc}
     * @return {@inheritDoc}
     */
    @Override
    public final boolean canAdd(int changeInSize) {
        if (factory == null)
            // this was closed
            return changeInSize == 0;
        return (changeInSize <= 0) ? ((changeInSize != Integer.MIN_VALUE) && (-changeInSize) <= size)
                                   : (changeInSize <= (maxSize() - size));
    }
    
    /**
     * Returns {@code true} if {@link #close() this.close()} has been invoked
     * once before and {@code false} otherwise.
     *
     * @return {@code true} if {@link #close() this.close()} has been invoked
     *         once before and {@code false} otherwise
     * @see    #close()
     */
    public final boolean isClosed() {
        return factory == null;
    }
    
    /**
     * Closes this temporary collection or map, releasing its reserved section
     * of the backing array to the factory for reuse.
     *
     * @throws IllegalStateException
     *         if {@code this.isTop()} would return {@code false} or
     *         if {@code this.close()} has already been invoked once before
     * @see    #isTop()
     */
    @Override
    public final void close() {
        ArrayTempFactory factory = this.factory;
        // TODO: should this be a no-op instead?
        if (factory == null)
            throw new IllegalStateException("this was already closed");
        factory.close(this);
        this.factory = null;
        
        previous = null;
        size = 0;
        capacity = 0;
    }
}