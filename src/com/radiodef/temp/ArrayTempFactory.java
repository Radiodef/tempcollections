/* MIT License
 *
 * Copyright (c) 2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.radiodef.temp;

import java.util.*;

/**
 * <p>{@code ArrayTempFactory} is the principal implementation of {@link TempFactory}.
 * {@code ArrayTempFactory} maintains a single reference to an array which all of its
 * collections share subsections of. This is designed to reduce array allocations when
 * using many temporary collections.</p>
 *
 * <p>{@code ArrayTempFactory} works like a stack: invoking a factory method to
 * create a new collection pushes the new collection on to the top of the stack.
 * Invoking the {@link TempObject#close() close()} method of the top collection
 * pops it off of the stack, and the previous collection becomes the top again.
 * The top collection may be freely resized, but collections which are not at the
 * top of the stack become capacity restricted.</p>
 *
 * <p>The following is a simple example of using an {@code ArrayTempFactory},
 * with commentary:</p>
 *
 *<pre><code>// Creates a new factory.
 *TempFactory factory = new ArrayTempFactory();
 *
 *{@code // Creates a new List<String> and places it at the top of the stack.}
 *{@code // The list has an initial capacity of 4, meaning it can support a}
 *{@code // size() of 4 elements and will ask the factory for additional space}
 *{@code // when an attempt is made to add a 5th element.}
 *try ({@code TempList<String>} strings = factory.newList(4)) {
 *    strings.add("Abc");
 *    strings.add("Def");
 *    strings.add("Ghi");
 *
 *    // Creates a new {@code List<Double>} and places it at the top of the stack.
 *    // At this point, the 'strings' list is no longer at the top of the
 *    // stack so it becomes capacity-restricted to a maximum size() of 4
 *    // elements.
 *    try ({@code TempList<Double>} doubles = factory.newList(4)) {
 *        // strings.capacity() is 4 and strings.size() is 3, so we can add
 *        // 1 more element.
 *        System.out.println(strings.canAdd(1)); // true
 *        strings.add("Jkl");
 *        System.out.println(strings.canAdd(1)); // false
 *        // This next line would throw an IllegalStateException.
 *{@code //       strings.add("Mno");}
 *        System.out.println(strings); // [Abc, Def, Ghi, Jkl]
 *        doubles.add(1.0);
 *        doubles.add(2.0);
 *        doubles.add(3.0);
 *        doubles.add(4.0);
 *        // Note that the 'doubles' list is at the top of the stack, so its
 *        // capacity may be increased.
 *        doubles.add(5.0);
 *        System.out.println(doubles.capacity()); // 7
 *        System.out.println(doubles); // [1.0, 2.0, 3.0, 4.0, 5.0]
 *    }
 *
 *    // The 'strings' list is once again at the top of the stack, so its
 *    // capacity may be increased.
 *    System.out.println(strings.canAdd(1)); // true
 *    strings.add("Mno");
 *    System.out.println(strings); // [Abc, Def, Ghi, Jkl, Mno]
 *}</code></pre>
 *
 * <p>Collections which are not at the top of the stack may be used
 * in every other way a typical collection can be used. They just can't
 * be structurally increased beyond their capacity. Attempts to increase
 * the size of a collection which is not at the top of the stack past its
 * capacity will result in an {@link IllegalStateException} being thrown
 * from the method which was used to attempt the increase.</p>
 *
 * <p>Collections created by an {@code ArrayTempFactory} must be closed
 * in the reverse order they're opened. Attempts to invoke {@code close()}
 * on a collection which is not at the top of the stack will result in
 * an {@code IllegalStateException}.</p>
 *
 * <p>Note that this implementation is not synchronized in any way, and
 * neither are the collections it creates. Concurrent use of an instance
 * of {@code ArrayTempFactory} or a collection created by one must use
 * external synchronization.</p>
 *
 * @see ArrayTempList
 * @see ArrayTempDeque
 * @see ArrayTempHashSet
 * @see ArrayTempHashMap
 */
public final class ArrayTempFactory implements TempFactory {
    
    Object[] array;
    
    ArrayTempObject top;
    
    private static final int DEFAULT_FACTORY_CAPACITY = 32;
    
    /**
     * Constructs a new, empty {@code ArrayTempFactory} with a default capacity
     * of {@value #DEFAULT_FACTORY_CAPACITY}.
     *
     * @see #ArrayTempFactory(int)
     */
    public ArrayTempFactory() {
        this(DEFAULT_FACTORY_CAPACITY);
    }
    
    private static final Object[] EMPTY_ARRAY = new Object[0];
    
    /**
     * Constructs a new, empty {@code ArrayTempFactory} with the specified
     * initial capacity. The initial capacity is the length which the backing
     * array is first instantiated with.
     *
     * @param  initialCapacity the initial capacity
     * @throws IllegalArgumentException if the initial capacity is negative
     */
    public ArrayTempFactory(int initialCapacity) {
        Tools.requireNonNegative(initialCapacity, "initialCapacity");
        this.array = (initialCapacity == 0) ? EMPTY_ARRAY : new Object[initialCapacity];
    }
    
    /**
     * Returns the {@code ArrayTempObject} at the top of the stack (i.e. most
     * recently created, and not yet closed), or {@code null} if there isn't one.
     *
     * @return the {@code ArrayTempObject} at the top of the stack
     */
    public final ArrayTempObject getTop() {
        return top;
    }
    
    static final int DEFAULT_LIST_EST_SIZE = 10;
    
    /**
     * <p>Creates and returns a new, empty list. An invocation of this method is
     * equivalent to <code>this.newList({@value #DEFAULT_LIST_EST_SIZE})</code>.</p>
     *
     * @param  <E> {@inheritDoc}
     * @return {@inheritDoc}
     * @see    #newList(int)
     */
    @Override
    public final <E> ArrayTempList<E> newList() {
        return newList(DEFAULT_LIST_EST_SIZE);
    }
    
    /**
     * {@inheritDoc}
     *
     * <p>The new list is first constructed as if by {@code this.newList(coll.size())}.</p>
     *
     * @param  coll {@inheritDoc}
     * @param  <E>  {@inheritDoc}
     * @return {@inheritDoc}
     * @throws NullPointerException {@inheritDoc}
     * @see    #newList(int)
     */
    @SuppressWarnings("JavaDoc")
    @Override
    public final <E> ArrayTempList<E> newList(Collection<? extends E> coll) {
        ArrayTempList<E> list = newList(coll.size());
        list.addAll(coll);
        return list;
    }
    
    /**
     * <p>Creates and returns a new, empty list with its capacity set to
     * the specified size estimate.</p>
     *
     * @param  estimatedSize {@inheritDoc}
     * @param  <E>           {@inheritDoc}
     * @return {@inheritDoc}
     * @throws IllegalArgumentException {@inheritDoc}
     * @see    ArrayTempList#capacity()
     */
    @SuppressWarnings("JavaDoc")
    @Override
    public final <E> ArrayTempList<E> newList(int estimatedSize) {
        int offset = resizeAndGetOffsetForNewInstance(estimatedSize);
        
        ArrayTempList<E> list = new ArrayTempList<>(this, top, 1 + offset, estimatedSize);
        return configureAfterNewInstance(list, offset);
    }
    
    static final int DEFAULT_DEQUE_EST_SIZE = 8;
    
    /**
     * <p>Creates and returns a new, empty deque. An invocation of this method is
     * equivalent to <code>this.newDeque({@value #DEFAULT_DEQUE_EST_SIZE})</code>.</p>
     *
     * @param  <E> {@inheritDoc}
     * @return {@inheritDoc}
     * @see    #newDeque(int)
     */
    @Override
    public final <E> ArrayTempDeque<E> newDeque() {
        return newDeque(DEFAULT_DEQUE_EST_SIZE);
    }
    
    /**
     * {@inheritDoc}
     *
     * <p>The new deque is first constructed as if by {@code this.newDeque(coll.size())}.</p>
     *
     * @param  coll {@inheritDoc}
     * @param  <E>  {@inheritDoc}
     * @return {@inheritDoc}
     * @throws NullPointerException {@inheritDoc}
     * @see    #newDeque(int)
     */
    @SuppressWarnings("JavaDoc")
    @Override
    public final <E> ArrayTempDeque<E> newDeque(Collection<? extends E> coll) {
        ArrayTempDeque<E> deque = newDeque(coll.size());
        deque.addAll(coll);
        return deque;
    }
    
    /**
     * <p>Creates and returns a new, empty deque with its capacity set to
     * the nearest power of 2 greater than or equal to the specified size
     * estimate. For example, if the estimated size is 12, the new deque's
     * capacity will be 16.</p>
     *
     * @param  estimatedSize {@inheritDoc}
     * @param  <E>           {@inheritDoc}
     * @return {@inheritDoc}
     * @throws IllegalArgumentException {@inheritDoc}
     * @see    ArrayTempDeque#capacity()
     */
    @SuppressWarnings("JavaDoc")
    @Override
    public final <E> ArrayTempDeque<E> newDeque(int estimatedSize) {
        estimatedSize = Tools.next2(Tools.requireNonNegative(estimatedSize, "estimatedSize"));
        
        int offset = resizeAndGetOffsetForNewInstance(estimatedSize);
        
        ArrayTempDeque<E> deque = new ArrayTempDeque<>(this, top, 1 + offset, estimatedSize);
        return configureAfterNewInstance(deque, offset);
    }
    
    static final int DEFAULT_SET_EST_SIZE = 8;
    
    /**
     * <p>Creates and returns a new, empty set. An invocation of this method is
     * equivalent to <code>this.newSet({@value #DEFAULT_SET_EST_SIZE})</code>.</p>
     *
     * @param  <E> {@inheritDoc}
     * @return {@inheritDoc}
     * @see    #newSet(int)
     */
    @Override
    public final <E> ArrayTempHashSet<E> newSet() {
        return newSet(DEFAULT_SET_EST_SIZE);
    }
    
    /**
     * {@inheritDoc}
     *
     * <p>The new set is first constructed as if by {@code this.newSet(coll.size())}.</p>
     *
     * @param  coll {@inheritDoc}
     * @param  <E>  {@inheritDoc}
     * @return {@inheritDoc}
     * @throws NullPointerException {@inheritDoc}
     * @see    #newSet(int)
     */
    @SuppressWarnings("JavaDoc")
    @Override
    public final <E> ArrayTempHashSet<E> newSet(Collection<? extends E> coll) {
        ArrayTempHashSet<E> set = newSet(coll.size());
        set.addAll(coll);
        return set;
    }
    
    /**
     * <p>Creates and returns a new, empty set with the specified estimated
     * size and a default load factor. An invocation of this method is
     * equivalent to <code>this.newSet(estimatedSize, {@value ArrayTempHashSet#DEFAULT_LOAD_FACTOR})</code>.</p>
     *
     * @param  estimatedSize {@inheritDoc}
     * @param  <E>           {@inheritDoc}
     * @return {@inheritDoc}
     * @throws IllegalArgumentException {@inheritDoc}
     * @see    #newSet(int, float)
     */
    @SuppressWarnings("JavaDoc")
    @Override
    public final <E> ArrayTempHashSet<E> newSet(int estimatedSize) {
        return newSet(estimatedSize, ArrayTempHashSet.DEFAULT_LOAD_FACTOR);
    }
    
    /**
     * <p>Creates and returns a new, empty set using the specified load factor.
     * The set's initial capacity is set to the nearest power of 2 greater than
     * or equal to {@code estimatedSize / loadFactor}. For example, if the
     * estimated size is 6 and the load factor is 0.5, then the new set's
     * capacity will be 16. This is because 6/0.5 is 12, and the next greatest
     * power of 2 is 16.</p>
     *
     * <p>New sets are constructed this way to allow for some extra space
     * in the hash table for fewer collisions.</p>
     *
     * <p>The load factor must be greater than 0 and less than or equal to 1
     * (i.e. {@code 0 < loadFactor <= 1} must be {@code true}). For more
     * explanation of the load factor, see the class-level documentation for
     * for {@link ArrayTempHashSet}.</p>
     *
     * @param  estimatedSize an estimate for the eventual size of the new set
     * @param  loadFactor    the ratio of size to capacity the set should attempt to maintain
     * @param  <E>           the type of the new set's elements
     * @return a new, empty set
     * @throws IllegalArgumentException
     *         if the estimated size is negative or
     *         if the load factor is {@code NaN} or outside the range 0 (exclusive) to 1 (inclusive)
     * @see    ArrayTempHashSet
     */
    public final <E> ArrayTempHashSet<E> newSet(int estimatedSize, float loadFactor) {
        int initialCapacity = computeInitialCapacityForHashTable(estimatedSize, loadFactor);
        int offset = resizeAndGetOffsetForNewInstance(initialCapacity);
        
        ArrayTempHashSet<E> set = new ArrayTempHashSet<>(this, top, 1 + offset, initialCapacity, loadFactor);
        return configureAfterNewInstance(set, offset);
    }
    
    static final int DEFAULT_MAP_EST_SIZE = 8;
    
    /**
     * <p>Creates and returns a new, empty map. An invocation of this method is
     * equivalent to <code>this.newMap({@value #DEFAULT_MAP_EST_SIZE})</code>.</p>
     *
     * @param  <K> {@inheritDoc}
     * @param  <V> {@inheritDoc}
     * @return {@inheritDoc}
     * @see    #newMap(int)
     */
    @Override
    public final <K, V> ArrayTempHashMap<K, V> newMap() {
        return newMap(DEFAULT_MAP_EST_SIZE);
    }
    
    /**
     * {@inheritDoc}
     *
     * <p>The new map is first constructed as if by {@code this.newMap(entries.size())}.</p>
     *
     * @param  entries {@inheritDoc}
     * @param  <K>     {@inheritDoc}
     * @param  <V>     {@inheritDoc}
     * @return {@inheritDoc}
     * @throws NullPointerException {@inheritDoc}
     * @see    #newMap(int)
     */
    @SuppressWarnings("JavaDoc")
    @Override
    public final <K, V> ArrayTempHashMap<K, V> newMap(Map<? extends K, ? extends V> entries) {
        ArrayTempHashMap<K, V> map = newMap(entries.size());
        map.putAll(entries);
        return map;
    }
    
    /**
     * <p>Creates and returns a new, empty map with the specified estimated
     * size and a default load factor. An invocation of this method is
     * equivalent to <code>this.newMap(estimatedSize, {@value ArrayTempHashMap#DEFAULT_LOAD_FACTOR})</code>.</p>
     *
     * @param  estimatedSize {@inheritDoc}
     * @param  <K>           {@inheritDoc}
     * @param  <V>           {@inheritDoc}
     * @return {@inheritDoc}
     * @throws IllegalArgumentException {@inheritDoc}
     * @see    #newMap(int, float)
     */
    @SuppressWarnings("JavaDoc")
    @Override
    public final <K, V> ArrayTempHashMap<K, V> newMap(int estimatedSize) {
        return newMap(estimatedSize, ArrayTempHashMap.DEFAULT_LOAD_FACTOR);
    }
    
    /**
     * <p>Creates and returns a new, empty map using the specified load factor.
     * The map's table is initially sized to fit a maximum number of entries
     * equal to the nearest power of 2 greater than or equal to
     * {@code estimatedSize / loadFactor}. For example, if the estimated size
     * is 6 and the load factor is 0.5, then the new map's table can fit 16
     * entries. This is because 6/0.5 is 12, and the next greatest power of 2
     * is 16. (Note that an invocation of the new map's
     * {@link ArrayTempHashMap#capacity() capacity()} method would return 32,
     * because {@link ArrayTempHashMap} uses 2 array slots for each entry.)</p>
     *
     * <p>New maps are constructed this way to allow for some extra space
     * in the hash table for fewer collisions.</p>
     *
     * <p>The load factor must be greater than 0 and less than or equal to 1
     * (i.e. {@code 0 < loadFactor <= 1} must be {@code true}). For more
     * explanation of the load factor, see the class-level documentation for
     * {@code ArrayTempHashMap}.</p>
     *
     * @param  estimatedSize an estimate for the eventual size of the new map
     * @param  loadFactor    the ratio of entry count to table slots the map should attempt to maintain
     * @param  <K>           the type of the new map's keys
     * @param  <V>           the type of the new map's values
     * @return a new, empty map
     * @throws IllegalArgumentException
     *         if the estimated size is negative or
     *         if the load factor is {@code NaN} or outside the range 0 (exclusive) to 1 (inclusive)
     * @see    ArrayTempHashMap
     */
    public final <K, V> ArrayTempHashMap<K, V> newMap(int estimatedSize, float loadFactor) {
        int initialCapacity = computeInitialCapacityForHashTable(estimatedSize, loadFactor)
                            << 1; // Note: multiplied by 2
        int offset = resizeAndGetOffsetForNewInstance(initialCapacity);
        
        ArrayTempHashMap<K, V> map = new ArrayTempHashMap<>(this, top, 1 + offset, initialCapacity, loadFactor);
        return configureAfterNewInstance(map, offset);
    }
    
    private static int computeInitialCapacityForHashTable(int estimatedSize, float loadFactor) {
        Tools.requireNonNegative(estimatedSize, "estimatedSize");
        
        if (loadFactor != 1.0f) {
            ArrayTempOpenAddressedHashTable.requireValidLoadFactor(loadFactor);
            // use load factor to compute an ideal initial capacity for the given size estimate
            estimatedSize = (int) (Tools.toCeilFloat(estimatedSize) / loadFactor);
        }
        
        estimatedSize = Math.min(estimatedSize, ArrayTempHashSet.MAX_CAPACITY);
        
        return Tools.next2(estimatedSize);
    }
    
    private int resizeAndGetOffsetForNewInstance(int capacity) {
        Tools.requireNonNegative(capacity, "capacity");
        
        // 1 extra for the mark
        resizeIfNecessary(1 + capacity);
        
        int offset;
        
        ArrayTempObject top = this.top;
        if (top == null) {
            offset = 0;
        } else {
            offset = top.offset + top.capacity;
        }
        
        return offset;
    }
    
    private <T extends ArrayTempObject> T configureAfterNewInstance(T obj, int offset) {
        this.array[offset] = Mark.INSTANCE;
        
        this.top = obj;
        return obj;
    }
    
    @SuppressWarnings("UnusedReturnValue")
    private boolean resizeIfNecessary(int additionalCapacity) {
        if (top == null) {
            return resizeIfLengthLessThan(additionalCapacity);
        } else {
            return resizeIfUnclaimedCapacityLessThan(additionalCapacity);
        }
    }
    
    private boolean resizeIfLengthLessThan(int minCapacity) {
        Object[] array = this.array;
        
        if (array.length < minCapacity) {
            this.array = Arrays.copyOf(array, minCapacity);
            return true;
        }
        
        return false;
    }
    
    static final int MAX_ARRAY_SIZE = Integer.MAX_VALUE - 8;
    
    private boolean resizeIfUnclaimedCapacityLessThan(int additionalCapacity) {
        ArrayTempObject top = this.top;
        
        int topOffset = top.offset;
        int topCapacity = top.capacity;
        
        Object[] oldArray = this.array;
        int oldLength = oldArray.length;
        
        int claimedCapacity = topOffset + topCapacity;
        int unclaimedCapacity = oldLength - claimedCapacity;
        
        if (unclaimedCapacity >= additionalCapacity) {
            return false;
        }
        
        long requiredNewCapacity = (long) claimedCapacity + (long) additionalCapacity;
        long newLength = ((requiredNewCapacity + 1L) * 3L) >>> 1;
        
        if (newLength > MAX_ARRAY_SIZE) {
            newLength = requiredNewCapacity;
            if (newLength > MAX_ARRAY_SIZE) {
                throw new OutOfMemoryError("required new capacity = " + requiredNewCapacity);
            }
        }
        
        this.array = Arrays.copyOf(oldArray, (int) newLength);
        return true;
    }
    
    final void close(ArrayTempObject obj) {
        assert obj != null;
        
        if (obj != this.top) {
            throw new IllegalStateException();
        }
        
        int offset = obj.offset;
        int capacity = obj.capacity;
        int end = offset + capacity;
        
        Object[] array = this.array;
        assert Mark.INSTANCE == array[offset - 1] : toDebugString();
        
        Arrays.fill(array, offset, end, null);
        array[offset - 1] = null;
        
        this.top = obj.previous;
    }
    
    /**
     * <p>Requests a resize of the backing array.</p>
     *
     * <p>Returns the maximum extra space available for the temp object to claim
     * from the resized array. The size of this available space may be greater
     * than the requested additional size. The temp object may claim any amount
     * of the available space.</p>
     *
     * <p>The available space starts (inclusively) at {@code obj.offset + obj.capacity}
     * and ends (exclusively) at {@code obj.offset + obj.capacity + availableSize}.</p>
     *
     * @param  obj the temp object requesting a resize
     * @param  additionalSize the additional size requested
     *
     * @return the maximum extra space available for the temp object to claim from
     *         the resized array
     *
     * @throws IllegalStateException if the specified temp object is not the top
     */
    final int ensureAvailableCapacity(ArrayTempObject obj, int additionalSize) {
        if (obj != top) {
            throw new IllegalStateException();
        }
        resizeIfNecessary(additionalSize);
        return array.length - (obj.offset + obj.capacity);
    }
    
    final Object getDirect(ArrayTempObject obj, int index) {
        assert this == obj.getFactory();
        return array[obj.offset + index];
    }
    
    private String toDebugString() {
        return toCompactDebugString(new StringBuilder(), top).toString();
    }
    
    private StringBuilder toCompactDebugString(StringBuilder b, ArrayTempObject obj) {
        if (obj != null) {
            ArrayTempObject previous = obj.previous;
            toCompactDebugString(b, previous);
            if (previous != null) {
                b.append(' ');
            }
            
            int offset = obj.offset;
            int capacity = obj.capacity;
            int size = obj.size();
            
            b.append(obj.getClass().getSimpleName());
            b.append('[');
            
            Object[] array = this.array;
            
            for (int i = 0; i < capacity; ++i) {
                if (i < size) {
                    b.append((null == array[offset + i]) ? '0' : '1');
                } else {
                    b.append('x');
                }
            }
            
            b.append("];");
        }
        return b;
    }
}
