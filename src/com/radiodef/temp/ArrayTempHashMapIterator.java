/* MIT License
 *
 * Copyright (c) 2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.radiodef.temp;

import static com.radiodef.temp.ArrayTempHashMap.*;

import java.util.*;

abstract class ArrayTempHashMapIterator<K, V, E> implements Iterator<E> {
    final ArrayTempHashMap<K, V> map;
    
    private final int originalSize;
    private int count;
    private final int endIndex;
    private int prevIndex;
    private int prevIndexForRemove = NO_PREV_INDEX;
    
    private static final int NO_PREV_INDEX = -1;
    
    ArrayTempHashMapIterator(ArrayTempHashMap<K, V> map) {
        assert map != null;
        this.map = map;
        
        originalSize = map.size;
        
        int offset = map.offset;
        endIndex = offset + map.capacity;
        prevIndex = offset - 2;
    }
    
    @Override
    public final boolean hasNext() {
        return count < originalSize;
    }
    
    final int nextIndex() {
        int count = this.count;
        
        if (count >= originalSize)
            throw new NoSuchElementException();
        
        Object[] array = map.getFactory().array;
        int endIndex = this.endIndex;
        
        for (int i = (prevIndex + 2); i < endIndex; i += 2) {
            if (isKey(array[i])) {
                prevIndexForRemove = prevIndex = i;
                this.count = count + 1;
                return i;
            }
        }
        
        throw new ConcurrentModificationException();
    }
    
    @Override
    public final void remove() {
        int prev = prevIndexForRemove;
        
        if (NO_PREV_INDEX == prev)
            throw new IllegalStateException();
        
        ArrayTempHashMap<K, V> map = this.map;
        
        Object[] array = map.getFactory().array;
        
        array[prev] = Del.INSTANCE;
        array[prev + 1] = null;
        
        --map.size;
        
        prevIndexForRemove = NO_PREV_INDEX;
    }
    
    static final class OfKeys<K, V> extends ArrayTempHashMapIterator<K, V, K> {
        OfKeys(ArrayTempHashMap<K, V> map) { super(map); }
        
        @Override
        @SuppressWarnings("unchecked")
        public final K next() {
            return (K) Nil.unbox( map.getFactory().array[ nextIndex() ] );
        }
    }
    
    static final class OfValues<K, V> extends ArrayTempHashMapIterator<K, V, V> {
        OfValues(ArrayTempHashMap<K, V> map) { super(map); }
        
        @Override
        @SuppressWarnings("unchecked")
        public final V next() {
            return (V) map.getFactory().array[ nextIndex() + 1 ];
        }
    }
    
    static final class OfEntries<K, V> extends ArrayTempHashMapIterator<K, V, Entry<K, V>> {
        OfEntries(ArrayTempHashMap<K, V> map) { super(map); }
        
        @Override
        @SuppressWarnings("unchecked")
        public final Entry<K, V> next() {
            int i = nextIndex();
            ArrayTempHashMap<K, V> map = this.map;
            return new VirtualEntry<>(map, (K) Nil.unbox( map.getFactory().array[i] ), i + 1);
        }
    }
    
    // Note: The behavior of Map.Entry is only defined during iteration
    //       of the entrySet, so we can use a very simple implementation.
    // See https://docs.oracle.com/javase/10/docs/api/java/util/Map.Entry.html
    static final class VirtualEntry<K, V> implements Entry<K, V> {
        private final ArrayTempHashMap<K, V> map;
        private final K key;
        private final int valueIndex;
        
        VirtualEntry(ArrayTempHashMap<K, V> map, K key, int valueIndex) {
            assert map != null;
            this.map = map;
            this.key = key;
            this.valueIndex = valueIndex;
        }
        
        private Object[] array() {
            return map.getFactory().array;
        }
        
        @Override
        public final K getKey() {
            return key;
        }
        
        @Override
        @SuppressWarnings("unchecked")
        public final V getValue() {
            return (V) array()[valueIndex];
        }
        
        @Override
        public final V setValue(V val) {
            Object[] array = array();
            int valueIndex = this.valueIndex;
            @SuppressWarnings("unchecked")
            V old = (V) array[valueIndex];
            array[valueIndex] = val;
            return old;
        }
        
        @Override
        public final int hashCode() {
            return Objects.hashCode(key) ^ Objects.hashCode(array()[valueIndex]);
        }
        
        @Override
        public final boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj instanceof Entry<?, ?>) {
                Entry<?, ?> that = (Entry<?, ?>) obj;
                return Objects.equals(key, that.getKey())
                    && Objects.equals(array()[valueIndex], that.getValue());
            }
            return false;
        }
        
        @Override
        public final String toString() {
            return key + "=" + array()[valueIndex];
        }
    }
}
