/* MIT License
 *
 * Copyright (c) 2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.radiodef.temp;

final class Nil {
    private Nil() {
    }
    
    private static final int HASH_CODE = java.util.Objects.hashCode(null);
    
    @Override
    public final int hashCode() {
        return HASH_CODE;
    }
    
    @Override
    public final boolean equals(Object that) {
        return this == that || that instanceof Nil;
    }
    
    private static final String TO_STRING = String.valueOf((Object) null);
    
    @Override
    public final String toString() {
        return TO_STRING;
    }
    
    static final Nil INSTANCE = new Nil();
    
    static Object box(Object obj) {
        return (null == obj) ? INSTANCE : obj;
    }
    
    static Object unbox(Object obj) {
        return INSTANCE.equals(obj) ? null : obj;
    }
}
