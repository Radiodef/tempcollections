/* MIT License
 *
 * Copyright (c) 2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.radiodef.temp;

import java.util.*;
import java.util.Map.*;

final class ArrayTempHashMapEntrySet<K, V> extends AbstractSet<Entry<K, V>> {
    final ArrayTempHashMap<K, V> map;
    
    ArrayTempHashMapEntrySet(ArrayTempHashMap<K, V> map) {
        assert map != null;
        this.map = map;
    }
    
    @Override
    public final int size() {
        return map.size;
    }
    
    @Override
    public final void clear() {
        map.clear();
    }
    
    @Override
    public final boolean add(Entry<K, V> e) {
        throw new UnsupportedOperationException("Map.entrySet() does not support add");
    }
    
    @Override
    public final boolean addAll(Collection<? extends Entry<K, V>> c) {
        throw new UnsupportedOperationException("Map.entrySet() does not support addAll");
    }
    
    @Override
    public final boolean remove(Object obj) {
        if (obj instanceof Entry<?, ?>) {
            Entry<?, ?> e = (Entry<?, ?>) obj;
            return map.remove(e.getKey(), e.getValue());
        }
        return false;
    }
    
    @Override
    public final boolean contains(Object obj) {
        if (obj instanceof Entry<?, ?>) {
            Entry<?, ?> e = (Entry<?, ?>) obj;
            return map.containsEntry(e.getKey(), e.getValue());
        }
        return false;
    }
    
    @Override
    public final ArrayTempHashMapIterator.OfEntries<K, V> iterator() {
        return new ArrayTempHashMapIterator.OfEntries<>(map);
    }
    
    @Override
    public final int hashCode() {
        // Note: Map.hashCode() is defined as entrySet().hashCode()
        return map.hashCode();
    }
    
    @Override
    public final boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj instanceof Set<?>) {
            // our iterator() is expensive, so we don't
            // want to use it
            
            if (obj instanceof ArrayTempHashMapEntrySet<?, ?>)
                return map.equals(((ArrayTempHashMapEntrySet<?, ?>) obj).map);
            
            Set<?> that = (Set<?>) obj;
            
            if (that.size() != map.size)
                return false;
            
            for (Object e : that)
                if (!contains(e))
                    return false;
            
            return true;
        }
        return false;
    }
    
    // TODO: add any other optimizing method implementations
}
