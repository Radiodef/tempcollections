/* MIT License
 *
 * Copyright (c) 2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.radiodef.temp;

import static com.radiodef.temp.ArrayTempDeque.*;

import java.util.*;
import java.util.function.*;

abstract class ArrayTempDequeIterator<E> implements Iterator<E> {
    final ArrayTempDeque<E> q;
    
    private final int increment;
    private final int endIndex;
    private int nextIndex;
    private int prevIndex = NOT_INDEX;
    
    ArrayTempDequeIterator(ArrayTempDeque<E> q, int start, int end, int increment) {
        assert q != null;
        this.q = q;
        
        nextIndex = start;
        endIndex = end;
        
        assert increment == +1 || increment == -1 : increment;
        this.increment = increment;
    }
    
    
    @Override
    public final boolean hasNext() {
        int next = nextIndex;
        if (next != NOT_INDEX) {
            ArrayTempDeque<E> q = this.q;
            
            int head = q.head;
            int tail = q.tail;
            
            if (head <= tail) {
                return head <= next && next <= tail;
            } else {
                return next <= tail || head <= next;
            }
        }
        return false;
    }
    
    @Override
    public final E next() {
        if (hasNext()) {
            ArrayTempDeque<E> q = this.q;
            
            int next = nextIndex;
            @SuppressWarnings("unchecked")
            E e = (E) q.getFactory().array[next];
            
            prevIndex = next;
            nextIndex = (
                (next == endIndex) ? NOT_INDEX
                                   : add(next, q.offset, q.capacity, increment)
            );
            return e;
        }
        throw new NoSuchElementException();
    }
    
    @Override
    public final void remove() {
        int i = prevIndex;
        if (i == NOT_INDEX)
            throw new IllegalStateException();
        
        ArrayTempDeque<E> q = this.q;
        Object[] arr = q.getFactory().array;
        int off = q.offset;
        int cap = q.capacity;
        
        int inc = -increment;
        int end = (inc < 0) ? q.head : q.tail;
        
        while (i != end) {
            int next = add(i, off, cap, inc);
            arr[i] = arr[next];
            i = next;
        }
        
        arr[i] = null;
        
        if (--q.size == 0)
            q.head = q.tail = NOT_INDEX;
        else {
            i = add(i, off, cap, -inc);
            if (inc < 0)
                q.head = i;
            else
                q.tail = i;
        }
        
        prevIndex = NOT_INDEX;
    }
    
    @Override
    public final void forEachRemaining(Consumer<? super E> action) {
        // TODO: optimize?
        Iterator.super.forEachRemaining(action);
    }
    
    static final class Ascending<E> extends ArrayTempDequeIterator<E> {
        Ascending(ArrayTempDeque<E> q) {
            super(q, q.head, q.tail, +1);
        }
    }
    
    static final class Descending<E> extends ArrayTempDequeIterator<E> {
        Descending(ArrayTempDeque<E> q) {
            super(q, q.tail, q.head, -1);
        }
    }
}