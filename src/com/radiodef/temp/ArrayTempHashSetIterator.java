/* MIT License
 *
 * Copyright (c) 2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.radiodef.temp;

import static com.radiodef.temp.ArrayTempHashSet.*;

import java.util.*;
import java.util.function.*;

final class ArrayTempHashSetIterator<E> implements Iterator<E> {
    final ArrayTempHashSet<E> set;
    
    private int seenCount;
    private final int originalSize;
    
    private static final int NO_PREV = -1;
    
    private int prevIndexForRemove = NO_PREV;
    private int prevIndex;
    private final int end;
    
    ArrayTempHashSetIterator(ArrayTempHashSet<E> set) {
        assert set != null;
        this.set = set;
        
        originalSize = set.size;
        
        int offset = set.offset;
        prevIndex = offset - 1;
        end = offset + set.capacity;
    }
    
    @Override
    public final boolean hasNext() {
        return seenCount < originalSize;
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public final E next() {
        ArrayTempHashSet<E> set = this.set;
        int seenCount = this.seenCount;
        
        if (seenCount >= originalSize) {
            throw new NoSuchElementException();
        }
        
        Object[] array = set.getFactory().array;
        int end = this.end;
        
        for (int i = (prevIndex + 1); i < end; ++i) {
            Object e = array[i];
            
            if (isElement(e)) {
                prevIndex = prevIndexForRemove = i;
                this.seenCount = seenCount + 1;
                return (E) Nil.unbox(e);
            }
        }
        
        throw new ConcurrentModificationException();
    }
    
    @Override
    public final void remove() {
        // TODO: think of another reasonable way which doesn't use deletion marks
        removeByMarkingDeletion();
    }
    
    private void removeByMarkingDeletion() {
        int prev = prevIndexForRemove;
        
        if (NO_PREV == prev) {
            throw new IllegalStateException();
        }
        
        ArrayTempHashSet<E> set = this.set;
        
        set.getFactory().array[prev] = Del.INSTANCE;
        --set.size;
        
        prevIndexForRemove = NO_PREV;
    }
    
    @Override
    public final void forEachRemaining(Consumer<? super E> action) {
        // TODO: optimize?
        Iterator.super.forEachRemaining(action);
    }
}
