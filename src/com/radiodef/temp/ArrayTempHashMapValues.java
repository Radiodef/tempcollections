/* MIT License
 *
 * Copyright (c) 2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.radiodef.temp;

import java.util.*;

import static com.radiodef.temp.ArrayTempHashMap.isKey;

final class ArrayTempHashMapValues<V> extends AbstractCollection<V> {
    final ArrayTempHashMap<?, V> map;
    
    ArrayTempHashMapValues(ArrayTempHashMap<?, V> map) {
        assert map != null;
        this.map = map;
    }
    
    @Override
    public final int size() {
        return map.size;
    }
    
    @Override
    public final void clear() {
        map.clear();
    }
    
    @Override
    public final boolean contains(Object obj) {
        // noinspection SuspiciousMethodCalls
        return map.containsValue(obj);
    }
    
    @Override
    public final boolean remove(Object obj) {
        ArrayTempHashMap<?, V> map = this.map;
        Object[] arr = map.getFactory().array;
        
        for (int i = (map.offset + 1), end = (i + map.capacity); i < end; i += 2) {
            if (Objects.equals(arr[i], obj)) {
                int k = i - 1;
                if (isKey(arr[k]) && map.removeAtIndex(k)) {
                    return true;
                }
            }
        }
        
        return false;
    }
    
    @Override
    public final boolean add(V val) {
        throw new UnsupportedOperationException("Map.values() does not support add");
    }
    
    @Override
    public final boolean addAll(Collection<? extends V> coll) {
        throw new UnsupportedOperationException("Map.values() does not support addAll");
    }
    
    @Override
    public final ArrayTempHashMapIterator.OfValues<?, V> iterator() {
        return new ArrayTempHashMapIterator.OfValues<>(map);
    }
    
    // TODO: reimplement any optimized methods
}