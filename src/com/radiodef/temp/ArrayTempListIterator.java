/* MIT License
 *
 * Copyright (c) 2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.radiodef.temp;

import java.util.*;
import java.util.function.*;
    
final class ArrayTempListIterator<E> implements ListIterator<E> {
    final ArrayTempList<E> list;
    
    private int previousIndex;
    private int nextIndex;
    private int lastIndex;
    
    private static final int NO_LAST_INDEX = -1;
    
    ArrayTempListIterator(ArrayTempList<E> list, int startIndex) {
        assert list != null;
        assert 0 <= startIndex && startIndex <= list.size : "startIndex=" + startIndex;
        
        this.list = list;
        this.previousIndex = startIndex - 1;
        this.nextIndex = startIndex;
        this.lastIndex = NO_LAST_INDEX;
    }
    
    @Override
    public final int nextIndex() {
        return nextIndex;
    }
    
    @Override
    public final boolean hasNext() {
        return nextIndex < list.size;
    }
    
    @Override
    public final E next() {
        ArrayTempList<E> list = this.list;
        int nextIndex = this.nextIndex;
        int size = list.size;
        
        if (nextIndex < size) {
            @SuppressWarnings("unchecked")
            E next = (E) list.getFactory().array[list.offset + nextIndex];
            
            this.previousIndex = nextIndex;
            this.nextIndex = nextIndex + 1;
            this.lastIndex = nextIndex;
            return next;
        }
        
        throw new NoSuchElementException("nextIndex=" + nextIndex + ", size=" + size);
    }
    
    @Override
    public final int previousIndex() {
        return previousIndex;
    }
    
    @Override
    public final boolean hasPrevious() {
        return previousIndex >= 0;
    }
    
    @Override
    public final E previous() {
        ArrayTempList<E> list = this.list;
        int previousIndex = this.previousIndex;
        
        if (previousIndex >= 0) {
            @SuppressWarnings("unchecked")
            E previous = (E) list.getFactory().array[list.offset + previousIndex];
            
            this.previousIndex = previousIndex - 1;
            this.nextIndex = previousIndex;
            this.lastIndex = previousIndex;
            return previous;
        }
        
        throw new NoSuchElementException("previousIndex=" + previousIndex + ", size=" + list.size);
    }
    
    private int getLastIndex() {
        int lastIndex = this.lastIndex;
        
        if (NO_LAST_INDEX == lastIndex) {
            throw new IllegalStateException("no next or previous index");
        }
        
        return lastIndex;
    }
    
    @Override
    public final void remove() {
        int lastIndex = getLastIndex();
        list.remove(lastIndex);
        
        int nextIndex = this.nextIndex;
        if (lastIndex != nextIndex) {
            // only update the indexes when doing forward iteration
            this.previousIndex = (this.nextIndex = (nextIndex - 1)) - 1;
        }
        
        this.lastIndex = NO_LAST_INDEX;
    }
    
    @Override
    public final void add(E elem) {
        int nextIndex = this.nextIndex;
        list.add(nextIndex, elem);
        
        this.previousIndex = nextIndex;
        this.nextIndex = nextIndex + 1;
        this.lastIndex = NO_LAST_INDEX;
    }
    
    @Override
    public final void set(E elem) {
        int lastIndex = getLastIndex();
        
        ArrayTempList<E> list = this.list;
        list.getFactory().array[list.offset + lastIndex] = elem;
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public final void forEachRemaining(Consumer<? super E> action) {
        ArrayTempList<E> list = this.list;
        
        int nextIndex = this.nextIndex;
        int size = list.size;
        
        if (nextIndex < size) {
            Object[] array = list.getFactory().array;
            int offset = list.offset;
            int end = offset + size;
            
            for (int i = offset + nextIndex; i < end; ++i) {
                action.accept((E) array[i]);
            }
            
            this.nextIndex = size;
            this.lastIndex = this.previousIndex = (size - 1);
        }
    }
}