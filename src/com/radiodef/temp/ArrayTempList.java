/* MIT License
 *
 * Copyright (c) 2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.radiodef.temp;

import java.util.*;
import java.util.function.*;

/**
 * <p>{@code ArrayTempList} is the {@link TempList} implementation for
 * {@link ArrayTempFactory}.</p>
 *
 * <p>This list implementation supports {@code null} elements.</p>
 *
 * @param <E> the element type of this list
 * @see   ArrayTempFactory#newList(int)
 */
public final class ArrayTempList<E> extends ArrayTempObject
        implements TempList<E>, RandomAccess {
    
    ArrayTempList(ArrayTempFactory factory,
                  ArrayTempObject previous,
                  int offset,
                  int capacity) {
        super(factory, previous, offset, capacity);
    }
    
    /**
     * <p>Sets the capacity of this list to be equal to its size. The list's capacity
     * is the length of the subsection of the factory array used by the list.</p>
     *
     * @throws IllegalStateException if this list is not at the top of the factory stack
     * @see    #isTop()
     * @see    #capacity()
     */
    public final void trimCapacity() {
        if (!isTop())
            throw new IllegalStateException("this is not the top");
        capacity = size;
    }
    
    @Override
    public final void clear() {
        if (size != 0) {
            int start = offset;
            int end = start + size;
            Arrays.fill(getFactory().array, start, end, null);
            size = 0;
        }
    }
    
    private static void requireBounds(int size, int index) {
        if (index < 0 || size <= index) {
            throw new IndexOutOfBoundsException("size=" + size + ", index=" + index);
        }
    }
    
    private static void requireBoundsClosed(int size, int index) {
        if (index < 0 || size < index) {
            throw new IndexOutOfBoundsException("size=" + size + ", index=" + index);
        }
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public final E get(int index) {
        requireBounds(size, index);
        return (E) getFactory().array[offset + index];
    }
    
    @Override
    public final E set(int index, E elem) {
        requireBounds(size, index);
        int rawIndex = offset + index;
        Object[] array = getFactory().array;
        
        @SuppressWarnings("unchecked")
        E old = (E) array[rawIndex];
        array[rawIndex] = elem;
        
        return old;
    }
    
    private void resizeIfNecessary(int additionalSize) {
        int size = this.size;
        int capacity = this.capacity;
        
        assert additionalSize >= 0 : additionalSize;
        int extraCapacityNeeded = additionalSize - (capacity - size);
        
        if (extraCapacityNeeded <= 0)
            return;
        
        int extraCapacity = getFactory().ensureAvailableCapacity(this, extraCapacityNeeded);
        // The factory might have resized to be even larger than
        // we requested, so we won't be greedy and claim all of it.
        this.capacity = (int) Math.min(((capacity + 1) * 3L) >>> 1,
                                       capacity + extraCapacity);
    }
    
    /**
     * <p>Appends the specified element to the end of this list.</p>
     *
     * <p>If this list is not at the top of the factory stack and
     * does not have enough capacity to fit another element, then
     * an {@link IllegalStateException} is thrown instead.</p>
     *
     * @param  elem the element to add
     * @return {@code true}
     * @throws IllegalStateException if {@code this.canAdd(1)} would return {@code false}
     * @see    ArrayTempList#canAdd(int)
     */
    @Override
    public final boolean add(E elem) {
        resizeIfNecessary(1);
        getFactory().array[offset + ( size++ )] = elem;
        return true;
    }
    
    /**
     * <p>Inserts the specified element at the specified index in this list.</p>
     *
     * <p>If this list is not at the top of the factory stack and
     * does not have enough capacity to fit another element, then
     * an {@link IllegalStateException} is thrown instead.</p>
     *
     * @param  index the index to insert the element at
     * @param  elem  the element to insert
     * @throws IndexOutOfBoundsException
     *         if the index is negative or greater than {@code this.size()}
     * @throws IllegalStateException
     *         if {@code this.canAdd(1)} would return {@code false}
     * @see    ArrayTempList#canAdd(int)
     */
    @Override
    public final void add(int index, E elem) {
        int size = this.size;
        requireBoundsClosed(size, index);
        resizeIfNecessary(1);
        
        Object[] array = getFactory().array;
        int offset = this.offset;
        int rawIndex = offset + index;
        
        if (index != size) {
            System.arraycopy(array, rawIndex, array, rawIndex + 1, size - index);
        }
        
        array[rawIndex] = elem;
        this.size = size + 1;
    }
    
    /**
     * <p>Appends all of the elements in the specified collection to the
     * end of this list.</p>
     *
     * <p>If this list does not have the capacity to fit <em>all</em> of
     * the elements in the specified collection (i.e. {@code this.canAdd(coll.size())}
     * would return {@code false}), an {@link IllegalStateException} is thrown
     * and this list is not modified.</p>
     *
     * @param  coll the elements to add
     * @return {@code true} if the specified collection is not empty and
     *         {@code false} otherwise
     * @throws NullPointerException  if the collection is {@code null}
     * @throws IllegalStateException if {@code this.canAdd(coll.size())} would
     *                               return {@code false}
     * @see    ArrayTempList#canAdd(int)
     */
    @Override
    public final boolean addAll(Collection<? extends E> coll) {
        int addSize = coll.size();
        if (addSize == 0) {
            return false;
        }
        resizeIfNecessary(addSize);
        
        Object[] array = getFactory().array;
        int size = this.size;
        int offset = this.offset;
        
        try {
            for (E e : coll) {
                array[offset + ( size++ )] = e;
            }
        } finally {
            // if coll's Iterator throws, we set the size no matter what
            this.size = size;
        }
        return true;
    }
    
    /**
     * <p>Inserts all of the elements in the specified collection at the
     * specified index in this list.</p>
     *
     * <p>If this list is not at the top of the factory stack and does not
     * have the capacity to fit <em>all</em> of the elements in the specified
     * collection (i.e. {@code this.canAdd(coll.size())} would return
     * {@code false}), an {@link IllegalStateException} is thrown and this
     * list is <em>not</em> modified.</p>
     *
     * @param  index the index to insert the elements at
     * @param  coll  the elements to insert
     * @return {@code true} if the specified collection is not empty and
     *         {@code false} otherwise
     * @throws NullPointerException
     *         if the collection is {@code null}
     * @throws IndexOutOfBoundsException
     *         if the index is negative or greater than {@code this.size()}
     * @throws IllegalStateException
     *         if {@code this.canAdd(1)} would return {@code false}
     * @see    ArrayTempList#canAdd(int)
     */
    @Override
    public final boolean addAll(int index, Collection<? extends E> coll) {
        int oldSize = size;
        requireBoundsClosed(oldSize, index);
        
        int addSize = coll.size();
        if (addSize == 0) {
            return false;
        }
        resizeIfNecessary(addSize);
        
        Object[] array = getFactory().array;
        int offset = this.offset;
        int rawIndex = offset + index;
        int endIndex = rawIndex;
        
        if (index != oldSize) {
            System.arraycopy(array, rawIndex, array, rawIndex + addSize, oldSize - index);
        }
        
        try {
            for (E e : coll) {
                array[endIndex++] = e;
            }
        } finally {
            int copyCount = (endIndex - rawIndex);
            int newSize = oldSize + copyCount;
            if (copyCount != addSize) {
                // if coll's Iterator throws, we piece together the array
                // as it should be with the fewer elements
                int shiftStart = rawIndex + addSize;
                int shiftAmount = oldSize - index;
                System.arraycopy(array, shiftStart, array, endIndex, shiftAmount);
                Arrays.fill(array, offset + newSize, offset + oldSize + addSize, null);
            }
            // and set the size no matter what
            size = newSize;
        }
        return true;
    }
    
    @Override
    public final E remove(int index) {
        int size = this.size;
        requireBounds(size, index);
        
        Object[] array = getFactory().array;
        int offset = this.offset;
        
        int rawIndex = offset + index;
        @SuppressWarnings("unchecked")
        E e = (E) array[rawIndex];
        
        --size;
        System.arraycopy(array, rawIndex + 1, array, rawIndex, size - index);
        array[offset + size] = null;
        this.size = size;
        
        return e;
    }
    
    @Override
    public final boolean remove(Object obj) {
        int index = indexOf(obj);
        if (index == -1) {
            return false;
        } else {
            remove(index);
            return true;
        }
    }
    
    private <T> boolean removeAll(T arg0, BiPredicate<? super T, ? super E> cond) {
        int size = this.size;
        if (size == 0) {
            return false;
        }
        
        Object[] array = getFactory().array;
        int start = offset;
        int end = start + size;
        
        int src = start;
        int dst = start;
        
        boolean modified;
        
        try {
            for (; src < end; ++src) {
                @SuppressWarnings("unchecked")
                E e = (E) array[src];
                
                if (!cond.test(arg0, e)) {
                    if (src != dst) {
                        array[dst] = e;
                    }
                    ++dst;
                }
            }
        } finally {
            int removedCount = (src - dst);
            modified = (removedCount != 0);
            // if the condition test threw an exception,
            // make sure to leave ourselves in a valid state
            System.arraycopy(array, src, array, dst, end - src);
            // and fill
            Arrays.fill(array, end - removedCount, end, null);
            this.size = (size - removedCount);
        }
        
        return modified;
    }
    
    @Override
    public final boolean removeAll(Collection<?> coll) {
        if (coll.isEmpty())
            return false;
        return removeAll(coll, Tools.callingCollectionContains());
    }
    
    @Override
    public final boolean retainAll(Collection<?> coll) {
        if (coll.isEmpty()) {
            if (isEmpty())
                return false;
            clear();
            return true;
        }
        return removeAll(coll, Tools.callingNotCollectionContains());
    }
    
    @Override
    public final boolean removeIf(Predicate<? super E> cond) {
        Objects.requireNonNull(cond);
        return removeAll(cond, Tools.callingPredicateTest());
    }
    
    final void removeRange(int start, int end) {
        int size = this.size;
        
        if (start < 0 || start > end || end > size) {
            throw new IndexOutOfBoundsException("start=" + start + ", end=" + end + ", size=" + size);
        }
        
        Object[] array = getFactory().array;
        int offset = this.offset;
        
        int rawStart = offset + start;
        int rawEnd = offset + end;
        
        System.arraycopy(array, rawEnd, array, rawStart, size - end);
        
        int removeCount = end - start;
        
        rawEnd = offset + size;
        rawStart = rawEnd - removeCount;
        Arrays.fill(array, rawStart, rawEnd, null);
        
        this.size = size - removeCount;
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public final void replaceAll(UnaryOperator<E> op) {
        Objects.requireNonNull(op);
        
        int size = this.size;
        if (size != 0) {
            Object[] array = getFactory().array;
            int start = offset;
            int end = start + size;
            
            for (int i = start; i < end; ++i) {
                array[i] = op.apply((E) array[i]);
            }
        }
    }
    
    @Override
    public final int indexOf(Object obj) {
        int size = this.size;
        if (size != 0) {
            Object[] array = getFactory().array;
            int start = offset;
            int end = start + size;
            
            if (null == obj) {
                for (int i = start; i < end; ++i)
                    if (null == array[i])
                        return i - start;
            } else {
                for (int i = start; i < end; ++i)
                    if (obj.equals(array[i]))
                        return i - start;
            }
        }
        return -1;
    }
    
    @Override
    public final int lastIndexOf(Object obj) {
        int size = this.size;
        if (size != 0) {
            Object[] array = getFactory().array;
            int start = offset;
            int end = start + size - 1;
            
            if (null == obj) {
                for (int i = end; i >= start; --i)
                    if (null == array[i])
                        return i - start;
            } else {
                for (int i = end; i >= start; --i)
                    if (obj.equals(array[i]))
                        return i - start;
            }
        }
        return -1;
    }
    
    @Override
    public final boolean contains(Object obj) {
        return indexOf(obj) != -1;
    }
    
    @Override
    public final boolean containsAll(Collection<?> coll) {
        for (Object obj : coll)
            if (!contains(obj))
                return false;
        return true;
    }
    
    @Override
    public final Object[] toArray() {
        int size = this.size;
        if (size == 0)
            return new Object[0];
        int start = offset;
        int end = start + size;
        return Arrays.copyOfRange(getFactory().array, start, end);
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public final <T> T[] toArray(T[] in) {
        int inLength = in.length; // Note: throws contractual NPE.
        
        int size = this.size;
        if (size != 0) {
            Object[] array = getFactory().array;
            int offset = this.offset;
            
            if (inLength < size) {
                return (T[]) Arrays.copyOfRange(array, offset, offset + size, in.getClass());
            }
        
            // noinspection SuspiciousSystemArraycopy
            System.arraycopy(array, offset, in, 0, size);
        }
        
        if (inLength > size) {
            in[size] = null;
        }
        
        return in;
    }
    
    /**
     * Returns a list iterator over the elements of this list. Note: the
     * returned iterator's {@link java.util.ListIterator#add(Object) add(E)}
     * method throws {@link IllegalStateException} if it's invoked when
     * {@code this.canAdd(1)} would return {@code false}.
     *
     * @param  startIndex the starting index
     * @return a list iterator over the elements of this list
     * @see    ArrayTempList#canAdd(int)
     */
    @Override
    public final ListIterator<E> listIterator(int startIndex) {
        requireBoundsClosed(size, startIndex);
        return new ArrayTempListIterator<>(this, startIndex);
    }
    
    /**
     * Returns a list iterator over the elements of this list. Note: the
     * returned iterator's {@link java.util.ListIterator#add(Object) add(E)}
     * method throws {@link IllegalStateException} if it's invoked when
     * {@code this.canAdd(1)} would return {@code false}.
     *
     * @return a list iterator over the elements of this list
     * @see    ArrayTempList#canAdd(int)
     */
    @Override
    public final ListIterator<E> listIterator() {
        return new ArrayTempListIterator<>(this, 0);
    }
    
    @Override
    public final Iterator<E> iterator() {
        return new ArrayTempListIterator<>(this, 0);
    }
    
    @Override
    public final Spliterator<E> spliterator() {
        int start = offset;
        int end = start + size;
        return Spliterators.spliterator(getFactory().array, start, end, Spliterator.ORDERED);
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public final void forEach(Consumer<? super E> action) {
        Objects.requireNonNull(action);
        
        int size = this.size;
        if (size != 0) {
            Object[] array = getFactory().array;
            int start = offset;
            int end = start + size;
            
            for (int i = start; i < end; ++i) {
                action.accept((E) array[i]);
            }
        }
    }
    
    /**
     * Returns a sublist view of the specified range in this list.
     * Note: methods of the returned sublist which would increase
     * the size of this list (such as {@link List#add(Object) add}
     * and {@link List#addAll(Collection) addAll}) will throw
     * {@link IllegalStateException} if they're invoked when
     * <code>this.canAdd(&hellip;)</code> would return {@code false}.
     *
     * @param  fromIndex the starting index of the sublist range (inclusive)
     * @param  toIndex   the ending index of the sublist range (exclusive)
     * @return a sublist view of the specified range in this list
     * @throws IndexOutOfBoundsException
     *         if {@code fromIndex} or {@code toIndex} is negative,
     *         if {@code fromIndex} is greater than {@code toIndex} or
     *         if {@code toIndex} is greater than {@code this.size()}
     * @see    ArrayTempList#canAdd(int)
     */
    @Override
    public final List<E> subList(int fromIndex, int toIndex) {
        // note: index validation is done in ArrayTempSubList constructor
        return new ArrayTempSubList<>(this, fromIndex, toIndex);
    }
    
    /**
     * <p>Sorts this list with the specified comparator.</p>
     *
     * <p>This implementation uses a merge sort which uses the factory
     * backing array as auxiliary storage.</p>
     *
     * @param comp the comparator to sort this list with,
     *             or {@code null} if the elements' natural order
     *             should be used
     * @throws ClassCastException if the specified comparator is
     *         {@code null} and this list contains elements which
     *         are not mutually comparable or which do not implement
     *         {@link Comparable}
     */
    @Override
    public final void sort(Comparator<? super E> comp) {
        ArrayTempListSorting.sort(this, comp);
    }
    
    @Override
    public final int hashCode() {
        int hash = 1;
        
        int size = this.size;
        if (size != 0) {
            Object[] array = getFactory().array;
            int start = offset;
            int end = start + size;
            
            for (int i = start; i < end; ++i) {
                hash = 31 * hash + Objects.hashCode(array[i]);
            }
        }
        
        return hash;
    }
    
    @Override
    public final boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj instanceof List<?>) {
            List<?> that = (List<?>) obj;
            
            int size = this.size;
            if (that.size() != size)
                return false;
            if (size == 0)
                return true;
            
            Object[] array = getFactory().array;
            int offset = this.offset;
            
            if (that instanceof RandomAccess) {
                for (int i = 0; i < size; ++i) {
                    if (!Objects.equals(array[offset + i], that.get(i)))
                        return false;
                }
            } else {
                Iterator<?> iter = that.iterator();
                int end = offset + size;
                for (int i = offset; i < end; ++i) {
                    assert iter.hasNext() : that.getClass();
                    if (!Objects.equals(array[i], iter.next()))
                        return false;
                }
            }
            return true;
        }
        return false;
    }
    
    @Override
    public final String toString() {
        int size = this.size;
        if (size == 0)
            return "[]";
        
        StringBuilder b = new StringBuilder(10 * size).append('[');
        
        Object[] array = getFactory().array;
        int start = offset;
        int end = start + size;
        
        for (int i = start; i < end; ++i) {
            if (i != start) {
                b.append(", ");
            }
            b.append(array[i]);
        }
        
        return b.append(']').toString();
    }
}