/* MIT License
 *
 * Copyright (c) 2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.radiodef.temp;

import java.util.*;
import java.util.function.*;

/**
 * <p>{@code ArrayTempDeque} is the {@link TempDeque} implementation for
 * {@link ArrayTempFactory}.</p>
 *
 * <p>This deque implementation supports {@code null} elements.</p>
 *
 * @param <E> the element type of this deque
 * @see   ArrayTempFactory#newDeque(int)
 */
public final class ArrayTempDeque<E> extends ArrayTempObject implements TempDeque<E> {
    
    private static final int MAX_CAPACITY = 1 << 30;
    
    static final int NOT_INDEX = -1;
    
    // Note: indexes are absolute indexes in to the factory array,
    //       not indexes relative to the offset.
    int head = NOT_INDEX;
    int tail = NOT_INDEX;
    
    ArrayTempDeque(ArrayTempFactory factory,
                   ArrayTempObject previous,
                   int offset,
                   int capacity) {
        super(factory, previous, offset, capacity);
        
        assert Tools.isPowerOf2(capacity) : capacity;
    }
    
    static int RESIZES; // Note: only used for testing.
    
    /**
     * @param  additionalSize the number of additional elements to attempt
     *                        to fit
     * @return {@code true} if there's space for the additional elements and
     *         {@code false} otherwise
     */
    private boolean ensureCapacity(int additionalSize) {
        if (additionalSize == 0)
            return true;
        assert additionalSize > 0 : additionalSize;
        
        int size = this.size;
        int cap = capacity;
        
        int extraSpace = cap - size;
        if (additionalSize <= extraSpace)
            return true;
        
        if (!isTop())
            return false;
        if (cap == MAX_CAPACITY)
            return false;
        
        int newCapacity = cap << 1;
        
        long newSize = (long) size + (long) additionalSize;
        if ((long) newCapacity < newSize) {
            if (newSize > MAX_CAPACITY)
                return false;
            
            newCapacity = Tools.next2((int) newSize);
        }
        
        ArrayTempFactory factory = getFactory();
        
        int requested = newCapacity - cap;
        int available = factory.ensureAvailableCapacity(this, requested);
        assert available >= requested : available + ", " + requested;
        
        capacity = newCapacity;
        
        int head = this.head;
        int tail = this.tail;
        
        if (tail < head) {
            int off = offset;
            
            int hiCount = off + cap - head;
            int newHead = off + newCapacity - hiCount;
            
            Object[] arr = factory.array;
            System.arraycopy(arr, head, arr, newHead, hiCount);
            
            // capacity doubled, so hiCount must be < oldCapacity,
            // and newHead must be > offset + oldCapacity
            assert (off + cap) <= newHead;
            Arrays.fill(arr, head, off + cap, null);
            
            this.head = newHead;
        }
        
        ++RESIZES;
        return true;
    }
    
    static int add(int i, int off, int cap, int increment) {
        return off + ((i - off + increment) & (cap - 1));
    }
    
    private static int next(int i, int off, int cap) {
        return off + ((i - off + 1) & (cap - 1));
    }
    
    private static int prev(int i, int off, int cap) {
        return off + ((i - off - 1) & (cap - 1));
    }
    
    private static int absolute(int i, int off, int cap, int head) {
        return off + ((head - off + i) & (cap - 1));
    }
    
    @Override
    public final void clear() {
        if (size != 0) {
            Arrays.fill(getFactory().array, offset, offset + capacity, null);
            size = 0;
        }
    }
    
    /**
     * <p>Adds the specified element to the head of this queue.</p>
     *
     * <p>If this {@code ArrayTempDeque} is not at the top of the factory
     * stack and does not have enough capacity to fit another element, then
     * an {@link IllegalStateException} is thrown instead.</p>
     *
     * <p>This method is equivalent to {@link #addFirst(Object) addFirst(E)}.</p>
     *
     * @param  elem the element to add
     * @throws IllegalStateException if {@code this.canAdd(1)} would return {@code false}
     * @see    ArrayTempDeque#canAdd(int)
     */
    @Override
    public final void push(E elem) {
        addFirst(elem);
    }
    
    /**
     * <p>Adds the specified element to the tail of the queue represented by
     * this deque.</p>
     *
     * <p>If this deque is not at the top of the factory stack and does not
     * have enough capacity to fit another element (i.e. {@code this.canAdd(1)}
     * would return {@code false}), then an {@link IllegalStateException} is
     * thrown instead.</p>
     *
     * <p>This method is equivalent to {@link #addLast(Object) addLast(E)}.</p>
     *
     * @param  elem the element to add
     * @return {@code true}
     * @throws IllegalStateException if {@code this.canAdd(1)} would return {@code false}
     * @see    ArrayTempDeque#canAdd(int)
     */
    @Override
    public final boolean add(E elem) {
        addLast(elem);
        return true;
    }
    
    private IllegalStateException newCapacityFullException() {
        return new IllegalStateException("size=" + size + ", capacity=" + capacity);
    }
    
    /**
     * <p>Adds the specified element to the head of the queue represented by
     * this deque.</p>
     *
     * <p>If this deque is not at the top of the factory stack and does not
     * have enough capacity to fit another element (i.e. {@code this.canAdd(1)}
     * would return {@code false}), then an {@link IllegalStateException} is
     * thrown instead.</p>
     *
     * @param  elem the element to add
     * @throws IllegalStateException if {@code this.canAdd(1)} would return {@code false}
     * @see    ArrayTempDeque#canAdd(int)
     */
    @Override
    public final void addFirst(E elem) {
        if (!offerFirst(elem))
            throw newCapacityFullException();
    }
    
    /**
     * <p>Adds the specified element to the tail of the queue represented by
     * this deque.</p>
     *
     * <p>If this deque is not at the top of the factory stack and does not
     * have enough capacity to fit another element (i.e. {@code this.canAdd(1)}
     * would return {@code false}), then an {@link IllegalStateException} is
     * thrown instead.</p>
     *
     * @param  elem the element to add
     * @throws IllegalStateException if {@code this.canAdd(1)} would return {@code false}
     * @see    ArrayTempDeque#canAdd(int)
     */
    @Override
    public final void addLast(E elem) {
        if (!offerLast(elem))
            throw newCapacityFullException();
    }
    
    /**
     * <p>Adds all of the elements in the specified collection to the tail
     * of the queue represented by this deque, in the order they are returned
     * by the collection's iterator. This is i.e. equivalent to the following:</p>
     *
     *<pre><code>for (E e : coll)
     *    this.add(e);</code></pre>
     *
     * <p>If this deque is not at the top of the factory stack and does not
     * have enough capacity to fit all of the elements (i.e. {@code this.canAdd(coll.size())}
     * would return {@code false}), then an {@link IllegalStateException} is
     * thrown instead.</p>
     *
     * @param  coll the elements to add
     * @return {@code true} if the specified collection is not empty, and
     *         {@code false} otherwise
     * @throws NullPointerException  if the specified collection is {@code null}
     * @throws IllegalStateException if {@code this.canAdd(coll.size())} would
     *                               return {@code false}
     */
    @Override
    public final boolean addAll(Collection<? extends E> coll) {
        if (coll.isEmpty())
            return false;
        ensureCapacity(coll.size());
        for (E e : coll)
            add(e);
        return true;
    }
    
    /**
     * <p>If this deque is not at the top of the factory stack and does not
     * have enough capacity to fit another element (i.e. {@code this.canAdd(1)}
     * would return {@code false})), then the method returns {@code false}
     * and the queue represented by this deque is not modified.</p>
     *
     * <p>Otherwise, adds the specified element to the tail of the queue
     * represented by this deque, and the method returns {@code true}.</p>
     *
     * <p>This method is equivalent to {@link #offerLast(Object) offerLast(E)}.</p>
     *
     * @param  elem the element to add
     * @return {@code true} if there was space for the new element and {@code false} otherwise
     * @see    ArrayTempDeque#canAdd(int)
     */
    @Override
    public final boolean offer(E elem) {
        return offerLast(elem);
    }
    
    /**
     * <p>If this deque is not at the top of the factory stack and does not
     * have enough capacity to fit another element (i.e. {@code this.canAdd(1)}
     * would return {@code false})), then the method returns {@code false}
     * and the queue represented by this deque is not modified.</p>
     *
     * <p>Otherwise, adds the specified element to the head of the queue
     * represented by this deque, and the method returns {@code true}.</p>
     *
     * @param  elem the element to add
     * @return {@code true} if there was space for the new element and {@code false} otherwise
     * @see    ArrayTempDeque#canAdd(int)
     */
    @Override
    public final boolean offerFirst(E elem) {
        if (ensureCapacity(1)) {
            if (size == 0) {
                int i = offset + capacity - 1;
                getFactory().array[i] = elem;
                head = tail = i;
                size = 1;
            } else {
                int i = prev(head, offset, capacity);
                getFactory().array[i] = elem;
                head = i;
                ++size;
            }
            return true;
        }
        return false;
    }
    
    /**
     * <p>If this deque is not at the top of the factory stack and does not
     * have enough capacity to fit another element (i.e. {@code this.canAdd(1)}
     * would return {@code false})), then the method returns {@code false}
     * and the queue represented by this deque is not modified.</p>
     *
     * <p>Otherwise, adds the specified element to the tail of the queue
     * represented by this deque, and the method returns {@code true}.</p>
     *
     * @param  elem the element to add
     * @return {@code true} if there was space for the new element and {@code false} otherwise
     * @see    ArrayTempDeque#canAdd(int)
     */
    @Override
    public final boolean offerLast(E elem) {
        if (ensureCapacity(1)) {
            if (size == 0) {
                int i = offset;
                getFactory().array[i] = elem;
                head = tail = i;
                size = 1;
            } else {
                int i = next(tail, offset, capacity);
                getFactory().array[i] = elem;
                tail = i;
                ++size;
            }
            return true;
        }
        return false;
    }
    
    @Override
    public final boolean remove(Object obj) {
        return removeFirstOccurrence(obj);
    }
    
    @Override
    public final boolean removeFirstOccurrence(Object obj) {
        return removeAtIndex(firstIndexOf(obj));
    }
    
    @Override
    public final boolean removeLastOccurrence(Object obj) {
        return removeAtIndex(lastIndexOf(obj));
    }
    
    private boolean removeAtIndex(int index) {
        if (index == NOT_INDEX)
            return false;
        
        int head = this.head;
        int tail = this.tail;
        
        assert (head <= tail) ? (head <= index && index <= tail)
                              : (index <= tail || head <= index)
            : ("index=" + index + ", head=" + head + ", tail=" + tail);
        
        int size = this.size;
        switch (size) {
        case 0:
            assert false : index;
            return false;
        case 1:
            assert (head == tail) : ("head=" + head + ", tail=" + tail);
            remove();
            return true;
        }
        
        int off = offset;
        int cap = capacity;
        assert ( off <= index ) && ( index < (off + cap) ) : index;
        
        Object[] arr = getFactory().array;
        
        if (index <= tail) {
            // Note: this case handles the (head < tail) case too.
            System.arraycopy(arr, index + 1, arr, index, tail - index);
            arr[tail] = null;
            this.tail = prev(tail, off, cap);
        } else {
            // head <= index
            System.arraycopy(arr, head, arr, head + 1, index - head);
            arr[head] = null;
            this.head = next(head, off, cap);
        }
        
        this.size = (size - 1);
        return true;
    }
    
    private <T> boolean removeAll(T arg0, BiPredicate<? super T, ? super E> cond) {
        assert arg0 != null;
        assert cond != null;
        
        int size = this.size;
        if (size == 0)
            return false;
        
        Object[] arr = getFactory().array;
        int off = offset;
        int cap = capacity;
        
        int head = this.head;
        
        int src = 0;
        int dst = 0;
        
        boolean modified = false;
        
        try {
            for (; src < size; ++src) {
                @SuppressWarnings("unchecked")
                E e = (E) arr[ absolute(src, off, cap, head) ];
                
                if (cond.test(arg0, e)) {
                    modified = true;
                } else {
                    if (modified)
                        arr[ absolute(dst, off, cap, head) ] = e;
                    
                    ++dst;
                }
            }
        } finally {
            if (modified) {
                // Make sure the array is in good shape in the
                // case that the predicate throws an exception.
                for (; src < size; ++src, ++dst) {
                    arr[ absolute(dst, off, cap, head) ] = arr[ absolute(src, off, cap, head) ];
                }
                
                int count = src - dst;
                assert count > 0;
                
                for (; dst < size; ++dst) {
                    arr[ absolute(dst, off, cap, head) ] = null;
                }
                
                this.size = (size -= count);
                this.tail = absolute(size-1, off, cap, head);
            }
        }
        
        return modified;
    }
    
    @Override
    public final boolean removeAll(Collection<?> coll) {
        if (coll.isEmpty())
            return false;
        return removeAll(coll, Tools.callingCollectionContains());
    }
    
    @Override
    public final boolean retainAll(Collection<?> coll) {
        if (coll.isEmpty()) {
            if (isEmpty())
                return false;
            clear();
            return true;
        }
        return removeAll(coll, Tools.callingNotCollectionContains());
    }
    
    @Override
    public final boolean removeIf(Predicate<? super E> cond) {
        Objects.requireNonNull(cond);
        return removeAll(cond, Tools.callingPredicateTest());
    }
    
    @Override
    public final E pop() {
        return removeFirst();
    }
    
    @Override
    public final E remove() {
        return removeFirst();
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public final E removeFirst() {
        return throwForNoElement(removeFirstImpl());
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public final E removeLast() {
        return throwForNoElement(removeLastImpl());
    }
    
    @SuppressWarnings("unchecked")
    private E throwForNoElement(Object e) {
        if (e == NoElement.INSTANCE)
            throw newEmptyDequeException();
        return (E) e;
    }
    
    @Override
    public final E poll() {
        return pollFirst();
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public final E pollFirst() {
        return nullForNoElement(removeFirstImpl());
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public final E pollLast() {
        return nullForNoElement(removeLastImpl());
    }
    
    @SuppressWarnings("unchecked")
    private E nullForNoElement(Object e) {
        return (e == NoElement.INSTANCE) ? null : (E) e;
    }
    
    private enum NoElement {INSTANCE}
    
    private Object removeFirstImpl() {
        int size = this.size;
        if (size != 0) {
            Object[] arr = getFactory().array;
            
            int i = head;
            @SuppressWarnings("unchecked")
            E e = (E) arr[i];
            arr[i] = null;
            
            if ((this.size = (size - 1)) == 0) {
                head = tail = NOT_INDEX;
            } else {
                head = next(i, offset, capacity);
            }
            
            return e;
        }
        return NoElement.INSTANCE;
    }
    
    private Object removeLastImpl() {
        int size = this.size;
        if (size != 0) {
            Object[] arr = getFactory().array;
            
            int i = tail;
            @SuppressWarnings("unchecked")
            E e = (E) arr[i];
            arr[i] = null;
            
            if ((this.size = (size - 1)) == 0) {
                head = tail = NOT_INDEX;
            } else {
                tail = prev(i, offset, capacity);
            }
            
            return e;
        }
        return NoElement.INSTANCE;
    }
    
    @Override
    public final E peek() {
        return peekFirst();
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public final E peekFirst() {
        if (size != 0) {
            return (E) getFactory().array[head];
        }
        return null;
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public final E peekLast() {
        if (size != 0) {
            return (E) getFactory().array[tail];
        }
        return null;
    }
    
    @Override
    public final E element() {
        return getFirst();
    }
    
    private static NoSuchElementException newEmptyDequeException() {
        return new NoSuchElementException("empty Deque");
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public final E getFirst() {
        if (size != 0) {
            return (E) getFactory().array[head];
        }
        throw newEmptyDequeException();
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public final E getLast() {
        if (size != 0) {
            return (E) getFactory().array[tail];
        }
        throw newEmptyDequeException();
    }
    
    private int firstIndexOf(Object obj) {
        return indexOf(obj, head, tail, +1);
    }
    
    private int lastIndexOf(Object obj) {
        return indexOf(obj, tail, head, -1);
    }
    
    private int indexOf(Object obj, int i, int end, int increment) {
        assert increment == +1 || increment == -1 : increment;
        
        if (size != 0) {
            Object[] arr = getFactory().array;
            int off = offset;
            int cap = capacity;
            
            if (obj == null) {
                for (;;) {
                    if (arr[i] == null)
                        return i;
                    if (i == end)
                        break;
                    i = add(i, off, cap, increment);
                }
            } else {
                for (;;) {
                    if (obj.equals(arr[i]))
                        return i;
                    if (i == end)
                        break;
                    i = add(i, off, cap, increment);
                }
            }
        }
        
        return NOT_INDEX;
    }
    
    @Override
    public final boolean contains(Object obj) {
        return firstIndexOf(obj) != NOT_INDEX;
    }
    
    @Override
    public final boolean containsAll(Collection<?> coll) {
        for (Object obj : coll)
            if (!contains(obj))
                return false;
        return true;
    }
    
    @Override
    public final Object[] toArray() {
        int size = this.size;
        if (size == 0)
            return new Object[0];
        int head = this.head;
        int tail = this.tail;
        Object[] arr = getFactory().array;
        if (head <= tail)
            return Arrays.copyOfRange(arr, head, tail + 1);
        Object[] copy = new Object[size];
        int off = offset;
        int loCount = (off + capacity) - head;
        int hiCount = (tail - off) + 1;
        System.arraycopy(arr, head, copy, 0, loCount);
        System.arraycopy(arr, off, copy, loCount, hiCount);
        return copy;
    }
    
    @Override
    @SuppressWarnings({"unchecked", "SuspiciousSystemArraycopy"})
    public final <T> T[] toArray(T[] in) {
        int inLength = in.length; // Note: throws contractual NPE.
        
        int size = this.size;
        if (size != 0) {
            Object[] arr = getFactory().array;
            int head = this.head;
            int tail = this.tail;
            
            if (inLength < size) {
                Class<T[]> t = (Class<T[]>) in.getClass();
                
                if (head <= tail)
                    return Arrays.copyOfRange(arr, head, tail + 1, t);
                
                in = (T[]) java.lang.reflect.Array.newInstance(t.getComponentType(), size);
            }
            
            if (head <= tail)
                System.arraycopy(arr, head, in, 0, size);
            else {
                int off = offset;
                int end = off + capacity;
                
                int hiCount = tail - off + 1;
                int loCount = end - head;
                
                System.arraycopy(arr, head, in, 0, loCount);
                System.arraycopy(arr, off, in, loCount, hiCount);
            }
        }
        
        if (inLength > size)
            in[size] = null;
        
        return in;
    }
    
    final ArrayList<E> toArrayList() {
        return new ArrayList<>(this);
    }
    
    @Override
    public final Iterator<E> iterator() {
        return new ArrayTempDequeIterator.Ascending<>(this);
    }
    
    @Override
    public final Iterator<E> descendingIterator() {
        return new ArrayTempDequeIterator.Descending<>(this);
    }
    
    @Override
    public final Spliterator<E> spliterator() {
        // TODO: optimize?
        return TempDeque.super.spliterator();
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public final void forEach(Consumer<? super E> action) {
        Objects.requireNonNull(action);
        if (size != 0) {
            Object[] arr = getFactory().array;
            int off = offset;
            int cap = capacity;
            
            int i = head;
            int end = tail;
            
            for (;;) {
                action.accept((E) arr[i]);
                if (i == end)
                    break;
                i = next(i, off, cap);
            }
        }
    }
    
    // Note: There is no hashCode() and equals(Object) specification for Deque.
    
    @Override
    public final String toString() {
        int size = this.size;
        if (size == 0)
            return "[]";
        
        Object[] arr = getFactory().array;
        int off = offset;
        int cap = capacity;
        
        int i = head;
        int end = tail;
        
        StringBuilder b = new StringBuilder(16 * size).append('[');
        
        for (;;) {
            b.append(arr[i]);
            if (i == end)
                break;
            b.append(", ");
            i = next(i, off, cap);
        }
        
        return b.append(']').toString();
    }
}