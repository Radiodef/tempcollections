/* MIT License
 *
 * Copyright (c) 2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.radiodef.temp;

import java.util.*;
import java.util.function.*;

final class Tools {
    private Tools() {
    }
    
    static int requireNonNegative(int num, String name) {
        if (num < 0) {
            String msg = (null == name) ? Integer.toString(num)
                                        : (name + "=" + num);
            throw new IllegalArgumentException(msg);
        }
        return num;
    }
    
    static int requireRange(int num, int min, int max, String name) {
        if (num < min || max <= num) {
            String msg = (null == name) ? Integer.toString(num)
                                        : (name + "=" + num);
            throw new IllegalArgumentException(msg);
        }
        return num;
    }
    
    static int requireRangeClosed(int num, int min, int max, String name) {
        if (num < min || max < num) {
            String msg = (null == name) ? Integer.toString(num)
                                        : (name + "=" + num);
            throw new IllegalArgumentException(msg);
        }
        return num;
    }
    
    static Object[] newArray(int length, Object... elements) {
        return Arrays.copyOf(elements, length);
    }
    
    static int ceilLog2(int n) {
        if (n <= 0) {
            throw new ArithmeticException(Integer.toString(n));
        }
        --n;
        n |= n >> 1;
        n |= n >> 2;
        n |= n >> 4;
        n |= n >> 8;
        n |= n >> 16;
        return Integer.bitCount(n);
    }
    
    /**
     * Computes the power of 2 nearest to {@code n} which is greater than
     * or equal to {@code n}. If {@code n} is negative or 0, the result
     * is 1. If {@code n} is greater than 2<sup>30</sup>, the result is
     * {@code Integer.MIN_VALUE}.
     *
     * @param  n the number whose next power of 2 ought to be returned
     *
     * @return the power of 2 nearest to {@code n} which is greater than
     *         or equal to {@code n} (unless {@code n} is greater than
     *         the 30th power of 2)
     */
    static int next2(int n) {
        // modified to handle negative and 0
        int n1 = n - 1;
        int sign = (n | n1) & 0x80_00_00_00;
        
        n = n1 | sign;
        
        n |= n >> 1;
        n |= n >> 2;
        n |= n >> 4;
        n |= n >> 8;
        n |= n >> 16;
        
        return n + 1 + (sign >>> 31);
    }
    
    /*
    public static int next2(int n) {
        --n;
        n |= n >> 1;
        n |= n >> 2;
        n |= n >> 4;
        n |= n >> 8;
        n |= n >> 16;
        return n + 1;
    }
    */
    
    /**
     * @param  n the number to test
     * @return {@code true} if {@code n} is an integer power of 2, else {@code false}
     */
    static boolean isPowerOf2(int n) {
        return ( n > 0 ) && ( (n & (n - 1)) == 0 );
    }
    
    /**
     * @param  n the {@code int} to convert to {@code float}
     * @return converts the specified {@code int} to {@code float},
     *         always rounding up towards positive infinity
     */
    static strictfp float toCeilFloat(int n) {
        float f = n;
        
        if ((int) f < n)
            f = Math.nextUp(f);
        
        return f;
    }
    
    // this works but it proved to be slower than the naive implementation
    @SuppressWarnings("unused")
    private static strictfp float toCeilFloatBitwise(int n) {
        int sign = n >>> 31;
        int mask = ((n + sign) ^ sign) >>> 24;
        
        mask |= mask >>> 1;
        mask |= mask >>> 2;
        mask |= mask >>> 4;
        
        return ((long) n + (long) mask) & ~mask;
    }
    
    static <T> boolean isSorted(Iterable<? extends T> iterable, Comparator<? super T> comp) {
        Objects.requireNonNull(iterable, "Iterable");
        Objects.requireNonNull(comp, "Comparator");
        
        Iterator<? extends T> it = iterable.iterator();
        
        if (it.hasNext()) {
            T prev = it.next();
            
            while (it.hasNext()) {
                T next = it.next();
                
                if (comp.compare(prev, next) > 0) {
                    return false;
                }
                
                prev = next;
            }
        }
        
        return true;
    }
    
    /*
    public static long add1Multiply3Divide2(int n) {
        return (((long) n + 1L) * 3L) >>> 1;
    }
    */
    
    private enum CollectionPredicate implements BiPredicate<Collection<?>, Object> {
        CONTAINS {
            @Override
            public final boolean test(Collection<?> c, Object o) {
                return c.contains(o);
            }
        },
        NOT_CONTAINS {
            @Override
            public final boolean test(Collection<?> c, Object o) {
                return !c.contains(o);
            }
        }
    }
    private enum PredicateTest implements BiPredicate<Predicate<Object>, Object> {
        INSTANCE;
        @Override
        public final boolean test(Predicate<Object> p, Object o) {
            return p.test(o);
        }
    }
    
    static BiPredicate<Collection<?>, Object> callingCollectionContains() {
        return CollectionPredicate.CONTAINS;
    }
    
    static BiPredicate<Collection<?>, Object> callingNotCollectionContains() {
        return CollectionPredicate.NOT_CONTAINS;
    }
    
    @SuppressWarnings("unchecked")
    static <T> BiPredicate<Predicate<? super T>, T> callingPredicateTest() {
        return (BiPredicate<Predicate<? super T>, T>)
            (BiPredicate<? extends Predicate<?>, ?>)
                PredicateTest.INSTANCE;
    }
}
