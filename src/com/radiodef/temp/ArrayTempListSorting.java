/* MIT License
 *
 * Copyright (c) 2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.radiodef.temp;

import java.util.*;

final class ArrayTempListSorting {
    private ArrayTempListSorting() {
    }
    
    @SuppressWarnings("unchecked")
    static <T> void sort(ArrayTempList<T> list, Comparator<? super T> comp) {
        assert list != null;
        
        if (comp == null) {
            // noinspection RedundantCast
            comp = (Comparator<T>) Comparator.naturalOrder();
        }
        
        int offset = list.offset;
        int size = list.size;
        
        if (size != 0) {
            if (size < INSERTION_SORT_THRESHOLD) {
                insertionSort(list.getFactory().array, offset, size, comp);
            } else {
                mergeSort(list.getFactory(), offset, size, comp);
            }
        }
    }
    
    @SuppressWarnings("unchecked")
    static <T> void jdkSort(ArrayTempList<T> list, Comparator<? super T> comp) {
        int start = list.offset;
        int end = start + list.size;
        Arrays.sort(list.getFactory().array, start, end, (Comparator<Object>) comp);
    }
    
    static final int INSERTION_SORT_THRESHOLD = 7;
    
    static <T> void insertionSort(ArrayTempList<T> list, Comparator<? super T> comp) {
        insertionSort(list.getFactory().array, list.offset, list.size, comp);
    }
    
    @SuppressWarnings("unchecked")
    private static void insertionSort(Object[] array, int offset, int size, Comparator<?> comp) {
        int end = offset + size;
        
        for (int i = offset + 1; i < end; ++i) {
            for (int j = i; j != offset; --j) {
                int jm1 = j - 1;
                
                Object a = array[j];
                Object b = array[jm1];
                
                if (((Comparator<Object>) comp).compare(a, b) < 0) {
                    array[j] = b;
                    array[jm1] = a;
                } else {
                    break;
                }
            }
        }
    }
    
    static <T> void mergeSort(ArrayTempList<T> list, Comparator<? super T> comp) {
        mergeSort(list.getFactory(), list.offset, list.size, comp);
    }
    
    private static <T> void mergeSort(ArrayTempFactory factory, int offset, int size, Comparator<? super T> comp) {
        try (ArrayTempList<T> aux = factory.newList(size)) {
            // Note: factory.newList might resize the array,
            //       so we must read it afterwards.
            Object[] array = factory.array;
            
            mergeSort(array, offset, array, aux.offset, size, comp);
        }
    }
    
    private static void mergeSort(Object[] arr,
                                  int arrOff,
                                  Object[] aux,
                                  int auxOff,
                                  int size,
                                  Comparator<?> comp) {
        // Note: this check must be here because divideAndMerge
        // calls Tools.ceilLog2(size) which throws for size=0.
        if (size == 0) {
            return;
        }
        
        if (aux == null) {
            aux = new Object[size];
            auxOff = 0;
        } else {
            System.arraycopy(arr, arrOff, aux, auxOff, size);
        }
        
        divideAndMerge(arr, arrOff, aux, auxOff, size, comp);
        
        if (arr == aux) {
            Arrays.fill(arr, auxOff, auxOff + size, null);
        }
    }
    
    private static void divideAndMerge(Object[] a,
                                       int aOff,
                                       Object[] b,
                                       int bOff,
                                       int size,
                                       Comparator<?> comp) {
        Object[] c;
        int cOff;
        if ((Tools.ceilLog2(size) & 1) == 1) {
            System.arraycopy(a, aOff, b, bOff, size);
            
            c = a;
            a = b;
            b = c;
            
            cOff = aOff;
            aOff = bOff;
            bOff = cOff;
        }
        
        int width2;
        int mid;
        int end;
        for (int width = 1; width < size; width = width2) {
            width2 = (width << 1);
            
            for (int i = 0; i < size; i += width2) {
                mid = i   + width;
                end = mid + width;
                if (size < end)
                    end = size;
                merge(a, aOff, b, bOff, i, mid, end, comp);
            }
            
            c = a;
            a = b;
            b = c;
            
            cOff = aOff;
            aOff = bOff;
            bOff = cOff;
        }
    }
    
    @SuppressWarnings("unchecked")
    private static void merge(Object[] src,
                              int srcOff,
                              Object[] dst,
                              int dstOff,
                              int start,
                              int mid,
                              int end,
                              Comparator<?> comp) {
        int i = start;
        int j = mid;
        
        for (int k = start; k < end; ++k) {
            Object a;
            Object b;
            
            chooseRangeToCopyFrom: {
                if (i < mid) {
                    a = src[srcOff + i];
                    
                    if (j < end) {
                        b = src[srcOff + j];
                        
                        if (((Comparator<Object>) comp).compare(a, b) > 0)
                            break chooseRangeToCopyFrom;
                    }
                    
                    dst[dstOff + k] = a;
                    ++i;
                    
                    continue;
                } else {
                    b = src[srcOff + j];
                }
            }
            
            dst[dstOff + k] = b;
            ++j;
        }
    }
}
