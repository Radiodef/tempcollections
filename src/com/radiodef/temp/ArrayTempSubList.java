/* MIT License
 *
 * Copyright (c) 2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.radiodef.temp;

import java.util.*;

final class ArrayTempSubList<E> extends AbstractList<E> implements RandomAccess {
    // TODO: support modCount fail-fast iterator?
    
    private final List<E> list;
    
    private final int fromIndex;
    private int toIndex;
    
    ArrayTempSubList(List<E> list, int fromIndex, int toIndex) {
        assert list != null;
        this.list = list;
        
        assert list instanceof RandomAccess : list.getClass();
        assert list instanceof ArrayTempList<?> || list instanceof ArrayTempSubList<?> : list.getClass();
        
        int size = list.size();
        if (fromIndex < 0 || toIndex > size || fromIndex > toIndex) {
            throw new IndexOutOfBoundsException("fromIndex=" + fromIndex + ", toIndex=" + toIndex);
        }
        
        this.fromIndex = fromIndex;
        this.toIndex = toIndex;
    }
    
    private IndexOutOfBoundsException newOutOfBoundsException(int index) {
        return new IndexOutOfBoundsException("size=" + size() + ", index=" + index);
    }
    
    private int translateAndCheck(int index) {
        int fromIndex = this.fromIndex;
        int toIndex = this.toIndex;
        int translated = fromIndex + index;
        if (fromIndex <= translated && translated < toIndex)
            return translated;
        throw newOutOfBoundsException(index);
    }
    
    private int translateAndCheckClosed(int index) {
        int fromIndex = this.fromIndex;
        int toIndex = this.toIndex;
        int translated = fromIndex + index;
        if (fromIndex <= translated && translated <= toIndex)
            return translated;
        throw newOutOfBoundsException(index);
    }
    
    @Override
    public final int size() {
        return toIndex - fromIndex;
    }
    
    @Override
    public final E get(int index) {
        return list.get(translateAndCheck(index));
    }
    
    @Override
    public final E set(int index, E elem) {
        return list.set(translateAndCheck(index), elem);
    }
    
    @Override
    public final void add(int index, E elem) {
        list.add(translateAndCheckClosed(index), elem);
        ++toIndex;
    }
    
    @Override
    public final E remove(int index) {
        E elem = list.remove(translateAndCheck(index));
        --toIndex;
        return elem;
    }
    
    // Note: AbstractList.clear() is implemented as this.removeRange(0, size()).
    @Override
    protected final void removeRange(int fromIndex, int toIndex) {
        int thisFrom = this.fromIndex;
        int thisTo = this.toIndex;
        int size = thisTo - thisFrom;
        
        if (fromIndex < 0 || fromIndex > toIndex || toIndex > size) {
            throw new IndexOutOfBoundsException("fromIndex=" + fromIndex + ", toIndex=" + toIndex + ", size=" + size);
        }
        
        fromIndex += thisFrom;
        toIndex += thisFrom;
        
        List<E> list = this.list;
        if (list instanceof ArrayTempList<?>) {
            ((ArrayTempList<E>) list).removeRange(fromIndex, toIndex);
        } else {
            ((ArrayTempSubList<E>) list).removeRange(fromIndex, toIndex);
        }
        
        this.toIndex = thisTo - (toIndex - fromIndex);
    }
    
    @Override
    public final ArrayTempSubList<E> subList(int fromIndex, int toIndex) {
        // Note:
        // We can't do new ArrayTempList<>(this.list, ...) because
        // structural modifications to the returned sublist should
        // ALSO update this sublist. In other words, if somebody
        // does:
        //   List<?> list = new ArrayList<>(List.of(1, 2, 3, 4, 5));
        //   List<?> sub  = list.subList(1, 4);
        //   sub.subList(1, 3).clear();
        // That should result BOTH in 'list' becoming the following:
        //   [1, 2, 4, 5]
        // AND 'sub' becoming the following:
        //   [2, 4]
        // If we did new ArrayTempList<>(this.list, ...), then 'sub'
        // would become out of sync from the main list, because its
        // fromIndex and toIndex would not be updated.
        return new ArrayTempSubList<>(this, fromIndex, toIndex);
    }
}