/* MIT License
 *
 * Copyright (c) 2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.radiodef.temp;

import java.util.*;

/**
 * {@code TempFactory} is an object for creating and managing temporary collections.
 * An implementation may create and manage the resources for these collections in
 * any manner. The implementation should clearly document any details relevant to
 * its usage code, such as the identity of objects returned by factory methods, when
 * to appropriately close these objects and whether or not there are circumstances
 * when its collections become capacity-restricted.
 *
 * @see TempObject
 * @see ArrayTempFactory
 */
public interface TempFactory {
    /**
     * <p>Creates and returns a new, empty list.</p>
     *
     * @param  <E> the type of the new list's elements
     * @return a new, empty list
     */
    <E> TempList<E> newList();
    
    /**
     * <p>Creates and returns a new list, initially populated with the elements
     * of the specified collection. The initial elements are added to the new
     * list in the order they are returned by the collection's iterator, as
     * if by {@code list.addAll(coll)}.</p>
     *
     * @param  coll the initial elements for the new list
     * @param  <E>  the type of the new list's elements
     * @return a new list, populated with the elements of the specified
     *         collection
     * @throws NullPointerException if the specified collection is {@code null}
     */
    <E> TempList<E> newList(Collection<? extends E> coll);
    
    /**
     * <p>Creates and returns a new, empty list.</p>
     *
     * <p>The estimated size provides a hint to the factory for how much space
     * the new list may require, if applicable to its implementation.</p>
     *
     * @param  estimatedSize an estimate for the eventual size of the new list
     * @param  <E>           the type of the new list's elements
     * @return a new, empty list
     * @throws IllegalArgumentException if the estimated size is negative
     */
    <E> TempList<E> newList(int estimatedSize);
    
    /**
     * <p>Creates and returns a new, empty set.</p>
     *
     * @param  <E> the type of the new set's elements
     * @return a new, empty set
     */
    <E> TempSet<E> newSet();
    
    /**
     * <p>Creates and returns a new set, initially populated with the unique
     * elements of the specified collection. The initial elements are added
     * to the new set in the order they are returned by the collection's
     * iterator, as if by {@code set.addAll(coll)}.</p>
     *
     * @param  coll the initial elements for the new set
     * @param  <E>  the type of the new set's elements
     * @return a new set, populated with the unique elements of the specified
     *         collection
     * @throws NullPointerException if the specified collection is {@code null}
     */
    <E> TempSet<E> newSet(Collection<? extends E> coll);
    
    /**
     * <p>Creates and returns a new, empty set.</p>
     *
     * <p>The estimated size provides a hint to the factory for how much space
     * the new set may require, if applicable to its implementation.</p>
     *
     * @param  estimatedSize an estimate for the eventual size of the new set
     * @param  <E>           the type of the new set's elements
     * @return a new, empty set
     * @throws IllegalArgumentException if the estimated size is negative
     */
    <E> TempSet<E> newSet(int estimatedSize);
    
    /**
     * <p>Creates and returns a new, empty deque.</p>
     *
     * @param  <E> the type of the new deque's elements
     * @return a new, empty deque
     */
    <E> TempDeque<E> newDeque();
    
    /**
     * <p>Creates and returns a new deque, initially populated with the elements
     * of the specified collection. The initial elements are added to the new
     * deque in the order they are returned by the collection's iterator, as
     * if by {@code deque.addAll(coll)}.</p>
     *
     * @param  coll the initial elements for the new deque
     * @param  <E>  the type of the new deque's elements
     * @return a new deque, populated with the elements of the specified
     *         collection
     * @throws NullPointerException if the specified collection is {@code null}
     */
    <E> TempDeque<E> newDeque(Collection<? extends E> coll);
    
    /**
     * <p>Creates and returns a new, empty deque.</p>
     *
     * <p>The estimated size provides a hint to the factory for how much space
     * the new deque may require, if applicable to its implementation.</p>
     *
     * @param  estimatedSize an estimate for the eventual size of the new deque
     * @param  <E>           the type of the new deque's elements
     * @return a new, empty deque
     * @throws IllegalArgumentException if the estimated size is negative
     */
    <E> TempDeque<E> newDeque(int estimatedSize);
    
    /**
     * <p>Creates and returns a new, empty map.</p>
     *
     * @param  <K> the type of the new map's keys
     * @param  <V> the type of the new map's values
     * @return a new, empty map
     */
    <K, V> TempMap<K, V> newMap();
    
    /**
     * <p>Creates and returns a new map, initially populated with the entries
     * of the specified map argument. The initial entries are added to the
     * new map in the order they are returned by the argument's iterator, as
     * if by {@code map.putAll(entries)}.<p>
     *
     * @param  entries the initial entries for the new map
     * @param  <K>     the type of the new map's keys
     * @param  <V>     the type of the new map's values
     * @return a new map, populated with the entries of the specified
     *         map argument
     * @throws NullPointerException if the specified map argument is {@code null}
     */
    <K, V> TempMap<K, V> newMap(Map<? extends K, ? extends V> entries);
    
    /**
     * <p>Creates and returns a new, empty map.</p>
     *
     * <p>The estimated size provides a hint to the factory for how much space
     * the new map may require, if applicable to its implementation.</p>
     *
     * @param  estimatedSize an estimate for the eventual size of the new map
     * @param  <K>           the type of the new map's keys
     * @param  <V>           the type of the new map's values
     * @return a new, empty map
     * @throws IllegalArgumentException if the estimated size is negative
     */
    <K, V> TempMap<K, V> newMap(int estimatedSize);
}