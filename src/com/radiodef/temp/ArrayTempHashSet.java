/* MIT License
 *
 * Copyright (c) 2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.radiodef.temp;

import java.util.*;
import java.util.function.*;

/**
 * <p>{@code ArrayTempHashSet} is the {@link TempSet} implementation for
 * {@link ArrayTempFactory}. {@code ArrayTempHashSet} is an open addressed
 * hash table with linear probing.</p>
 *
 * <p>This set implementation supports {@code null} elements.</p>
 *
 * <p>Each {@code ArrayTempHashSet} is created with a {@code float} load
 * factor, which is the maximum ratio of elements to table slots which the
 * set will normally allow. If the set's {@link #size() size()} grows to a
 * point where this ratio would exceed the load factor, the set will attempt
 * to grow and rehash its table to a larger capacity. For example, if the
 * load factor is {@code 0.5f}, then the set will attempt to maintain a
 * table with twice as many slots as its size.</p>
 *
 * <p>The ratio of size to table slots for a given {@code ArrayTempHashSet}
 * can be i.e. understood as the following:</p>
 *
 *<pre><code>(float) set.size() / (float) set.capacity()</code></pre>
 *
 * <p>If that ratio exceeds the set's load factor, the set will attempt to
 * increase its table capacity by requesting more space from the factory.
 * ({@link #capacity() capacity()} is the length of the array space reserved
 * by the set.)</p>
 *
 * <p>Using a lower load factor will reduce the number of collisions at the
 * cost of extra storage space. The default load factor is <code>{@value #DEFAULT_LOAD_FACTOR}</code>.
 * Since {@code ArrayTempHashSet} uses open addressing, the load factor
 * may not be greater than {@code 1.0f}, because the set's size is strictly
 * limited by the length of the table.</p>
 *
 * <p>(A set which is not at the top of the factory stack will allow its size
 * to grow past what its load factor would normally allow. Such a set can be
 * considered to have an effective load factor of {@code 1.0f}, meaning that
 * it will allow its size to increase until its table is actually full.)</p>
 *
 * <h3>Notes about aggregate removals</h3>
 *
 * <p>The {@link Iterator#remove() Iterator.remove()} implementation of
 * this set's {@link #iterator() iterator()} removes an element by simply
 * marking its place in the table as deleted. This is efficient for a small
 * number of removals, but may degrade lookup performance if the table has
 * too many of these deletion marks. Code which relies on {@code Iterator.remove()}
 * may wish to invoke the {@link #rehash() rehash()} method after iteration
 * is completed, which will remove any deletion marks:</p>
 *
 *<pre><code>{@code ArrayTempHashSet<T>} set = ...;
 *for ({@code Iterator<T>} it = set.iterator(); it.hasNext();) {
 *    ...
 *    if (...(it.next()))
 *        it.remove(); // marks the element as deleted
 *}
 *set.rehash(); // rehash the table when iteration is complete</code></pre>
 *
 * <p>(However, note that {@code rehash()} should never be invoked <em>during</em>
 * iteration.)</p>
 *
 * <p>The implementation behind {@link #removeAll(Collection) removeAll},
 * {@link #retainAll(Collection) retainAll} and {@link #removeIf(Predicate) removeIf}
 * causes the table to be rehashed. These methods are optimal for when the
 * set is large and the number of removals is expected to be large as well.
 * Given the way these methods are specified by the {@link Collection} class,
 * it's normal for them to run in linear time, but rehashing the table is fairly
 * expensive as far as linear time operations go, and can be quadratic in the
 * worst case (i.e. many collisions).</p>
 *
 * <p>For just a few removals from a large set, code such as the following may
 * be much more efficient than {@code removeAll}:</p>
 *
 *<pre><code>{@code ArrayTempHashSet<T>} set = ...;
 *for (T obj : objectsToRemove)
 *    set.remove(obj);</code></pre>
 *
 * <p>(However, if the set is small, then these optimization notes should be
 * considered irrelevant.)</p>
 *
 * @param <E> the element type of this set
 * @see   ArrayTempFactory#newSet(int, float)
 */
public final class ArrayTempHashSet<E> extends ArrayTempOpenAddressedHashTable implements TempSet<E> {
    
    ArrayTempHashSet(ArrayTempFactory factory,
                     ArrayTempObject previous,
                     int offset,
                     int capacity,
                     float loadFactor) {
        super(factory, previous, offset, capacity, loadFactor);
    }
    
    static int indexOf(Object elem, int offset, int capacity) {
        int hash = elem.hashCode();
        hash ^= hash >>> 16;
        
        return offset + (hash & (capacity - 1));
    }
    
    static int nextIndex(int index, int offset, int capacity) {
        return offset + (((index - offset) + 1) & (capacity - 1));
    }
    
    static boolean isElement(Object obj) {
        return (obj != null) && (obj != Del.INSTANCE);
    }
    
    private boolean resizeIfNecessary(int additionalSize) {
        assert additionalSize >= 0 : additionalSize;
        
        if (additionalSize == 0)
            return false;
        
        int size = this.size;
        int capacity = this.capacity;
        float loadFactor = this.loadFactor;
        
        // Note: if we aren't the top, then we allow ourselves to be
        //       filled all the way up, as if by loadFactor=1.0
        int effectiveCapacity = isTop() ? (int) (capacity * loadFactor) : capacity;
        int extraSpace = effectiveCapacity - size;
        
        if (additionalSize <= extraSpace)
            return false;
        
        int newCapacity = nextCapacity(capacity, size, additionalSize, loadFactor);
        if (newCapacity != NO_CAPACITY) {
            rehash(capacity, newCapacity);
            return true;
        }
        
        throw new OutOfMemoryError("capacity=" + capacity
                                 + ", size=" + size
                                 + ", additionalSize=" + additionalSize);
    }
    
    private void rehash(int oldCapacity, int newCapacity) {
        int additionalCapacity = newCapacity - oldCapacity;
        
        ArrayTempFactory factory = this.getFactory();
        Object[] oldArray = factory.array;
        
        int availableCapacity = factory.ensureAvailableCapacity(this, additionalCapacity);
        
        assert availableCapacity >= additionalCapacity :
            "oldCapacity=" + oldCapacity +
            ", newCapacity=" + newCapacity +
            ", availableCapacity=" + availableCapacity;
        
        Object[] newArray = factory.array;
        
        capacity = newCapacity;
        
        if (oldArray != newArray) {
            rehashFromOldArray(oldArray, oldCapacity, newArray, newCapacity);
        } else {
            rehashFromBackingArray(newArray, oldCapacity, newCapacity);
        }
    }
    
    private void rehashFromOldArray(Object[] oldArray,
                                    int oldCapacity,
                                    Object[] newArray,
                                    int newCapacity) {
        int offset = this.offset;
        rehash(oldArray, offset, oldCapacity,
               newArray, offset, newCapacity);
    }
    
    @SuppressWarnings("unused")
    private void rehashFromCopy(Object[] array, int oldCapacity, int newCapacity) {
        int offset = this.offset;
        Object[] copy = Arrays.copyOfRange(array, offset, offset + oldCapacity);
        
        rehash(copy, 0, oldCapacity,
               array, offset, newCapacity);
    }
    
    /**
     * <p>Rehashes the table. This causes each element's location in the table
     * to be recomputed, and removes any deletion marks which were placed by the
     * {@link Iterator#remove() remove()} method of this set's {@link #iterator() iterator()}.</p>
     *
     * <p>This method has a similar effect to the following:</p>
     *
     *<pre><code>{@code List<E> elements = new ArrayList<>(this);}
     *this.clear();
     *this.addAll(elements);</code></pre>
     */
    public final void rehash() {
        int capacity = this.capacity;
        rehashFromBackingArray(getFactory().array, capacity, capacity);
    }
    
    private void rehashFromBackingArray(Object[] array, int oldCapacity, int newCapacity) {
        ArrayTempFactory factory = getFactory();
        
        try (ArrayTempList<E> auxList = factory.newList(oldCapacity)) {
            Object[] auxArray = factory.array;
            
            if (array != auxArray) {
                // Note: we don't actually care about the space allocated by
                //       the new list. All that matters here is that the array
                //       was resized, so we can rehash using the old array as
                //       the source.
                rehashFromOldArray(array, oldCapacity, auxArray, newCapacity);
                return;
            }
            
            int thisOffset = offset;
            int auxOffset = auxList.offset;
            
            System.arraycopy(array, thisOffset, auxArray, auxOffset, oldCapacity);
            
            rehash(auxArray, auxOffset, oldCapacity, array, thisOffset, newCapacity);
        }
    }
    
    static int REHASH_COUNT = 0;
    
    // Used by test of e.g. addAll(Collection)
    static final boolean TESTING = true;
    
    private void rehash(Object[] oldArray,
                        int oldOffset,
                        int oldCapacity,
                        Object[] newArray,
                        int newOffset,
                        int newCapacity) {
        if (TESTING) {
            ++REHASH_COUNT;
        }
        
        // Note: always fill, because the array could contain deletion markers.
        Arrays.fill(newArray, newOffset, newOffset + newCapacity, null);
        
        int size = this.size;
        assert newCapacity >= size : "new capacity=" + newCapacity + ", size=" + size;
        
        if (size == 0) {
            return;
        }
        
        int oldEnd = oldOffset + oldCapacity;
        
        for (int o = oldOffset; o < oldEnd; ++o) {
            Object elem = oldArray[o];
            
            if (isElement(elem)) {
                int n = indexOf(elem, newOffset, newCapacity);
                
                while (newArray[n] != null) {
                    n = nextIndex(n, newOffset, newCapacity);
                }
                
                newArray[n] = elem;
                
                if (( --size ) == 0) {
                    break;
                }
            }
        }
    }
    
    @Override
    public final void clear() {
        if (size != 0) {
            int start = offset;
            int end = start + capacity;
            Arrays.fill(getFactory().array, start, end, null);
            size = 0;
        }
    }
    
    private boolean add(Object elem, int additionalSize) {
        if (additionalSize < 0) {
            // This prevents a StackOverflowError sometimes (if there's some other bug).
            throw new AssertionError("add recursed more than once: " + additionalSize);
        }
        
        assert additionalSize <= 1 : additionalSize;
        
        int offset = this.offset;
        int capacity = this.capacity;
        
        int start = indexOf(elem, offset, capacity);
        
        Object[] array = getFactory().array;
        
        Object obj;
        int i = start;
        do {
            if ((obj = array[i]) == null || obj == Del.INSTANCE) {
                if (resizeIfNecessary(additionalSize)) {
                    return add(elem, additionalSize - 1); // Table was rehashed.
                }
                
                array[i] = elem;
                ++size;
                return true;
            }
            
            if (elem.equals(obj)) {
                return false;
            }
        } while ((i = nextIndex(i, offset, capacity)) != start);
        
        // Note:
        // This code path means we did a full circle of the array and found no vacancy.
        
        boolean didResize = resizeIfNecessary(additionalSize);
        assert didResize;
        return add(elem, additionalSize - 1);
    }
    
    /**
     * Adds the specified element to this set if it is not already present.
     *
     * @param  elem the element to add
     * @return {@code true} if the element was added to this set and {@code false}
     *         if it was already present
     * @throws IllegalStateException
     *         if the element is not already present, and {@code this.canAdd(1)}
     *         would return {@code false}
     * @see    ArrayTempHashSet#canAdd(int)
     */
    @Override
    public final boolean add(E elem) {
        assert isExternal(elem) : elem;
        return add(Nil.box(elem), 1);
    }
    
    /**
     * <p>Adds to this set all of the elements in the specified collection which
     * are not already present.</p>
     *
     * <p>If this set is not at the top of the factory stack then given an</p>
     *
     *<pre><code>int n = this.maxSize() - this.size();</code></pre>
     *
     * <p>then up to {@code n} new elements will be added to this set. After adding
     * {@code n} new elements to this set (in the order they are returned by the
     * specified collection's iterator), if there are further elements to add
     * which are not already present in this set, then this set is modified no
     * further and the method throws an {@link IllegalStateException}.</p>
     *
     * <p>This is i.e. equivalent to the result of the following:</p>
     *
     *<pre><code>for (E e : coll)
     *    this.add(e);</code></pre>
     *
     * @param  coll the elements to add
     * @return {@code true} if at least one element was added to this set and
     *         {@code false} otherwise
     * @throws NullPointerException
     *         if the specified collection is {@code null}
     * @throws IllegalStateException
     *         if <code>this.canAdd(&hellip;)</code> would return {@code false}
     *         with the number of new elements as the argument
     * @see    ArrayTempHashSet#maxSize()
     * @see    ArrayTempHashSet#canAdd(int)
     */
    @Override
    public final boolean addAll(Collection<? extends E> coll) {
        int size = coll.size();
        if (size == 0) {
            return false;
        }
        
        if (isTop()) {
            resizeIfNecessary(size);
        }
        /*
        // this works fine, but it's probably pointless
        else if (size > (capacity - this.size)) {
            int count = 0;
            // this is kind of expensive, but we only do it in a rare case,
            // to avoid throwing an exception
            try (TempSet<E> unique = factory.newSet(size)) {
                for (E e : coll)
                    if (!contains(e) && unique.add(e))
                        ++count;
            }
            // throws if count is too big
            resizeIfNecessary(count);
        }
        */
        
        boolean modified = false;
        
        for (E e : coll) {
            if (add(e)) {
                modified = true;
            }
        }
        
        return modified;
    }
    
    @Override
    public final boolean remove(Object obj) {
        assert isExternal(obj) : obj;
        
        int size = this.size;
        if (size == 0) {
            return false;
        }
        
        obj = Nil.box(obj);
        
        Object[] array = getFactory().array;
        int offset = this.offset;
        int capacity = this.capacity;
        
        int start = indexOf(obj, offset, capacity);
        
        Object e;
        int i = start;
        
        for (;;) {
            if ((e = array[i]) == null)
                return false;
            if (e.equals(obj))
                break;
            if ((i = nextIndex(i, offset, capacity)) == start)
                return false;
        }
        
        array[i] = null;
        int j = i;
        
        for (;;) {
            j = nextIndex(j, offset, capacity);
            if ((e = array[j]) == null)
                break;
            
            if (e != Del.INSTANCE) {
                int k = indexOf(e, offset, capacity);
                
                if (!( (i < j) ? (k <= i || j < k) : (j < k && k <= i) ))
                    continue;
            }
            
            array[i] = e;
            array[j] = null;
            i = j;
        }
        
        this.size = size - 1;
        return true;
    }
    
    private <T> boolean removeAll(T arg0, BiPredicate<? super T, ? super E> cond) {
        return removeAllByAuxiliaryAndRehash(arg0, cond);
    }
    
    @SuppressWarnings({"unchecked", "unused"})
    private <T> boolean removeAllByAuxiliaryAndRehash(T arg0, BiPredicate<? super T, ? super E> cond) {
        int size = this.size;
        if (size == 0)
            return false;
        
        ArrayTempFactory factory = getFactory();
        int capacity = this.capacity;
        
        try (ArrayTempList<E> auxList = factory.newList(capacity)) {
            int aux = auxList.offset;
            
            Object[] array = factory.array;
            int offset = this.offset;
            int end = offset + capacity;
            
            int marked = 0; // count of any found deletion markers
            int removed = 0; // count of current removals
            int i = offset;
            Object e;
            
            try {
                for (; i < end; ++i) {
                    if ((e = array[i]) != null) {
                        if (e == Del.INSTANCE) {
                            ++marked;
                        } else if (cond.test(arg0, (E) Nil.unbox(e))) {
                            ++removed;
                        } else {
                            int j = indexOf(e, aux, capacity);
                            while (array[j] != null)
                                j = nextIndex(j, aux, capacity);
                            array[j] = e;
                        }
                    }
                }
            } finally {
                // if we removed at least 1 element OR found at least 1
                // old deletion marker, copy the rehashed table
                if (removed != 0 || marked != 0) {
                    // in case e.g. cond.test(..) threw an exception,
                    // rehash the rest of the table
                    for (; i < end; ++i) {
                        if (isElement(e = array[i])) {
                            int j = indexOf(e, aux, capacity);
                            while (array[j] != null)
                                j = nextIndex(j, aux, capacity);
                            array[j] = e;
                        }
                    }
                    
                    System.arraycopy(array, aux, array, offset, capacity);
                    this.size = (size - removed);
                }
            }
            
            return removed != 0;
        }
    }
    
    // This might be better than rehashing. Some benchmarks might help decide.
    @SuppressWarnings("unused")
    private <T> boolean removeAllByAuxiliaryCopy(T arg0, BiPredicate<? super T, ? super E> cond) {
        int size = this.size;
        if (size == 0)
            return false;
        
        ArrayTempFactory factory = getFactory();
        
        try (ArrayTempList<E> auxList = factory.newList(size)) {
            int auxOffset = auxList.offset;
            Object[] arr = factory.array;
            {
                int src = this.offset;
                int dst = auxOffset;
                
                for (int end = (src + capacity); src < end; ++src) {
                    Object e = arr[src];
                    if (isElement(e))
                        arr[dst++] = e;
                }
            }
            
            boolean modified = false;
            
            for (int i = auxOffset, end = (i + size); i < end; ++i) {
                @SuppressWarnings("unchecked")
                E e = (E) Nil.unbox(arr[i]);
                
                if (cond.test(arg0, e)) {
                    remove(e);
                    modified = true;
                }
            }
            
            return modified;
        }
    }
    
    @SuppressWarnings({"unchecked", "unused"})
    private <T> boolean removeAllByMarkingAsDeleted(T arg0, BiPredicate<? super T, ? super E> cond) {
        int size = this.size;
        if (size == 0) {
            return false;
        }
        
        Object[] array = getFactory().array;
        int offset = this.offset;
        int capacity = this.capacity;
        int end = offset + capacity;
        
        int removeCount = 0;
        
        try {
            for (int i = offset; i < end; ++i) {
                Object e = array[i];
                
                if (isElement(e) && cond.test(arg0, (E) Nil.unbox(e))) {
                    array[i] = Del.INSTANCE;
                    ++removeCount;
                }
            }
        } finally {
            this.size = (size - removeCount);
        }
        
        return removeCount != 0;
    }
    
    @SuppressWarnings({"unchecked", "unused"})
    private <T> boolean removeAllPartialWip(T arg0, BiPredicate<? super T, ? super E> cond) {
        int size = this.size;
        if (size == 0) {
            return false;
        }
        
        // Note: this WORKs generally, but it only removes SOME of the Del.INSTANCE marks.
        
        Object[] array = getFactory().array;
        int offset = this.offset;
        int capacity = this.capacity;
        int end = offset + capacity;
        
        int removeCount = 0;
        int aDeletion = Integer.MIN_VALUE;
        
        try {
            for (int i = offset; i < end; ++i) {
                Object e = array[i];
                
                if (isElement(e) && cond.test(arg0, (E) Nil.unbox(e))) {
                    array[i] = Del.INSTANCE;
                    ++removeCount;
                    
                    aDeletion = i;
                }
            }
        } finally {
            this.size = (size - removeCount);
            
            if (removeCount != 0) {
                int d = aDeletion;
                int s = aDeletion;
                
                array[d] = null;
                
                boolean firstPassDone = false;
                
                outer:
                for (;;) {
                    if ((s = nextIndex(s, offset, capacity)) == aDeletion)
                        firstPassDone = true;
                    
                    Object e = array[s];
                    
                    if (null == e) {
                        if (firstPassDone)
                            break;
                        
                        d = s;
                        do {
                            if ((d = nextIndex(d, offset, capacity)) == aDeletion)
                                break outer;
                        } while (array[d] != Del.INSTANCE);
                        
                        array[d] = null;
                        s = d;
                        continue;
                    }
                    
                    if (e != Del.INSTANCE) {
                        int i = indexOf(e, offset, capacity);
                        
                        if (!( (d < s) ? (i <= d || s < i) : (s < i && i <= d) ))
                            continue;
                    }
                    
                    array[d] = e;
                    array[s] = null;
                    d = s;
                }
            }
        }
        
        return removeCount != 0;
    }
    
    @Override
    public final boolean removeAll(Collection<?> coll) {
        if (coll.isEmpty())
            return false;
        return removeAll(coll, Tools.callingCollectionContains());
    }
    
    @Override
    public final boolean retainAll(Collection<?> coll) {
        if (coll.isEmpty()) {
            if (isEmpty())
                return false;
            clear();
            return true;
        }
        return removeAll(coll, Tools.callingNotCollectionContains());
    }
    
    @Override
    public final boolean removeIf(Predicate<? super E> cond) {
        Objects.requireNonNull(cond);
        return removeAll(cond, Tools.callingPredicateTest());
    }
    
    @Override
    public final boolean contains(Object obj) {
        assert isExternal(obj) : obj;
        if (isEmpty())
            return false;
        
        obj = Nil.box(obj);
        
        int offset = this.offset;
        int capacity = this.capacity;
        Object[] array = getFactory().array;
        
        int start = indexOf(obj, offset, capacity);
        int i = start;
        
        do {
            Object e = array[i];
            
            if (e == null)
                break;
            if (e.equals(obj))
                return true;
            
        } while ((i = nextIndex(i, offset, capacity)) != start);
        
        return false;
    }
    
    @Override
    public final boolean containsAll(Collection<?> coll) {
        for (Object e : coll)
            if (!contains(e))
                return false;
        return true;
    }
    
    @Override
    public final Object[] toArray() {
        return toArray(new Object[size]);
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public final <T> T[] toArray(T[] in) {
        int inLength = in.length;
        
        int size = this.size;
        if (size != 0) {
            if (inLength < size) {
                in = (T[]) java.lang.reflect.Array.newInstance(in.getClass().getComponentType(), size);
            }
            
            Object[] array = getFactory().array;
            
            int src = offset;
            int end = src + capacity;
            int dst = 0;
            
            int count = 0;
            
            for (; src < end; ++src) {
                Object e = array[src];
                
                if (isElement(e)) {
                    in[dst++] = (T) Nil.unbox(e);
                    
                    if (++count == size)
                        break;
                }
            }
        }
        
        if (inLength > size) {
            in[size] = null;
        }
        
        return in;
    }
    
    @Override
    public final Iterator<E> iterator() {
        return new ArrayTempHashSetIterator<>(this);
    }
    
    @Override
    public final Spliterator<E> spliterator() {
        // This could be reimplemented to optimize, but it's probably not worth it.
        // Basically, the only difference would be for parallel streams. The JDK
        // Spliterator from an Iterator implements trySplit by copying the stream
        // elements to a temporary array, but that could be avoided.
        return TempSet.super.spliterator();
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public final void forEach(Consumer<? super E> action) {
        Objects.requireNonNull(action);
        
        int size = this.size;
        if (size != 0) {
            Object[] array = getFactory().array;
            int start = offset;
            int end = start + capacity;
            
            for (int i = start; i < end; ++i) {
                Object e = array[i];
                
                if (isElement(e)) {
                    action.accept((E) Nil.unbox(e));
                    
                    if (--size == 0)
                        break;
                }
            }
        }
    }
    
    @Override
    public final int hashCode() {
        int hash = 0;
        int size = this.size;
        
        if (size != 0) {
            Object[] array = getFactory().array;
            int start = offset;
            int end = start + capacity;
            
            for (int i = start; i < end; ++i) {
                Object e = array[i];
                
                if (isElement(e)) {
                    hash += e.hashCode();
                    
                    if (--size == 0)
                        break;
                }
            }
        }
        
        return hash;
    }
    
    @Override
    public final boolean equals(Object obj) {
        if (this == obj)
            return true;
        
        if (obj instanceof Set<?>) {
            Set<?> that = (Set<?>) obj;
            
            int size = this.size;
            if (size != that.size())
                return false;
            if (size == 0)
                return true;
            
            Object[] array = getFactory().array;
            int start = offset;
            int end = start + capacity;
            
            for (int i = start; i < end; ++i) {
                Object e = array[i];
                
                if (isElement(e)) {
                    try {
                        if (!that.contains(Nil.unbox(e)))
                            return false;
                    } catch (NullPointerException | ClassCastException x) {
                        return false;
                    }
                    
                    if (--size == 0)
                        break;
                }
            }
            
            return true;
        }
        
        return false;
    }
    
    @Override
    public final String toString() {
        int size = this.size;
        if (size == 0)
            return "[]";
        
        StringBuilder b = new StringBuilder(10 * size).append('[');
        
        Object[] array = getFactory().array;
        int start = offset;
        int end = start + capacity;
        
        for (int i = start; i < end; ++i) {
            Object e = array[i];
            
            if (isElement(e)) {
                b.append(e);
                
                if (--size == 0)
                    break;
                
                b.append(", ");
            }
        }
        
        return b.append(']').toString();
    }
}