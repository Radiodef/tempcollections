/* MIT License
 *
 * Copyright (c) 2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.radiodef.temp;

abstract class ArrayTempOpenAddressedHashTable extends ArrayTempObject {
    
    static final int MAX_CAPACITY = 1 << 30;
    
    static final float DEFAULT_LOAD_FACTOR = 0.625f; // 0.625f is an exact float value
    
    // Note: we're using open addressing, so loadFactor MUST be <= 1.0.
    final float loadFactor;
    
    ArrayTempOpenAddressedHashTable(ArrayTempFactory factory,
                                    ArrayTempObject previous,
                                    int offset,
                                    int capacity,
                                    float loadFactor) {
        super(factory, previous, offset, capacity);
        
        assert Tools.isPowerOf2(capacity) : "capacity=" + capacity;
        assert 0 <= capacity && capacity <= MAX_CAPACITY : "capacity=" + capacity;
        assert 0.0f < loadFactor && loadFactor <= 1.0f : "loadFactor=" + loadFactor;
        
        this.loadFactor = loadFactor;
    }
    
    static boolean isExternal(Object o) {
        return o != Nil.INSTANCE && o != Del.INSTANCE;
    }
    
    static void requireValidLoadFactor(float loadFactor) {
        if (0.0f < loadFactor && loadFactor <= 1.0f)
            return;
        throw new IllegalArgumentException("loadFactor=" + loadFactor);
    }
    
    static final int NO_CAPACITY = -1;
    
    static int nextCapacity(int capacity, int size, int additionalSize, float loadFactor) {
        if (capacity == MAX_CAPACITY)
            return NO_CAPACITY;
        
        long newSize = (long) size + (long) additionalSize;
        if (newSize > (long) MAX_CAPACITY)
            return NO_CAPACITY;
        
        int newCapacity = capacity << 1;
        
        // old computation left here for reference (broken)
//        if ((newCapacity * loadFactor) < (float) newSize) {
//            newSize = (long) Math.ceil(newSize / loadFactor);
//
//            newCapacity =
//                (newSize < (long) MAX_CAPACITY)
//                    ? Tools.next2((int) newSize)
//                    : MAX_CAPACITY;
//        }
        
        if (additionalSize > ((int) (newCapacity * loadFactor) - size)) {
            float scaledSize = Tools.toCeilFloat((int) newSize) / loadFactor;

            newCapacity =
                (scaledSize < (float) MAX_CAPACITY)
                    ? Tools.next2((int) scaledSize)
                    : MAX_CAPACITY;
        }
        
        assert newCapacity >= newSize : "newCapacity=" + newCapacity + ", newSize=" + newSize;
        return newCapacity;
    }
}
