/* MIT License
 *
 * Copyright (c) 2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.radiodef.temp;

import java.util.*;

final class ArrayTempHashMapKeySet<K> extends AbstractSet<K> {
    final ArrayTempHashMap<K, ?> map;
    
    ArrayTempHashMapKeySet(ArrayTempHashMap<K, ?> map) {
        assert map != null;
        this.map = map;
    }
    
    @Override
    public final int size() {
        return map.size;
    }
    
    @Override
    public final void clear() {
        map.clear();
    }
    
    @Override
    public final boolean contains(Object obj) {
        // noinspection SuspiciousMethodCalls
        return map.containsKey(obj);
    }
    
    @Override
    public final boolean remove(Object obj) {
        return map.removeReturningBoolean(obj);
    }
    
    @Override
    public final boolean add(K key) {
        throw new UnsupportedOperationException("Map.keySet() does not support add");
    }
    
    @Override
    public final boolean addAll(Collection<? extends K> coll) {
        throw new UnsupportedOperationException("Map.keySet() does not support addAll");
    }
    
    @Override
    public final ArrayTempHashMapIterator.OfKeys<K, ?> iterator() {
        return new ArrayTempHashMapIterator.OfKeys<>(map);
    }
    
    // TODO: reimplement any optimized methods
}
