/* MIT License
 *
 * Copyright (c) 2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.radiodef.temp;

/**
 * {@code TempObject} is the superinterface of all temporary collections.
 *
 * @see TempCollection
 * @see TempMap
 */
public interface TempObject extends AutoCloseable {
    /**
     * <p>Returns the number of elements or entries in this collection
     * or map.</p>
     *
     * @return the number of elements or entries in this collection or map
     * @see    java.util.Collection#size()
     * @see    java.util.Map#size()
     */
    int size();
    
    /**
     * <p>Returns {@code true} if the size of this collection or map is 0,
     * and {@code false} otherwise.</p>
     *
     * @return {@code true} if the size of this collection or map is 0,
     *         and {@code false} otherwise.
     * @see    java.util.Collection#isEmpty()
     * @see    java.util.Map#isEmpty()
     */
    boolean isEmpty();
    
    /**
     * <p>Returns the current maximum size of this collection or map. If this
     * collection or map is capacity-restricted, then its size may be increased
     * up to this maximum (inclusively), at which point additional attempts to
     * increase its size will fail with an {@link IllegalStateException}.</p>
     *
     * <p>Specifically, given an</p>
     *
     *<pre><code>int n = this.maxSize() - this.size();</code></pre>
     *
     * <p>a capacity-restricted collection or map will support {@code n}
     * additional elements or entries.</p>
     *
     * <p>If this collection or map is <em>not</em> capacity-restricted, then
     * the method may return {@link Integer#MAX_VALUE Integer.MAX_VALUE} or a
     * similarly arbitrarily-large value. For a collection or map which is not
     * capacity-restricted, no specific guarantees are made about how much its
     * size can actually increase, because this will depend on how much memory
     * the computer system running the JVM has available and other unpredictable
     * factors.</p>
     *
     * @return the current maximum size of this collection or map
     * @see    #size()
     * @see    #canAdd(int)
     */
    int maxSize();
    
    /**
     * <p>Returns whether or not this collection or map will support a change in
     * size equal to the specified {@code changeInSize}.</p>
     *
     * <ul>
     * <li>
     * <p>For a {@code changeInSize} which is positive, the method returns {@code true}
     * if this collection or map would support an increase in size equal to {@code changeInSize}.
     * This is equivalent to the expression {@code changeInSize <= this.maxSize() - this.size()}.</p>
     * </li>
     *
     * <li>
     * <p>For a {@code changeInSize} which is negative, the method returns {@code true}
     * if this collection or map can decrease in size an amount equal to the absolute
     * value of {@code changeInSize}, but handling {@link Integer#MIN_VALUE Integer.MIN_VALUE}.
     * This is equivalent to the expression
     * {@code changeInSize != Integer.MIN_VALUE && Math.abs(changeInSize) <= this.size()}.</p>
     * </li>
     *
     * <li>
     * <p>For a {@code changeInSize} which is 0, the method will always return
     * {@code true}, because it's always valid for the size to not change.</p>
     * </li>
     * </ul>
     *
     * <p>If this method returns {@code false} for a given positive argument, then
     * the implication is that an attempt to increase this collection or map's size
     * by the given amount would result in an {@link IllegalStateException} being
     * thrown from the method which was used to attempt the unsupported increase.</p>
     *
     * @param  changeInSize the number of elements or entries to add or remove
     * @return {@code true} if this object will support a change in size equal
     *         to the specified amount, and {@code false} otherwise
     * @see    #size()
     * @see    #maxSize()
     */
    boolean canAdd(int changeInSize);
    
    /**
     * <p>Closes this collection or map, releasing any resources back to the factory
     * which created it.</p>
     *
     * <p>Note: The behavior of invoking a method or otherwise interacting
     * with a closed {@code TempObject} is undefined, unless the implementation
     * specifies otherwise.</p>
     *
     * @throws IllegalStateException
     *         if the method is invoked at an inappropriate time,
     *         according to the implementation
     */
    @Override
    void close() throws IllegalStateException;
    
    /**
     * Returns the factory which created this object.
     *
     * @return the factory which created this object
     */
    TempFactory getFactory();
}