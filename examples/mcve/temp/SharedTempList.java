/* MIT License
 *
 * Copyright (c) 2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package mcve.temp;
import java.util.*;

/**
 * <p>This is an example of how an old prototype of the API worked.
 * See the wiki for more information.</p>
 *
 * @see mcve.temp.ProtoTempFactory
 * @see https://bitbucket.org/Radiodef/tempcollections/wiki/Home
 */
public final class SharedTempList<E> extends AbstractList<E> implements AutoCloseable {
    private final ProtoTempFactory factory;
    
    int offset;
    int size;
    
    SharedTempList(ProtoTempFactory factory) {
        this.factory = factory;
    }
    
    @Override
    public void close() {
        factory.unwind();
    }
    
    @Override
    public int size() {
        return size;
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public E get(int i) {
        if (i < 0 || i >= size) {
            throw new IndexOutOfBoundsException("i=" + i);
        }
        return (E) factory.array[offset + i];
    }
    
    @Override
    public E set(int i, E e) {
        E old = get(i);
        factory.array[offset + i] = e;
        return old;
    }
    
    @Override
    public void add(int i, E e) {
        if (i < 0 || i > size) {
            throw new IndexOutOfBoundsException("i=" + i);
        }
        factory.resizeArrayIfNecessary(offset + size + 1);
        
        System.arraycopy(factory.array, offset + i,
                         factory.array, offset + i + 1, size - i);
        
        factory.array[offset + i] = e;
        size++;
    }
    
    @Override
    public E remove(int i) {
        E e = get(i);
        System.arraycopy(factory.array, offset + i + 1,
                         factory.array, offset + i, size - i - 1);
        size--;
        factory.array[offset + size] = null;
        return e;
    }
}