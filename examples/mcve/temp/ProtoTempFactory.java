/* MIT License
 *
 * Copyright (c) 2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package mcve.temp;
import java.util.*;

/**
 * <p>This is an example of how an old prototype of the API worked.
 * See the wiki for more information.</p>
 *
 * @see mcve.temp.SharedTempList
 * @see https://bitbucket.org/Radiodef/tempcollections/wiki/Home
 */
public final class ProtoTempFactory {
    // This is the shared backing array.
    Object[] array = new Object[10];
    
    // This is the permanent object which openList() always returns.
    private final SharedTempList<?> list = new SharedTempList<>(this);
    
    public ProtoTempFactory() {
    }
    
    private enum Mark { INSTANCE }
    
    @SuppressWarnings("unchecked")
    public <T> SharedTempList<T> openList() {
        // Find the end of the previous list's array subsection,
        // or 0 if there is no previous list.
        int end = list.offset + list.size;
        
        // Mark the end of the previous list's array subsection.
        array[end] = Mark.INSTANCE;
        
        // Reset the permanent list object.
        list.size = 0;
        list.offset = end + 1;
        
        // Always returns the same object.
        return (SharedTempList<T>) list;
    }
    
    void unwind() {
        if (list.offset == 0) {
            throw new IllegalStateException("no list to close");
        }
        
        int size = list.size;
        // Clear the subsection of the array used by the list being closed.
        while (size > 0) {
            size--;
            array[list.offset + size] = null;
        }
        
        int mark = list.offset - 1;
        // (This should always be the case.)
        assert array[mark] == Mark.INSTANCE;
        array[mark] = null;
        
        int offset = mark;
        // Now restore the previous list, if there was one.
        if (offset > 0) {
            // Unwind back through the array,
            // looking for the previous mark.
            while (array[offset - 1] != Mark.INSTANCE) {
                offset--;
                size++;
            }
        }
        
        list.offset = offset;
        list.size = size;
    }
    
    void resizeArrayIfNecessary(int requiredLength) {
        if (requiredLength > array.length) {
            int newLength = Math.max((array.length * 2) + 1, requiredLength);
            array = Arrays.copyOf(array, newLength);
        }
    }
}