/* MIT License
 *
 * Copyright (c) 2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package mcve.temp;

/**
 * <p>This is just a simple test file for the other two classes in this package.</p>
 *
 * @see mcve.temp.ProtoTempFactory
 * @see mcve.temp.SharedTempList
 * @see https://bitbucket.org/Radiodef/tempcollections/wiki/Home
 */
public class Main {
    public static void main(String[] args) {
        ProtoTempFactory factory = new ProtoTempFactory();
        
        try (SharedTempList<String> strings = factory.openList()) {
            strings.add("A");
            strings.add("B");
            strings.add("C");
            System.out.println(strings);
            
            for (int i = 0; i < strings.size(); i++) {
                System.out.printf("%d = %s%n", i, strings.get(i));
            }
            
            strings.set(0, "X");
            strings.set(1, "Y");
            strings.set(2, "Z");
            System.out.println(strings);
            
            System.out.printf("strings[1] was %s%n", strings.remove(1));
            System.out.println(strings.size());
            System.out.println(strings);
            
            try (SharedTempList<Double> doubles = factory.openList()) {
                System.out.printf("strings == doubles is %b%n",
                    (Object) strings == (Object) doubles);
                
                doubles.add(0, 3.0);
                doubles.add(0, 2.0);
                doubles.add(0, 1.0);
                System.out.println(doubles);
                
                for (int i = doubles.size() - 1; i >= 0; i--) {
                    System.out.printf("%d = %f%n", i, doubles.remove(i));
                }
                
                System.out.println(doubles);
                doubles.add(Math.PI);
                System.out.println(doubles);
            }
            
            System.out.println(strings);
        }
    }
}